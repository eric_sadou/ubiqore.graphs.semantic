package limics.skos.services.api;

import org.openrdf.model.URI;
import org.openrdf.sail.SailException;

import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;

public class UmlsSkosGraphImpl extends SkosGraphImpl{
	/*
	 * loading resource from official distribution of terminologies.
	 * use of baseGraph.setCheckElementsInTransaction(true); is most important here : 
	 * the process dont check the add of uniqueness elements. Increase the speed of creation.
	 * 
	 */
	public UmlsSkosGraphImpl( String path, boolean destroy)
			throws SailException {
		super(path, destroy);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void connexion() throws SailException{
		System.err.println("in umls skos connexion");
		baseGraph = new Neo4jGraph(graph_dir);
		baseGraph.setCheckElementsInTransaction(true);
		//GraphSail<Neo4jGraph> gSail= new GraphSail<Neo4jGraph>(baseGraph, "p,c,pc,poc");
		GraphSail<Neo4jGraph> gSail= new GraphSail<Neo4jGraph>(baseGraph);
		gSail.enforceUniqueStatements(false);
		sail=gSail;
		
		sail.initialize();
		vf=sail.getValueFactory();
	}
	
}
