package limics.skos.services.api;

import info.aduna.iteration.CloseableIteration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;


 





import org.apache.commons.io.FilenameUtils;
import org.openrdf.model.Namespace;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.vocabulary.DC;
import org.openrdf.model.vocabulary.SKOS;
import org.openrdf.rio.RDFHandler;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
//import org.openrdf.rio.nquads.NQuadsWriter;
import org.openrdf.rio.helpers.*;
import org.openrdf.rio.nquads.NQuadsWriter;
import org.openrdf.rio.rdfxml.RDFXMLParser;
import org.openrdf.rio.trig.TriGParser;
import org.openrdf.rio.n3.N3ParserFactory;
import org.openrdf.rio.turtle.TurtleParser;
import org.openrdf.rio.trix.TriXParser; 
import org.openrdf.rio.trix.TriXWriter;
import org.openrdf.rio.rdfxml.util.RDFXMLPrettyWriter;
import org.openrdf.sail.Sail;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import com.tinkerpop.blueprints.impls.sail.SailTokens;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;


public abstract class BasicGraphBU {
	private String defaultdir="/tmp/basicGraph";
	protected String graph_dir=null;
	
	public final static String default_context="context:default";
	protected Neo4jGraph baseGraph =null;
	protected ValueFactory vf=null;
	protected Sail sail;
	protected SailConnection sc=null;
	protected List<Statement> rulesStatements=Lists.newArrayList();
	protected Map<String,String> namespaces=Maps.newHashMap();
	protected URI context;
	public URI getContext() {
		return context;
	}

	protected String queryprepare;
	/*
	 * abstract methode to implements.
	 */
	/*
	 * 
	 */
	protected abstract void populateRulesStatements();
	
	protected abstract void createNamespacesMap();
	
	
	public boolean connexionIsOk(){
		return this.activeConnection;
	}
	public BasicGraphBU(URI context,String path,boolean destroy) throws SailException{
		
		if (null==context|| context.toString().trim().isEmpty()){
			System.out.println("inf  : no context was given.stop of init.");
			System.out.println("no specific context will be used");
		    context=null;
		}else this.context=context;
		
		if (destroy)System.out.println("destruction/creation/connection of graph on path :"+path);
		
		else System.out.println("connection  of graph on path :"+path);
		
		
		
		if (!Strings.isNullOrEmpty(path))this.graph_dir=path;
		else {
			this.graph_dir=defaultdir;
			System.err.println("WARNING ! No path declared when you init your graph ! default is used and can be wrong path on your machine !! =>"+this.graph_dir);
		}
		if (destroy){
			this.initialyzeNewGraphDirectory();
		}
		this.connexion();
		if (destroy){
			// this methode is implemented in class that implements BasicGraph !
			this.populateRulesStatements();
			this.createSpecificGraphStatements();
			// this methode is implemented in class that implements BasicGraph !
			this.createNamespacesMap();
			this.addNamespaces();
		}
		this.queryPrepare();
	}
	protected void queryPrepare() {
		
		if (Strings.isNullOrEmpty(queryprepare)){
			String prepare="";
			try {
				SailConnection sc=sail.getConnection();
			CloseableIteration<? extends Namespace, SailException> map=sc.getNamespaces();
			while (map.hasNext()){
				Namespace ns=map.next();
				prepare+="PREFIX "+ns.getPrefix()+": <"+ns.getName() +">  ";
			}
			sc.close();
			}catch (Exception e){
				e.printStackTrace();
			}
			System.out.println(queryprepare);
			queryprepare=prepare;
			System.out.println(queryprepare);
		}
		
	}
	 public void addNamespaces() {
		 if (this.namespaces.isEmpty())return;
		 Iterator<Entry<String, String>> it=this.namespaces.entrySet().iterator();
		 while (it.hasNext()){
			 Entry<String, String> entry=it.next();
			 this.addNamespace(entry.getKey(), entry.getValue());
		 }
	 }

	 public void addNamespace(final String prefix, final String namespace)  {
		 	SailConnection sc=null;
	        try {
	        	sc=sail.getConnection();
	            sc.setNamespace(prefix, namespace);
	            sc.commit();
	        } catch (SailException e) {
	            throw new RuntimeException(e.getMessage(), e);
	        }finally {
	        	if (sc!=null){
	        		try {
						sc.close();
					} catch (SailException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	        	}
	        }
	    }
	
	protected void connexion() throws SailException{
		baseGraph = new Neo4jGraph(graph_dir);
		baseGraph.setCheckElementsInTransaction(true);
		sail= new GraphSail<Neo4jGraph>(baseGraph, "p,c,pc,poc");
		((GraphSail)sail).enforceUniqueStatements(true);
		sail.initialize();
		vf=sail.getValueFactory();
		activeConnection=true;
	}
	
	boolean activeConnection=false;
	public void shutdown(){
		try {
			baseGraph.shutdown();
			sail.shutDown();
			activeConnection=false;
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	 @Override
	 protected void finalize() throws Throwable {
		 try {
			    System.err.println("finalize"); 
			    baseGraph.shutdown();
				sail.shutDown();
			} catch (SailException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
	 }
	 private void createSpecificGraphStatements() throws SailException{
			if (rulesStatements.isEmpty())return;
			
			try {
				SailConnection sc=sail.getConnection();
				ValueFactory vf = sail.getValueFactory();
				for (Statement st:rulesStatements)sc.addStatement(st.getSubject(),st.getPredicate(),st.getObject(),st.getContext());
				sc.commit();
				sc.close();
				
			}catch (Exception e){
				
				if (sc!=null)if (sc.isOpen())sc.close();
			}
			
			
		}

		private void initialyzeNewGraphDirectory(){
			// remove the dir..
			File dir=new File (this.graph_dir);
			if (dir.exists()){
				System.out.println("the dir \""+this.graph_dir+"\" will be destroyed and re-initialised with a fresh new  graph");
				BasicGraphBU.recursiveDelete(dir);
				
			}else {
				System.out.println("the dir \""+this.graph_dir+"\" will be used to initialyse a fresh new graph");
				dir.mkdirs();
			}
		}
		private static void recursiveDelete(File file) {
	        if (!file.exists()) return;
	        if (file.isDirectory()) {
	           for (File f : file.listFiles()) recursiveDelete(f);
	           file.delete();
	        } else {
	           file.delete();
	        }
	} 
		
		protected void useClassicNamespaces(){
		    this.namespaces.put(SailTokens.RDF_PREFIX, SailTokens.RDF_NS);
	        this.namespaces.put(SailTokens.RDFS_PREFIX, SailTokens.RDFS_NS);
	        this.namespaces.put(SailTokens.OWL_PREFIX, SailTokens.OWL_NS);
	        this.namespaces.put(SailTokens.XSD_PREFIX, SailTokens.XSD_NS);
	        this.namespaces.put(SailTokens.FOAF_PREFIX, SailTokens.FOAF_NS);
	        this.namespaces.put(SKOS.PREFIX, SKOS.NAMESPACE);
	        this.namespaces.put(DC.PREFIX, DC.NAMESPACE);
	}

		public Set<Statement> getFullStatements() throws SailException{
			Set<Statement> set=Sets.newHashSet();
			SailConnection sc=sail.getConnection();
			
			CloseableIteration<? extends Statement, SailException> results = sc.getStatements(null, null, null, true);
			while(results.hasNext()) {
			    set.add(results.next());
			}
			
			//System.out.println("\nget statements:  ?p ?o ?g");
			
			sc.close();
			return set;
		}
		protected URI uriExist(URI  uriInput)throws SailException{
			URI uri=null;
			try {
				SailConnection sc=sail.getConnection();
				
				
				CloseableIteration<? extends Statement, SailException> results =sc.getStatements(uriInput,null,null,false);
				while(results.hasNext()) {
					
				    Statement first=results.next();
				    uri=(URI) first.getSubject();
				    
				    break;
				}
				
				
				sc.close();
				
				
			}catch (Exception e){
				
				e.printStackTrace();
			}
			finally {
				if (sc!=null)if (sc.isOpen())sc.close();
			
			}
			return uri; 
		}
		@SuppressWarnings("finally")
		protected URI statementExist(Statement st)throws SailException{
			URI uri=null;
			try {
				SailConnection sc=sail.getConnection();
				CloseableIteration<? extends Statement, SailException> results =sc.getStatements(st.getSubject(),st.getPredicate(),st.getObject(),true,st.getContext());
				while(results.hasNext()) {
					
				    Statement first=results.next();
				    uri=(URI) first.getSubject();
				    
				    break;
				}
				
				
				sc.close();
				
				
			}catch (Exception e){
				
				if (sc!=null)if (sc.isOpen())sc.close();
			}
			finally {
				return uri; 
			}
		}
		
		protected boolean addMultipleStatements(List<Statement> sts)throws SailException{
			if (sts==null)return false;
			if (sts.isEmpty())return false;
			String string=null;
			try {
				SailConnection sc=sail.getConnection();
				for (Statement st:sts){
					sc.addStatement(st.getSubject(),st.getPredicate(),st.getObject(),st.getContext());	
				}
				sc.commit();
				sc.close();
				return true;
			}catch (Exception e){
				System.out.println("error addStatement..."+string);
				e.printStackTrace();
				if (sc!=null)if (sc.isOpen())sc.close();
			}
			return false;
		}
		
		protected boolean addMultipleStatements(Set<Statement> sts)throws SailException{
			if (sts==null)return false;
			if (sts.isEmpty())return false;
			String string=null;
			try {
				SailConnection sc=sail.getConnection();
				for (Statement st:sts){
					//sc.getStatements(st.getSubject(),st.getPredicate(),st.getObject(),true,st.getContext()).;
					
					sc.addStatement(st.getSubject(),st.getPredicate(),st.getObject(),st.getContext());	
				}
				sc.commit();
				sc.close();
				return true;
			}catch (Exception e){
				System.out.println("error addStatement..."+string);
				e.printStackTrace();
				if (sc!=null)if (sc.isOpen())sc.close();
			}
			return false;
		}
		
		public boolean addMultipleStatements(Set<Statement> sts,int commitFlag)throws SailException{
			if (sts==null)return false;
			if (sts.isEmpty())return false;
			String string=null;
			boolean tocontinue=false;
			try {
				SailConnection sc=sail.getConnection();
				int i=0;
				Set<Statement> treated=Sets.newHashSet();
				for (Statement st:sts){
					i++;
					if (i%commitFlag==0)System.out.println(i+"+++++>"+st.toString());
					//string=st.getSubject().toString()+" "+st.getPredicate().getLocalName()+ " "+st.getObject().toString();
					//System.out.println("triplet to add "+string);
					sc.addStatement(st.getSubject(),st.getPredicate(),st.getObject(),st.getContext());
					treated.add(st);
					if (i%commitFlag==0){
						tocontinue=true;
						sts.removeAll(treated);
			
						sc.commit();
						sc.close();
						break;
					}
					
				}
				if (tocontinue){
					sts.removeAll(treated);
					Set<Statement> rest=sts;
					sts=Sets.newHashSet();
					return this.addMultipleStatements(rest,commitFlag);
				}
				sc.commit();
				sc.close();
				return true;
			}catch (Exception e){
				System.out.println("error addStatement..."+string);
				e.printStackTrace();
				if (sc!=null)if (sc.isOpen())sc.close();
			}
			return false;
		}

		/**
		 * @return the queryprepare
		 */
		public String getQueryprepare() {
			return queryprepare;
		}
		/*
		 * 
		 *import org.openrdf.rio.nquads.NQuadsWriter;
import org.openrdf.rio.rdfxml.RDFXMLParser;
import org.openrdf.rio.trig.TriGParser;
import org.openrdf.rio.n3.N3ParserFactory;
import org.openrdf.rio.turtle.TurtleParser;


		 */
		public void exportRDF(String where) throws SailException{
			
			Path p=Paths.get(where);
			String ext=FilenameUtils.getExtension(p.getFileName().toString());
			RDFHandler tripleWriter = null;
			if (where==null)return ;
			else {
				File f=new File (where);
				java.io.FileOutputStream fos;
				try {
					fos = new java.io.FileOutputStream(f);
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return;
				}
				System.out.println(ext);
				 if (ext.equalsIgnoreCase("n3"))
					 tripleWriter=new org.openrdf.rio.n3.N3Writer(fos);
				 else if (ext.equalsIgnoreCase("n4"))
					 tripleWriter=new NQuadsWriter(fos);
				 else  if (ext.equalsIgnoreCase("trig"))
					 tripleWriter=new org.openrdf.rio.trig.TriGWriter(fos);
				 else if (ext.equalsIgnoreCase("xml"))
					 tripleWriter=new TriXWriter(fos);
				 else if (ext.equalsIgnoreCase("ttl"))
					 tripleWriter=new  org.openrdf.rio.turtle.TurtleWriter(fos);
				 
				 else // means RDF or XMLRDF etc ... (xml for trix is better !!!)
				 tripleWriter=new RDFXMLPrettyWriter(fos);
			}
			
			SailConnection sc=sail.getConnection();
			CloseableIteration<? extends Namespace, SailException> nss=sc.getNamespaces();
			
			//CloseableIteration<? extends Statement, SailException> res=sc.getStatements(null, null, null, true, this.context);
			CloseableIteration<? extends Statement, SailException> res=sc.getStatements(null, null, null, false);
			try {
				tripleWriter.startRDF();
			} catch (RDFHandlerException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			while (nss.hasNext()){
				try {
					Namespace ns=nss.next();
					tripleWriter.handleNamespace(ns.getPrefix(),ns.getName());
				} catch (RDFHandlerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			while(res.hasNext()) {
				try {
					
					tripleWriter.handleStatement(res.next());
				} catch (RDFHandlerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			    
			}
			try {
				tripleWriter.endRDF();
			} catch (RDFHandlerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			sc.close();
		}
		
		
		

		
		
		public void importRDF(String filePath) throws SailException{
			
			/*import org.openrdf.rio.trig.TriGParser;
			import org.openrdf.rio.n3.N3ParserFactory;
			import org.openrdf.rio.turtle.TurtleParser;
			import org.openrdf.rio.trix.TriXParser;*/
			Path p=Paths.get(filePath);
			String ext=FilenameUtils.getExtension(p.getFileName().toString());
			RDFParser theParser=null;
			if (ext.equalsIgnoreCase("n3")){
				theParser=(RDFParser) new N3ParserFactory().getParser();
			}else if (ext.equalsIgnoreCase("trig")){
				theParser=(RDFParser) new TriGParser();
			
			}else if (ext.equalsIgnoreCase("ttl")){
				theParser=(RDFParser) new TurtleParser();
			}
			else if (ext.equalsIgnoreCase("n4")){
			theParser=(RDFParser) new TurtleParser();
			
			}
			else if (ext.equalsIgnoreCase("xml")){
				theParser=(RDFParser) new org.openrdf.rio.trix.TriXParser();
				
			}
			else { // rdf etc ..
				theParser=(RDFParser) new RDFXMLParser();
			}
			
			
			FileReader file=null;
			try {
				file = new FileReader (filePath);
				
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				System.out.println("nothing done");
				return;
			}
			
			List<Statement> myList = Lists.newArrayList();
			StatementCollector collector = new StatementCollector(myList);
			
			//RDFParser rdfParser = (RDFParser) new RDFXMLParser();
			theParser.setRDFHandler(collector);
			try {
				theParser.parse(file, "http://");
				  
				} 
				catch (IOException e) {
				  // handle IO problems (e.g. the file could not be read)
				}
				catch (RDFParseException e) {
				  // handle unrecoverable parse error
				}
				catch (RDFHandlerException e) {
				  // handle a problem encountered by the RDFHandler
				}
			int count=collector.getStatements().size();
			System.out.println(count+" --> nb statements inside the file");
			List<Statement> sts=Lists.newArrayList(collector.getStatements());
			this.addMultipleStatements(sts);
			
		
		}
		
		protected boolean removeMultipleStatements(Set<Statement> sts)throws SailException{
			
			if (sts==null)return false;
			if (sts.isEmpty())return false;
			try {
				SailConnection sc=sail.getConnection();
				for (Statement st:sts){
					//string=st.getSubject().toString()+" "+st.getPredicate().getLocalName()+ " "+st.getObject().toString();
					//System.out.println("triplet to add "+string);
					sc.removeStatements(st.getSubject(),st.getPredicate(),st.getObject(),st.getContext());
					
				}
				sc.commit();
				sc.close();
				return true;
			}catch (Exception e){
				sc.rollback();
				System.out.println("error removeStatement...rollback for everything");
				e.printStackTrace();
				if (sc!=null)if (sc.isOpen())sc.close();
			}
			return false;
		}
}


