package limics.skos.services.api;

import info.aduna.iteration.CloseableIteration;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ubiqore.skos.impl.*;
import ubiqore.skos.impl.notes.*;
import ubiqore.skos.impl.datatypes.SimpleCustomXSDRelatedDataTypeUtil;
import ubiqore.skos.impl.datatypes.XSDRelatedCustomDatatype;
import ubiqore.skos.impl.datatypes.XSDRelatedEnumCustomDatatype;
import ubiqore.skos.impl.labels.*;

import org.joda.time.DateTime;
import org.openrdf.model.BNode;
import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.impl.BNodeImpl;
import org.openrdf.model.impl.ContextStatementImpl;
import org.openrdf.model.impl.LiteralImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.OWL;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.model.vocabulary.SKOS;
import org.openrdf.query.BindingSet; 
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.impl.BindingImpl;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.repository.sparql.query.SPARQLQueryBindingSet;
import org.openrdf.rio.RDFFormat;
import org.openrdf.rio.Rio;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;
import org.semarglproject.vocab.XSD;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.neovisionaries.i18n.*;
/*
 * arbitrary implementation of skos.
 * skos:broader is used.
 * For full import -> narrower are transformed as broader link.
 *
 *to implenent creation/get specific datatype.
 * ex:lcc a rdfs:Datatype ;
  rdfs:label "Library of Congress Classification" .
  
  query with datatype :
  select * {

  ?s ?p ?o

  filter ( datatype(?o) = xsd:integer )
  filter ( coalesce(xsd:integer(str(?o)), '!') = '!')

}
  
 */
public class SkosGraphImpl extends BasicGraph implements SkosGraph  {
	
	
	
	//implemented
	public SkosGraphImpl(String path, boolean destroy) throws SailException {
		super( path, destroy);
		
	}

	public Set<ConceptScheme> inferredConceptSchemes(){
		SailConnection sc=null;
		Set<ConceptScheme> set=Sets.newHashSet();
		try {
			sc=sail.getConnection();
			SPARQLParser parser=new SPARQLParser();
			CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
			
			String queryString =this.getQueryprepare()+ 
					//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
					  	"SELECT DISTINCT ?scheme WHERE { " +
					  	"?x skos:inScheme|skos:topConceptOf|^skos:hasTopConcept ?scheme . " +
					  	// skos:hasTopConcept|^skos:topConceptOf
					  	"}"; 
					  
			ParsedQuery query = parser.parseQuery(queryString , null);
			SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
			
		    
			sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
			
			try {
				if (sparqlResults.hasNext()){
					set=Sets.newHashSet();
				    
					while (sparqlResults.hasNext()){
						BindingSet a=sparqlResults.next();
						if (a.getValue("scheme")!=null){
							ConceptScheme cs=new ConceptScheme();
							cs.setUri(new URIImpl(a.getValue("scheme").stringValue()));
							set.add(cs);
						}
					}
						
						
					}
					
				
				
				} catch (QueryEvaluationException e) {
				
					e.printStackTrace();
				}
			
			sc.close();
			
			return set;
			
		
		}catch (Exception e) {
		e.printStackTrace();
		try {
			if (null!=sc && sc.isOpen()){
				sc.close();
			}
		} catch (SailException e1) {
			
			e1.printStackTrace();
		}
		}
		return null;
		
	}

	//implemented
	public Set<ConceptScheme> getConceptSchemes()  {
		SailConnection sc=null;
		
		try {
		sc=sail.getConnection();
		Set<ConceptScheme> set=Sets.newHashSet();
		
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.getQueryprepare()+ 
						//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
						  	"CONSTRUCT { " +
						  	"?uri	    rdf:type       		skos:ConceptScheme ." +
						  	"?uri 		skos:prefLabel 		?preflabel . " +
						  	"?uri	  	skos:altLabel   	?altlabel . " +
						  	"?uri	  	skos:notation   	?notation . " +
						  	"?uri	  	skos:hiddenLabel   	?hiddenlabel . " +
						  	"?uri	    skos:note  			?note . " + 
						  	"?uri	    skos:changeNote		?changeNote . " + 
						  	"?uri	    skos:definition 	?definition . " + 
						  	"?uri	    skos:editorialNote  ?editorialNote . " + 
						  	"?uri	    skos:example  		?example . " + 
						  	"?uri	    skos:historyNote  	?historyNote . " + 
						  	"?uri	    skos:scopeNote  	?scopeNote . " + 
						  	"}"+
						  
							SPARQL.WHERE+
								SPARQL.OPEN+
									  " { ?uri	    rdf:type       	skos:ConceptScheme  }  "+
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri 		skos:prefLabel 	?preflabel }  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	  	skos:altLabel   ?altlabel }  "+
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	  	skos:notation   ?notation }  "+
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	    skos:note  		?note  }  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	  	skos:hiddenLabel   	?hiddenlabel  }  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	    skos:changeNote		?changeNote }  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	    skos:definition 	?definition }  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	    skos:editorialNote  ?editorialNote }  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	    skos:example  		?example }  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	    skos:historyNote  	?historyNote}  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	    skos:scopeNote  	?scopeNote }  " +
							
								SPARQL.CLOSE  + "ORDER BY ?uri ";	
							
		System.out.println(queryString);
		ParsedQuery query=null;
		try {
			query = parser.parseQuery(queryString , null);
		} catch (MalformedQueryException e) {
			
			e.printStackTrace();
		}
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		try {
			if (sparqlResults.hasNext()){
			    ConceptScheme cs=new ConceptScheme();
			    int i=0;
				while (sparqlResults.hasNext()){
					BindingSet a=sparqlResults.next();
					if(a.getValue("object")!=null){
						//System.out.println("construct="+a.toString());
						URI uri=(URI)a.getValue("subject");
						if (i==0){
							cs.setUri(uri);
						}
						else {
							if (!uri.equals(cs.getUri())){
								set.add(cs);
								cs=new ConceptScheme();
								cs.setUri(uri);
							}
							
						}
						bindValueResource(a,cs);
						i++;
					}
				}
				// add the last.
				set.add(cs);
			}
		} catch (QueryEvaluationException e) {
			
			e.printStackTrace();
		}
		
		sc.close();
		if (set.isEmpty())return this.inferredConceptSchemes();
		return set;
		}catch (Exception e) {
		e.printStackTrace();
			try {
				if (null!=sc && sc.isOpen()){
					sc.close();
				}
			} catch (SailException e1) {
				
				e1.printStackTrace();
			}
		}
		return null;
	}

	//implemented
	private void bindValueResource(BindingSet triple,Resource res){
		URI predicate=(URI)triple.getValue("predicate");
		URI objUri=null;
		Literal objLit=null;
		//TypedURIImpl typedUri=null;
		if (triple.getValue("object") instanceof URI){
			objUri=(URI)triple.getValue("object");
		}
		else if (triple.getValue("object") instanceof Literal){
			objLit=(Literal)triple.getValue("object");
		}
		/*else if (triple.getValue("object") instanceof TypedURIImpl){
			System.out.println("coucou c'est moi");
			typedUri=(TypedURIImpl)triple.getValue("object");
		    System.out.println(typedUri.toString());
		}*/
		
		
		if (predicate.equals(SKOS.ALT_LABEL))res.getAltLabels().add(new AltLabel(objLit));
		else if (predicate.equals(SKOS.PREF_LABEL))res.getPrefLabels().add(new PrefLabel(objLit));
		else if (predicate.equals(SKOS.HIDDEN_LABEL))res.getHiddenLabels().add(new HiddenLabel(objLit));
		else if (predicate.equals(SKOS.NOTATION))res.getNotations().add(new Notation(objLit));
		
		else if (predicate.equals(SKOS.NOTE)			||
				 predicate.equals(SKOS.CHANGE_NOTE) 	||
				 predicate.equals(SKOS.DEFINITION) 		||
				 predicate.equals(SKOS.EDITORIAL_NOTE) 	||
				 predicate.equals(SKOS.EXAMPLE) 		||
				 predicate.equals(SKOS.HISTORY_NOTE) 	||
				 predicate.equals(SKOS.SCOPE_NOTE) 
				){
			if (null!=objLit){
				if (predicate.equals(SKOS.NOTE))res.getAnnotations().add(new Note(objLit));
				if (predicate.equals(SKOS.CHANGE_NOTE))res.getAnnotations().add(new ChangeNote(objLit));
				if (predicate.equals(SKOS.DEFINITION))res.getAnnotations().add(new Definition(objLit));
				if (predicate.equals(SKOS.EDITORIAL_NOTE))res.getAnnotations().add(new EditorialNote(objLit));
				if (predicate.equals(SKOS.EXAMPLE))res.getAnnotations().add(new Example(objLit));
				if (predicate.equals(SKOS.HISTORY_NOTE))res.getAnnotations().add(new HistoryNote(objLit));
				if (predicate.equals(SKOS.SCOPE_NOTE))res.getAnnotations().add(new ScopeNote(objLit));
				
			}else if (null!=objUri){
				if (res instanceof Concept)((Concept) res).setChildrenPreviewNumber(((Concept) res).getChildrenPreviewNumber()+1);
				
				if (predicate.equals(SKOS.NOTE))res.getAnnotations().add(new Note(objUri));
				if (predicate.equals(SKOS.CHANGE_NOTE))res.getAnnotations().add(new ChangeNote(objUri));
				if (predicate.equals(SKOS.DEFINITION))res.getAnnotations().add(new Definition(objUri));
				if (predicate.equals(SKOS.EDITORIAL_NOTE))res.getAnnotations().add(new EditorialNote(objUri));
				if (predicate.equals(SKOS.EXAMPLE))res.getAnnotations().add(new Example(objUri));
				if (predicate.equals(SKOS.HISTORY_NOTE))res.getAnnotations().add(new HistoryNote(objLit));
				if (predicate.equals(SKOS.SCOPE_NOTE))res.getAnnotations().add(new ScopeNote(objUri));
			}
		   /* else if (null!=typedUri){
			if (predicate.equals(SKOS.NOTE))res.getAnnotations().add(new Note(typedUri.getUri(),typedUri.getDatatype()));
			if (predicate.equals(SKOS.CHANGE_NOTE))res.getAnnotations().add(new ChangeNote(typedUri.getUri(),typedUri.getDatatype()));
			if (predicate.equals(SKOS.DEFINITION))res.getAnnotations().add(new Definition(typedUri.getUri(),typedUri.getDatatype()));
			if (predicate.equals(SKOS.EDITORIAL_NOTE))res.getAnnotations().add(new EditorialNote(typedUri.getUri(),typedUri.getDatatype()));
			if (predicate.equals(SKOS.EXAMPLE))res.getAnnotations().add(new Example(typedUri.getUri(),typedUri.getDatatype()));
			if (predicate.equals(SKOS.HISTORY_NOTE))res.getAnnotations().add(new HistoryNote(typedUri.getUri(),typedUri.getDatatype()));
			if (predicate.equals(SKOS.SCOPE_NOTE))res.getAnnotations().add(new ScopeNote(typedUri.getUri(),typedUri.getDatatype()));
		}*/
		}
		
	}
	
	//implemented
		private void bindValueConcept(BindingSet triple,Concept concept){
			URI predicate=(URI)triple.getValue("predicate");
			URI objUri=null;
			if (triple.getValue("object") instanceof URI){
				objUri=(URI)triple.getValue("object");
			}
			else return;
			
			if (predicate.equals(SKOS.TOP_CONCEPT_OF)){
				ConceptScheme cs=new ConceptScheme();
				cs.setUri(objUri);
				concept.getTopConceptOf().add(cs);
				
			}else if (predicate.equals(SKOS.IN_SCHEME)){
				ConceptScheme cs=new ConceptScheme();
				cs.setUri(objUri);
				concept.getInScheme().add(cs);
			}
		}
	//implemnted
	private void bindValueResource(BindingSet triple,Collection res){
		URI predicate=(URI)triple.getValue("predicate");
		URI objUri=null;
		Literal objLit=null;
		if (triple.getValue("object") instanceof URI){
			objUri=(URI)triple.getValue("object");
		}
		else if (triple.getValue("object") instanceof Literal){
			objLit=(Literal)triple.getValue("object");
		}
		
		
		if (predicate.equals(SKOS.ALT_LABEL))res.getAltLabels().add(new AltLabel(objLit));
		else if (predicate.equals(SKOS.PREF_LABEL))res.getPrefLabels().add(new PrefLabel(objLit));
		else if (predicate.equals(SKOS.HIDDEN_LABEL))res.getHiddenLabels().add(new HiddenLabel(objLit));
		else if (predicate.equals(SKOS.NOTATION))res.getNotations().add(new Notation(objLit));
		
		else if (predicate.equals(SKOS.NOTE)			||
				 predicate.equals(SKOS.CHANGE_NOTE) 	||
				 predicate.equals(SKOS.DEFINITION) 		||
				 predicate.equals(SKOS.EDITORIAL_NOTE) 	||
				 predicate.equals(SKOS.EXAMPLE) 		||
				 predicate.equals(SKOS.HISTORY_NOTE) 	||
				 predicate.equals(SKOS.SCOPE_NOTE) 
				){
			if (null!=objLit){
				if (predicate.equals(SKOS.NOTE))res.getAnnotations().add(new Note(objLit));
				if (predicate.equals(SKOS.CHANGE_NOTE))res.getAnnotations().add(new ChangeNote(objLit));
				if (predicate.equals(SKOS.DEFINITION))res.getAnnotations().add(new Definition(objLit));
				if (predicate.equals(SKOS.EDITORIAL_NOTE))res.getAnnotations().add(new EditorialNote(objLit));
				if (predicate.equals(SKOS.EXAMPLE))res.getAnnotations().add(new Example(objLit));
				if (predicate.equals(SKOS.HISTORY_NOTE))res.getAnnotations().add(new HistoryNote(objLit));
				if (predicate.equals(SKOS.SCOPE_NOTE))res.getAnnotations().add(new ScopeNote(objLit));
				
			}else if (null!=objUri){
				if (predicate.equals(SKOS.NOTE))res.getAnnotations().add(new Note(objUri));
				if (predicate.equals(SKOS.CHANGE_NOTE))res.getAnnotations().add(new ChangeNote(objUri));
				if (predicate.equals(SKOS.DEFINITION))res.getAnnotations().add(new Definition(objUri));
				if (predicate.equals(SKOS.EDITORIAL_NOTE))res.getAnnotations().add(new EditorialNote(objUri));
				if (predicate.equals(SKOS.EXAMPLE))res.getAnnotations().add(new Example(objUri));
				if (predicate.equals(SKOS.HISTORY_NOTE))res.getAnnotations().add(new HistoryNote(objLit));
				if (predicate.equals(SKOS.SCOPE_NOTE))res.getAnnotations().add(new ScopeNote(objUri));
			}
		}
		
	}
	//implemented
	public Set<Collection> getRootCollections() {
		Set<Collection> rootCollection=Sets.newHashSet();
		SailConnection sc=null;
		
		// ASK "select * {?s rdf:type skos:OrdoredCollection. }"
		try {
			sc=sail.getConnection();
		
			SPARQLParser parser=new SPARQLParser();
			CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
			String queryString =this.getQueryprepare()+" SELECT ?uri  "+
					" WHERE "+
					"{ " +
					  " { ?uri rdf:type  skos:Collection  } UNION  " +
					"   { ?uri rdf:type skos:OrderedCollection  . } ."+
					" FILTER NOT EXISTS { ?x skos:member ?uri } . " +
					" FILTER NOT EXISTS { ?z skos:definition ?uri } . " +
					" FILTER NOT EXISTS { _:y  rdf:rest|rdf:first  ?uri } ." +
					" } "
					;
				
					
			/*queryString =this.getQueryprepare()+" SELECT ?uri1 ?uri2  "+
					" WHERE "+
					"{ " +
					  " { ?uri1 rdf:type  skos:Collection  . " +
						" FILTER NOT EXISTS { ?x skos:member ?uri1 } . " +
						" FILTER NOT EXISTS { _:y  rdf:rest|rdf:first  ?uri1 }  " +
					    " } "
					    + "UNION  " +
					"   { ?uri2 rdf:type skos:OrderedCollection . "+
					" FILTER NOT EXISTS { ?z skos:member ?uri2 } . " +
					" FILTER NOT EXISTS { _:w  rdf:rest|rdf:first  ?uri2 } } " +
					" } "
					;*/
			
			/*queryString =this.getQueryprepare()+" SELECT ?uri1 ?uri2  "+
					" WHERE "+
					"{ " +
					  " { ?uri1 rdf:type  skos:Collection  . " +
						" FILTER NOT EXISTS { ?x ?p ?uri1 } . " +
						" " +
					    " } "
					    + "UNION  " +
					"   { ?uri2 rdf:type skos:OrderedCollection . "+
					" FILTER NOT EXISTS { ?z ?p ?uri2 } . " +
					"  } " +
					" } "
					;*/
				
			try {
				DateTime dt_start=DateTime.now();
				System.out.println("start ! at >>"+dt_start.toString());
				ParsedQuery query = parser.parseQuery(queryString , null);
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				//bs.addBinding(new BindingImpl("uri",uri));
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
				
				try {
					if (sparqlResults.hasNext()){
						System.out.println("Some Result for collection");
						int cpt=0;
							while (sparqlResults.hasNext()){
								cpt++;
								System.out.println("result N°"+cpt);
								
							BindingSet result=sparqlResults.next();
							System.out.println(result);
							try {
							URI uri=(URIImpl)result.getValue("uri");
							Collection c=new Collection();
							if (uri==null)throw new Exception("uri is null");
							c.setUri(uri);
							rootCollection.add(c);}catch(Exception e ){e.printStackTrace();}
							
						}
					//	System.out.println("domain="+domain+" has as narrower .. "+candidate);
					}else {
					//	System.out.println("domain="+domain+" no rel with .. "+candidate);
					}
				} catch (QueryEvaluationException e) {
					
					e.printStackTrace();
				}
				sc.close();
				DateTime dt_stop=DateTime.now();
				System.out.println(DateTime.now()+":::request getRootConceptScheme ->duration="+(dt_stop.getMillis()-dt_start.getMillis())+" ms.");
				Set<Collection> tmp=Sets.newHashSet();
				for (Collection c:rootCollection){
					System.out.println("Une collection ROOT ==>"+c.getUri());
					tmp.add(this.getCollectionWithElementsAsResources(c.getUri()));
				}
				return  tmp;
			} catch (MalformedQueryException e) {
				
				e.printStackTrace();
			}
			
		} catch (SailException e) {
			
			e.printStackTrace();
		} 
		return null;
	}
	
	
	// same as root excpt -> return root  Collection with number of listmember / setmember
		public Set<Collection> getTopCollections() {
			Set<Collection> rootCollection=Sets.newHashSet();
			SailConnection sc=null;
			
			// ASK "select * {?s rdf:type skos:OrdoredCollection. }"
			try {
				sc=sail.getConnection();
			
				SPARQLParser parser=new SPARQLParser();
				CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
				String queryString =this.getQueryprepare()+" SELECT ?uri  "+
						" WHERE "+
						"{ " +
						  " { ?uri rdf:type  skos:Collection  } UNION  " +
						"   { ?uri rdf:type skos:OrderedCollection  . } ."+
						" FILTER NOT EXISTS { ?x skos:member ?uri } . " +
						" FILTER NOT EXISTS { ?z skos:definition ?uri } . " +
						" FILTER NOT EXISTS { _:y  rdf:rest|rdf:first  ?uri } ." +
						" } "
						;
					
						
			
				try {
					
					ParsedQuery query = parser.parseQuery(queryString , null);
					SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
					//bs.addBinding(new BindingImpl("uri",uri));
					sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
					
					try {
						if (sparqlResults.hasNext()){
							System.out.println("Some Result for collection");
							int cpt=0;
								while (sparqlResults.hasNext()){
									cpt++;
									System.out.println("result N°"+cpt);
									
								BindingSet result=sparqlResults.next();
								System.out.println(result);
								try {
								URI uri=(URIImpl)result.getValue("uri");
								Collection c=new Collection();
								if (uri==null)throw new Exception("uri is null");
								c.setUri(uri);
								rootCollection.add(c);}catch(Exception e ){e.printStackTrace();}
								
							}
						//	System.out.println("domain="+domain+" has as narrower .. "+candidate);
						}else {
						//	System.out.println("domain="+domain+" no rel with .. "+candidate);
						}
					} catch (QueryEvaluationException e) {
						
						e.printStackTrace();
					}
					sc.close();
					Set<Collection> tmp=Sets.newHashSet();
					for (Collection c:rootCollection){
						System.out.println("Une collection ROOT ==>"+c.getUri());
						tmp.add(this.getCollectionWithElementsAsNumber(c.getUri()));
					}
					return  tmp;
				} catch (MalformedQueryException e) {
					
					e.printStackTrace();
				}
				
			} catch (SailException e) {
				
				e.printStackTrace();
			} 
			return null;
		}

	//implemented
	public ConceptScheme getConceptSchemeAsAResource(URI uri) {
		SailConnection sc=null;
		
		try {
		sc=sail.getConnection();
		ConceptScheme cs=null;
		
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.getQueryprepare()+ 
						//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
						  	"CONSTRUCT { " +
						  	"?uri	    rdf:type       		skos:ConceptScheme ." +
						  	"?uri 		skos:prefLabel 		?preflabel . " +
						  	"?uri	  	skos:altLabel   	?altlabel . " +
						  	"?uri	  	skos:notation   	?notation . " +
						  	"?uri	  	skos:hiddenLabel   	?hiddenlabel . " +
						  	"?uri	    skos:note  			?note . " + 
						  	"?uri	    skos:changeNote		?changeNote . " + 
						  	"?uri	    skos:definition 	?definition . " + 
						  	"?uri	    skos:editorialNote  ?editorialNote . " + 
						  	"?uri	    skos:example  		?example . " + 
						  	"?uri	    skos:historyNote  	?historyNote . " + 
						  	"?uri	    skos:scopeNote  	?scopeNote . " + 
						  	"}"+
						  
							SPARQL.WHERE+
								SPARQL.OPEN+
									  " { ?uri	    rdf:type       	skos:ConceptScheme  }  "+
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri 		skos:prefLabel 	?preflabel }  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	  	skos:altLabel   ?altlabel }  "+
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	  	skos:notation   ?notation }  "+
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	    skos:note  		?note  }  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	  	skos:hiddenLabel   	?hiddenlabel  }  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	    skos:changeNote		?changeNote }  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	    skos:definition 	?definition }  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	    skos:editorialNote  ?editorialNote }  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	    skos:example  		?example }  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	    skos:historyNote  	?historyNote}  " +
									  "UNION { ?uri	    rdf:type       	skos:ConceptScheme . ?uri	    skos:scopeNote  	?scopeNote }  " +
							
								SPARQL.CLOSE  + "ORDER BY ?uri ";	
							
		//System.out.println(queryString);
		ParsedQuery query=null;
		try {
			query = parser.parseQuery(queryString , null);
		} catch (MalformedQueryException e) {
			
			e.printStackTrace();
		}
		
		SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
		bs.addBinding(new BindingImpl("uri",uri));
		
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
		
		try {
			if (sparqlResults.hasNext()){
			    cs=new ConceptScheme();
			    int i=0;
				while (sparqlResults.hasNext()){
					BindingSet a=sparqlResults.next();
					if(a.getValue("object")!=null){
						//System.out.println("construct="+a.toString());
						if (i==0)cs.setUri(uri);
						bindValueResource(a,cs);
						i++;
					}
				}
				
			}
		} catch (QueryEvaluationException e) {
			
			e.printStackTrace();
		}
		
		sc.close();
		return cs;
		}catch (Exception e) {
			e.printStackTrace();
			try {
				if (null!=sc && sc.isOpen()){
					sc.close();
				}
			} catch (SailException e1) {
				
				e1.printStackTrace();
			}
		}
		return null;
	}
    //implemented
	public ConceptScheme getConceptSchemeAsAResource(Notation notation) {
		ConceptScheme cs=null;
		SailConnection sc=null;
		
		try {
				sc=sail.getConnection();
				SPARQLParser parser=new SPARQLParser();
				CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
				
				String queryString =this.getQueryprepare()+ 
						//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
						  	"SELECT DISTINCT ?scheme WHERE { " +
						  	"?scheme skos:notation ?notation . " +
						    "?scheme rdf:type skos:conceptScheme . " +
						  	"}"; 
						  
				ParsedQuery query = parser.parseQuery(queryString , null);
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				bs.addBinding(new BindingImpl("notation",notation.getModelValue()));
			    
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
				
				try {
					if (sparqlResults.hasNext()){
			
					    
						while (sparqlResults.hasNext()){
							BindingSet a=sparqlResults.next();
							if (a.getValue("scheme")!=null){
								cs=new ConceptScheme();
								cs.setUri(new URIImpl(a.getValue("scheme").stringValue()));
								break;
								}
							}
							
							
						}
						
					
					
					} catch (QueryEvaluationException e) {
					
						e.printStackTrace();
					}
				
				sc.close();
				
				
				
			
			}catch (Exception e) {
			e.printStackTrace();
			try {
				if (null!=sc && sc.isOpen()){
					sc.close();
				}
			} catch (SailException e1) {
				
				e1.printStackTrace();
			}
		}
		try {return this.getConceptSchemeAsAResource(cs.getUri());}catch (Exception e ){}
		
		return null;
	}
	// is only called if getConceptScheme() top=empty.
	private ConceptScheme getConceptSchemeWithNoTopConceptDeclared(ConceptScheme cs,int maxAnswer,int offset){
		SailConnection sc=null;
		   System.out.println("ds le patch New york Times people.");
		try {
				sc=sail.getConnection();
				SPARQLParser parser=new SPARQLParser();
				CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
				
				String queryString =this.getQueryprepare()+ 
						//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
						  	"SELECT DISTINCT ?top WHERE { " +
						  	"?top skos:inScheme ?scheme ." +
						   
						  	"}  ORDER BY ?top OFFSET "+offset+" LIMIT "+ (maxAnswer+1) +" "; 
						  
				ParsedQuery query = parser.parseQuery(queryString , null);
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				bs.addBinding(new BindingImpl("scheme",cs.getUri()));
			
				
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
				
				try {
					if (sparqlResults.hasNext()){
			
					    System.out.println("on trouve des pseudo top concepts");
					    
						while (sparqlResults.hasNext()){
							BindingSet a=sparqlResults.next();
							if (a.getValue("top")!=null){
								URI topConceptUri=new URIImpl(a.getValue("top").stringValue());
								Concept top=new Concept();
								top.setUri(topConceptUri);
								cs.getHasTopConcepts().add(top);
								}
							}
							
							
						}
						
					
					
					} catch (QueryEvaluationException e) {
					
						e.printStackTrace();
					}
				
				sc.close();
				
				List<Concept> tops=Lists.newArrayList();
			    if (cs.getHasTopConcepts().isEmpty()){
			    	// l'uri du cs en entrée n'a meme pas de inscheme -> ce n'est pas un cs : on renvoie null.
			    	return null;
			    }
				for (Concept top:cs.getHasTopConcepts()){
					tops.add(this.getConceptAsResource(top.getUri(),null));
				}
				cs.setHasTopConcepts(tops);
			
			}catch (Exception e) {
			e.printStackTrace();
			try {
				if (null!=sc && sc.isOpen()){
					sc.close();
				}
			} catch (SailException e1) {
				
				e1.printStackTrace();
			}
		}
				return cs;
	}
    // implemented // si on renvoie 11 elements -> 10 + au moins 1 autre (next page !!! exists ) 
	public ConceptScheme getConceptScheme(URI uri,int maxAnswer,int offset) {
		ConceptScheme cs=this.getConceptSchemeAsAResource(uri);
		if (cs==null){ // peut etre un cs non definit par des éléments (Ny Times Organisation par exemple)
			cs=new ConceptScheme();
			cs.setUri(uri);
			return this.getConceptSchemeWithNoTopConceptDeclared(cs, maxAnswer, offset);
		}
		SailConnection sc=null;
		
		try {
				sc=sail.getConnection();
				SPARQLParser parser=new SPARQLParser();
				CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
				 
				String queryString =this.getQueryprepare()+ 
						//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
						  	"SELECT DISTINCT ?top WHERE { " +
						  	"?scheme skos:hasTopConcept|^skos:topConceptOf ?top ." +
						   
						  	"}  ORDER BY ?top OFFSET "+offset+" LIMIT "+ (maxAnswer+1) +" "; 
						  
				ParsedQuery query = parser.parseQuery(queryString , null);
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				bs.addBinding(new BindingImpl("scheme",cs.getUri()));
			
				
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
				
				try {
					if (sparqlResults.hasNext()){
			
					    
						while (sparqlResults.hasNext()){
							BindingSet a=sparqlResults.next();
							if (a.getValue("top")!=null){
								URI topConceptUri=new URIImpl(a.getValue("top").stringValue());
								Concept top=new Concept();
						
								top.setUri(topConceptUri);
								cs.getHasTopConcepts().add(top);
								}
							}
							
							
						}
						
					
					
					} catch (QueryEvaluationException e) {
					
						e.printStackTrace();
					}
				
				sc.close();
				
				List<Concept> tops=Lists.newArrayList();
				if (cs.getHasTopConcepts().isEmpty())return this.getConceptSchemeWithNoTopConceptDeclared(cs,maxAnswer, offset);
				for (Concept top:cs.getHasTopConcepts()){
					Concept c=this.getConceptAsResource(top.getUri(),cs.getUri());
					ConceptScheme ersatz=new ConceptScheme();
					ersatz.setUri(cs.getUri());
					c.getInScheme().add(ersatz);
					c.getTopConceptOf().add(ersatz);
					tops.add(c);
				}
				cs.setHasTopConcepts(tops);
				
				return cs;
			}catch (Exception e) {
			e.printStackTrace();
			try {
				if (null!=sc && sc.isOpen()){
					sc.close();
				}
			} catch (SailException e1) {
				
				e1.printStackTrace();
			}
		}
				return null;
	}
    // implemented.
	public ConceptScheme getConceptScheme(Notation notation) {
		ConceptScheme cs=null;
		SailConnection sc=null;
		
		try {
				sc=sail.getConnection();
				SPARQLParser parser=new SPARQLParser();
				CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
				
				String queryString =this.getQueryprepare()+ 
						//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
						  	"SELECT DISTINCT ?scheme WHERE { " +
						  	"?scheme skos:notation ?notation . " +
						    "?scheme rdf:type skos:conceptScheme . " +
						  	"}"; 
						  
				ParsedQuery query = parser.parseQuery(queryString , null);
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				bs.addBinding(new BindingImpl("notation",notation.getModelValue()));
			    
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
				
				try {
					if (sparqlResults.hasNext()){
			
					    
						while (sparqlResults.hasNext()){
							BindingSet a=sparqlResults.next();
							if (a.getValue("scheme")!=null){
								cs=new ConceptScheme();
								cs.setUri(new URIImpl(a.getValue("scheme").stringValue()));
								break;
								}
							}
							
							
						}
						
					
					
					} catch (QueryEvaluationException e) {
					
						e.printStackTrace();
					}
				
				sc.close();
				
				
				
			
			}catch (Exception e) {
			e.printStackTrace();
			try {
				if (null!=sc && sc.isOpen()){
					sc.close();
				}
			} catch (SailException e1) {
				
				e1.printStackTrace();
			}
		}
		try {return this.getConceptScheme(cs.getUri(),20,0);}catch (Exception e ){}
		
		return null;
	}

	//implemented
	public List<Concept> getTopElementOf(URI conceptSchemeUri) {
		try {return this.getConceptScheme(conceptSchemeUri,20,0).getHasTopConcepts();}
		catch (Exception e ){}
		return null;
	}

	//implemented
	public Concept getConceptAsResource(URI uri,URI cs) {
		SailConnection sc=null;
		try {
		Concept concept=null;
		sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		
		String queryString =this.getQueryprepare()+ 
				//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
				  	"CONSTRUCT { " +
				  	"?uri	    rdf:type       		skos:Concept ." +
				  	"?uri 		skos:prefLabel 		?preflabel . " +
				  	"?uri	  	skos:altLabel   	?altlabel . " +
				  	"?uri	  	skos:notation   	?notation . " +
				  	"?uri	  	skos:hiddenLabel   	?hiddenlabel . " +
				  	"?uri	    skos:note  			?note . " + 
				  	"?uri	    skos:changeNote		?changeNote . " + 
				  	"?uri	    skos:definition 	?definition . " + 
				  	"?uri	    skos:editorialNote  ?editorialNote . " + 
				  	"?uri	    skos:example  		?example . " + 
				  	"?uri	    skos:historyNote  	?historyNote . " + 
				  	"?uri	    skos:scopeNote  	?scopeNote . " + 
				  
				  	"}"+
				  
					SPARQL.WHERE+
						SPARQL.OPEN+
							  " { ?uri	    rdf:type         	skos:Concept  }  "+
							  "UNION { ?uri	    rdf:type       	skos:Concept . ?uri 	skos:prefLabel 	?preflabel }  " +
							  "UNION { ?uri	    rdf:type       	skos:Concept . ?uri	  	skos:altLabel   ?altlabel }  "+
							  "UNION { ?uri	    rdf:type       	skos:Concept . ?uri	  	skos:notation   ?notation }  "+
							  "UNION { ?uri	    rdf:type       	skos:Concept . ?uri	    skos:note  		?note  }  " +
							  "UNION { ?uri	    rdf:type       	skos:Concept . ?uri	  	skos:hiddenLabel   	?hiddenlabel  }  " +
							  "UNION { ?uri	    rdf:type       	skos:Concept . ?uri	    skos:changeNote		?changeNote }  " +
							  "UNION { ?uri	    rdf:type       	skos:Concept . ?uri	    skos:definition 	?definition }  " +
							  "UNION { ?uri	    rdf:type       	skos:Concept . ?uri	    skos:editorialNote  ?editorialNote }  " +
							  "UNION { ?uri	    rdf:type       	skos:Concept . ?uri	    skos:example  		?example }  " +
							  "UNION { ?uri	    rdf:type       	skos:Concept . ?uri	    skos:historyNote  	?historyNote}  " +
							  "UNION { ?uri	    rdf:type       	skos:Concept . ?uri	    skos:scopeNote  	?scopeNote }  " +
					
						SPARQL.CLOSE  + "ORDER BY ?uri ";	
					
		//	System.out.println(queryString);
		
		
		 ParsedQuery query = parser.parseQuery(queryString , null);
			SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
			bs.addBinding(new BindingImpl("uri",uri));
		
		
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
		
		try {
			if (sparqlResults.hasNext()){
			    concept=new Concept();
			    int i=0;
				while (sparqlResults.hasNext()){
					BindingSet a=sparqlResults.next();
					if (null!=a.getValue("object")){
					//	System.out.println("getConceptAsResource="+a.toString());
						if (i==0){
							concept.setUri(uri);
							
						}
						bindValueResource(a,concept);
						i++;
					}
				}
				
			}
		} catch (QueryEvaluationException e) {
			
			e.printStackTrace();
		}
		
		sc.close();
		if (concept==null)return null;
	   
		/* 
	     * recup le nombre de fils ds son Inscheme Principale (celui ui constitue son namespace !
	     * 
	     */
		int cpt=0;
		sc=sail.getConnection();
		parser=new SPARQLParser();
		//System.out.println(concept.getUri().getNamespace().substring(0, concept.getUri().getNamespace().length()-1));
				
				queryString =this.getQueryprepare()+ 
						//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
						  	"SELECT distinct ?narrower  WHERE { " +
						  	"?uri  skos:narrower|^skos:broader ?narrower ." +
						  	"?narrower skos:inScheme ?schemeUri. " +
						  	"}"; 
						  
				query = parser.parseQuery(queryString , null);
				bs=new SPARQLQueryBindingSet();
				if (cs==null)
				bs.addBinding(new BindingImpl("schemeUri",new URIImpl(concept.getUri().getNamespace().substring(0, concept.getUri().getNamespace().length()-1))));
				else 
					bs.addBinding(new BindingImpl("schemeUri",cs));
				
				bs.addBinding(new BindingImpl("uri",uri));
				
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
				
				try {
					if (sparqlResults.hasNext()){
			
					  
						while (sparqlResults.hasNext()){
							
						
							BindingSet a=sparqlResults.next();
							if (a.getValue("narrower")!=null){
								//URI narrowerUri=new URIImpl(a.getValue("narrower").stringValue());
								cpt++;
								}
							}
							
							
						}
						
					
					
			} catch (QueryEvaluationException e) {
					
						e.printStackTrace();
			}
				
		sc.close();
		
		sc=sail.getConnection();
		parser=new SPARQLParser();
		//System.out.println(concept.getUri().getNamespace().substring(0, concept.getUri().getNamespace().length()-1));
				
				queryString =this.getQueryprepare()+ 
						//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
						  	"SELECT distinct ?rel  WHERE { " +
						  	"?uri  skos:related ?rel ." +
						  
						  	"}"; 
						  
				query = parser.parseQuery(queryString , null);
				bs=new SPARQLQueryBindingSet();
				
				bs.addBinding(new BindingImpl("uri",uri));
				
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
				
				try {
					if (sparqlResults.hasNext()){
			
					  
						while (sparqlResults.hasNext()){
							
						
							BindingSet a=sparqlResults.next();
							if (a.getValue("rel")!=null){
								//URI narrowerUri=new URIImpl(a.getValue("narrower").stringValue());
								cpt++;
								}
							}
							
							
						}
						
					
					
			} catch (QueryEvaluationException e) {
					
						e.printStackTrace();
			}
				
		sc.close();
		
		
		concept.setChildrenPreviewNumber(concept.getChildrenPreviewNumber()+cpt);
		// on rajoute les relations scheme !!!
		this.addSchemeRelationsToConcept(concept);
		return concept;
		
		}catch (Exception e) {
			e.printStackTrace();
			try {
				if (null!=sc && sc.isOpen()){
					sc.close();
				}
			} catch (SailException e1) {
				
				e1.printStackTrace();
			}
		}
		return null;
	}
	
	
	//implemented
	private void addSchemeRelationsToConcept(Concept concept) {
			SailConnection sc=null;
			try {
			
			sc=sail.getConnection();
			SPARQLParser parser=new SPARQLParser();
			CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
			   
			String queryString =this.getQueryprepare()+ 
					//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
					  	"CONSTRUCT { " +
					  	"?uri	    skos:topConceptOf ?schemeA ." +
					  	"?uri	    skos:inScheme  		?schemeB . " + 
					  
					  	"}"+
					  
					SPARQL.WHERE+
							SPARQL.OPEN+
								  " { ?uri	    rdf:type         	skos:Concept  }  "+
								  "UNION { ?uri	    rdf:type       	skos:Concept . ?uri 	skos:topConceptOf|^skos:hasTopConcept ?schemeA }  " +
								  "UNION { ?uri	    rdf:type       	skos:Concept . ?uri	  	skos:inScheme  		?schemeB }  "+
								
						
							SPARQL.CLOSE  ;	
						
			//	System.out.println(queryString);
			
			
			ParsedQuery query = parser.parseQuery(queryString , null);
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				bs.addBinding(new BindingImpl("uri",concept.getUri()));
			
			
			sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
			
			try {
				if (sparqlResults.hasNext()){
				   
					while (sparqlResults.hasNext()){
						BindingSet a=sparqlResults.next();
						if (null!=a.getValue("object")){
							bindValueConcept(a,concept);
						}
					}
					
				}
			} catch (QueryEvaluationException e) {
				
				e.printStackTrace();
			}
			
			sc.close();
			
			
			}catch (Exception e) {
				e.printStackTrace();
				try {
					if (null!=sc && sc.isOpen()){
						sc.close();
					}
				} catch (SailException e1) {
					
					e1.printStackTrace();
				}
			}
			
		}
	//implemented
	private void populateCollectionWithResource(Collection collection) {
			SailConnection sc=null;
			try {
		
			sc=sail.getConnection();
			SPARQLParser parser=new SPARQLParser();
			CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
			
			String queryString =this.getQueryprepare()+ 
					//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
					  	"CONSTRUCT { " +
					  	"?uri	    rdf:type       		?type ." +
					  	"?uri 		skos:prefLabel 		?preflabel . " +
					  	"?uri	  	skos:altLabel   	?altlabel . " +
					  	"?uri	  	skos:notation   	?notation . " +
					  	"?uri	  	skos:hiddenLabel   	?hiddenlabel . " +
					  	"?uri	    skos:note  			?note . " + 
					  	"?uri	    skos:changeNote		?changeNote . " + 
					  	"?uri	    skos:definition 	?definition . " + 
					  	"?uri	    skos:editorialNote  ?editorialNote . " + 
					  	"?uri	    skos:example  		?example . " + 
					  	"?uri	    skos:historyNote  	?historyNote . " + 
					  	"?uri	    skos:scopeNote  	?scopeNote . " + 
					  	"}"+
					  
						SPARQL.WHERE+
							SPARQL.OPEN+
								  " { ?uri	    rdf:type         	?type  }  "+
								  "UNION { ?uri	    rdf:type       	?type . ?uri 	skos:prefLabel 	?preflabel }  " +
								  "UNION { ?uri	    rdf:type       	?type . ?uri	  	skos:altLabel   ?altlabel }  "+
								  "UNION { ?uri	    rdf:type       	?type . ?uri	  	skos:notation   ?notation }  "+
								  "UNION { ?uri	    rdf:type       	?type . ?uri	    skos:note  		?note  }  " +
								  "UNION { ?uri	    rdf:type       	?type . ?uri	  	skos:hiddenLabel   	?hiddenlabel  }  " +
								  "UNION { ?uri	    rdf:type       	?type . ?uri	    skos:changeNote		?changeNote }  " +
								  "UNION { ?uri	    rdf:type       	?type . ?uri	    skos:definition 	?definition }  " +
								  "UNION { ?uri	    rdf:type       	?type . ?uri	    skos:editorialNote  ?editorialNote }  " +
								  "UNION { ?uri	    rdf:type       	?type . ?uri	    skos:example  		?example }  " +
								  "UNION { ?uri	    rdf:type       	?type . ?uri	    skos:historyNote  	?historyNote}  " +
								  "UNION { ?uri	    rdf:type       	?type . ?uri	    skos:scopeNote  	?scopeNote }  " +
						
							SPARQL.CLOSE  + "ORDER BY ?uri ";	
						
			//	System.out.println(queryString);
			
			
			ParsedQuery query = parser.parseQuery(queryString , null);
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				bs.addBinding(new BindingImpl("uri",collection.getUri()));
			    if (collection instanceof OrderedCollection){
			    	bs.addBinding(new BindingImpl("type",SKOS.ORDERED_COLLECTION));
			    }else {
			    	bs.addBinding(new BindingImpl("type",SKOS.COLLECTION));
			    }
			
			sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
			
			try {
				if (sparqlResults.hasNext()){
				   
					while (sparqlResults.hasNext()){
						BindingSet a=sparqlResults.next();
						if (null!=a.getValue("object")){
					//		System.out.println("getOrderedCollectionResutlTriple="+a.getValue("object"));
						
							bindValueResource(a,collection);
						
						}
					}
					
				}
			} catch (QueryEvaluationException e) {
				
				e.printStackTrace();
			}
			
			sc.close();
			
			
			}catch (Exception e) {
				e.printStackTrace();
				try {
					if (null!=sc && sc.isOpen()){
						sc.close();
					}
				} catch (SailException e1) {
					
					e1.printStackTrace();
				}
			}
			
		}
	
	
	// on compte les URI , on set le Size et on clear les members. (coll + ocol)
		public Collection getCollectionWithElementsAsNumber(URI uri) {
			
			SailConnection sc=null;
			boolean isordered=false;
			// ASK "select * {?s rdf:type skos:OrdoredCollection. }"
			try {
				sc=sail.getConnection();
			
				SPARQLParser parser=new SPARQLParser();
				CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
				String queryString =this.getQueryprepare()+" ASK  "+
						" WHERE "+
						"{ " +
						" ?uri rdf:type skos:OrderedCollection . "+
						" } "
						;
				try {
					ParsedQuery query = parser.parseQuery(queryString , null);
					SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
					bs.addBinding(new BindingImpl("uri",uri));
					sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
					System.out.println("collection found ?"+uri);
					try {
						if (sparqlResults.hasNext()){
						//	System.out.println("Resultat==>ASK"+sparqlResults.next());
							isordered=true;
						//	System.out.println("domain="+domain+" has as narrower .. "+candidate);
						}else {
						//	System.out.println("domain="+domain+" no rel with .. "+candidate);
						}
					} catch (QueryEvaluationException e) {
						
						e.printStackTrace();
					}
					sc.close();
				} catch (MalformedQueryException e) {
					
					e.printStackTrace();
				}
				
			} catch (SailException e) {
				
				e.printStackTrace();
			} 
			
			Collection collection=null;
			// etape 1 recupation structure de collection.
			if(isordered) {
				
				collection=this.getOrderedCollection(uri);
				// etape 2 commune : recu des notations, preflabel etc (ressources) pour l'object collection
				this.populateCollectionWithResource(collection);
				long size=((OrderedCollection)collection).getMemberLists().size();
				collection.setSize(size);
				((OrderedCollection)collection).getMemberLists().clear();
			}
			else {
				collection=this.getCollection(uri);
				this.populateCollectionWithResource(collection);
				Set<CollectionEntity> willReplace=Sets.newHashSet();
				long size=collection.getMembers().size();
				collection.setSize(size);
				collection.getMembers().clear();
			}
			
			
			
			return collection;
		}
	
	
	//implemented
	// retourne info collection + members resource (+ members childs numbers)
	public Collection getCollectionWithElementsAsResources(URI uri) {
		
		SailConnection sc=null;
		boolean isordered=false;
		// ASK "select * {?s rdf:type skos:OrdoredCollection. }"
		try {
			sc=sail.getConnection();
		
			SPARQLParser parser=new SPARQLParser();
			CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
			String queryString =this.getQueryprepare()+" ASK  "+
					" WHERE "+
					"{ " +
					" ?uri rdf:type skos:OrderedCollection . "+
					" } "
					;
			try {
				ParsedQuery query = parser.parseQuery(queryString , null);
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				bs.addBinding(new BindingImpl("uri",uri));
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
				System.out.println("collection found ?"+uri);
				try {
					if (sparqlResults.hasNext()){
						System.out.println("Resultat==>ASK"+sparqlResults.next());
						isordered=true;
					//	System.out.println("domain="+domain+" has as narrower .. "+candidate);
					}else {
					//	System.out.println("domain="+domain+" no rel with .. "+candidate);
					}
				} catch (QueryEvaluationException e) {
					
					e.printStackTrace();
				}
				sc.close();
			} catch (MalformedQueryException e) {
				
				e.printStackTrace();
			}
			
		} catch (SailException e) {
			
			e.printStackTrace();
		} 
		
		Collection collection=null;
		// etape 1 recupation structure de collection.
		if(isordered) {
			collection=this.getOrderedCollection(uri);
			// etape 2 commune : recu des notations, preflabel etc (ressources) pour l'object collection
			this.populateCollectionWithResource(collection);
			List<CollectionEntity> willReplace=Lists.newArrayList();
			
			for (CollectionEntity ce:((OrderedCollection)collection).getMemberLists()){
				if (ce instanceof Concept){
					Concept c=this.getConceptAsResource(((Concept) ce).getUri(),(URI)null);
					if (c!=null)willReplace.add(c);
					else willReplace.add(ce);
				}
				if (ce instanceof Collection){
					ce=this.getCollectionWithElementsAsNumber(((Collection) ce).getUri());
					willReplace.add(ce);
				}
			}
			((OrderedCollection)collection).setMemberLists(willReplace);
		}
		else {
			collection=this.getCollection(uri);
			this.populateCollectionWithResource(collection);
			Set<CollectionEntity> willReplace=Sets.newHashSet();
			for (CollectionEntity ce:collection.getMembers()){
				if (ce instanceof Concept){
					Concept c=this.getConceptAsResource(((Concept) ce).getUri(),null);
					if (c!=null)willReplace.add(c);
					else willReplace.add(ce);
				}
				if (ce instanceof Collection){
					ce=this.getCollectionWithElementsAsNumber(((Collection) ce).getUri());
					willReplace.add(ce);
				}
			}
			collection.setMembers(willReplace);
		}
		
		 // etapes 3 communes : recup des ressources des membres.
		
		
		
		return collection;
	}
	//implemented
	public Collection getCollection(URI uri){
		// "select ?m  {<http://server.ubiqore.com/v1/menu/MENU1003> skos:memberList ?list . ?list rdf:rest*/rdf:first ?m }"
		SailConnection sc=null;
		Collection collection=new Collection();
		collection.setUri(uri);
		try {
		sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		
		String queryString =this.getQueryprepare()+ 
				//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
				  	"SELECT ?uri ?member ?type WHERE { " +
				  	"?uri rdf:type skos:Collection ." +
				  	"OPTIONAL { ?uri 		skos:member 		?member . " + 
				  	"OPTIONAL { ?member rdf:type ?type } } . " +
				  	"}";
				  
		ParsedQuery query = parser.parseQuery(queryString , null);
			SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
			bs.addBinding(new BindingImpl("uri",uri));
		
		
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
		boolean res=false;
		try {
			if (sparqlResults.hasNext()){
				res=true;
				while (sparqlResults.hasNext()){
					BindingSet a=sparqlResults.next();
					if (a.getValue("type")!=null){
						URI type=new URIImpl(a.getValue("type").stringValue());
						if (type.toString().equals(SKOS.CONCEPT.toString())){
							Concept c=new Concept();
							c.setUri(new URIImpl(a.getValue("member").stringValue()));
							collection.getMembers().add(c);
						}else if (type.equals(SKOS.COLLECTION)) {
							Collection c=new Collection();
							c.setUri(new URIImpl(a.getValue("member").stringValue()));
							collection.getMembers().add(c);
						}else if (type.equals(SKOS.ORDERED_COLLECTION)){
							OrderedCollection c=new OrderedCollection();
							c.setUri(new URIImpl(a.getValue("member").stringValue()));
							collection.getMembers().add(c);
						}else { // external element are concepts ( arbitrary decision)
							Concept c=new Concept();
							c.setUri(new URIImpl(a.getValue("member").stringValue()));
							collection.getMembers().add(c);
						}
					}
					
					
				}
				
			}
			else{
				sc.close();
				return null;
			}
		} catch (QueryEvaluationException e) {
			
			e.printStackTrace();
		}
		
		sc.close();
		return collection;
		
		}catch (Exception e) {
			e.printStackTrace();
			try {
				if (null!=sc && sc.isOpen()){
					sc.close();
				}
			} catch (SailException e1) {
				
				e1.printStackTrace();
			}
		}
		return null;
	}
	//implemented
	public OrderedCollection getOrderedCollection(URI uri){
		// "select ?m  {<http://server.ubiqore.com/v1/menu/MENU1003> skos:memberList ?list . ?list rdf:rest*/rdf:first ?m }"
		SailConnection sc=null;
		OrderedCollection orderedCollection=new OrderedCollection();
		orderedCollection.setUri(uri);
		try {
		sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		
		String queryString =this.getQueryprepare()+ 
				//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
				  	"SELECT ?uri ?member ?type WHERE { " +
				  	"?uri rdf:type skos:OrderedCollection ." +
				  	"OPTIONAL { { " +
				  	 " ?uri 		skos:memberList 		_:x ."+
				  	 " _:x	   rdf:rest*/rdf:first  	  ?member } . " + 
				  	"OPTIONAL { ?member rdf:type ?type  } } . " +
				  	"}";
				  
		ParsedQuery query = parser.parseQuery(queryString , null);
			SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
			bs.addBinding(new BindingImpl("uri",uri));
		
		
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
		boolean res=false;
		try {
			if (sparqlResults.hasNext()){
	
			    res=true;
				while (sparqlResults.hasNext()){
					BindingSet a=sparqlResults.next();
					if (a.getValue("type")!=null){
						URI type=new URIImpl(a.getValue("type").stringValue());
						if (type.toString().equals(SKOS.CONCEPT.toString())){
							Concept c=new Concept();
							c.setUri(new URIImpl(a.getValue("member").stringValue()));
							orderedCollection.getMemberLists().add(c);
						}else if (type.equals(SKOS.COLLECTION)) {
							Collection collection=new Collection();
							collection.setUri(new URIImpl(a.getValue("member").stringValue()));
							orderedCollection.getMemberLists().add(collection);
						}else if (type.equals(SKOS.ORDERED_COLLECTION)){
							OrderedCollection collection=new OrderedCollection();
							collection.setUri(new URIImpl(a.getValue("member").stringValue()));
							orderedCollection.getMemberLists().add(collection);
						}else { // external element are concept (desition arbitraire)
							Concept c=new Concept();
							c.setUri(new URIImpl(a.getValue("member").stringValue()));
							orderedCollection.getMemberLists().add(c);
						}
					}
					
					
				}
				
			}
			else{
				sc.close();
				return null;
			}
		} catch (QueryEvaluationException e) {
			
			e.printStackTrace();
		}
		
		sc.close();
		return orderedCollection;
		
		}catch (Exception e) {
			e.printStackTrace();
			try {
				if (null!=sc && sc.isOpen()){
					sc.close();
				}
			} catch (SailException e1) {
				
				e1.printStackTrace();
			}
		}
		return null;
	}
	
	
	public Concept getConceptWithMapping(URI uri,URI schemeUri){
		Concept c=this.getConcept(uri, schemeUri);
		if (c==null)return null;
		if (schemeUri ==null){ 
			try {schemeUri=c.getInScheme().get(0).getUri();}catch (Exception e ){
				e.printStackTrace();
				return c;}
		}
		else {
			boolean isInScheme = false;

			for (ConceptScheme cs : c.getInScheme()) {
				if (cs.getUri().toString()
						.equalsIgnoreCase(schemeUri.toString())) {
					isInScheme = true;
				}
			}
			if (!isInScheme)
				return null;
		}
		

		SailConnection sc=null;
		try {
				sc=sail.getConnection();
				SPARQLParser parser=new SPARQLParser();
				CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
				
				String queryString =this.getQueryprepare()+ 
						//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
						  	"SELECT distinct ?p  ?match  WHERE { " +
						  	"?uri  ?p  ?match filter ( ?p in (skos:closeMatch, "
						  								 + 	" skos:exactMatch,"
						  								 + 	" skos:broadMatch,"
						  								 + 	" skos:narrowMatch,"
						  								 + "  skos:relatedMatch ) ) " +
						  	"}"; 
						  
				ParsedQuery query = parser.parseQuery(queryString , null);
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				bs.addBinding(new BindingImpl("uri",uri));
				
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
				
				try {
					if (sparqlResults.hasNext()){
						System.out.println("some MATCH  for>>"+uri);
					    
						while (sparqlResults.hasNext()){
							BindingSet a=sparqlResults.next();
							if (a.getValue("match")!=null){
								URI narrowerUri=new URIImpl(a.getValue("match").stringValue());
								Concept match=new Concept();
								match.setUri(narrowerUri);
								URI predicate=(URI)a.getValue("p");
								if (predicate.equals(SKOS.CLOSE_MATCH))c.getCloseMatch().add(match);
								if (predicate.equals(SKOS.EXACT_MATCH))c.getExactMatch().add(match);
								if (predicate.equals(SKOS.BROAD_MATCH))c.getBroadMatch().add(match);
								if (predicate.equals(SKOS.NARROW_MATCH))c.getNarrowMatch().add(match);
								if (predicate.equals(SKOS.RELATED_MATCH))c.getRelatedMatch().add(match);
								}
							}
							
					
						}
						
					
					
					} catch (QueryEvaluationException e) {
					
						e.printStackTrace();
					}
				
				sc.close();
				
				return c;
				
				}catch (Exception e) {
					e.printStackTrace();
					try {
						if (null!=sc && sc.isOpen()){
							sc.close();
						}
					} catch (SailException e1) {
						
						e1.printStackTrace();
					}
				}
		
		return null;
	}
    // si scheme=null , on teste dans le premier scheme . sans scheme pas de relations.
	//implemented
	public Concept getConcept(URI uri, URI schemeUri) {
		Concept c=this.getConceptAsResource(uri,schemeUri);
		if (c==null)return null;
		
		
		
		if (schemeUri ==null){ 
			try {schemeUri=c.getInScheme().get(0).getUri();}catch (Exception e ){return c;}
		}
		else {
			boolean isInScheme = false;

			for (ConceptScheme cs : c.getInScheme()) {
				if (cs.getUri().toString()
						.equalsIgnoreCase(schemeUri.toString())) {
					isInScheme = true;
				}
			}
			if (!isInScheme)
				return null;
		}
		
		
		SailConnection sc=null;
		try {
				sc=sail.getConnection();
				SPARQLParser parser=new SPARQLParser();
				CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
				
				String queryString =this.getQueryprepare()+ 
						//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
						  	"SELECT distinct ?narrower  WHERE { " +
						  	"?uri  skos:narrower|^skos:broader ?narrower ." +
						  	"?narrower skos:inScheme ?schemeUri. " +
						  	"}"; 
						  
				ParsedQuery query = parser.parseQuery(queryString , null);
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				bs.addBinding(new BindingImpl("schemeUri",schemeUri));
				bs.addBinding(new BindingImpl("uri",uri));
				
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
				
				try {
					if (sparqlResults.hasNext()){
			
					    
						while (sparqlResults.hasNext()){
							BindingSet a=sparqlResults.next();
							if (a.getValue("narrower")!=null){
								URI narrowerUri=new URIImpl(a.getValue("narrower").stringValue());
								Concept narrower=new Concept();
								narrower.setUri(narrowerUri);
								c.getNarrowers().add(narrower);
								}
							}
							
						c.setChildrenPreviewNumber(c.getNarrowers().size());
						}
						
					
					
					} catch (QueryEvaluationException e) {
					
						e.printStackTrace();
					}
				
				sc.close();
				
				sc=sail.getConnection();
				parser=new SPARQLParser();
				
				queryString =this.getQueryprepare()+ 
						//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
						  	"SELECT distinct ?broader  WHERE { " +
						  	"?uri  skos:broader|^skos:narrower ?broader ." +
						  	"?broader skos:inScheme ?schemeUri. " +
						  	"}"; 
						  
				query = parser.parseQuery(queryString , null);
				bs=new SPARQLQueryBindingSet();
				bs.addBinding(new BindingImpl("schemeUri",schemeUri));
				bs.addBinding(new BindingImpl("uri",uri));
				
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
				
				try {
					if (sparqlResults.hasNext()){
			
					   
						while (sparqlResults.hasNext()){
							BindingSet a=sparqlResults.next();
							if (a.getValue("broader")!=null){
								URI broaderUri=new URIImpl(a.getValue("broader").stringValue());
								Concept broader=new Concept();
								broader.setUri(broaderUri);
								c.getBroaders().add(broader);
								}
							}
							
							
						}
						
					
					
					} catch (QueryEvaluationException e) {
					
						e.printStackTrace();
					}
				
				sc.close();
				// Related !!
				sc=sail.getConnection();
				parser=new SPARQLParser();
				
				queryString =this.getQueryprepare()+ 
						//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
						  	"SELECT * WHERE { " +
						  	"GRAPH ?context {?uri  skos:related ?relation .} " +
						  	
						  	"}"; 
						  
				query = parser.parseQuery(queryString , null);
				bs=new SPARQLQueryBindingSet();
				bs.addBinding(new BindingImpl("uri",uri));
				
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
				
				try {
					if (sparqlResults.hasNext()){
						
					    
						while (sparqlResults.hasNext()){
							
							BindingSet a=sparqlResults.next();
							System.out.println(a);
							if (a.getValue("relation")!=null){
								URI relationUri=new URIImpl(a.getValue("relation").stringValue());
								Concept relation=new Concept();
								
								relation.setUri(relationUri);
								relation.setRelationGraph(new URIImpl(a.getValue("context").stringValue()));
								c.getRelated().add(relation);
								c.setChildrenPreviewNumber(c.getChildrenPreviewNumber()+1);
								}
							
							}
							
							
						}
						
					
					
					} catch (QueryEvaluationException e) {
					
						e.printStackTrace();
					}
				
				sc.close();
				
				if (c.getRelated().isEmpty()){
					/*relation sans graph si rien ...*/
					// Related !!
					sc=sail.getConnection();
					parser=new SPARQLParser();
					
					queryString =this.getQueryprepare()+ 
							//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
							  	"SELECT distinct ?relation WHERE { " +
							  	" ?uri  skos:related ?relation  " +
							  	
							  	"}"; 
							  
					query = parser.parseQuery(queryString , null);
					bs=new SPARQLQueryBindingSet();
					bs.addBinding(new BindingImpl("uri",uri));
					
					sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
					
					try {
						if (sparqlResults.hasNext()){
							
						    
							while (sparqlResults.hasNext()){
								
								BindingSet a=sparqlResults.next();
								System.out.println(a);
								if (a.getValue("relation")!=null){
									URI relationUri=new URIImpl(a.getValue("relation").stringValue());
									Concept relation=new Concept();
									
									relation.setUri(relationUri);
									relation.setRelationGraph(new URIImpl("context:noSpecificContext"));
									c.getRelated().add(relation);
									}
								
								}
								
								
							}
							
						
						
						} catch (QueryEvaluationException e) {
						
							e.printStackTrace();
						}
					
					sc.close();
				}
				
				
				
				
				
				Set<Concept> fullbroaders=Sets.newHashSet();
				Set<Concept> fullnarrowers=Sets.newHashSet();
				Set<Concept> fullrelated=Sets.newHashSet();
				
				for (Concept b:c.getBroaders()){
					fullbroaders.add(this.getConceptAsResource(b.getUri(),schemeUri));
				}
				for (Concept n:c.getNarrowers()){
					fullnarrowers.add(this.getConceptAsResource(n.getUri(),schemeUri));
				}
				
				for (Concept r:c.getRelated()){ // related to
					//http://skos.um.es/unescothes/C03862
					Concept full=this.getConceptAsResource(r.getUri(),schemeUri);
					if (full==null){
						full=r; // just uri ... mieux que rien :> qd des concepts sont pas bien declarés...
					}
					full.setRelationGraph(r.getRelationGraph());
					fullrelated.add(full);
					
				}
				c.setBroaders(fullbroaders);
				c.setNarrowers(fullnarrowers);
				c.setRelated(fullrelated);
				
				return c;
				
				}catch (Exception e) {
					e.printStackTrace();
					try {
						if (null!=sc && sc.isOpen()){
							sc.close();
						}
					} catch (SailException e1) {
						
						e1.printStackTrace();
					}
				}
				return null;
		
	}
	
	private URI getConceptUriFromNotation(Notation notation, URI schemeUri){
		Concept c=null;
		SailConnection sc=null;
		
		try {
				sc=sail.getConnection();
				SPARQLParser parser=new SPARQLParser();
				CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
				
				String queryString =this.getQueryprepare()+ 
						//	SPARQL.SELECT + " DISTINCT ?uri ?preflabel ?notation ?altlabel ?note "+
						  	"SELECT DISTINCT ?concept WHERE { " +
						  	"?concept skos:inScheme ?scheme . " +
						  	"?concept skos:notation ?notation ."+
						    "?scheme rdf:type skos:conceptScheme . " +
						  	"}"; 
						  
				ParsedQuery query = parser.parseQuery(queryString , null);
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				bs.addBinding(new BindingImpl("notation",notation.getModelValue()));
				bs.addBinding(new BindingImpl("scheme",schemeUri));
			    
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
				
				try {
					if (sparqlResults.hasNext()){
			
					    
						while (sparqlResults.hasNext()){
							BindingSet a=sparqlResults.next();
							if (a.getValue("concept")!=null){
								c=new Concept();
								c.setUri(new URIImpl(a.getValue("concept").stringValue()));
								break;
								}
							}
							
							
						}
						
					
					
					} catch (QueryEvaluationException e) {
					
						e.printStackTrace();
					}
				
				sc.close();
				
				
				
			
			}catch (Exception e) {
			e.printStackTrace();
			try {
				if (null!=sc && sc.isOpen()){
					sc.close();
				}
			} catch (SailException e1) {
				
				e1.printStackTrace();
			}
		}
		try {return c.getUri();}catch (Exception e ){}
		return null;
	}
	//implemented
	public Concept getConceptAsResource(Notation notation, URI schemeUri) {
			try {return this.getConceptAsResource(this.getConceptUriFromNotation(notation, schemeUri),schemeUri);}catch (Exception e ){}
			
			return null;
		}
		
	// implemented
	public Concept getConcept(Notation notation, URI schemeUri) {
		try {
			URI c=this.getConceptUriFromNotation(notation, schemeUri);
			if (c!=null){
			return this.getConcept(c, schemeUri);
			}
		}catch(Exception e ){}
		
		return null;
	}
	
	// TODO 
	public List<Pattern> searchConcept(String pattern, URI optionalScheme, int maxAnswer,
			int offset, boolean startwith,boolean prefLabel,boolean notation,boolean hiddenLabel,boolean altLabel) {
		
		 List<Pattern>  l=Lists.newArrayList();
		if (!prefLabel && !notation && !hiddenLabel && altLabel)return null; // nowhere to search !
		String filterType="CONTAINS";
		
		if (startwith)filterType="STRSTARTS";
		if (maxAnswer<=0)maxAnswer=10; // 10 si maxAnswer is crazy negative int.
		boolean withFrom=false;
		String GRAPH_START="";
		String GRAPH_END="";
		URI myContext=null;
		/*
		 * "SELECT * WHERE { " +
						  	"GRAPH ?context {?uri  skos:related ?relation .} " +
						  	
						  	"}"; 
						  
		 */
		if (optionalScheme!=null){
			withFrom=true;
			GRAPH_START= "GRAPH ?context {";
			GRAPH_END=" } ";
			myContext=optionalScheme;
		}
		
		SailConnection sc=null;
		
		try {
		    	SPARQLParser parser=new SPARQLParser();
				sc=sail.getConnection();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.getQueryprepare()+" SELECT ?uri ?pattern  "+
							
							" WHERE { "+
							GRAPH_START +
							"  ?uri a skos:Concept . "; 
		if (prefLabel)	 	queryString+= "  OPTIONAL { ?uri skos:prefLabel ?pattern } . ";
		if (notation)	   	queryString+= "  OPTIONAL { ?uri skos:notation ?pattern } . ";
		if (hiddenLabel)	queryString+= "  OPTIONAL { ?uri skos:hiddenLabel ?pattern } . ";
		if (altLabel)	   	queryString+= "  OPTIONAL { ?uri skos:altLabel ?pattern } . ";
			  queryString+= " FILTER "+filterType+" ( UCASE(str(?pattern)), UCASE( \""+pattern+"\" ) )  " ; //GOOD BUT BAD BINDING
			  // queryString+= " FILTER "+filterType+" ( UCASE( str(?pattern) ) , UCASE( str(?input) ) )  " ;
			   queryString+=  
					   //  " . FILTER ( EXISTS {?uri skos:inScheme ?schemeUri }) " +
								
					   " } "+GRAPH_END+" ORDER BY ?uri OFFSET "+offset+" LIMIT "+ (maxAnswer+1)+" ";
			    System.out.println(queryString);
			    ParsedQuery query = parser.parseQuery(queryString , null);
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				
				if (withFrom)bs.addBinding("context", myContext);
				
			    // bs.addBinding("input",vf.createLiteral(pattern));
			     
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
				
				try {
					if (sparqlResults.hasNext()){
						l=Lists.newArrayList();
						while (sparqlResults.hasNext()){
							BindingSet a=sparqlResults.next();
							System.out.println(a);
							Pattern p=new Pattern();p.setUri(new URIImpl(a.getValue("uri").stringValue()));
							Literal lit=(Literal)a.getValue("pattern");
							p.setValue(lit.getLabel());
							l.add(p);
						}
							
							
					}else {
						System.out.println("no data from research !");
					}
						
					
					
					} catch (QueryEvaluationException e) {
					
						e.printStackTrace();
					}
				
				sc.close();
				
				return l;
				
			
			}catch (Exception e) {
			e.printStackTrace();
			try {
				if (null!=sc && sc.isOpen()){
					sc.close();
				}
			} catch (SailException e1) {
				
				e1.printStackTrace();
			}
		}
			   return l;
	}
	
	// TODO 
		public List<Pattern> searchConcept2(String pattern, URI optionalScheme, int maxAnswer,
				int offset, boolean startwith,boolean prefLabel,boolean notation,boolean hiddenLabel,boolean altLabel) {
			
			 List<Pattern>  l=Lists.newArrayList();
			if (!prefLabel && !notation && !hiddenLabel && altLabel)return null; // nowhere to search !
			String filterType="CONTAINS";
			
			if (startwith)filterType="STRSTARTS";
			if (maxAnswer<=0)maxAnswer=10; // 10 si maxAnswer is crazy negative int.
			boolean withFrom=false;
			String GRAPH_START="";
			String GRAPH_END="";
			URI myContext=null;
			/*
			 * "SELECT * WHERE { " +
							  	"GRAPH ?context {?uri  skos:related ?relation .} " +
							  	
							  	"}"; 
							  
			 */
			if (optionalScheme!=null){
				withFrom=true;
				GRAPH_START= "GRAPH ?context {";
				GRAPH_END=" } ";
				myContext=optionalScheme;
			}
			
			SailConnection sc=null;
			
			try {
			    	SPARQLParser parser=new SPARQLParser();
					sc=sail.getConnection();
			CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
			String queryString =this.getQueryprepare()+" SELECT ?uri ?pattern  "+
								
								" WHERE { "+
								GRAPH_START ;
			//					
			if (prefLabel)	 	queryString+= "  OPTIONAL { ?uri skos:prefLabel ?pattern } . ";
			if (notation)	   	queryString+= "  OPTIONAL { ?uri skos:notation ?pattern } . ";
			if (hiddenLabel)	queryString+= "  OPTIONAL { ?uri skos:hiddenLabel ?pattern } . ";
			if (altLabel)	   	queryString+= "  OPTIONAL { ?uri skos:altLabel ?pattern } . ";
				  queryString+= " FILTER "+filterType+" ( UCASE(str(?pattern)), UCASE( \""+pattern+"\" ) )  " ; //GOOD BUT BAD BINDING
				  //queryString+= "  FILTER(lang(?pattern) = 'en') ";
				  queryString+= "  BIND (STRLEN(str(?pattern)) AS ?len) . ";
				  queryString+= "  ?uri a skos:Concept .  ";
				  // queryString+= " FILTER "+filterType+" ( UCASE( str(?pattern) ) , UCASE( str(?input) ) )  " ;
				   queryString+=  
						   //  " . FILTER ( EXISTS {?uri skos:inScheme ?schemeUri }) " +
									
						   " } "+GRAPH_END+" ORDER BY ?len ?pattern ?uri OFFSET "+offset+" LIMIT "+ (maxAnswer+1)+" ";
				    System.out.println(queryString);
				    ParsedQuery query = parser.parseQuery(queryString , null);
					SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
					
					if (withFrom)bs.addBinding("context", myContext);
					
				    // bs.addBinding("input",vf.createLiteral(pattern));
				     
					sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
					
					try {
						if (sparqlResults.hasNext()){
							l=Lists.newArrayList();
							while (sparqlResults.hasNext()){
								BindingSet a=sparqlResults.next();
								System.out.println(a);
								Pattern p=new Pattern();p.setUri(new URIImpl(a.getValue("uri").stringValue()));
								Literal lit=(Literal)a.getValue("pattern");
								p.setValue(lit.getLabel());
								l.add(p);
							}
								
								
						}else {
							System.out.println("no data from research !");
						}
							
						
						
						} catch (QueryEvaluationException e) {
						
							e.printStackTrace();
						}
					
					sc.close();
					
					return l;
					
				
				}catch (Exception e) {
				e.printStackTrace();
				try {
					if (null!=sc && sc.isOpen()){
						sc.close();
					}
				} catch (SailException e1) {
					
					e1.printStackTrace();
				}
			}
				   return l;
		}
	
	// TODO 
		public List<Pattern> testsearchConcept(String pattern) {
			
			 List<Pattern>  l=Lists.newArrayList();
		
			/*
			 * "SELECT * WHERE { " +
							  	"GRAPH ?context {?uri  skos:related ?relation .} " +
							  	
							  	"}"; 
							  
			 */
			
			
			SailConnection sc=null;
			
			try {
			    	SPARQLParser parser=new SPARQLParser();
					sc=sail.getConnection();
			CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
			String queryString =this.getQueryprepare()+" SELECT ?uri  ?pattern  "+
								
								" WHERE { ";
							
				  queryString+= " FILTER CONTAINS ( UCASE(str(?pattern)), UCASE( \""+pattern+"\" ) ) . " ; //GOOD BUT BAD BINDING
				 // queryString+=	"  ?uri a skos:Concept . "; 
			     // queryString+= "  ?uri skos:prefLabel ?pattern  . ";
				  queryString+= "  ?uri skos:prefLabel ?pattern  . ";
			    
				 // queryString+= " } ORDER BY ?uri OFFSET 0 LIMIT 10 ";
				  queryString+= " } ORDER BY ?uri OFFSET 0 LIMIT 10 ";
				    System.out.println(queryString);
				    ParsedQuery query = parser.parseQuery(queryString , null);
					SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
					
					sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
					
					try {
						if (sparqlResults.hasNext()){
							l=Lists.newArrayList();
							while (sparqlResults.hasNext()){
								BindingSet a=sparqlResults.next();
								System.out.println("after next");
								System.out.println(a);
								Pattern p=new Pattern();
								//p.setUri(new URIImpl(a.getValue("uri").stringValue()));
								Literal lit=(Literal)a.getValue("pattern");
								p.setValue(lit.getLabel());
								l.add(p);
								System.out.println("go next");
							}
							System.out.println("after boucle");	
								
						}else {
							System.out.println("no data from research !");
						}
							
						
						
						} catch (QueryEvaluationException e) {
						
							e.printStackTrace();
						}
					
					sc.close();
					
					return l;
					
				
				}catch (Exception e) {
				e.printStackTrace();
				try {
					if (null!=sc && sc.isOpen()){
						sc.close();
					}
				} catch (SailException e1) {
					
					e1.printStackTrace();
				}
			}
				   return l;
		}
	
	//implemented
	private List<Statement> prepareCollectionStatements(Collection resource,URI context){
		List<Statement> sts=Lists.newArrayList();
		 // statement declaration de conceptresource : 
		 // preparation du multiple statements.
		for (Notation not:resource.getNotations()){
			sts.add(new ContextStatementImpl(resource.getUri(),SKOS.NOTATION,not.getModelValue(),context));	 
		}
		for (PrefLabel lab:resource.getPrefLabels()){
			sts.add(new ContextStatementImpl(resource.getUri(),SKOS.PREF_LABEL,lab.getLit(),context));	 
		}
		for (AltLabel lab:resource.getAltLabels()){
			sts.add(new ContextStatementImpl(resource.getUri(),SKOS.ALT_LABEL,lab.getLit(),context));	 
		}
		for (HiddenLabel lab:resource.getHiddenLabels()){
			sts.add(new ContextStatementImpl(resource.getUri(),SKOS.HIDDEN_LABEL,lab.getLit(),context));	 
			
		}
		for (Note note:resource.getAnnotations()){
			
			if (note instanceof ChangeNote)sts.add(new ContextStatementImpl(resource.getUri(),SKOS.CHANGE_NOTE,note.getModelValue(),context));	 
			else if (note instanceof Definition)sts.add(new ContextStatementImpl(resource.getUri(),SKOS.DEFINITION,note.getModelValue(),context));	 
			else if (note instanceof EditorialNote)sts.add(new ContextStatementImpl(resource.getUri(),SKOS.EDITORIAL_NOTE,note.getModelValue(),context));	 
			else if (note instanceof Example)sts.add(new ContextStatementImpl(resource.getUri(),SKOS.EXAMPLE,note.getModelValue(),context));	 
			else if (note instanceof HistoryNote)sts.add(new ContextStatementImpl(resource.getUri(),SKOS.HISTORY_NOTE,note.getModelValue(),context));	 
			else if (note instanceof ScopeNote)sts.add(new ContextStatementImpl(resource.getUri(),SKOS.SCOPE_NOTE,note.getModelValue(),context));	 
			else if (note instanceof Note)sts.add(new ContextStatementImpl(resource.getUri(),SKOS.NOTE,note.getModelValue(),context));	 
		}
		return sts;
	}
	//implemented
	private List<Statement> prepareResourceStatements(Resource resource,URI context){
		List<Statement> sts=Lists.newArrayList();
		 // statement declaration de conceptresource : 
		 // preparation du multiple statements.
		
		Map<String,String> shouldBeOnlyOne=Maps.newHashMap();
		for (Notation not:resource.getNotations()){
			
			if (!shouldBeOnlyOne.containsKey(not.getDatatype().getLocalName())){
				shouldBeOnlyOne.put(not.getDatatype().getLocalName(), not.getDatatype().toString());
				sts.add(new ContextStatementImpl(resource.getUri(),SKOS.NOTATION,not.getModelValue(),context));	 
			}
		}
		shouldBeOnlyOne=Maps.newHashMap();
		for (PrefLabel lab:resource.getPrefLabels()){
			if (!shouldBeOnlyOne.containsKey(lab.getLanguage().toString())){
				shouldBeOnlyOne.put(lab.getLanguage().toString(), lab.getValue());
				sts.add(new ContextStatementImpl(resource.getUri(),SKOS.PREF_LABEL,lab.getLit(),context));
			}
		}
		Set<String> setLabels=Sets.newHashSet();
		for (AltLabel lab:resource.getAltLabels()){
			if (!setLabels.contains(lab.getValue()+":"+lab.getLanguage().name())){
				sts.add(new ContextStatementImpl(resource.getUri(),SKOS.ALT_LABEL,lab.getLit(),context));
				setLabels.add(lab.getValue()+":"+lab.getLanguage());
			}
		}
		setLabels=Sets.newHashSet();
		for (HiddenLabel lab:resource.getHiddenLabels()){
			if (!setLabels.contains(lab.getValue()+":"+lab.getLanguage().name())){
				sts.add(new ContextStatementImpl(resource.getUri(),SKOS.HIDDEN_LABEL,lab.getLit(),context));	
				setLabels.add(lab.getValue()+":"+lab.getLanguage());
			}
		}
		for (Note note:resource.getAnnotations()){
			
			if (note instanceof ChangeNote)sts.add(new ContextStatementImpl(resource.getUri(),SKOS.CHANGE_NOTE,note.getModelValue(),context));	 
			else if (note instanceof Definition)sts.add(new ContextStatementImpl(resource.getUri(),SKOS.DEFINITION,note.getModelValue(),context));	 
			else if (note instanceof EditorialNote)sts.add(new ContextStatementImpl(resource.getUri(),SKOS.EDITORIAL_NOTE,note.getModelValue(),context));	 
			else if (note instanceof Example)sts.add(new ContextStatementImpl(resource.getUri(),SKOS.EXAMPLE,note.getModelValue(),context));	 
			else if (note instanceof HistoryNote)sts.add(new ContextStatementImpl(resource.getUri(),SKOS.HISTORY_NOTE,note.getModelValue(),context));	 
			else if (note instanceof ScopeNote)sts.add(new ContextStatementImpl(resource.getUri(),SKOS.SCOPE_NOTE,note.getModelValue(),context));	 
			else if (note instanceof Note)sts.add(new ContextStatementImpl(resource.getUri(),SKOS.NOTE,note.getModelValue(),context));	 
		}
		return sts;
	}
	//implemented
	public URI createConceptScheme(ConceptScheme scheme) {
		// step 1. resource should not exists with the uri.
		try {
			URI rep=this.uriExist(scheme.getUri());
			if (rep!=null)return null;
			if (scheme.getNotations().size()==0)return null;
			
			List<Statement> sts=Lists.newArrayList();
			sts.add(new ContextStatementImpl(scheme.getUri(),RDF.TYPE,SKOS.CONCEPT_SCHEME,scheme.getUri()));
			sts.addAll(this.prepareResourceStatements(scheme,scheme.getUri()));	
			
			if (this.addMultipleStatements(sts))return scheme.getUri();
		} catch (SailException e) {
			
			e.printStackTrace();
		}
		
		return null;
	}
	
	// we use the first inscheme scheme to declare context( graph)  of properties inside the concept object ! 
	//implemented
	
	private Set<Statement> prepareConcept(Concept concept){
		// Un concept est ok si il a une URI et une notation (qui peut etre localname de uri) et un scheme.
		try {
			if (concept.getUri()==null)return null;
			
			URI rep=this.uriExist(concept.getUri());
			// concept should have at least a notation and should be declared in one scheme.
			if (rep!=null)return null;
			
			if (concept.getNotations().size()==0){
				// on utilise son localName pour créer une notation ! on verifie tt de meme.
				try {
					String localName=concept.getUri().getLocalName();
					Concept ca=this.getConcept(new Notation(localName), null);
					if (ca !=null)return null;
					else concept.getNotations().add(new Notation(localName));
				}catch (Exception e ) {
					return null;
				}
			}
			if (concept.getInScheme().size()==0)return null;
			
			Set<Statement> sts=Sets.newHashSet();
			ConceptScheme conceptSchemeReference=null;
			for (ConceptScheme cs:concept.getInScheme()){
				if (conceptSchemeReference==null)conceptSchemeReference=cs;
				sts.add(new ContextStatementImpl(concept.getUri(),SKOS.IN_SCHEME,cs.getUri(),cs.getUri()));
			}
			sts.add(new ContextStatementImpl(concept.getUri(),RDF.TYPE,SKOS.CONCEPT,conceptSchemeReference.getUri()));
			
			sts.addAll(this.prepareResourceStatements(concept,conceptSchemeReference.getUri()));
			
			for (ConceptScheme cs:concept.getTopConceptOf()){
				sts.add(new ContextStatementImpl(concept.getUri(),SKOS.TOP_CONCEPT_OF,cs.getUri(),cs.getUri()));
				sts.add(new ContextStatementImpl(cs.getUri(),SKOS.HAS_TOP_CONCEPT,concept.getUri(),cs.getUri()));
			}
			
			// hierarchical relations are created with context declared : FIRST INSCHEME !! (règle non SKOS !!!!)
			for (Concept broader:concept.getBroaders()){
				if (concept.getUri()==null || broader.getUri()==null || conceptSchemeReference.getUri()==null){
					System.err.println(concept.getUri()+ " "+broader.getUri()+ "  "+conceptSchemeReference.getUri());
					throw new Exception ("PB !!!");
				}
				sts.add(new ContextStatementImpl(concept.getUri(),SKOS.BROADER,broader.getUri(),conceptSchemeReference.getUri()));
			}
			for (Concept narrower:concept.getNarrowers()){
				sts.add(new ContextStatementImpl(narrower.getUri(),SKOS.BROADER,concept.getUri(),conceptSchemeReference.getUri()));
			}
			// graph de relation : 1. declared graph uri / 2. first scheme of related object / 3. first  scheme of related subject.
			for (Concept related:concept.getRelated()){
				URI graph=related.getRelationGraph();
				if (graph==null){
					try {graph=related.getInScheme().get(0).getUri();}catch (Exception e){}
					if (graph==null)graph=conceptSchemeReference.getUri();
				}

				sts.add(new ContextStatementImpl(concept.getUri(),SKOS.RELATED,related.getUri(),graph));
			}
			
			for (Concept broadMatch:concept.getBroadMatch()){
				sts.add(new ContextStatementImpl(concept.getUri(),SKOS.BROAD_MATCH,broadMatch.getUri(),conceptSchemeReference.getUri()));
			}
			for (Concept closeMatch:concept.getCloseMatch()){
				sts.add(new ContextStatementImpl(concept.getUri(),SKOS.CLOSE_MATCH,closeMatch.getUri(),conceptSchemeReference.getUri()));
			}
			for (Concept exactMatch:concept.getExactMatch()){
				sts.add(new ContextStatementImpl(concept.getUri(),SKOS.EXACT_MATCH,exactMatch.getUri(),conceptSchemeReference.getUri()));
			}
			for (Concept narrowMatch:concept.getNarrowMatch()){
				sts.add(new ContextStatementImpl(concept.getUri(),SKOS.NARROW_MATCH,narrowMatch.getUri(),conceptSchemeReference.getUri()));
			}
			return sts;
			
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		return null;
	}
	public static Set<Concept> setstatic;
	
	public boolean createASetOfConcept( int commitFlag){
		if (setstatic==null || setstatic.isEmpty())return false;
		
		Set<Statement> sts=Sets.newHashSet();
		for (Concept c:setstatic){
			Set<Statement> set2=this.prepareConcept(c);
			if (null!=set2 && !set2.isEmpty())sts.addAll(set2);
			else {
				//System.err.println(c.toString());
			}
		}
		try {
			//setstatic=null;
			//System.gc();
			//System.out.println("Nb statements to  write="+sts.size());
			if(this.addMultipleStatements(sts,commitFlag))return true;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return false;
	}
	
	public URI createConcept(Concept concept) {
		// step 1. resource should not exists with the uri.
		try {
			if (this.uriExist(concept.getUri()) != null)return null;
		} catch (SailException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return null;
		}
		try {
			if(this.addMultipleStatements(this.prepareConcept(concept)))return concept.getUri();
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
			
	}
	
	public URI addMemberToCollection(URI c, URI uri){
		List<Statement> sts=Lists.newArrayList();
		sts.add(new ContextStatementImpl(c,SKOS.MEMBER,uri,c));
		try {
			this.addMultipleStatements(sts);
			return  c;
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	// the member must be inside graph , else the members are external resource (uri)
	//implemented
	public URI createCollection(CollectionEntity collection,boolean replace){
		List<Statement> sts=Lists.newArrayList();
	
		URI collectionUri=null;
		if (collection instanceof OrderedCollection){
			OrderedCollection oc=(OrderedCollection)collection;
			collectionUri=oc.getUri();
			try {
				if (this.uriExist(collectionUri)!=null){
					if (replace){
						Set<Statement> toremove=Sets.newHashSet();
								Statement init=this.getStatement(collectionUri, SKOS.MEMBER_LIST, null).iterator().next();
								toremove.add(init);
								try {toremove.add( this.getStatement(collectionUri,RDF.TYPE,SKOS.ORDERED_COLLECTION).iterator().next());}
								catch (Exception e){};
								
								try { // if nothing in the collection ..will catch exception.
								boolean stop=false;
								BNode subject=(BNode)init.getObject();
								while (!stop){
									Statement first=this.getStatement(subject,RDF.FIRST, null).iterator().next();
									toremove.add(first);
									Statement rest=this.getStatement(subject,RDF.REST, null).iterator().next();
									toremove.add(rest);
									if ( ((URI)rest.getObject()).equals(RDF.NIL)){
										stop=true;
									}else subject=(BNode)rest.getObject();
								}
								}catch (Exception e ){}
								
								System.out.println(toremove.size()+"removeok="+this.removeMultipleStatements(toremove));
					}else {
						System.out.println("collection deja existante ! no replace");
						return null;
					}
				}
			} catch (SailException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//URI firstElement=new URIImpl(oc.getUri().toString()+"%Element0");
			BNode elementFirst=vf.createBNode();
			sts.add(new ContextStatementImpl(oc.getUri(),RDF.TYPE,SKOS.ORDERED_COLLECTION,oc.getUri()));
			
			sts.add(new ContextStatementImpl(oc.getUri(),SKOS.MEMBER_LIST,elementFirst,oc.getUri()));
			int i=0;
			int size=oc.getMemberLists().size();
			for (CollectionEntity entity: oc.getMemberLists()){
				URI uri=null;
				if (entity instanceof Concept){
					uri=((Concept) entity).getUri();
				}else if (entity instanceof Collection) {
					uri=((Collection) entity).getUri();
				}
				BNode elementNext=vf.createBNode();
				sts.add(new ContextStatementImpl(elementFirst,RDF.FIRST,uri,oc.getUri()));
				if (i+1==size) sts.add(new ContextStatementImpl(elementFirst,RDF.REST,RDF.NIL,oc.getUri()));
				else           sts.add(new ContextStatementImpl(elementFirst,RDF.REST,elementNext,oc.getUri()));
				
				// for next round, the first become the next.
				elementFirst=elementNext;
				i++;
			}
			
		}
		else {
			Collection c=(Collection)collection;
			collectionUri=c.getUri();
			sts.add(new ContextStatementImpl(c.getUri(),RDF.TYPE,SKOS.COLLECTION,c.getUri()));
			for (CollectionEntity entity: c.getMembers()){
				URI uri=null;
				if (entity instanceof Concept){
					uri=((Concept) entity).getUri();
				}else if (entity instanceof Collection) {
					uri=((Collection) entity).getUri();
				}
				sts.add(new ContextStatementImpl(c.getUri(),SKOS.MEMBER,uri,c.getUri()));
			}
			
		}
		Collection col=(Collection)collection;
		sts.addAll(this.prepareCollectionStatements(col,col.getUri()));
		try {
			//for (Statement st : sts){System.out.println(st.toString());
			//}
			
			this.addMultipleStatements(sts);
			return collectionUri;
		} catch (SailException e) {
			
			e.printStackTrace();
		}
		
		return null;
	}
	
	
	// update is doing : full replacement of a/many skos:resource
	public URI updateConcept(Concept concept, boolean replace,URI... skosResources) {
		int numberOfResources = skosResources.length;
		for (int i=0;i<numberOfResources;i++){
			
			Set<Statement> toremove=null;
			if (replace)toremove=this.getStatement(concept.getUri(), skosResources[i], null);
			Set<Statement> updates=Sets.newHashSet();
			// labels to replaces.
			if(skosResources[i].equals(SKOS.BROADER)){
				for (Concept father:concept.getBroaders()){updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],father.getUri(),new URIImpl(concept.getUri().getNamespace())));}
			}
			else if(skosResources[i].equals(SKOS.NARROWER)){
				for (Concept son:concept.getNarrowers()){updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],son.getUri(),new URIImpl(concept.getUri().getNamespace())));}
			}
			else if(skosResources[i].equals(SKOS.PREF_LABEL)){
				for (PrefLabel pref:concept.getPrefLabels()){updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],pref.getLit(),new URIImpl(concept.getUri().getNamespace())));}
			}
			else if(skosResources[i].equals(SKOS.ALT_LABEL)){
				for (AltLabel alt:concept.getAltLabels()){updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],alt.getLit(),new URIImpl(concept.getUri().getNamespace())));}
			}
			else if(skosResources[i].equals(SKOS.HIDDEN_LABEL)){
				for (HiddenLabel hid:concept.getHiddenLabels()){updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],hid.getLit(),new URIImpl(concept.getUri().getNamespace())));}
			}
			else if (skosResources[i].equals(SKOS.NOTATION)){
				for (Notation not:concept.getNotations()){updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],not.getModelValue(),new URIImpl(concept.getUri().getNamespace())));}
			}
			else if (skosResources[i].equals(SKOS.CLOSE_MATCH)){
				for (Concept c:concept.getCloseMatch()){updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],c.getUri(),new URIImpl(concept.getUri().getNamespace())));}
			}
			else if (skosResources[i].equals(SKOS.EXACT_MATCH)){
				for (Concept c:concept.getExactMatch()){
					updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],c.getUri(),new URIImpl(concept.getUri().getNamespace())));
					
				}
			}
			else if (skosResources[i].equals(SKOS.BROAD_MATCH)){
				for (Concept c:concept.getBroadMatch()){updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],c.getUri(),new URIImpl(concept.getUri().getNamespace())));}
			}
			else if (skosResources[i].equals(SKOS.NARROW_MATCH)){
				for (Concept c:concept.getNarrowMatch()){updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],c.getUri(),new URIImpl(concept.getUri().getNamespace())));}
			}
			else if (skosResources[i].equals(SKOS.RELATED_MATCH)){
				for (Concept c:concept.getRelatedMatch()){updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],c.getUri(),new URIImpl(concept.getUri().getNamespace())));}
			}
			
			else if (skosResources[i].equals(SKOS.RELATED)){
				for (Concept related:concept.getRelated()){
					URI graph=related.getRelationGraph();
					if (graph==null){
						try {graph=related.getInScheme().get(0).getUri();}catch (Exception e){}
						if (graph==null)graph=null;
					}

					updates.add(new ContextStatementImpl(concept.getUri(),
							                             skosResources[i],
							                             related.getUri(),graph));
				}
				
			}
			
			else if (skosResources[i].equals(SKOS.CHANGE_NOTE)||
					 skosResources[i].equals(SKOS.DEFINITION)||
					 skosResources[i].equals(SKOS.EDITORIAL_NOTE)||
					 skosResources[i].equals(SKOS.EXAMPLE)||
					 skosResources[i].equals(SKOS.HISTORY_NOTE)||
					 skosResources[i].equals(SKOS.SCOPE_NOTE)||
					 skosResources[i].equals(SKOS.NOTE)
				    ){
				for (Note note:concept.getAnnotations()){
					if (
						(note instanceof ChangeNote && skosResources[i].equals(SKOS.CHANGE_NOTE))||
						(note instanceof Definition && skosResources[i].equals(SKOS.DEFINITION))||
						(note instanceof EditorialNote && skosResources[i].equals(SKOS.EDITORIAL_NOTE))||
						(note instanceof Example && skosResources[i].equals(SKOS.EXAMPLE))||
						(note instanceof HistoryNote && skosResources[i].equals(SKOS.HISTORY_NOTE))||
						(note instanceof ScopeNote && skosResources[i].equals(SKOS.SCOPE_NOTE))||
						(note instanceof Note  && skosResources[i].equals(SKOS.NOTE))
					  ){
						updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],note.getModelValue(),new URIImpl(concept.getUri().getNamespace())));
					}	
					
				}
			
			}
			
			try {
					System.out.println("remove="+this.removeMultipleStatements(toremove));
					System.out.println("add="+this.addMultipleStatements(updates));
				} catch (SailException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return null;
				}
		
			
		
		}// end boucle for.
		return concept.getUri();
	}
	
	
	// specific update of S P O (C) with  O with same Language / Datatype /
	// dont use for broader/narrower or mapping !!!!!! be careful !!
	// replace specific :
	// prefLabel => remplace le prefLabel Meme langue.
	// altLabel/hidden label : on ajoute sauf si le label et la langue ==same.
		public URI updateReplaceConceptPropertiesSpecific(Concept concept,URI... skosResources) {
			int numberOfResources = skosResources.length;
			for (int i=0;i<numberOfResources;i++){
				
				Set<Statement> generals=this.getStatement(concept.getUri(), skosResources[i], null);
				Set<Statement> specifics =Sets.newHashSet();
				
				Set<Statement> updates=Sets.newHashSet();
				// labels to replaces.
				if(skosResources[i].equals(SKOS.PREF_LABEL)){
					for (PrefLabel pref:concept.getPrefLabels()){
						for (Statement general : generals ){
							Label tmp=new Label((Literal)general.getObject());
							if (tmp.getLanguage().equals(pref.getLanguage())){
								specifics.add(general); // should be remove ! not the others.
							}
						}
						updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],pref.getLit(),null));}
				}
				else if(skosResources[i].equals(SKOS.ALT_LABEL)){
					boolean toAdd=true;
					for (AltLabel alt:concept.getAltLabels()){
						for (Statement general : generals ){
							Label tmp=new Label((Literal)general.getObject());
							if (tmp.getLanguage().equals(alt.getLanguage())){
								if (tmp.getValue().equals(alt.getValue())){
								   toAdd=false;	// exists soon.
								}// should be remove ! not the others.
							}
						}
						if (toAdd)updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],alt.getLit(),null));}
				}
				else if(skosResources[i].equals(SKOS.HIDDEN_LABEL)){
					   boolean toAdd=true;
					   for (HiddenLabel hid:concept.getHiddenLabels()){
						for (Statement general : generals ){
							Label tmp=new Label((Literal)general.getObject());
							if (tmp.getLanguage().equals(hid.getLanguage())){
								if (tmp.getValue().equals(hid.getValue())){
									   toAdd=false;	// exists soon.
								}// should be remove ! not the others.
							}
						}
						if(toAdd)updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],hid.getLit(),null));}
				}
				else if (skosResources[i].equals(SKOS.NOTATION)){
					   for (Notation not:concept.getNotations()){
						   for (Statement general : generals ){
								Literal tmp= (Literal)general.getObject();
								if (tmp.getDatatype().equals(not.getDatatype())){
									specifics.add(general); // should be remove ! not the others.
								}
							}
						   updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],not.getModelValue(),null));}
				}
				else if (skosResources[i].equals(SKOS.CHANGE_NOTE)||
						 skosResources[i].equals(SKOS.DEFINITION)||
						 skosResources[i].equals(SKOS.EDITORIAL_NOTE)||
						 skosResources[i].equals(SKOS.EXAMPLE)||
						 skosResources[i].equals(SKOS.HISTORY_NOTE)||
						 skosResources[i].equals(SKOS.SCOPE_NOTE)||
						 skosResources[i].equals(SKOS.NOTE)
					    ){
							for (Note note:concept.getAnnotations()){
								if (
									(note instanceof ChangeNote && skosResources[i].equals(SKOS.CHANGE_NOTE))||
									(note instanceof Definition && skosResources[i].equals(SKOS.DEFINITION))||
									(note instanceof EditorialNote && skosResources[i].equals(SKOS.EDITORIAL_NOTE))||
									(note instanceof Example && skosResources[i].equals(SKOS.EXAMPLE))||
									(note instanceof HistoryNote && skosResources[i].equals(SKOS.HISTORY_NOTE))||
									(note instanceof ScopeNote && skosResources[i].equals(SKOS.SCOPE_NOTE))||
									(note instanceof Note  && skosResources[i].equals(SKOS.NOTE))
								  ){
									for (Statement general : generals ){
											if (note.getUri()!=null){ // si notre note de concept renvoie vers  une uri / on remplace rien !!
												
											}else {
											Literal tmp= (Literal)general.getObject();
											if (tmp.getDatatype().equals(note.getDatatype())){
												specifics.add(general); // should be remove ! not the others.
											}
										}
									}
									updates.add(new ContextStatementImpl(concept.getUri(),skosResources[i],note.getModelValue(),null));
								}	
								
							}
				
				}
				
				try {
						System.out.println("remove="+this.removeMultipleStatements(specifics));
						System.out.println("add="+this.addMultipleStatements(updates));
					} catch (SailException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						return null;
					}
			
				
			
			}// end boucle for.
			return concept.getUri();
		}

	public static void mainXXX(String []args){
		SkosGraphImpl g=null;
		try {
		//	String context="http://server.ubiqore.com/ontologies/onto1/v1/";
			g=new  SkosGraphImpl("/Reborn/software/apache-tomcat-7.0.47/skos.server.graphs/ccam",false);
			//g.importRDF("/tmp/ccam.skos.rdf");
			
			/*
			 *  pagination des top concepts example :
			 
			Set<ConceptScheme> set=g.getConceptSchemes();
			for (ConceptScheme cs:set){
				System.out.println(cs.toString());
				int max=10;
				int offset=0;
				int res=max+1;
				while (res>max){
					ConceptScheme cstmp=g.getConceptScheme(cs.getUri(),max,offset);
					res=cstmp.getHasTopConcepts().size();
					
					System.out.println(offset+res +" :nbtopconcept :"+cstmp.toString());
					if (res>max)System.out.println("pagination en cours");
					offset=offset+10;
					break;
				}
			}
			*/
			
			
		//	System.out.println(g.getConceptAsResource(new URIImpl("http://server.ubiqore.com/ontologies/onto1/v1/test#ab876674")).toString());
			System.out.println("start of search");
			List<Pattern> res=g.searchConcept("coeur", null, 20, 0, false, true, false, false, false);
			for (Pattern pat:res){
				System.out.println("previousnarrowers::>"+g.getConceptAsResource(pat.getUri(),null).getChildrenPreviewNumber());
				
			}
			System.out.println("--------------");
			g.searchConcept("coeur", null, 10, 20, false, true, false, false, false);
			System.out.println("--------------");
			res=g.searchConcept("ENREGISTRER", new URIImpl("http://server.ubiqore.com/v1/CCAM/AT_ACT_ARBO"), 10, 0, false, true, false, false, false);
			for (Pattern pat:res){
				System.out.println("previousnarrowers::>"+g.getConceptAsResource(pat.getUri(),null).getChildrenPreviewNumber());
				
			}
			System.out.println("--------------");
			g.searchConcept("RACCOURCISSEMENT", null, 10, 0, false, true, false, false, false);
			System.out.println("--------------");
			System.out.println("end of search");
		}catch (Exception e ){
			e.printStackTrace();
		}finally {if (g!=null)g.shutdown();}
		
	}
	
	public static void main(String []args ){
		 SkosGraphImpl g=null;
		
		//	String context="http://server.ubiqore.com/ontologies/onto1/v1/";
			try {
				g=new  SkosGraphImpl("/Users/roky/Reborn/tmp/termapp",false);
				
				for (ConceptScheme cs:g.getConceptSchemes()){
					cs=g.getConceptScheme(cs.getUri(), 20, 0);
					System.out.println(cs.getUri());
					Set<Statement> set=g.getStatement(new URIImpl("termapp:2014#off"), (URI)null,(Value)null);
					OutputStream output = new FileOutputStream("/Users/roky/Reborn/tmp/termapp.json");
					Rio.write(set, output,RDFFormat.JSONLD);
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				g.shutdown();
			}
		
			
	}
	public static void mainTT(String []args ){
		 SkosGraphImpl g=null;
		
		//	String context="http://server.ubiqore.com/ontologies/onto1/v1/";
			try {
				g=new  SkosGraphImpl("/Users/roky/Reborn/data/tmp/ccam444",false);
				//g.importRDF("/Users/roky/Reborn/data/tmp/ccam.skos.rdf");
				
				//g=new  SkosGraphImpl("/Users/roky/Reborn/data/tmp/_ccam.skos_1390817160502BU",false);
				
				Set<ConceptScheme> setcs=g.getConceptSchemes();
				for (ConceptScheme cs:setcs){
					cs=g.getConceptScheme(cs.getUri(), 20, 0);
					System.out.println(cs.toString());
				}
				
				//g.importRDF("/Users/roky/Reborn/data/tmp/ccam.skos.rdf");
				List<Pattern> l=g.searchConcept("suture", null, 10, 0, false, true, false, false, false);
				for (Pattern p:l)System.out.println(p.getValue()+ " "+p.getUri());
			
				//g.exportRDF("/Users/roky/Reborn/data/tmp/ccam23423.trig");
			} catch (SailException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				g.shutdown();
			}
		
			
	}
	public static void mainWWW (String[]args){
		SkosGraphImpl g=null;
		try {
		//	String context="http://server.ubiqore.com/ontologies/onto1/v1/";
			g=new  SkosGraphImpl("/Users/roky/Reborn/data/tmp/experimental",true);
			URI isPartOf=new URIImpl("relation:isPartOf");
			isPartOf=g.createCustomDatatype(isPartOf, SKOS.NOTE, new URIImpl(XSD.ANY_URI));
			if (isPartOf==null){
				System.out.println("pb de datatype...non");
			}else {
				ConceptScheme cs=new ConceptScheme();
				cs.setUri(new URIImpl("resource:context:sc"));
				cs.getPrefLabels().add(new PrefLabel("cs"));
				g.createConceptScheme(cs);
				Concept c1=new Concept ();c1.setUri(new URIImpl("resource:context:sc#c1"));
				c1.getInScheme().add(cs);
				c1.getTopConceptOf().add(cs);
				c1.getNotations().add(new Notation("stringnotation"));
				Notation notation=new Notation("notationNotation",new URIImpl(XSD.NOTATION));
				c1.getNotations().add(notation);
				
				g.createConcept(c1);
				
				c1 =g.getConcept(c1.getUri(),null);
				c1.getNotations().clear();
				Notation notationUpdated=new Notation("notationNotationUpdated",new URIImpl(XSD.NOTATION));
				c1.getNotations().add(notationUpdated);
				g.updateConcept(c1, true, SKOS.NOTATION);
				
				Concept c2=new Concept ();c2.setUri(new URIImpl("resource:context:sc#c2"));
				c2.getInScheme().add(cs);
				c2.getTopConceptOf().add(cs);
				g.createConcept(c2);
				
				Note n=new Note(c2.getUri(), isPartOf);
				
				c2.setRelationGraph(new URIImpl("isa:friend"));
				c1.getRelated().add(c2); // with context
				g.updateConcept(c1, true, SKOS.RELATED);
				c1.getAnnotations().add(n);
				g.updateConcept(c1, false, SKOS.NOTE);
				Concept ret=g.getConceptAsResource(c1.getUri(), cs.getUri());
				g.exportRDF("/Users/roky/tmp/test/exp.ttl");
				g.exportRDF("/Users/roky/tmp/test/exp.trig");
				g.exportRDF("/Users/roky/tmp/test/exp.xml");
				g.exportRDF("/Users/roky/tmp/test/exp.n3");
				g.exportRDF("/Users/roky/tmp/test/exp.n4");
				
				g.exportRDF("/Users/roky/tmp/test/exp.xml.rdf");
				
				Set<Statement> full=g.getFullStatements();
				for (Statement st:full){System.out.println("statement:"+st.toString());}
			}
		
		}catch (Exception e ){
			e.printStackTrace();
			
		}finally {
			g.shutdown();
		}
	}
	// 1390041668162
	public static void mainX(String []args){
		SkosGraphImpl g=null;
		try {
		//	String context="http://server.ubiqore.com/ontologies/onto1/v1/";
			g=new  SkosGraphImpl("/Users/roky/Reborn/data/ubiqskos/tmp/atc",false);
			
			g.exportRDF("/Users/roky/tmp/test/atc.trix.xml");
			g.exportRDF("/Users/roky/tmp/test/atc.n3.n3");
			g.exportRDF("/Users/roky/tmp/test/atc.n4.n4");
			g.exportRDF("/Users/roky/tmp/test/atc.ttl.ttl");
			g.exportRDF("/Users/roky/tmp/test/atc.rdf.xmlrdf");
			
		/*	
			//Set<Statement> full=g.getFullStatements();
			//for (Statement st:full){System.out.println("statement:"+st.toString());}
			URI uri=new URIImpl("http://ubiq.skos/main/scheme/Mappings#J-AAFA007");
			Concept c=g.getConceptWithMapping(uri, null);
			for (Concept c1:c.getBroadMatch()){
				c1=g.getConcept(c1.getUri(),null);
				System.out.println(c1.toString());
				System.out.println("Buri"+c1.getUri());
				System.out.println("Bscheme"+c1.getInScheme().iterator().next().getUri().toString());
				
				for (Note n:c1.getAnnotations()){
					if (n instanceof HistoryNote ){
						HistoryNote h=(HistoryNote)n;
						if (h.getDatatype().equals(new URIImpl("http://ubiq.skos/main/datatype#origine"))){
							System.out.println(h.toString());
						}
					}
				}
			}
			for (Concept c1:c.getNarrowMatch()){
				c1=g.getConcept(c1.getUri(),null);
				System.out.println(c1.getAnnotations().toString());
				System.out.println("Nuri"+c1.getUri());
				System.out.println("Nscheme"+c1.getInScheme().iterator().next().getUri().toString());
				
				for (Note n:c1.getAnnotations()){
					if (n instanceof HistoryNote ){
						HistoryNote h=(HistoryNote)n;
						if (h.getDatatype().equals(new URIImpl("http://ubiq.skos/main/datatype#origine"))){
							System.out.println(h.toString());
						}
					}
				}
			}*/
			/*Set<Statement> s=g.getStatement(new URIImpl("http://ubiq.skos/main/scheme/Mappings#AAFA001-A"),null, (Value)null);
		
			for (Statement st:s){
				
				System.out.println("avant" +st.toString());
			}
			Concept c=g.getConcept(new URIImpl("http://ubiq.skos/main/scheme/Mappings#AAFA001-A"), null);
			Concept e=new Concept();
			e.setUri(new URIImpl("titi:876"));
			c.getExactMatch().add(e);
			g.updateConcept(c,false,SKOS.EXACT_MATCH);
			
			s=g.getStatement(new URIImpl("http://ubiq.skos/main/scheme/Mappings#AAFA001-A"),null, (Value)null);
			
			for (Statement st:s){
				
				System.out.println("apres" +st.toString());
			}
			
			System.out.println("NB exact=>"+g.getConceptWithMapping(c.getUri(), null).getExactMatch().size());
			*/
			
			/*
			 *  pagination des top concepts example :
			 
			Set<ConceptScheme> set=g.getConceptSchemes();
			for (ConceptScheme cs:set){
				System.out.println(cs.toString());
				int max=10;
				int offset=0;
				int res=max+1;
				while (res>max){
					ConceptScheme cstmp=g.getConceptScheme(cs.getUri(),max,offset);
					res=cstmp.getHasTopConcepts().size();
					
					System.out.println(offset+res +" :nbtopconcept :"+cstmp.toString());
					if (res>max)System.out.println("pagination en cours");
					offset=offset+10;
					break;
				}
			}
			*/
			
			
		//	System.out.println(g.getConceptAsResource(new URIImpl("http://server.ubiqore.com/ontologies/onto1/v1/test#ab876674")).toString());
	
		}catch (Exception e ){
			e.printStackTrace();
		}
		finally {if (g!=null)g.shutdown();}
		
	}
	
	public static void mainScheme(String []args){
		SkosGraphImpl g=null;
		try {
		//	String context="http://server.ubiqore.com/ontologies/onto1/v1/";
			g=new  SkosGraphImpl("/Users/roky/Reborn/data/ubiqskos/1389519861579",false);
			Set<Statement> full=g.getFullStatements();
			for (Statement st:full){System.out.println("statement:"+st.toString());}
			
			Set<Statement> s=g.getStatement((org.openrdf.model.Resource)null,SKOS.IN_SCHEME, (Value)null);
			int i=0;
			for (Statement st:s){
				if (i>15)break;
				i++;
				System.out.println(st.toString());
			}
			Set<ConceptScheme> css=g.getConceptSchemes();
			for (ConceptScheme cs:css){
				cs=g.getConceptScheme(cs.getUri(), 10, 0);
				for (Concept c:cs.getHasTopConcepts()){
					System.out.println(c.toString());
				}
			}
			//g.importRDF("/tmp/ccam.skos.rdf");
			
			/*
			 *  pagination des top concepts example :
			 
			Set<ConceptScheme> set=g.getConceptSchemes();
			for (ConceptScheme cs:set){
				System.out.println(cs.toString());
				int max=10;
				int offset=0;
				int res=max+1;
				while (res>max){
					ConceptScheme cstmp=g.getConceptScheme(cs.getUri(),max,offset);
					res=cstmp.getHasTopConcepts().size();
					
					System.out.println(offset+res +" :nbtopconcept :"+cstmp.toString());
					if (res>max)System.out.println("pagination en cours");
					offset=offset+10;
					break;
				}
			}
			*/
			
			
		//	System.out.println(g.getConceptAsResource(new URIImpl("http://server.ubiqore.com/ontologies/onto1/v1/test#ab876674")).toString());
	
		}catch (Exception e ){
			e.printStackTrace();
		}
		finally {if (g!=null)g.shutdown();}
		
	}
	
	
	public static void mainBAD(String []args){
		SkosGraphImpl g=null;
		try {
		//	String context="http://server.ubiqore.com/ontologies/onto1/v1/";
			g=new  SkosGraphImpl("/tmp/skosCCAM",true);
			g.importRDF("/tmp/ccam.skos.rdf");
			System.out.println(g.getRootCollections().toString());
			System.out.println(g.getConceptSchemes().toString());
		}catch (Exception e ){
			
		}finally {if (g!=null)g.shutdown();}
		
	}
	
	public static void mainZZZZ(String []args){
		SkosGraphImpl g=null;
		try {
		//	String context="http://server.ubiqore.com/ontologies/onto1/v1/";
			g=new  SkosGraphImpl("/tmp/skosG",false);
			//g.exportNquads("/tmp/ccam.skos.n4");
				Set<ConceptScheme> set=g.getConceptSchemes();
			for (ConceptScheme cs:set){
				System.out.println(cs.toString());
				System.out.println("	"+g.getConceptScheme(cs.getUri(),10,0).getHasTopConcepts().toString());
			}
		//	System.out.println(g.getConceptAsResource(new URIImpl("http://server.ubiqore.com/ontologies/onto1/v1/test#ab876674")).toString());
			System.out.println("start of search");
			g.searchConcept("coeur", null, 20, 0, false, true, false, false, false);
			System.out.println("--------------");
			g.searchConcept("sang", null, 10, 0, false, true, false, false, false);
			System.out.println("--------------");
			g.searchConcept("sang", new URIImpl("http://server.ubiqore.com/ontologies/onto1/v1/scheme1"), 10, 0, false, true, false, false, false);
			System.out.println("--------------");
		
		}catch (Exception e ){
			e.printStackTrace();
		}finally {if (g!=null)g.shutdown();}
		
	}
	
	// update 
	// position -1 : end of list
	public URI updateCollection(URI collection,List<URI> resources,int position){
		Collection c=this.getCollectionWithElementsAsResources(collection);
		List<Statement> sts=Lists.newArrayList();
		if (c instanceof OrderedCollection){
			for (CollectionEntity e: ((OrderedCollection) c).getMemberLists()){
				URI uri=null;
				if (e instanceof Collection)uri=((Collection) e).getUri();
				if (e instanceof Concept)uri=((Concept) e).getUri();
				if (uri!=null){
					Set<URI> tmp=Sets.newHashSet();
					for (URI u:resources){
						if (u.equals(uri))tmp.add(u);
					}
					resources.remove(tmp); // on enleve ds la liste a ajouter les resources deja dans la liste  du graph.
				}
			}
			List<CollectionEntity> listToAdd=Lists.newArrayList();
			for (URI uri:resources){
				Concept concept=new Concept();
				concept.setUri(uri);  // ici on utilise un Concept comme porteur d'uri ! (astuce car il peut s'agir d'une uri de collection !! héhé)
				listToAdd.add(concept); 
			}
			 int sizeOfList=((OrderedCollection) c).getMemberLists().size();
			 if (position>sizeOfList || position <0){
				 position=sizeOfList;
			 }
			 ((OrderedCollection) c).getMemberLists().addAll(position, listToAdd);
			 
			 this.createCollection(c,true);
			 return collection;
		}
		else {
			for (URI uri:resources)sts.add(new ContextStatementImpl(c.getUri(),SKOS.MEMBER,uri,c.getUri()));
		}
		
		try {
			if (!sts.isEmpty())this.addMultipleStatements(sts);
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
		return collection;
	}
	
	
	
	public static void mainTTTTTT(String []args){
		SkosGraphImpl g=null;
		try {
			String context="http://server.ubiqore.com/ontologies/onto1/v1/";
			g=new  SkosGraphImpl("/tmp/skosG",true);
			
			ConceptScheme cs=new ConceptScheme();
			cs.setUri(new URIImpl(context+"scheme1"));
			cs.getNotations().add(new Notation("1"));
			cs.getNotations().add(new Notation("scheme1"));
			cs.getPrefLabels().add(new PrefLabel("pref1",LanguageCode.fr));
			cs.getPrefLabels().add(new PrefLabel("pref2",LanguageCode.en));
			cs.getAltLabels().add(new AltLabel("alt1",LanguageCode.mk));
			cs.getAltLabels().add(new AltLabel("alt2",LanguageCode.mk));
			cs.getAnnotations().add(new Note("note",LanguageCode.ja));
			cs.getAnnotations().add(new ChangeNote("changenote",LanguageCode.fr));
			cs.getAnnotations().add(new Definition("definition",LanguageCode.fr));
			cs.getAnnotations().add(new EditorialNote("editorial",LanguageCode.fr));
			cs.getAnnotations().add(new Example("example",LanguageCode.fr));
			cs.getAnnotations().add(new HistoryNote("hisotry",LanguageCode.fr));
			cs.getAnnotations().add(new ScopeNote("scopenote",LanguageCode.fr));
			cs.getHiddenLabels().add(new HiddenLabel("hidden",LanguageCode.fr));
			g.createConceptScheme(cs);
			
			
			ConceptScheme cs2=new ConceptScheme();
			cs2.setUri(new URIImpl(context+"scheme2"));
			cs2.getNotations().add(new Notation("2"));
			cs2.getNotations().add(new Notation("scheme2"));
			cs2.getPrefLabels().add(new PrefLabel("scheme2pref1",LanguageCode.fr));
			cs2.getPrefLabels().add(new PrefLabel("scheme2pref1",LanguageCode.en));
			cs2.getAltLabels().add(new AltLabel("scheme number 2 in chinese",LanguageCode.mk));
			cs2.getAnnotations().add(new ScopeNote(new URIImpl("http://wikipedia.external/uri")));
			g.createConceptScheme(cs2);
			
			
			
			
			Concept conceptSang=null;
			for (int i=0;i<6;i++){
				Concept c=new Concept();
				c.setUri(new URIImpl(context+"test#gg345345"+i));
				c.getInScheme().add(cs2);
				c.getTopConceptOf().add(cs2);
				c.getPrefLabels().add(new PrefLabel("sang"+i,LanguageCode.fr));
				c.getNotations().add(new Notation("gg345345"+i));
				c.setRelationGraph(new URIImpl(context+"relation#countains"));
				g.createConcept(c);
				conceptSang=c;
			}
			
			for (int i=0;i<6;i++){
				Concept c=new Concept();
				c.setUri(new URIImpl(context+"test#ab87667"+i));
				c.getInScheme().add(cs);
				c.getTopConceptOf().add(cs);
				c.getPrefLabels().add(new PrefLabel("coeur"+i,LanguageCode.fr));
				c.getNotations().add(new Notation("ab87667"+i));
				c.getRelated().add(conceptSang);
				g.createConcept(c);
			}
			
			Concept c=new Concept();
			c.setUri(new URIImpl(context+"test#ab8766777777"));
			c.getInScheme().add(cs);
			c.getTopConceptOf().add(cs);
			c.getPrefLabels().add(new PrefLabel("sang77777777",LanguageCode.fr));
			c.getNotations().add(new Notation("sang7777777"));
			c.getRelated().add(conceptSang);
			g.createConcept(c);
			
			Concept readConcept=null;
			
			readConcept = g.getConcept(new URIImpl(context+"test#ab876671"),cs.getUri());
			System.out.println(readConcept);
			
			/*OrderedCollection oc=new OrderedCollection();
			oc.setUri(new URIImpl(context+"collectionordonnee"));
			oc.getPrefLabels().add(new PrefLabel("collection de coeur list",Locale.FRENCH));
			
			Collection col=new Collection();
			col.setUri(new URIImpl(context+"collectionnonordonnee"));
			col.getPrefLabels().add(new PrefLabel("collection de coeur set",Locale.FRENCH));
			
			
			for (int i=5; i>=0;i--){
				Concept c=new Concept();
				c.setUri(new URIImpl(context+"test#ab87667"+i));
				oc.getMemberLists().add(c);
				col.getMembers().add(c);
			}
			g.createCollection(col);
			g.createCollection(oc);
			OrderedCollection toto=(OrderedCollection) g.getCollectionWithElementsAsResources(new URIImpl(context+"collectionordonnee"));
			System.out.println("ordonnée ??"+toto.toString());
			for (CollectionEntity ce:toto.getMemberLists()){
				if (ce instanceof Concept){
					System.out.println(((Concept)ce).toString());
				}
			}
			
			Collection titi=(Collection) g.getCollectionWithElementsAsResources(new URIImpl(context+"collectionnonordonnee"));
			System.out.println("non ordoné"+titi.toString());
			for (CollectionEntity ce:titi.getMembers()){
				if (ce instanceof Concept){
					System.out.println(((Concept)ce).toString());
				}
			}
			
			OrderedCollection rootCollection=new OrderedCollection();
			rootCollection.setUri(new URIImpl(context+"livre"));
			rootCollection.getPrefLabels().add(new PrefLabel("la supercollection",Locale.FRENCH));
			rootCollection.getMemberLists().add(toto);
			rootCollection.getMemberLists().add(titi);
			g.createCollection(rootCollection);
			
			Set<Collection> setRoot=g.getRootCollections();
			for (Collection c:setRoot){
				System.out.println("ROOOT ??=="+c.toString());
			}*/
			
			
			
			/*System.err.println("concept="+readConcept.toString());
			
			Set<ConceptScheme> set=g.getConceptSchemes();
			for (ConceptScheme conceptScheme:set){
				System.err.println("conceptScheme : "+conceptScheme.toString());
			}*/
			
		
			/*Set<Statement> full=g.getFullStatements();
			for (Statement st:full){System.out.println("statement:"+st.toString());}*/
			
			
		} catch (SailException e) {
			
			e.printStackTrace();
		} finally {
			if (g!=null)g.shutdown();
		}
	}
	
	@Override
	protected void finalize() throws Throwable {
		super.finalize();
	}


/*
 * (non-Javadoc)
 * @see limics.skos.services.api.BasicGraph#createVocabularyStatementsList()
 */
	
	@Override
	protected void populateRulesStatements() {
		//vocabularyStatements.add(new ContextStatementImpl(SKOS.NARROWER.getURI(),OWL.INVERSEOF,SKOS.BROADER.getURI(),context));
		//rulesStatements.add(new ContextStatementImpl(SKOS.BROADER,OWL.INVERSEOF,SKOS.NARROWER,context));
	   //rulesStatements.add(new ContextStatementImpl(SKOS.ORDERED_COLLECTION,RDFS.SUBCLASSOF,SKOS.COLLECTION,context));
		//vocabularyStatements.add(new ContextStatementImpl(SKOS.BROADER.getURI(),OWL.EQUIVALENTPROPERTY,RDFS.SUBCLASSOF,context));
		//vocabularyStatements.add(new ContextStatementImpl(SKOS.NARROWER.getURI(),OWL.INVERSEOF,RDFS.SUBCLASSOF,context));
	
	}



	@Override
	protected void createNamespacesMap() {
		
		// official namespaces..
		this.useClassicNamespaces();
	}



	public Set<Statement> getStatement(org.openrdf.model.Resource subject,
			URI predicate, Value object) {
		try {
			sc=sail.getConnection();
		
		Set<Statement> st=Sets.newHashSet();
		
			
			CloseableIteration<? extends Statement, SailException> statements = sc.getStatements(subject, predicate, object, false);
			
			while (statements.hasNext())st.add(statements.next());
			sc.close();
			return st;
		
		} catch (SailException e1) {
			
			e1.printStackTrace();
			if (null!=sc) {
				
					try {
						if (sc.isOpen())sc.close();
					} catch (SailException e) {
						
						e.printStackTrace();
					}
			}
		}
		return null;
	}

	public URI createCustomEnumdatatype(XSDRelatedEnumCustomDatatype datatype){
		try { 
			  SimpleCustomXSDRelatedDataTypeUtil util=new SimpleCustomXSDRelatedDataTypeUtil();
			  List<Statement> l=Lists.newArrayList();
				try {if (datatype.getUri().getLocalName()==null||datatype.getUri().getLocalName().isEmpty())return null;}catch(Exception e){return null;}
				try {if (datatype.getUri().getNamespace()==null||datatype.getUri().getNamespace().isEmpty())return null;}catch(Exception e){return null;}
				if (!util.validateSkosElement(datatype.getDomain()))return null;
				if (!util.validateXSDElement(datatype.getXsdPrimitive()))return null;
				
				URI context=new URIImpl(datatype.getUri().getNamespace());
				l.add(new ContextStatementImpl(datatype.getUri(), RDF.TYPE,OWL.DATATYPEPROPERTY, context));
				l.add(new ContextStatementImpl(datatype.getUri(), RDFS.DOMAIN,SKOS.DEFINITION, context));
				
				BNode link=null;
				int size=datatype.getListOfElements().size();
				int i=0;
				for (String o:datatype.getListOfElements()){
					if (link==null){
						BNode bA=vf.createBNode();
						l.add(new ContextStatementImpl(datatype.getUri(), RDFS.RANGE,bA, context));
						BNode b2=vf.createBNode();
						l.add(new ContextStatementImpl(bA, new URIImpl(org.semarglproject.vocab.OWL.DATA_RANGE),b2, context));
						BNode b3=vf.createBNode();
						l.add(new ContextStatementImpl(b2, new URIImpl(org.semarglproject.vocab.OWL.ONE_OF),b3, context));
						BNode b4=vf.createBNode();
						l.add(new ContextStatementImpl(b3, RDF.LIST,b4, context));
						l.add(new ContextStatementImpl(b4, RDF.FIRST,vf.createLiteral(o, datatype.getXsdPrimitive()), context));
						link=b4;
					}
					else if (i+1<size) {
						BNode b5=vf.createBNode();
						l.add(new ContextStatementImpl(link, RDF.REST,b5, context));
						BNode b6=vf.createBNode();
						l.add(new ContextStatementImpl(b5, RDF.LIST,b6, context));
						l.add(new ContextStatementImpl(b6, RDF.FIRST,vf.createLiteral(o, datatype.getXsdPrimitive()), context));
						link=b6;
					}else { // last
						l.add(new ContextStatementImpl(link, RDF.REST,RDF.NIL, context));
					}
					
					i++;
				}
				boolean ok=this.addMultipleStatements(l);
				if (ok)return datatype.getUri();
		}
		catch(Exception e ){}
		return null;
	}
	
	
	public URI createCustomDatatype(URI newdatatypeUri, URI skosElement,URI primitiveXSD) {
		  try {
			  SimpleCustomXSDRelatedDataTypeUtil util=new SimpleCustomXSDRelatedDataTypeUtil();
			  boolean ok=this.addMultipleStatements(util.generateStatementForCustomDatatypeForSkos(newdatatypeUri, skosElement, primitiveXSD)); 
			 if (ok) return newdatatypeUri;
			 else return null;
		  }catch (Exception e){}
		return null;
	}
   /*
    * PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
	SELECT DISTINCT (lang(?label) AS ?lang)
	WHERE {
  		?concept a skos:Concept .
  		?concept skos:prefLabel|skos:altLabel|skos:hiddenLabel ?label .
	}
    */
	public Set<LanguageCode> getLanguageList(){
		Set<LanguageCode> set=null;
		try {
		    	SPARQLParser parser=new SPARQLParser();
				sc=sail.getConnection();
				CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		        String queryString =this.getQueryprepare()+" SELECT DISTINCT (lang(?label) AS ?lang)  "+
							" WHERE { "+
							"?x skos:prefLabel|skos:altLabel|skos:hiddenLabel ?label ."+
							" }  ";
		
			    ParsedQuery query = parser.parseQuery(queryString ,null);
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
				
				try {
					if (sparqlResults.hasNext()){
						set=Sets.newHashSet();
						while (sparqlResults.hasNext()){
							BindingSet a=sparqlResults.next();
							String lang=a.getValue("lang").stringValue();
							if (lang.length()>2){
								//en-GB.
								lang=LocaleCode.getByCode(lang).getLanguage().toString();
							}
							LanguageCode isolang=LanguageCode.getByCode(lang, false);
							set.add(isolang);
						}
							
							
					}else {
						
					}
						
					
					
					} catch (QueryEvaluationException e) {
					
						e.printStackTrace();
					}
				
				sc.close();
				
				return set;
				
			
			}catch (Exception e) {
			e.printStackTrace();
			try {
				if (null!=sc && sc.isOpen()){
					sc.close();
				}
			} catch (SailException e1) {
				
				e1.printStackTrace();
			}
		}
			   return null;
	}
	
	public Set<XSDRelatedCustomDatatype> getMyCustomDatatypes() {
		Set<XSDRelatedCustomDatatype> set=null;
		try {
		    	SPARQLParser parser=new SPARQLParser();
				sc=sail.getConnection();
				CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		        String queryString =this.getQueryprepare()+" SELECT ?uri ?domain ?xsdPrimitive  "+
							
							" WHERE { "+
							
							"  ?uri a owl:DatatypeProperty . " +
							"  ?uri rdfs:domain ?domain . " +
							"  ?uri rdfs:range ?xsdPrimitive . " +
							" }  ";
		
			    ParsedQuery query = parser.parseQuery(queryString ,null);
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
				
				try {
					if (sparqlResults.hasNext()){
						set=Sets.newHashSet();
						while (sparqlResults.hasNext()){
							BindingSet a=sparqlResults.next();
							URI uri=new URIImpl(a.getValue("uri").stringValue());
							URI domain=new URIImpl(a.getValue("domain").stringValue());
							URI xsdPrimitive=new URIImpl(a.getValue("xsdPrimitive").stringValue());
							XSDRelatedCustomDatatype datatype=new XSDRelatedCustomDatatype(uri,domain,xsdPrimitive);
							set.add(datatype);
						}
							
							
					}else {
						
					}
						
					
					
					} catch (QueryEvaluationException e) {
					
						e.printStackTrace();
					}
				
				sc.close();
				
				return set;
				
			
			}catch (Exception e) {
			e.printStackTrace();
			try {
				if (null!=sc && sc.isOpen()){
					sc.close();
				}
			} catch (SailException e1) {
				
				e1.printStackTrace();
			}
		}
			   return null;
	}
	
	public Set<XSDRelatedEnumCustomDatatype> getMyCustomEnumDatatypes() {
		Set<XSDRelatedEnumCustomDatatype> set=null;
		try {
		    	SPARQLParser parser=new SPARQLParser();
				sc=sail.getConnection();
				CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		        String queryString =this.getQueryprepare()+" SELECT ?uri ?domain ?element  "+
							
							" WHERE { "+
							
							"  ?uri a owl:DatatypeProperty . " +
							"  ?uri rdfs:domain ?domain . " +
							"  ?uri rdfs:range _:x . " +
							"  _:x owl:DataRange  _:y . " +
							"  _:y owl:oneOf  _:z . " +
							"  _:z rdf:List  _:a . " +
							"  -:a rdf:rest|rdf:first  ?element }  ";
		// _:y  rdf:rest|rdf:first  ?uri
			    ParsedQuery query = parser.parseQuery(queryString , null);
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
				
				try {
					if (sparqlResults.hasNext()){
						set=Sets.newHashSet();
						boolean first=true;
						List<String> l=Lists.newArrayList();
						XSDRelatedEnumCustomDatatype datatype=null;
						URI uriChanged=null;
						while (sparqlResults.hasNext()){
							BindingSet a=sparqlResults.next();
							URI uri=new URIImpl(a.getValue("uri").stringValue());
							URI domain=new URIImpl(a.getValue("domain").stringValue());
							Literal element=null;
							URI nil=null;
							URI xsdPrimitive=null;
							
							if (a.getValue("element") instanceof Literal){
								element=(Literal)a.getValue("element");
								xsdPrimitive=element.getDatatype();
							}else if (a.getValue("element") instanceof URI ){
								nil=(URI)a.getValue("element");
							}
							
							
							if (uriChanged==null){
								datatype=new XSDRelatedEnumCustomDatatype(uri,domain,xsdPrimitive,l);
								uriChanged=uri;
								set.add(datatype);
							}
							
							if (nil!=null && nil.equals(RDF.NIL)){
								element=null;
								uriChanged=null;
							}
						
							
							if (element!=null){
								datatype.getListOfElements().add(element.getLabel());
							}
							first=false;
						}
						
							
					}else {
						
					}
						
					
					
					} catch (QueryEvaluationException e) {
					
						e.printStackTrace();
					}
				
				sc.close();
				
				return set;
				
			
			}catch (Exception e) {
			e.printStackTrace();
			try {
				if (null!=sc && sc.isOpen()){
					sc.close();
				}
			} catch (SailException e1) {
				
				e1.printStackTrace();
			}
		}
			   return null;
	}
}
