package limics.skos.services.api;

import java.util.List;
import java.util.Set;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.sail.SailException;
import org.openrdf.model.Resource;

import ubiqore.skos.impl.*;
import ubiqore.skos.impl.datatypes.XSDRelatedCustomDatatype;
 
public interface SkosGraph {
 // define the services that a skos graph should provide.
/*
 * Access the root elements : 
 *  - ConceptScheme are always Root Elements. 
 *  - CollectionEntity (Collection + orderedCollection) could be Root Elements (not always)
 */
	// No input services.
	public Set<ConceptScheme> getConceptSchemes();
	public Set<Collection> getRootCollections();
	
	// index key as input / return only the "Resource" Properties 
	public ConceptScheme getConceptSchemeAsAResource(URI uri);
	public ConceptScheme getConceptSchemeAsAResource(Notation notation);
	
	
	// index key as input // return a conceptScheme (included topConceptsAsResource)
	public ConceptScheme getConceptScheme(URI uri,int maxAnswer,int offset );
	public ConceptScheme getConceptScheme(Notation notation);
	// return Collection and direct members As Resources.
	public Collection getCollection(URI uri);
	public OrderedCollection getOrderedCollection(URI uri);

	public Set<Statement> getStatement(Resource subject,URI predicate,Value object);
	/*
	 * Access the topElements 
	 *  
	 *  - CollectionEntity (Collection + orderedCollection) could be Root Elements (not always)
	 */
	/*
	 *  Return concepts Set including direct Relation objects as Resources.
	 *  we use a list for possibility of lazy loading for case of lot topelements.
	 */ 
	public List<Concept> getTopElementOf(URI conceptSchemeUri);
	
	public URI createCustomDatatype(URI newdatatypeUri,URI skosElement,URI primitiveXSD);
	public Set<XSDRelatedCustomDatatype> getMyCustomDatatypes();
	/*
	 * Access to a Resource
	 *  
	 */
	// return a a concept object with Resource Properties.
	public Concept getConceptAsResource(URI uri,URI scheme);
	// a notation key is necessary. schemeUri should be not necesary for a monoscheme skosGraph
	public Concept getConceptAsResource(Notation notation,URI schemeUri);
	// return a collection (or orderedCollection) including Elements (List or/and Set) as Resources.
	// example : collection object output members are also collections objects-> these objects will not include their members with this service.
	public Collection getCollectionWithElementsAsResources(URI uri);
	
	/* 
	 * Access to a Concept 
	 * return full relations to objects as Resources in a specific scheme / except relation to a collection (?)
	 */
	public Concept getConcept(URI  uri,URI schemeUri); 
	public Concept getConcept(Notation notation,URI schemeUri);
	
	/*
	 * Search a concept (return Pattern object List / (pattern + uri )
	 */
	public List<Pattern> searchConcept(String pattern, URI optionalScheme, int maxAnswer,
			int offset, boolean startwith, boolean preflabel,boolean notation,boolean hiddenLabel,boolean altLabel);
	
	/*
	 * Creation.
	 * 
	 */
	public URI createConceptScheme(ConceptScheme scheme);
	public URI createConcept(Concept concept);
	public URI updateConcept(Concept concept,boolean replace, URI...resources);
	public URI createCollection(CollectionEntity collection,boolean replace);
	public URI addMemberToCollection(URI c, URI uri);
	// position is used only if the collection is instanceof OrederedCollection (of course)
	
	public URI updateCollection(URI collection,List<URI> resources,int position);
}

