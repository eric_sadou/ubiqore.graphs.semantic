package limics.skos.services.api;

import org.openrdf.model.URI;

public class Pattern {
	
	public Pattern() {
		

	}
	URI uri=null;
	String value=null;
	/**
	 * @return the uri
	 */
	public URI getUri() {
		return uri;
	}
	/**
	 * @param uri the uri to set
	 */
	public void setUri(URI uri) {
		this.uri = uri;
	}
	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}
	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	} 

}
