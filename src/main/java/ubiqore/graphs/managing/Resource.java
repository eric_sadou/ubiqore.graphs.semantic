package ubiqore.graphs.managing;

import org.joda.time.DateTime;

public class Resource {
	public Resource(){};
	
	public Resource(String oid, String label, String definition, String version,
			DateTime externalCreationDate) {
		super();
		this.oid = oid;
		this.label = label;
		this.definition = definition;
		this.version = version;
		this.externalCreationDate = externalCreationDate;
	}
	/*
	 * Resource Data Necessary Before the creation
	 */
	String oid,label,definition,version; // some officials inf. (for official
	DateTime externalCreationDate;
	
	
	/*
	 * Resource Data After Creation/Import ( generated during creation process) 
	 */
	String path;
	DateTime internalCreationDate;
	String creationDurationInMinutes;
	int ConceptsCount=0;
	int topConceptsCount=0;
	public String getOid() {
		return oid;
	}
	
	public String getDefinition() {
		return definition;
	}
	public String getVersion() {
		return version;
	}
	public DateTime getExternalCreationDate() {
		return externalCreationDate;
	}
	public DateTime getInternalCreationDate() {
		return internalCreationDate;
	}
	public int getConceptsCount() {
		return ConceptsCount;
	}
	public int getTopConceptsCount() {
		return topConceptsCount;
	}
	public void setOid(String oid) {
		this.oid = oid;
	}
	
	public void setDefinition(String definition) {
		this.definition = definition;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public void setExternalCreationDate(DateTime externalCreationDate) {
		this.externalCreationDate = externalCreationDate;
	}
	public void setInternalCreationDate(DateTime internalCreationDate) {
		this.internalCreationDate = internalCreationDate;
	}
	public void setConceptsCount(int conceptsCount) {
		ConceptsCount = conceptsCount;
	}
	public void setTopConceptsCount(int topConceptsCount) {
		this.topConceptsCount = topConceptsCount;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getCreationDurationInMinutes() {
		return creationDurationInMinutes;
	}
	public void setCreationDurationInMinutes(String creationDurationInMinutes) {
		this.creationDurationInMinutes = creationDurationInMinutes;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}
	
	
}
