package ubiqore.graphs.managing;

import java.util.Set;

import com.google.common.collect.Sets;

public class Admin {

	Configuration current;
	public Configuration getCurrent() {
		return current;
	}
	public void setCurrent(Configuration current) {
		this.current = current;
	}
	public String getMainDir() {
		return mainDir;
	}
	public void setMainDir(String mainDir) {
		this.mainDir = mainDir;
	}
	
	String mainDir;
	Set<Configuration> fullconfig=Sets.newHashSet();
	public Set<Configuration> getFullconfig() {
		return fullconfig;
	}
	public void setFullconfig(Set<Configuration> fullconfig) {
		this.fullconfig = fullconfig;
	}
	@Override
	public String toString() {
		return "Admin [current=" + current.toString() + ", mainDir=" + mainDir
				+ ", others=" + fullconfig.size() + "]";
	}
	
	
}
