package ubiqore.graphs.managing;

import java.util.Set;

import com.google.common.collect.Sets;

public class ResourcesAdmin {

	
	
	public String getMainDir() {
		return mainDir;
	}
	public void setMainDir(String mainDir) {
		this.mainDir = mainDir;
	}
	
	String mainDir;
	
	Set<Resource> resources=Sets.newHashSet();

	public Set<Resource> getResources() {
		return resources;
	}
	public void setResources(Set<Resource> resources) {
		this.resources = resources;
	}
	
	
}
