package ubiqore.graphs.managing;

import java.io.File;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.index.impl.lucene.LowerCaseKeywordAnalyzer;
import org.neo4j.kernel.EmbeddedGraphDatabase;

import ubiqore.graphs.semantic.AbstractGraph;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Index;
import com.tinkerpop.blueprints.Parameter;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
/*
 * available to manage Dir Path and current Configuration : 
 * 
 * Admin OUT--manage--IN conf1
 * Admin OUT--manage--IN conf2
 * Admin OUT--current--IN conf2
 * 
 * Rules : 
 * remove conf1 available
 * remove conf2 not possible As conf2 is current.
 * changeCurrent ok if conf exists.
 * 
 * Services : 
 * constructor with path.
 * createConfiguration --> path, label , date required. (path..label uniques)
 * declareCurrentConf (path required)
 * getAdmin() (full information)
 * getCurrent() (most usefull)
 * 
 */



public class MappingManager {
	Neo4jGraph graph;
	//protected GraphDatabaseService gds = null;
	public MappingManager(String path,boolean newone){
		if (newone){
		    File dir=new File (path);
			if (dir.exists()){
				System.out.println("The graph dir will be delete and all files inside.. sorry baby!! it's too late now..");
				recursiveDelete(dir);
				System.out.println("Graph will be install on the fresh place now..");
			}
		}
		// check if dir exists.
		// if newone : destroy and create.
		//gds = new EmbeddedGraphDatabase(path);
		if (!newone){
			graph=new Neo4jGraph(path,null);
			this.shutdown();
			
		}
		 graph=new Neo4jGraph(path,null);
		
		if (newone)this.init();
		
	}
	
	private static void recursiveDelete(File file) {
        if (!file.exists()) return;
        if (file.isDirectory()) {
           for (File f : file.listFiles()) recursiveDelete(f);
           file.delete();
        } else {
           file.delete();
        }
} 
	/*
	 * Model init.
	 */
	protected void init(){ // create admin vertex + property : creationDate.
		//Index<Vertex> index = graph.createIndex("index", Vertex.class, new Parameter("analyser",LowerCaseKeywordAnalyzer.class.getName()));
		//Index<Vertex> vertexIndex = graph.createIndex("vertices", Vertex.class);
		Transaction tx = graph.getRawGraph().beginTx();
		try {
			graph.createKeyIndex("name", Vertex.class);
			graph.createKeyIndex("mainDir", Vertex.class);
			graph.createKeyIndex("path", Vertex.class);
			graph.createKeyIndex("label", Vertex.class);
			
	        //Index<Edge> edgeIndex = graph.createIndex("edges", Edge.class);
			
			Vertex admin=graph.addVertex(null);
			admin.setProperty("creationDate", DateTime.now().toString());
			admin.setProperty("name", "admin");
			//vertexIndex.put("name", "admin", admin);
			
			admin.setProperty("mainDir", "/");
			tx.success();
		}catch (Exception e ){
			tx.failure();
		}finally {
			tx.finish();
		}
		
		
		
		
	}
	
	
	// unique path and label !!
	public boolean createConfiguration(Configuration conf){
		if (this.getConfByPath(conf.getPath())!=null)return false;
		if (this.getConfByLabel(conf.getLabel())!=null)return false;
		Transaction tx = graph.getRawGraph().beginTx();
		try {
		Vertex confV=graph.addVertex(null);
		confV.setProperty("path", conf.getPath());
		confV.setProperty("label", conf.getLabel());
		//graph.getIndex("vertices", Vertex.class).put("path", conf.getPath(), confV);
		//graph.getIndex("vertices", Vertex.class).put("label", conf.getLabel(), confV);
		confV.setProperty("creationDate",conf.getCreationDate().toString());
		this.graph.addEdge(null, this.getAdmin(), confV, "manage");
		tx.success();
		}catch (Exception e ){
			tx.failure();
			return false;
		}finally {
			tx.finish();
		}
		return true;
	}
	protected Vertex getConfByPath(String path){
		try {return graph.getVertices("path", path).iterator().next();}
		catch (Exception e ){
			
		}
		return null;
	}
	
	public Admin getFullAdmin(){
		Admin a=new Admin();
		Vertex v=this.getAdmin();
		a.setMainDir(v.getProperty("mainDir").toString());
		Configuration conf=this.getCurrent();
		a.setCurrent(conf);
		Iterator<Edge> ite=v.getEdges(Direction.OUT, "manage").iterator();
		while (ite.hasNext()){
			Configuration tmp=this.getConfiguration(ite.next().getVertex(Direction.IN).getProperty("path").toString());
			a.getFullconfig().add(tmp);
		}
		return a;
	}
	
	protected Vertex getConfByLabel(String label){
		try {return graph.getIndex("vertices", Vertex.class).get("label", label).iterator().next();}
		catch (Exception e ){
			
		}
		return null;
	}
	
	public void showAllConfig(){
		System.out.println("showAllConfig");
		for (Edge e:this.getAdmin().getEdges(Direction.OUT, "manage")){
			Vertex v=e.getVertex(Direction.IN);
			System.out.println("manage -->"+v.getProperty("path"));
		}
		for (Edge e:this.getAdmin().getEdges(Direction.OUT, "current")){
			Vertex v=e.getVertex(Direction.IN);
			System.out.println("current -->"+v.getProperty("path"));
		}
		System.out.println("showAllConfig End");
	}
	public Configuration getCurrent(){
		for (Edge e:this.getAdmin().getEdges(Direction.OUT, "current")){
			Vertex v=e.getVertex(Direction.IN);
			return this.getConfiguration(v.getProperty("path").toString());
		}
		return null;
	}
	
	public Configuration getConfiguration(String path){
		Configuration conf=new Configuration();
		conf.setPath(path);
		Vertex v=this.getConfByPath(path);
		if (null==v)return null;
		else {
			conf.setCreationDate(new DateTime(v.getProperty("creationDate").toString()));
			conf.setLabel(v.getProperty("label").toString());
			if (v.getEdges(Direction.IN, "current").iterator().hasNext())conf.setCurrent(true);
		}
		return conf;
	}
	
	public void removeConf(Configuration conf){
		Configuration config=this.getConfiguration(conf.getPath());
		if (config.isCurrent){
			System.out.println("current can not be removed");
			return;
		}
		Transaction tx = graph.getRawGraph().beginTx();
		try {
			
			this.graph.removeVertex(this.getConfByPath(conf.getPath()));
		tx.success();
		System.out.println(conf.getPath()+ " was removed");
		}catch (Exception ex ){
			ex.printStackTrace(System.err);
			tx.failure();
		}finally {
			tx.finish();
		}
		
	}
	public boolean declareCurrentConf(Configuration conf){
		Vertex confV=this.getConfByPath(conf.getPath());
		if (confV==null)return false;
		
		if (confV.getEdges(Direction.IN, "current").iterator().hasNext()){
			System.out.println(confV.getEdges(Direction.IN, "current").iterator().next().getVertex(Direction.OUT).getProperty("name"));
			System.out.println("declare current -->already current");
			return false;
		}
		// remove other current link.
		try {
			for (Edge e:this.getAdmin().getEdges(Direction.OUT, "current")){
				
				System.out.println("this current will be removed="+e.getVertex(Direction.IN).getProperty("path"));
				Transaction tx = graph.getRawGraph().beginTx();
				try {
					
					graph.removeEdge(e);
				tx.success();
				}catch (Exception ex ){
					tx.failure();
					return false;
				}finally {
					tx.finish();
				}
			}
			
		}catch (Exception e){
			System.out.println("no current !!");
			}
		Transaction tx = graph.getRawGraph().beginTx();
		try {
		this.graph.addEdge(null, this.getAdmin(), confV, "current");
		tx.success();
		}catch (Exception e ){
			tx.failure();
			return false;
		}finally {
			tx.finish();
		}
		return true;
	}
	
	protected Vertex getAdmin(){
		
		//Iterator<Vertex> ite=graph.getIndex("vertices", Vertex.class).get("name", "admin").iterator();
		return graph.getVertices("name", "admin").iterator().next();
		
		
	}
	public void shutdown(){
		try {this.graph.shutdown();}catch (Exception e){e.printStackTrace();}
	}
	
	public static void main(String ... args){
		
		
		MappingManager test=new MappingManager("/Reborn/tmp/mappingManager1",true);
		test.showAllConfig();
		for (Vertex v:test.graph.getVertices()){System.out.println(v.toString());};
		
		test.getAdmin();
		
		Configuration one=new Configuration();
		one.setCreationDate(DateTime.now());
		one.setLabel("1");
		one.setPath("/one/one");
		
		test.createConfiguration(one);
		
		Configuration two=new Configuration();
		two.setCreationDate(DateTime.now());
		two.setLabel("2");
		two.setPath("/deux/deux");
		test.createConfiguration(two);
		
		
		test.declareCurrentConf(one);
		
		System.out.println("current path is "+test.getCurrent().getPath()+ " datecreate  "+test.getCurrent().getCreationDate());
		
		Configuration conf=test.getConfiguration(two.getPath());
		System.out.println(test.declareCurrentConf(conf));
		System.out.println("current path is "+test.getCurrent().getPath()+ " datecreate  "+test.getCurrent().getCreationDate());
		test.declareCurrentConf(one);
		System.out.println("current path is "+test.getCurrent().getPath()+ " datecreate  "+test.getCurrent().getCreationDate());
		for (Vertex v:test.graph.getVertices()){System.out.println(v.toString());};
		System.out.println("_");
		test.showAllConfig();
		test.removeConf(one);
		test.showAllConfig();
		System.out.println("_");
		test.removeConf(two);
		test.removeConf(one);
		test.showAllConfig();
		System.out.println("_");
		System.out.println(test.getFullAdmin().toString());
		test.shutdown();
		
	}
}
