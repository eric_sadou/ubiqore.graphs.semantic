package ubiqore.graphs.managing;

import java.io.File;
import java.util.Iterator;

import org.joda.time.DateTime;
import org.neo4j.graphdb.GraphDatabaseService;
import org.neo4j.graphdb.Transaction;
import org.neo4j.index.impl.lucene.LowerCaseKeywordAnalyzer;
import org.neo4j.kernel.EmbeddedGraphDatabase;

import ubiqore.graphs.semantic.AbstractGraph;

import com.tinkerpop.blueprints.Direction;
import com.tinkerpop.blueprints.Edge;
import com.tinkerpop.blueprints.Index;
import com.tinkerpop.blueprints.Parameter;
import com.tinkerpop.blueprints.Vertex;
import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
/*
 * available to manage Dir Path and differents resources :
 *  
 * organisation physique : 
 *  ROOTDIR=ResourceGraphDir
 *         =SNOMEDCT
 *         		=123123123123 [dateInMilliSecond]
 *         		=123123153435 [dateInMilliSecond]
 *         =LOINC
 *         		=213231231231
 * 
 * ResourcesAdmin OUT--manage--IN resource1
 * ResourcesAdmin OUT--manage--IN resource2
 * ResourcesAdmin OUT--manage--IN resource3
 * 
 * 1 resource = id=oid#millisecondCreateDate;oid;creationDate;preflLabel;definition;officialDate;officialVersion.
 * 
 * Rules : 
 * remove resource available (only admin can do that.)
 * => remove means clear data in ResourceManagerGraph.
 * =>

 * 
 * Services : 
 * constructor with a path.
 * createResource From UMLS files.
 * createResource From CSV file (+bioportal access)
 * createResource From CSV file (flat resource)
 * import Resource (declare a resource from a TripleAndContextGraph )
 * declareCurrentConf (path required)
 * getResourcesAdmin() (full information)
 * getResourceStatements (a set a statemeents) 
 * 
  * 
 */



public class ResourceManagerGraph {
	Neo4jGraph graph;
	//protected GraphDatabaseService gds = null;
	public ResourceManagerGraph(String path,boolean newone){
		if (newone){
		    File dir=new File (path);
			if (dir.exists()){
				System.out.println("The graph dir will be delete and all files inside.. sorry baby!! it's too late now..");
				recursiveDelete(dir);
				System.out.println("Graph will be install on the fresh place now..");
			}
		}
		// check if dir exists.
		// if newone : destroy and create.
		//gds = new EmbeddedGraphDatabase(path);
		if (!newone){
			graph=new Neo4jGraph(path,null);
			this.shutdown();
			
		}
		 graph=new Neo4jGraph(path,null);
		
		if (newone)this.init();
		
	}
	
	private static void recursiveDelete(File file) {
        if (!file.exists()) return;
        if (file.isDirectory()) {
           for (File f : file.listFiles()) recursiveDelete(f);
           file.delete();
        } else {
           file.delete();
        }
} 
	/*
	 * Model init.
	 */
	protected void init(){ // create admin vertex + property : creationDate.
		//Index<Vertex> index = graph.createIndex("index", Vertex.class, new Parameter("analyser",LowerCaseKeywordAnalyzer.class.getName()));
		//Index<Vertex> vertexIndex = graph.createIndex("vertices", Vertex.class);
		Transaction tx = graph.getRawGraph().beginTx();
		try {
			graph.createKeyIndex("name", Vertex.class);
			graph.createKeyIndex("mainDir", Vertex.class);
			graph.createKeyIndex("path", Vertex.class);
			graph.createKeyIndex("label", Vertex.class);
			
	        //Index<Edge> edgeIndex = graph.createIndex("edges", Edge.class);
			
			Vertex admin=graph.addVertex(null);
			admin.setProperty("creationDate", DateTime.now().toString());
			admin.setProperty("name", "admin");
			//vertexIndex.put("name", "admin", admin);
			
			admin.setProperty("mainDir", "/");
			tx.success();
		}catch (Exception e ){
			tx.failure();
		}finally {
			tx.finish();
		}
		
		
		
		
	}
	
	
	// unique path and label !!
	public boolean createResource(Resource resource){
		DateTime now=DateTime.now();
		String path=resource.getOid()+"/"+now.getMillis();
		resource.setPath(path);
		resource.setInternalCreationDate(now);
		
		
		Transaction tx = graph.getRawGraph().beginTx();
		try {
		Vertex resourceV=graph.addVertex(null);
		
		resourceV.setProperty("path", resource.getPath());
		resourceV.setProperty("label", resource.getLabel());
		resourceV.setProperty("oid", resource.getOid());
		resourceV.setProperty("definition", resource.getDefinition());
		resourceV.setProperty("version", resource.getVersion());
		resourceV.setProperty("externalCreation",resource.getExternalCreationDate().toString());
		resourceV.setProperty("internalCreation", resource.getInternalCreationDate().toString());
		
		this.graph.addEdge(null, this.getResourcesAdmin(), resourceV, "manage");
		tx.success();
		}catch (Exception e ){
			tx.failure();
			return false;
		}finally {
			tx.finish();
		}
		return true;
	}
	protected Vertex getResourceByPath(String path){
		try {return graph.getVertices("path", path).iterator().next();}
		catch (Exception e ){
			
		}
		return null;
	}
	
	public ResourcesAdmin getFullResourcesAdmin(){
		ResourcesAdmin a=new ResourcesAdmin();
		Vertex v=this.getResourcesAdmin();
		a.setMainDir(v.getProperty("mainDir").toString());
		
		Iterator<Edge> ite=v.getEdges(Direction.OUT, "manage").iterator();
		while (ite.hasNext()){
			Resource tmp=this.getResource(ite.next().getVertex(Direction.IN).getProperty("path").toString());
			a.getResources().add(tmp);
		}
		return a;
	}
	
	protected Vertex getResourceByLabel(String label){
		try {return graph.getIndex("vertices", Vertex.class).get("label", label).iterator().next();}
		catch (Exception e ){
			
		}
		return null;
	}
	
	public void showAllResources(){
		System.out.println("showAllResources");
		for (Edge e:this.getResourcesAdmin().getEdges(Direction.OUT, "manage")){
			Vertex v=e.getVertex(Direction.IN);
			System.out.println("manage -->"+v.getProperty("path"));
		}
		
		System.out.println("showAllConfig End");
	}
	
	public Resource getResource(String path){
		Resource resource=new Resource();
		resource.setPath(path);
		Vertex v=this.getResourceByPath(path);
		if (null==v)return null;
		else {
			/*
			 * resourceV.setProperty("path", resource.getPath());
		resourceV.setProperty("label", resource.getName());
		resourceV.setProperty("definition", resource.getDefinition());
		resourceV.setProperty("version", resource.getVersion());
		resourceV.setProperty("externalCreation",resource.getExternalCreationDate().toString());
		resourceV.setProperty("internalCreation", resource.getInternalCreationDate().toString());
			 */
			resource.setExternalCreationDate(new DateTime(v.getProperty("externalCreation").toString()));
			resource.setInternalCreationDate(new DateTime(v.getProperty("internalCreation").toString()));
			resource.setLabel(v.getProperty("label").toString());
			resource.setOid(v.getProperty("oid").toString());
			resource.setDefinition(v.getProperty("definition").toString());
			resource.setVersion(v.getProperty("version").toString());
			
			
			
		}
		return resource;
	}
	
	public void removeResource(Resource resource){
		Resource res=this.getResource(resource.getPath());
		
		Transaction tx = graph.getRawGraph().beginTx();
		try {
			
			this.graph.removeVertex(this.getResourceByPath(res.getPath()));
		    tx.success();
		System.out.println(resource.getPath()+ " was removed");
		}catch (Exception ex ){
			ex.printStackTrace(System.err);
			tx.failure();
		}finally {
			tx.finish();
		}
		
	}

	
	protected Vertex getResourcesAdmin(){
		
		//Iterator<Vertex> ite=graph.getIndex("vertices", Vertex.class).get("name", "admin").iterator();
		return graph.getVertices("name", "admin").iterator().next();
		
		
	}
	public void shutdown(){
		try {this.graph.shutdown();}catch (Exception e){e.printStackTrace();}
	}
	
	public static void main(String ... args){
		
		
		
		
	}
}
