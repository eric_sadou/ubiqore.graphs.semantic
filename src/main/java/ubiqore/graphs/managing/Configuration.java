package ubiqore.graphs.managing;

import org.joda.time.DateTime;

public class Configuration {
	String label;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	boolean isCurrent=false;
	public boolean isCurrent() {
		return isCurrent;
	}
	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}
	String   path;
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public DateTime getCreationDate() {
		return creationDate;
	}
	public void setCreationDate(DateTime creationDate) {
		this.creationDate = creationDate;
	}
	DateTime creationDate;
	
	@Override
	public String toString(){
		return this.getLabel()+' '+this.getPath()+' '+this.getCreationDate().toString();
	}
}
