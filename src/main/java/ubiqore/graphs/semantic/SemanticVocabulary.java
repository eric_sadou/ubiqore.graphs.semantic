package ubiqore.graphs.semantic;

import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;

public class SemanticVocabulary {
	  
	public static final String domain_default="http://server.ubiqore.com";
	public static final String version_default="v1";
	public static final String context_default=domain_default+"/"+version_default+"/";
	
	public static final String scheme="scheme/";
	public static final String concept="concept/";
	public static final String collection="collection/";
	public static final String menu="menu/";
	public static final String originList="originList";
	
	public static final String skos_prefix="skos";									//	 [ SKOS]
	public static final String ubiq_prefix="ubiq";									//	 [ UBIQ]
	
	public static final String org_ns="http://www.w3.org/ns/org#";					//	 [ ORG]
	public static final String org_prefix="org";
	
	public static final String foaf="http://xmlns.com/foaf/0.1/";					//   [ FOAF ]
	public static final String gr="http://purl.org/goodrelations/v1#";				//	 [ GOOD-RELATIONS ]
	public static final String prov="http://www.w3.org/ns/prov#";					//	 [ PROV-O ]	
	public static final String owl="http://www.w3.org/2002/07/owl#";				//	 [ OWL2-PRIMER ]
	public static final String time="http://www.w3.org/2006/time#";				//	 [ OWL-TIME ]
	public static final String rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";	//	 [ RDF-CONCEPTS ]
	public static final String rdfs="http://www.w3.org/2000/01/rdf-schema#";		//	 [ RDF-SCHEMA ]
	public static final String skos="http://www.w3.org/2004/02/skos/core#";		//	 [ SKOS-REFERENCE ]
	public static final String vcard="http://www.w3.org/2006/vcard/ns#";			//	 [ VCARD ]
	public static final String dct="http://purl.org/dc/terms/";					//	 [ DC11 ]
	
	public static final String ubiq="http://server.ubiqore.com/ubiq/core#";
	
	public static final String  ORG_Class_Organization_String=org_ns+":Organization";
	public static final  URI    ORG_Class_Organization=new URIImpl(ORG_Class_Organization_String);
	
	// org:hasMember
	public static final String  ORG_PROPERTY_hasMember_String=org_ns+":hasMember";
	public static final  URI    ORG_PROPERTY_hasMember=new URIImpl(ORG_PROPERTY_hasMember_String);
	
	public static final String  ORG_PROPERTY_memberOf_String=org_ns+":memberOf";
	public static final  URI    ORG_PROPERTY_memberOf=new URIImpl(ORG_PROPERTY_memberOf_String);
	
	public static final  String SKOS_PROPERTY_prefLabel_String=skos+"prefLabel";
	public static final  URI    SKOS_PROPERTY_prefLabel=new URIImpl(SKOS_PROPERTY_prefLabel_String);
	

	
	public static final  String FOAF_PERSON_String=foaf+"Person";
	public static final  URI    FOAF_PERSON=new URIImpl(FOAF_PERSON_String);
	
	public static final String VALUESETELEMENT_String =ubiq+"ValueSetElement";
	public static final URI VALUESETELEMENT =new URIImpl(VALUESETELEMENT_String);
	
	public static final String RANK_String =ubiq+"Rank";
	public static final URI RANK =new URIImpl(RANK_String);
	
	public static final String CD_String =ubiq+"CD";
	public static final URI CD =new URIImpl(CD_String);
	
	public static final String SC_String =ubiq+"SC";
	public static final URI SC =new URIImpl(SC_String);
	
	public static final String PQ_String =ubiq+"PQ";
	public static final URI PQ =new URIImpl(PQ_String);
	public static final String CO_String =ubiq+"CO";
	public static final URI CO =new URIImpl(CO_String);
	public static final String TS_String =ubiq+"TS";
	public static final URI TS =new URIImpl(TS_String);
	
	public static final String UNIT_String =ubiq+"Unit";
	public static final URI UNIT =new URIImpl(UNIT_String);
	
	public static final String DE_String =ubiq+"DE";
	public static final URI DE =new URIImpl(DE_String);
	
	public static final String TopCategory_String =ubiq+"topCategory";
	public static final URI TopCategory =new URIImpl(TopCategory_String);
	
	public static final String pathLexChapter_String =ubiq+"pathLexChapter";
	public static final URI pathLexChapter =new URIImpl(pathLexChapter_String);
	
	/**
	 * UMLS VOCABULARY 
	 */
	public static final String AUI_String =ubiq+"AUI";
	public static final URI AUI =new URIImpl(AUI_String);
	
	public static final String CUI_String =ubiq+"CUI";
	public static final URI CUI =new URIImpl(CUI_String);
	
	// PQ CO TS
}
