package ubiqore.graphs.semantic;

import java.util.Set;

import org.openrdf.model.Literal;
import org.openrdf.model.URI;

import com.google.common.base.Strings;
import com.google.common.collect.Sets;

public class Scheme {
 
	PrefLabel preflabel;
	Set<SkosConcept> topElements=Sets.newHashSet();
	URI uri;
	String oid;
	String def;
	
	public String getDef() {
		return def;
	}
	public void setDef(String def) {
		this.def = def;
	}
	public PrefLabel getPreflabel() {
		return preflabel;
	}
	public void setPreflabel(PrefLabel preflabel) {
		this.preflabel = preflabel;
	}
	public URI getUri() {
		return uri;
	}
	public void setUri(URI uri) {
		this.uri = uri;
	}
	
	public String getOid() {
		return oid;
	}
	public void setOid(String oid) {
		this.oid=oid;
	}
	
	
	
	public void addTopElement(SkosConcept skos){
		this.topElements.add(skos);
	}
	
	public Set<SkosConcept> getTopElements() {
		return topElements;
	}
	public void setTopElements(Set<SkosConcept> topElements) {
		this.topElements = topElements;
	}
	
	@Override
	public String toString(){
		
		return this.getOid()+" "+this.getPreflabel().getLit().getLabel()+ " "+this.getTopElements().size();
		
	}
	
}
