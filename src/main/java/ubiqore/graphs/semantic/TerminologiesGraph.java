package ubiqore.graphs.semantic;

import info.aduna.iteration.CloseableIteration;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.impl.ContextStatementImpl;
import org.openrdf.model.impl.LiteralImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.OWL;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.impl.BindingImpl;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.repository.sparql.query.SPARQLQueryBindingSet;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;

import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
//import org.apache.commons.httpclient.util.URIUtil;


// Rule One : We only persist Narrower Link !!!
// 
@Deprecated
// replaced by TripleAndContextGraph.java
public class TerminologiesGraph extends AbstractGraph {
	 
	public TerminologiesGraph(boolean creation, String rep)
			throws SailException {
		
		super(creation, rep);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void createVocabularyStatementsList() {
			vocabularyStatements.add(new ContextStatementImpl(SemanticVocabulary.pathLexChapter,RDF.TYPE, RDFS.DATATYPE,context));
			//vocabularyStatements.add(new ContextStatementImpl(SKOS.NARROWER.getURI(),OWL.INVERSEOF,SKOS.BROADER.getURI(),context));
			vocabularyStatements.add(new ContextStatementImpl(SKOS.BROADER.getURI(),OWL.INVERSEOF,SKOS.NARROWER.getURI(),context));
			//vocabularyStatements.add(new ContextStatementImpl(SKOS.BROADER.getURI(),OWL.EQUIVALENTPROPERTY,RDFS.SUBCLASSOF,context));
			//vocabularyStatements.add(new ContextStatementImpl(SKOS.NARROWER.getURI(),OWL.INVERSEOF,RDFS.SUBCLASSOF,context));
			vocabularyStatements.add(new ContextStatementImpl(RDFS.SUBCLASSOF,OWL.INVERSEOF,SKOS.NARROWER.getURI(),context));
	}
	
	@Override
	public void link(URI concept1, URI relation1to2 ,URI concept2) throws SailException{
		// We only persist the IsInverse OR isEquivalent OR the original statement. 
		 Statement st=new ContextStatementImpl(concept1,relation1to2,concept2,this.context);
		
		 //
		
		if (this.isInverse(relation1to2)){
			this.applyInverse(st);
		}else if (this.isEquivalent(relation1to2)){
			this.applyEquivalent(st);
		}else {
			this.addSingleStatement(st);
		}
	}
	
    
	public Map<URI,String> searchInDomainNewTest(String search,URI domain) throws SailException, MalformedQueryException, QueryEvaluationException{
		Map<URI,String> l=Maps.newLinkedHashMap();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri ?prefLabel "+
				"WHERE "+
				"{ " +
				" ?uri skos:narrower+ <"+domain.toString()+"> . "+
	//			" ?uri rdf:type  skos:Concept . " +
				" ?uri skos:prefLabel ?prefLabel . " +
	    		
	//      		" ?uri rdfs:subClassOf+ <"+domain.toString()+"> . "+
				" FILTER regex (str(?prefLabel),\""+search+"\", \"i\" ) " +
				" } "
				;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				l.put(new URIImpl(a.getValue("uri").toString()),((Literal)a.getValue("prefLabel")).getLabel());
			}
		}
		sc.close();
		return l;
	}
	
	public boolean searchInDomain(URI candidate ,URI domain) throws SailException, MalformedQueryException, QueryEvaluationException{
		Map<URI,String> l=Maps.newLinkedHashMap();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?prefLabel "+
				"WHERE "+
				"{ " +
				" <"+domain.toString()+"> skos:narrower+ <"+candidate.toString()+"> . "+
	//			" ?uri rdf:type  skos:Concept . " +
				" ?uri skos:prefLabel ?prefLabel . " +
				" } "
				;
		
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		boolean res=false;
		if (sparqlResults.hasNext()){
			res=true;
		//	System.out.println("domain="+domain+" has as narrower .. "+candidate);
		}else {
		//	System.out.println("domain="+domain+" no rel with .. "+candidate);
		}
		sc.close();
		return res;
	}
	
	
	
	public Map<URI,String> searchInDomain(String search,URI domain) throws SailException, MalformedQueryException, QueryEvaluationException{
		Map<URI,String> l=Maps.newLinkedHashMap();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri ?prefLabel "+
				"WHERE "+
				"{ " +
				" <"+domain.toString()+"> skos:narrower+ ?uri . "+
	//			" ?uri rdf:type  skos:Concept . " +
				" ?uri skos:prefLabel ?prefLabel . " +
				
				"FILTER regex (?prefLabel,\""+search+"\", \"i\" ) " +
				" } "
				;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				l.put(new URIImpl(a.getValue("uri").toString()),((Literal)a.getValue("prefLabel")).getLabel());
			}
		}
		sc.close();
		return l;
	}
	
	public Set<URI>  searchInLocalREGEX (String st) throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> set=Sets.newHashSet();
		
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri ?code"+
							"WHERE "+
							"{ " +
							" ?uri rdf:type  skos:Concept . " +
							" ?uri skos:notation ?code . " +
							"FILTER regex (?code,\""+st+"\", \"i\" ) " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				set.add(new URIImpl(a.getValue("uri").toString()));
			}
				
		}
		sc.close();
		
		
	 
		return set;
	}
	
	public Set<URI>  searchLabel(String pattern,URI optionalSchemeURI,int maxAnswer,int offset,boolean autocompletion ) throws SailException, MalformedQueryException, QueryEvaluationException{
		//pattern=pattern.replaceAll("([^a-zA-z0-9])", "\\\\$1");
		String filterType="CONTAINS";
		System.out.println(pattern);
		if (autocompletion)filterType="STRSTARTS";
		if (optionalSchemeURI==null)return this.searchOnlyByLabel(pattern, maxAnswer,offset,autocompletion);
		
		Set<URI> set=Sets.newHashSet();
		
		if (maxAnswer<=0)maxAnswer=10;
		
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri "+
							"WHERE "+
							
							"{  "; 
			   queryString+= " ?uri skos:prefLabel ?prefLabel  ";
			   queryString+= " . FILTER "+filterType+" (UCASE(str(?prefLabel)),UCASE(\""+pattern+"\")) " ;
			 queryString+=  
					   //  " . FILTER ( EXISTS {?uri skos:inScheme ?schemeUri }) " +
								
					   " } ORDER BY ?preflabel OFFSET "+offset+" LIMIT "+ maxAnswer+" ";
		;
				
//sPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  PREFIX ubiq: <http://server.ubiqore.com/ubiq/core#>  PREFIX foaf: <http://xmlns.com/foaf/0.1/>  PREFIX owl: <http://www.w3.org/2002/07/owl#>  PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>  PREFIX value: <urn:com.tinkerpop.blueprints.pgm.oupls.sail:namespaces>  PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  PREFIX skos: <http://www.w3.org/2004/02/skos/core#>   SELECT DISTINCT ?uri WHERE {  ?uri skos:inScheme ?schemeUri  FILTER ( EXISTS { ?uri skos:prefLabel ?prefLabel   FILTER contains (?prefLabel , ?pattern , "i" ) } )  } LIMIT 10
// regex , \"i\"		
		System.out.println(queryString);				
							
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
	// http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.1
	//	ParsedQuery query=parser.parseQuery(queryString , "http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.1/");
			SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
			bs.addBinding(new BindingImpl("schemeUri",optionalSchemeURI));
	//		bs.addBinding(new BindingImpl("pattern",vf.createLiteral(pattern)));
			sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
			
		
		System.out.println("Before the boucle of result...");
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				
				BindingSet a=sparqlResults.next();
				System.out.println(a.toString());
				set.add(new URIImpl(a.getValue("uri").toString()));
			}
				
		}else {
			System.out.println("no result for searchLabel .."+queryString);
		}
		sc.close();
		
		
	 
		return set;
	}
	
	Set<URI>  searchOnlyByLabel(String pattern,int maxAnswer,int offset,boolean  autocompletion) throws SailException, MalformedQueryException, QueryEvaluationException{
		String filterType="CONTAINS";
		System.out.println(pattern);
		if (autocompletion)filterType="STRSTARTS";
		Set<URI> set=Sets.newHashSet();
		
		if (maxAnswer<=0)maxAnswer=10;
		
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT  ?uri ?preflabel "+
							"WHERE "+
							"{ " ;
		//					" ?uri rdf:type  skos:Concept . ";
		   queryString+= " ?uri skos:prefLabel ?prefLabel  ";
		   queryString+= " . FILTER "+filterType+" (UCASE(str(?prefLabel)),UCASE(\""+pattern+"\")) " +
							" } ORDER BY ?preflabel OFFSET "+offset+" LIMIT "+ maxAnswer+" ";
							;
		
		System.out.println(queryString);			
							
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			int nb=0;
			while (sparqlResults.hasNext()){
				nb++;
				BindingSet a=sparqlResults.next();
				System.out.println("reponse Numero ="+nb);
				//System.out.println(a.toString());
				set.add(new URIImpl(a.getValue("uri").toString()));
			}
				
		}else {
			System.out.println("no result for searchByLabel .."+queryString);
		}
		sc.close();
		
		
	 
		return set;
	}
	
	
	public Set<URI>  searchByLabel2(String pattern,URI optionalSchemeURI,int maxAnswer ) throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> set=Sets.newHashSet();
		
		if (maxAnswer<=0)maxAnswer=10;
		
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT  ?uri "+
							"WHERE "+
							"{ " ;
		//					" ?uri rdf:type  skos:Concept . ";
		   queryString+= " ?uri skos:prefLabel ?prefLabel " ; 
		   queryString+= " ; skos:inScheme  ?schemeUri " +
		   		" . FILTER regex (?prefLabel,\""+pattern+"\", \"i\" ) ";
	       queryString+= "  } " ;
	        //queryString+= " ?topConcept skos:narrower+  ?uri . " ;
			
							;
		
		System.out.println(queryString);			
							
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		
		if (optionalSchemeURI!=null){
			SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
			bs.addBinding(new BindingImpl("schemeUri",optionalSchemeURI));
			
			sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
			
		}
		else sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			int nb=0;
			
			while (sparqlResults.hasNext()){
				nb++;
				BindingSet a=sparqlResults.next();
				System.out.println("reponse Numero ="+nb);
				//System.out.println(a.toString());
				set.add(new URIImpl(a.getValue("uri").toString()));
			}
				
		}else {
			System.out.println("no result for searchByLabel .."+queryString);
		}
		sc.close();
		
		
	 
		return set;
	}
	
	 Set<URI>  searchOnlyByCode(String pattern,int maxAnswer,int offset,boolean autocompletion ) throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> set=Sets.newHashSet();
		String filterType="CONTAINS";
		System.out.println(pattern);
		if (autocompletion)filterType="STRSTARTS";
		
		if (maxAnswer<=0)maxAnswer=10;
		
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri "+
							"WHERE "+
							"{ " ;
						
		//       			" ?uri rdf:type  skos:Concept . ";
		queryString+=			" ?uri skos:notation ?code  ";
		
		

		queryString+=		".  FILTER "+filterType+" (UCASE(?code),UCASE(\""+pattern+"\") ) " +
							" }" 
					+	" ORDER BY ?code OFFSET "+offset+" LIMIT "+ maxAnswer+"  ";
							;
		
		System.out.println(queryString);				
							
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				set.add(new URIImpl(a.getValue("uri").toString()));
			}
				
		}else {
			System.out.println("no result for searchByCode .."+queryString);
		}
		sc.close();
		
		
	 
		return set;
	}
	
	public Set<URI>  searchCode(String pattern,URI optionalSchemeURI,int maxAnswer,int offset,boolean autocompletion ) throws SailException, MalformedQueryException, QueryEvaluationException{
		//pattern=pattern.replaceAll("([^a-zA-z0-9])", "\\\\$1");
		String filterType="CONTAINS";
		System.out.println(pattern);
		if (autocompletion)filterType="STRSTARTS";
		
	//	if (autocompletion)pattern="^"+pattern;
		if (optionalSchemeURI==null)return this.searchOnlyByCode(pattern, maxAnswer,offset,autocompletion);
		
	
		Set<URI> set=Sets.newHashSet();
		
		if (maxAnswer<=0)maxAnswer=10;
		
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri "+
							"WHERE "+
							"{ " 
						
		            		+	" ?uri skos:inScheme ?schemeUri  ";
		queryString+=			"FILTER ( EXISTS { ?uri skos:notation ?code  FILTER "+filterType+" (UCASE(?code),UCASE(\""+pattern+"\") ) } )  ";
		queryString+=	    " } ORDER BY ?code OFFSET "+offset+" LIMIT "+ maxAnswer+" ";
							;
				
		
		System.out.println(queryString);				
							
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		
		
			SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
			bs.addBinding(new BindingImpl("schemeUri",optionalSchemeURI));
			//bs.addBinding(new BindingImpl("pattern",vf.createLiteral("\""+pattern+"\"")));
			sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
			
		
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				set.add(new URIImpl(a.getValue("uri").toString()));
			}
				
		}else {
			System.out.println("no result for searchByCode .."+queryString);
		}
		sc.close();
		
		
	 
		return set;
	}
	
	public Set<URI>  searchCodeFrom(String pattern,URI schemeURI,int maxAnswer,int offset,boolean autocompletion ) throws Exception{
		//pattern=pattern.replaceAll("([^a-zA-z0-9])", "\\\\$1");
		String filterType="CONTAINS";
		System.out.println(pattern);
		if (autocompletion)filterType="STRSTARTS";
		
	//	if (autocompletion)pattern="^"+pattern;
		if (schemeURI==null)throw new Exception();
		
	
		Set<URI> set=Sets.newHashSet();
		
		if (maxAnswer<=0)maxAnswer=10;
		
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		String test="http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.1";
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri "+
							"FROM <"+test+"> " +
							"WHERE "+
							"{ " 
						
		            		+	" ?uri skos:notation ?code   ";
		queryString+=			"FILTER ( EXISTS {  FILTER "+filterType+" (UCASE(?code),UCASE(\""+pattern+"\") ) } )  ";
		queryString+=	    " } ORDER BY ?code OFFSET "+offset+" LIMIT "+ maxAnswer+" ";
							;
				
		
		System.out.println(queryString);				
							
		ParsedQuery query=parser.parseQuery(queryString , context.toString()+"concept/2.16.840.1.113883.6.1/");
		
		System.out.println(context.toString());
		
			SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
		//	bs.addBinding(new BindingImpl("schemeUri",optionalSchemeURI));
			//bs.addBinding(new BindingImpl("pattern",vf.createLiteral("\""+pattern+"\"")));
			sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
			
		
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				set.add(new URIImpl(a.getValue("uri").toString()));
			}
				
		}else {
			System.out.println("no result for searchByCode .."+queryString);
		}
		sc.close();
		
		
	 
		return set;
	}
	
	
	
	
	public Map<URI,String>  searchStringREGEX (String st) throws SailException, MalformedQueryException, QueryEvaluationException{
		Map<URI,String> l=Maps.newLinkedHashMap();
		
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri ?prefLabel "+
							"WHERE "+
							"{ " +
							" ?uri rdf:type  skos:Concept . " +
							" ?uri skos:prefLabel ?prefLabel . " +
							"FILTER regex (?prefLabel,\""+st+"\", \"i\" ) " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				l.put(new URIImpl(a.getValue("uri").toString()),((Literal)a.getValue("prefLabel")).getLabel());
			}
				
		}
		sc.close();
		
		
	 
		return l;
	}
	
	
	
	@Override
	protected void createNamespacesMap() {
		    // official namespaces..
			this.useClassicNamespaces();
		    // ubiq and others...
	        this.namespaces.put(SemanticVocabulary.skos_prefix,SemanticVocabulary.skos);
	        this.namespaces.put(SemanticVocabulary.ubiq_prefix,SemanticVocabulary.ubiq);
	}
	
	public List<SkosConcept> getOriginalList() throws SailException, MalformedQueryException, QueryEvaluationException{
		List<URI> l=this.getCollectionMembers(createOrGetOriginList());
		List<SkosConcept> rep=Lists.newArrayList();
		for (URI uri:l){
			rep.add(this.getConcept(uri));
		}
		return rep;
	}
	
	
	
	
	
	public List<SkosConcept> getFullList() throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> l=this.getConceptsURI();
		List<SkosConcept> rep=Lists.newArrayList();
		for (URI uri:l){
			rep.add(this.getConcept(uri));
		}
		return rep;
		
	}
	
	
	
	public URI createOrGetOriginList() throws SailException{
		URI uri=new URIImpl(this.context+SemanticVocabulary.collection+SemanticVocabulary.originList);
		if (this.entityExist(new ContextStatementImpl(uri,RDF.TYPE,SKOS.COLLECTION.getURI(),this.context))!=null){
			return uri;
		}
		this.addSingleStatement(new ContextStatementImpl(uri,RDF.TYPE,SKOS.COLLECTION.getURI(),this.context));
		return uri;
	}
	
	
	public void addOriginalMember(URI conceptURI){
		try {
			this.link(conceptURI, SKOS.MEMBER.getURI(), createOrGetOriginList());
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void addOriginalCOMember(URI conceptURI,float value){
		try {
			this.link(conceptURI, SKOS.MEMBER.getURI(), createOrGetOriginList());
			this.addSingleStatement(new ContextStatementImpl(conceptURI,SemanticVocabulary.CO,vf.createLiteral(value),this.context));
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// apply means : create and/or get uri. 
	public URI applyScheme(String oid,String prefLabel,String definition) throws SailException{
		URI uri=new URIImpl(this.context+SemanticVocabulary.scheme+oid);
		URI rep=this.entityExist(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPTSCHEME.getURI(),this.context));
		if (rep==null){
			List<Statement> sts=Lists.newArrayList();
			sts.add(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPTSCHEME.getURI(),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.PREFLABEL.getURI(),vf.createLiteral(prefLabel),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.NOTATION.getURI(),vf.createLiteral(oid),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.DEFINITION.getURI(),vf.createLiteral(definition),this.context));
			
			boolean ok=this.addMultipleStatements(sts);
			if (ok)return uri;
		}else {
			//System.out.println("scheme already there darling...");
		}
		return rep;
	}
	
	@Deprecated
	public Set<URI> getConceptsURI() throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> set=Sets.newHashSet();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x  "+
							"WHERE "+
							"{ " +
							" ?x rdf:type  skos:Concept ."+
							
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
		
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.getValue("x"));
				set.add(new URIImpl(a.getValue("x").stringValue()));
			}
			
		}
		
		sc.close();
		return set;
	 
	}
	
	public Set<URI> getSchemesURI() throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> set=Sets.newHashSet();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x  "+
							"WHERE "+
							"{ " +
							" ?x rdf:type  skos:ConceptScheme ."+
							
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
		
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.getValue("x"));
				set.add(new URIImpl(a.getValue("x").stringValue()));
			}
			
		}
		
		sc.close();
		return set;
	 
	}
	
	Set<Scheme> schemeCache=Sets.newHashSet();
	private Scheme getFromSchemeCache(URI input){
		for (Scheme s:this.schemeCache){
			if (s.getUri().toString().equalsIgnoreCase(input.toString()))return s;
		}
		return null;
	}
	private void addSchemeCache(Scheme s){
		this.schemeCache.add(s);
	}
	
	
	public Scheme getSchemeBase(URI input) throws SailException, MalformedQueryException, QueryEvaluationException{
		Scheme scheme=null;
		Scheme s=this.getFromSchemeCache(input);
		if (s!=null)return s;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?label ?oid ?def  "+
							"WHERE "+
							"{ " +
							"<"+ input.stringValue()+">		rdf:type  skos:ConceptScheme ."+
							"<"+ input.stringValue()+">		skos:prefLabel  ?label ."+
							"<"+ input.stringValue()+">	  	skos:notation  ?oid ."+
							"<"+ input.stringValue()+">	    skos:definition  ?def ."+
							" } "
							;
		/*
		 * sts.add(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPTSCHEME.getURI(),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.PREFLABEL.getURI(),vf.createLiteral(prefLabel),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.NOTATION.getURI(),vf.createLiteral(oid),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.DEFINITION.getURI(),vf.createLiteral(definition),this.context));
			
		 */
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				scheme=new Scheme();
				scheme.setUri(input);
				Literal loid=(Literal)a.getValue("oid");
				Literal llabel=(Literal)a.getValue("label");
				Literal ldef=(Literal)a.getValue("def");
				scheme.setOid(loid.getLabel());
				scheme.setDef(ldef.getLabel());
				scheme.setPreflabel(new PrefLabel(llabel.getLabel()));
				this.addSchemeCache(scheme);
			}
			
			
		}
		sc.close();
		
		return scheme;
	}
	
	
	
	
	public URI conceptExists(String code,String oid)throws SailException{
		URI uri=new URIImpl(this.context+SemanticVocabulary.concept+oid+"/"+code);
		URI rep=this.entityExist(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPT.getURI(),this.context));
		if (rep==null)return null;
		else return uri;
	}
	// no hierarchical there !
	public URI addOrGetSimpleConcept(SkosConcept c) throws SailException{
		
		URI uri=new URIImpl(this.context+SemanticVocabulary.concept+c.getScheme().getOid()+"/"+c.getSkosNotation());
		URI rep=this.entityExist(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPT.getURI(),this.context));
		if (rep==null){
			List<Statement> sts=Lists.newArrayList();
			sts.add(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPT.getURI(),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.PREFLABEL.getURI(),c.getPreflabel().getLit(),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.NOTATION.getURI(),vf.createLiteral(c.getSkosNotation()),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.INSCHEME.getURI(),c.getScheme().getUri(),this.context));
			if (Strings.emptyToNull(c.getPathLexChapter())!=null){
				LiteralImpl chapter=new LiteralImpl(c.getPathLexChapter(),SemanticVocabulary.pathLexChapter);
				sts.add(new ContextStatementImpl(uri, SKOS.NOTATION.getURI(), chapter,this.context));
			}
			
			boolean ok=this.addMultipleStatements(sts);
			if (ok)return uri;
		}
		return rep;
	

	}
	
	
	
	public String  updateStatements(Set<Statement> sts) throws SailException{
		String res="statistics :";
		int yes=0;
		int no =0;
		int total=0;
		for (Statement st:sts){
			total++;
			if (total%20==0)System.out.println("already treated="+total);
			boolean added=this.updateStatement(st);
			if (added)yes++;else no++;
		}
		return res+"->added statements="+yes+" / refused statements="+no+"/Nb statements="+sts.size();
	}
	
	public boolean updateStatement(Statement st) throws SailException{
		if (this.entityExist(st)!=null)return false;
		else this.addSingleStatement(st);
		return false;
	}
	
		
	
	public SkosConcept getErsatzConcept(String skosNotation,String schemeId,boolean analize){
		// schemeId can be urn:oid:2342342342" OR "324234234"
		// analize boolean : should we analyse the skosNotation to detect a /1.0 ??
		String schemeOID=null;
		String code=skosNotation;
		boolean checkCOValue=false;
		Float coValueToCheck=null;
		if (schemeId.startsWith("urn:oid:"))schemeOID=schemeId.replace("urn:oid:","");
		else schemeOID=schemeId;
		try {
			if (analize && skosNotation.contains("/")){
				// CO declaration case. 
				Iterator<String> stringI=Splitter.on("/").split(skosNotation).iterator();
				String savedCode=code;
				try {
					
					code=stringI.next();
					coValueToCheck=new Float(stringI.next().trim());}catch (Exception e){
						code=savedCode;
						coValueToCheck=null;}
				
				if (coValueToCheck!=null)checkCOValue=true;
				
			}
			if (this.conceptExists(code, schemeOID)!=null){
				URI uri=new URIImpl(this.context+SemanticVocabulary.concept+schemeOID+"/"+code);
				try {
					SkosConcept skos=this.getConcept(uri);
					if (checkCOValue){
						if (coValueToCheck.equals(skos.getCoValue())){
							skos.setSkosNotation(skosNotation);
							
						}else return null;
					}
					return skos;
				} catch (MalformedQueryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (QueryEvaluationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	// enter set is usefull for code+schemeOID only !!! 
	public Set<SkosConcept> validation( Set<SkosConcept> toValidate) throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<SkosConcept> result=Sets.newHashSet();
		for (SkosConcept skos:toValidate){
			SkosConcept res=this.getErsatzConcept(skos.getSkosNotation(), skos.getScheme().getOid(),true);
			//URI uri=new URIImpl(this.context+SemanticVocabulary.concept+skos.getScheme().getOid()+"/"+skos.getSkosNotation());
			//SkosConcept res=this.getConcept(uri);
			if (res!=null)result.add(res);
		}
		return result;
	}
	
	
	private URI check(URI uri){
		if (uri==null)return null;
		
		String uriS=uri.toString();
		URI newURI=new URIImpl(uriS.replace("|", "\\|"));
		return newURI;
	}  
	
	
	
	
	public SkosConcept getConcept(URI uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		String sparqlURI=uri.toString();
		
		sparqlURI = uri.stringValue().replace("|","\\\\|");
		
		//System.out.println(sparqlURI);
		SailConnection sc=sail.getConnection();
		SkosConcept skos=null;
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?code ?scheme ?prefLabel ?codingSchemeURI ?coValue  "+
							"WHERE "+
							"{ " +
							"?uri rdf:type  skos:Concept . " +
							"?uri skos:notation ?code . " +
							"?uri skos:prefLabel ?prefLabel . " +
							"?uri skos:inScheme ?scheme . " +
							"OPTIONAL { ?uri ubiq:CO ?coValue }. " +
							// "?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?code) = xsd:string ) " +
						 //	"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +
							" } "
							;
		
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
		bs.addBinding(new BindingImpl("uri",uri));
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
		
		if (sparqlResults.hasNext()){
			skos=new SkosConcept();
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				Literal v=(Literal)a.getValue("prefLabel");
				skos.setPreflabel(new PrefLabel(v));
				//System.out.println(skos.getPreflabel().getLit().getLabel());
				//System.out.println(skos.getPreflabel().getLit().getLanguage());
				skos.setScheme(this.getSchemeBase(new URIImpl(a.getValue("scheme").stringValue())));
				skos.setSkosNotation(a.getValue("code").stringValue());
				if (a.getValue("coValue")!=null){
					Literal value=(Literal)a.getValue("coValue");
					skos.setCoValue(new Float(value.floatValue()));
				}
				skos.setUri(uri);
				break;
			}
			
		}
		sc.close();
		return skos;
	}
	public SkosConcept getConceptDeprecated(URI uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		String sparqlURI=uri.toString();
		
		
		System.out.println(sparqlURI);
		SailConnection sc=sail.getConnection();
		SkosConcept skos=null;
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?code ?scheme ?prefLabel ?codingSchemeURI ?coValue  "+
							"WHERE "+
							"{ " +
							"<"+ sparqlURI+"> rdf:type  skos:Concept . " +
							"<"+ sparqlURI+"> skos:notation ?code . " +
							"<"+ sparqlURI+"> skos:prefLabel ?prefLabel . " +
							"<"+ sparqlURI+"> skos:inScheme ?scheme . " +
							"OPTIONAL { <"+ sparqlURI+"> ubiq:CO ?coValue }. " +
							// "?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?code) = xsd:string ) " +
						 //	"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +
							" } "
							;
	
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			skos=new SkosConcept();
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				Literal v=(Literal)a.getValue("prefLabel");
				skos.setPreflabel(new PrefLabel(v));
				//System.out.println(skos.getPreflabel().getLit().getLabel());
				//System.out.println(skos.getPreflabel().getLit().getLanguage());
				skos.setScheme(this.getSchemeBase(new URIImpl(a.getValue("scheme").stringValue())));
				skos.setSkosNotation(a.getValue("code").stringValue());
				if (a.getValue("coValue")!=null){
					Literal value=(Literal)a.getValue("coValue");
					skos.setCoValue(new Float(value.floatValue()));
				}
				skos.setUri(uri);
				break;
			}
			
		}
		sc.close();
		return skos;
	}
	
	public SkosConcept getFullConcept(URI uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SkosConcept c=this.getConcept(uri);
		c.setBroaders(this.getBroaders(c));
		c.setNarrowers(this.getNarrowers(c));
		return c;
	}
	public boolean addTopConcept(SkosConcept skosConcept) throws SailException{
		return this.addSingleStatement(new ContextStatementImpl(skosConcept.getUri(), SKOS.TOPCONCEPTOF.getURI(), skosConcept.getScheme().getUri(),context));
	}
	
	public Set<URI> getTopConceptsOfScheme(URI schemeURI)throws SailException, MalformedQueryException, QueryEvaluationException,Exception{
		SailConnection sc=sail.getConnection();
		Set<URI> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x  "+
							"WHERE "+
							"{ " +
							" ?x skos:topConceptOf <" + schemeURI.toString()+"> ."+
							
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				
			//	System.out.println(a.getValue("x"));
				set.add(new URIImpl(a.getValue("x").stringValue()));
			}
			
		}
		sc.close();
		
		return set;
	}
	
	public Set<String> getTopTerms(String uri,String schemeUri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri   "+
							"WHERE "+
							"{ " +
							" ?uri skos:topConceptOf <"+ schemeUri +"> . " +
							" ?uri skos:narrower+  <"+ uri+">  . " +
						
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				set.add(uriB);
			}
			
			
		}
		sc.close();
		return set;
	}
	
	
	public Set<String> getBroaders(String uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri   "+
							"WHERE "+
							"{ " +
							" ?uri  skos:narrower  <"+ uri+">  . " +
							" ?uri rdf:type  skos:Concept . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				set.add(uriB);
			}
			
			
		}
		sc.close();
		return set;
	}
	
	public Set<URI> getTopBroadersByLevel(URI uri,int distanceBetweenTopConceptAndGoodLevel) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		
		Set<URI> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		SkosConcept skos=this.getConcept(uri);
		 try {
			 Set<URI> set1=this.getTopConceptsOfScheme(skos.getScheme().getUri());
			 if (set1.contains(uri)){
				 System.out.println("IS Already a Top Concept !!!");
				 return null;
			 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (skos==null)return null;
		int i=distanceBetweenTopConceptAndGoodLevel;
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri "+
							"WHERE "+
							"{ " +
							"?uri skos:narrower+  <"+ uri.stringValue()+"> . "+ 
							
							" FILTER EXISTS {" +
							"	?topConcept skos:narrower{"+i+","+i+"} ?uri  . " +
							"   ?topConcept skos:topConceptOf   <"+skos.getScheme().getUri().stringValue()+ "> . } "
							+
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				set.add(new URIImpl(uriB));
			}
			
			
		}
		sc.close();
		return set;
	}
	
	
	public Set<URI> getTopBroaders(URI uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<URI> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		SkosConcept skos=this.getConcept(uri);
		 try {
			 Set<URI> set1=this.getTopConceptsOfScheme(skos.getScheme().getUri());
			 if (set1.contains(uri)){
				 System.out.println("IS Already a Top Concept !!!");
				 return null;
			 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (skos==null)return null;
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri "+
							"WHERE "+
							"{ " +
							"?uri skos:narrower+  <"+ uri.stringValue()+"> . "+ 
							
							" FILTER EXISTS {?uri skos:topConceptOf   <"+skos.getScheme().getUri().stringValue()+ "> } "
							+
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				set.add(new URIImpl(uriB));
			}
			
			
		}
		sc.close();
		return set;
	}
	
	public Set<SkosConcept> getBroaders(SkosConcept skos) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<SkosConcept> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri   "+
							"WHERE "+
							"{ " +
							" ?uri skos:narrower <"+ skos.getUri().toString()+"> . " +
							" ?uri rdf:type  skos:Concept . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		Set<String> uris=Sets.newHashSet();
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				uris.add(uriB);
			}
			
			
		}
		sc.close();
		
		for (String uri:uris){
			SkosConcept broader=this.getConcept(new URIImpl(uri));
			set.add(broader);
		}
		
		return set;
	}
	
	
	
	public Set<String> getBagOfNarrowers(String uri,int level) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?narrowURI  "+
							"WHERE "+
							"{ ";
		//queryString+=		  " ?narrowURI rdf:type  skos:Concept . ";
		if (level>1){queryString+="<"+uri+"> skos:narrower{1,"+level+"} ?narrowURI . ";}
		if (level==0){queryString+="<"+uri+"> skos:narrower+  ?narrowURI . ";}
		if (level==1){queryString+="<"+uri+"> skos:narrower  ?narrowURI . ";}
		queryString+=       " } ";
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			System.out.println("Hasext OK");
			int i=0;
			while (sparqlResults.hasNext()){
				//System.out.println("Hasext OK"+i++);
				BindingSet a=sparqlResults.next();
				String narrowURI=a.getValue("narrowURI").toString();
				set.add(narrowURI);
			
			}
			
			
		}
		sc.close();
		
		return set;
	}
	
	
	public Set<String> getCDList(int level) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		URI origin=this.createOrGetOriginList();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		set.addAll(this.getCollectionMembersString(origin));
		System.out.println("Before Narrow="+set.size());
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?y ?x  "+
							"WHERE "+
							"{ ";
		queryString+=         " ?x skos:member <"+ origin.toString()+"> .";
		//queryString+=		  " ?y rdf:type  skos:Concept . ";
		if (level>1){queryString+=" ?x skos:narrower{1,"+level+"} ?y . ";}
		if (level==0){queryString+=" ?x skos:narrower+  ?y . ";}
		if (level==1){queryString+=" ?x skos:narrower  ?y . ";}
		
		
		queryString+=" } ";
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		int cptX=0;
		int cptY=0;
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriS=a.getValue("y").toString();
				String uriO=a.getValue("x").toString();
				boolean addedX=set.add(uriO);
				boolean addedY=set.add(uriS);
				if (addedX)cptX++;
				if (addedY)cptY++;
			}
			
			
		}
		sc.close();	
		
		return set;
	}
	
	
	
	public Set<SkosConcept> getNarrowers(SkosConcept skos) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<SkosConcept> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri   "+
							"WHERE "+
							"{ " +
				//			" ?uri rdf:type  skos:Concept . " +
							" <"+ skos.getUri().toString()+"> skos:narrower ?uri . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			Set<String> uris=Sets.newHashSet();
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				uris.add(uriB);
			}
			
			for (String uri:uris){
				SkosConcept narrower=this.getConcept(new URIImpl(uri));
				set.add(narrower);
			}
		}
		sc.close();
		
		//if (set.isEmpty())return this.getNarrowersOposite(skos);
		return set;
	}
	
	// recursive creation of narrowers...
	public SkosConcept getAllNarrowers(SkosConcept skos ,int lim,boolean start) throws SailException, MalformedQueryException, QueryEvaluationException{
		 boolean nolimit=false;
		 int level=0;
		 if (start && lim==0){
			 nolimit=true;
		 }
		 else if (lim==0) {
			 return skos;
		 }
		 else level=lim-1;
		 
		 Set<SkosConcept> skosSet=this.getNarrowers(skos);
		 skos.setNarrowers(skosSet);
		 for (SkosConcept s:skos.getNarrowers()){
			 this.getAllNarrowers(s,level,nolimit);
		 }
		
		 return skos;
	}
	
	public SkosConcept getAllBroaders(SkosConcept skos ,int lim,boolean start) throws SailException, MalformedQueryException, QueryEvaluationException{
		 boolean nolimit=false;
		 int level=0;
		 if (start && lim==0){
			 nolimit=true;
		 }
		 else if (lim==0) {
			 return skos;
		 }
		 else level=lim-1;
		 
		 Set<SkosConcept> skosSet=this.getBroaders(skos);
		 skos.setBroaders(skosSet);
		 for (SkosConcept s:skosSet){
			 this.getAllBroaders(s,level,nolimit);
		 }
		
		 return skos;
	}
	
	public Set<String> getSubOriginFromLevels(int level) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		URI origin=this.createOrGetOriginList();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		set.addAll(this.getCollectionMembersString(origin));
		System.out.println("Before Narrow="+set.size());
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?y ?x  "+
							"WHERE "+
							"{ ";
		queryString+=         " ?x skos:member <"+ origin.toString()+"> .";
		//queryString+=		  " ?y rdf:type  skos:Concept . ";
		if (level>1){queryString+=" ?x skos:narrower{1,"+level+"} ?y . ";}
		if (level==0){queryString+=" ?x skos:narrower+  ?y . ";}
		if (level==1){queryString+=" ?x skos:narrower  ?y . ";}
		
		
		queryString+=" } ";
		
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		int cptX=0;
		int cptY=0;
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriS=a.getValue("y").toString();
				String uriO=a.getValue("x").toString();
				boolean addedX=set.add(uriO);
				boolean addedY=set.add(uriS);
				if (addedX)cptX++;
				if (addedY)cptY++;
			}
			
			
		}
		sc.close();
		
		
		return set;
	}
}
