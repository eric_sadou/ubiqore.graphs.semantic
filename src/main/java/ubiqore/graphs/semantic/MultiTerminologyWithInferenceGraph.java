package ubiqore.graphs.semantic;

  
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.impl.ContextStatementImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.OWL;
import org.openrdf.model.vocabulary.RDFS;
/*import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
//import org.openrdf.rio.helpers.StatementCollector;
import org.openrdf.rio.rdfxml.RDFXMLParser;*/
import org.openrdf.sail.SailException;
import org.openrdf.sail.inferencer.fc.ForwardChainingRDFSInferencer;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;

@Deprecated
public class MultiTerminologyWithInferenceGraph extends MultiTerminologyGraph {

	public MultiTerminologyWithInferenceGraph(boolean creation, String rep)
			throws SailException {
		super(creation, rep);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void createVocabularyStatementsList() {
			super.createVocabularyStatementsList();
			
			vocabularyStatements.add(new ContextStatementImpl(SKOS.NARROWER.getURI(),OWL.INVERSEOF,SKOS.BROADER.getURI(),context));
			vocabularyStatements.add(new ContextStatementImpl(SKOS.BROADER.getURI(),OWL.INVERSEOF,SKOS.NARROWER.getURI(),context));
			
			vocabularyStatements.add(new ContextStatementImpl(SKOS.BROADER.getURI(),OWL.EQUIVALENTPROPERTY,RDFS.SUBCLASSOF,context));
			vocabularyStatements.add(new ContextStatementImpl(SKOS.NARROWER.getURI(),OWL.INVERSEOF,RDFS.SUBCLASSOF,context));
			vocabularyStatements.add(new ContextStatementImpl(RDFS.SUBCLASSOF,OWL.INVERSEOF,SKOS.NARROWER.getURI(),context));

	}
	
	@Override 
	protected void initAccess() throws SailException{
		baseGraph = new Neo4jGraph(rep);
		
		baseGraph.setCheckElementsInTransaction(true);
		GraphSail<Neo4jGraph> gSail=new GraphSail<Neo4jGraph>(baseGraph);
		gSail.enforceUniqueStatements(false);
		sail= new ForwardChainingRDFSInferencer(gSail);
		
		sail.initialize();
		
		vf=sail.getValueFactory();
	}
	
	
	
	protected void test(){
	//	OWLOntologyManager manager = OWLManager.createOWLOntologyManager(); 
		 
        //OWLOntology ontology = manager.loadOntologyFromOntologyDocument(IRI.create(BASE_URL)); 
 
  //      OWLOntology ontology = manager.loadOntologyFromOntologyDocument(
 
  //      IRI.create(getClass().getClassLoader().getResource("Prueba.owl")));
    
      //  System.out.println(" Load ontology" );
  //      OWLReasonerFactory reasonerFactory = PelletReasonerFactory.getInstance(); 
	}
	
	/*
	 * Each time we create a Narrow / we infered a Broader (vice versa)
	 * */
	/*@Override
	public void link(SkosConcept concept1, URI relation1to2 ,URI concept2) throws SailException{
		super.link(concept1, relation1to2, concept2);
		
		if (relation1to2.equals(SKOS.BROADER.getURI())){
			// braoder add .so narrow to add.
			this.addSingleStatement(new ContextStatementImpl(concept2,SKOS.NARROWER.getURI(),concept1.getUri(),this.context));

		}else if (relation1to2.equals(SKOS.NARROWER.getURI())){
			this.addSingleStatement(new ContextStatementImpl(concept2,SKOS.BROADER.getURI(),concept1.getUri(),this.context));

		}
	}*/
	
	/*
	 * Si on ajoute un LINK quelque part : on teste la relation ASK REL -> hAS a  relation inverse.
	 * Si oui On persiste la relation inverse !! ... 
	 * 
	 */
	
	
	/*protected void inverseRes() throws SailException{
		System.out.println("override version used");
		
		SailConnection sc=sail.getConnection();
	
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		try {
		String queryString =this.queryPrepare()+" CONSTRUCT { ?y ?q ?x }   "+
							"WHERE "+
							"{ " +
							" ?p owl:inverseOf ?q . " +
							" ?x ?p ?y . " +
							" } "
							;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				
			}
			
			
		}
		}catch (Exception e){e.printStackTrace();}
		
		sc.close();
		
		
			}*/
	
	
	/*@Override
	public Set<SkosConcept> getBroaders(SkosConcept skos) throws SailException, MalformedQueryException, QueryEvaluationException{
		System.out.println("override version used");
		Set<String> uris=Sets.newHashSet();
		SailConnection sc=sail.getConnection();
		Set<SkosConcept> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		try {
		String queryString =this.queryPrepare()+" SELECT ?uri   "+
							"WHERE "+
							"{ " +
							" ?uri rdf:type  skos:Concept . " +
							" <"+ skos.getUri().toString()+"> skos:broader ?uri . " +
							" } "
							;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				uris.add(uriB);
			}
			
			
		}
		}catch (Exception e){e.printStackTrace();}
		
		sc.close();
		
		for (String uri:uris){
			SkosConcept broader=this.getConcept(new URIImpl(uri));
			set.add(broader);
		}
		
		return set;
	}*/
	
	/*
	 * http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/173447006
http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/173226005
http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/173382008
http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/173359006
http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/190630008
173382008
http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/37032002
	 */
	
@Override
public void importRDF(String filePath) throws SailException{
		/*
		FileReader file=null;
		try {
			file = new FileReader (filePath);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.out.println("nothing done");
			return;
		}
		
		List<Statement> myList = Lists.newArrayList();
		//StatementCollector collector = new StatementCollector(myList);
		RDFParser rdfParser = new RDFXMLParser();
		rdfParser.setRDFHandler(collector);
		try {
			   rdfParser.parse(file, "http://server.ubiqore.com/v1");
			  
			} 
			catch (IOException e) {
			  // handle IO problems (e.g. the file could not be read)
			}
			catch (RDFParseException e) {
			  // handle unrecoverable parse error
			}
			catch (RDFHandlerException e) {
			  // handle a problem encountered by the RDFHandler
			}
		int count=collector.getStatements().size();
		System.out.println(count+" --> nb statements inside the file");
		List<Statement> list= (List<Statement>)collector.getStatements(); 
		for (Statement st:list){
			System.out.println(st.getSubject().toString());
			this.addSingleStatement(st);
			
		}
		this.feed(list);
		
		*/
	
	}
	public void feed(List<Statement> list){
		List<Statement> tmp=Lists.newArrayList();
		int limit=50;
		int cpt=0;
		for (Statement st:list){
			cpt++;
			tmp.add(st);
			if (cpt>limit){
				System.out.println(cpt);
				try {
					this.addMultipleStatements(tmp);
					
					System.out.println(limit +" statements were comited");
				} catch (SailException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				tmp.clear();
				cpt=0;
			}
			
		}
		if (!tmp.isEmpty())
			try {
				System.out.println(cpt);
				this.addMultipleStatements(tmp);
			} catch (SailException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		try {
			this.sc.commit();
			this.sc.close();
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	
	public static void main(String... args){
		//MultiTerminologyWithInferenceGraph graph=null;
		MultiTerminologyGraph graph=null;
		try {
			
			graph=new MultiTerminologyGraph(false,"/Reborn/tmp/graphs/test3");
			System.out.println("Nb concepts="+graph.getConceptsURI().size());
			List<URI> base=graph.getCollectionMembers(graph.createOrGetOriginList());
			System.out.println("La base ="+base.size());
			
			Set<String> g=graph.getNarrowersBest( 1);
			System.out.println("Les narrow 1 Nb Result="+g.size());
			g=graph.getNarrowersBest( 2);
			System.out.println("Les narrow 2 Nb Result="+g.size());
			g=graph.getNarrowersBest( 3);
			System.out.println("Les narrow 3 Nb Result="+g.size());
			g=graph.getNarrowersBest( 4);
			System.out.println("Les narrow 4 Nb Result="+g.size());
			g=graph.getNarrowersBest( 5);
			System.out.println("Les narrow 5 Nb Result="+g.size());
			g=graph.getNarrowersBest( 0);
			System.out.println("Les narrow FULL Nb Result="+g.size());
			/*Set<String> l=graph.getNarrowersBest("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/367336001", 1);
			//for (String s:l)System.out.println(graph.getConcept(new URIImpl(s)).getPreflabel().getLit().getLabel());
			System.out.println("0 Nb Result="+l.size());
			l=graph.getNarrowersBest("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/367336001", 1);
			for (String s:l)System.out.println(graph.getConcept(new URIImpl(s)).getPreflabel().getLit().getLabel());
			System.out.println("1 Nb Result="+l.size());
			l=graph.getNarrowersBest("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/367336001", 2);
			//for (String s:l)System.out.println(graph.getConcept(new URIImpl(s)).getPreflabel().getLit().getLabel());
			System.out.println("2 Nb Result="+l.size());
			l=graph.getNarrowersBest("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/367336001", 3);
			//for (String s:l)System.out.println(graph.getConcept(new URIImpl(s)).getPreflabel().getLit().getLabel());
			System.out.println("3 Nb Result="+l.size());*/
			//List<SkosConcept> l=graph.getCodesList(DEVoc.LEVELS,2);
			//for (SkosConcept s:l)System.out.println(s.getPreflabel().getLit().getLabel());
			//graph.importRDF("/Reborn/tmp/graphs/test3Export.rdf");
			/*Set<String> subs= graph.getTransitiveSubClasses("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/71388002");
			//173382008
			for (String sub:subs){
				System.out.println(graph.getConcept(new URIImpl(sub)).getPreflabel().getLit().getLabel());
			}*/
			//graph.exportRDF("/Reborn/tmp/graphs/test3Export.rdf");
			//graph.importRDF("/Reborn/data/ehr4cr/04022013.rdf");
			//graph.statements();
			
			/*
			 * L'inference fonctionne si l'insert du statement a été effectué dans un MultiTerminologyWithInferenceGraph.
			 */
			
			//graph=new MultiTerminologyWithInferenceGraph(false,"/Reborn/tmp/graphs/testInfered");
			/*graph=new MultiTerminologyGraph(false,"/Reborn/tmp/graphs/testInfered");
			//graph.importRDF("/Reborn/data/ehr4cr/04022013.rdf");
			Map<URI, String> test=graph.searchInDomain("therapy",new URIImpl("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/416608005"));
			Iterator<Entry<URI, String>> ite=test.entrySet().iterator();
			while (ite.hasNext()){
				Entry<URI, String> e=ite.next();
				System.out.println(e.getValue()+" "+e.getKey().toString() );
			}*/
			
			
			/*graph.statements(new URIImpl("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/307276000"), true);
			System.out.println("end of statements");
			Set<String> subs= graph.getSubDomain("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/71388002");
			//173382008
			for (String sub:subs){
				System.out.println(graph.getConcept(new URIImpl(sub)).getPreflabel().getLit().getLabel());
			}
			subs= graph.getSubDomain("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/71388002");
			for (String sub:subs){
				System.out.println(graph.getConcept(new URIImpl(sub)).getPreflabel().getLit().getLabel());
			}
			System.out.println(subs.size());
			Set<String> subs2= graph.getSubDomain("http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.96/173382008");
			System.out.println("173382008");
			for (String sub:subs2)System.out.println(sub);
			*/
			
			/*graph=new MultiTerminologyWithInferenceGraph(true,"/Reborn/tmp/graphs/test4");
			URI scheme=graph.applyScheme("toto", "scheme1", "test of infered");
			SkosConcept c=new SkosConcept();
			Scheme s=graph.getSchemeBase(scheme);
			c.setPreflabel(new PrefLabel("super"));
			c.setSkosNotation("super");
			c.setScheme(s);
			c.setUri(graph.addOrGetSimpleConcept(c));
			graph.addTopConcept(c);
			
			SkosConcept c2=new SkosConcept();
			c2.setPreflabel(new PrefLabel("kiki"));
			c2.setSkosNotation("levelOne");
			c2.setScheme(s);
			c2.setUri(graph.addOrGetSimpleConcept(c2));
			
			SkosConcept c3=new SkosConcept();
			c3.setPreflabel(new PrefLabel("coucou"));
			c3.setSkosNotation("levelTwo");
			c3.setScheme(s);
			c3.setUri(graph.addOrGetSimpleConcept(c3));
			
			try {
				graph.link(c2.getUri(), SKOS.BROADER.getURI(), c.getUri());
				graph.link(c3.getUri(), SKOS.BROADER.getURI(), c2.getUri());
			}catch (Exception e){
				e.printStackTrace();
			}
			//graph.statements(true);
			System.out.println("Si infered, on a 1="+graph.getAllBroaders(c2,0,true).getBroaders().size());
			Set<String> uris=graph.getSubDomain(c.getUri().toString());
			for (String uri:uris)System.out.println(uri+" isSUBOF "+c.getUri().getLocalName());
			graph.statements(new URIImpl("http://server.ubiqore.com/v1/concept/toto/super"),true);*/
			
			//graph=new MultiTerminologyWithInferenceGraph(false,"/Reborn/tmp/graphs/test4");
			
		//	graph.statements(new URIImpl("http://server.ubiqore.com/v1/concept/toto/super"),true);
			
			
			
			
			//graph.inverseRes();
			if (graph!=null)graph.disconnect();
		}catch (Exception e){
			e.printStackTrace();
			if (graph!=null)graph.disconnect();
		}
	}
}
