package ubiqore.graphs.semantic;

import info.aduna.iteration.CloseableIteration;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.impl.ContextStatementImpl;
import org.openrdf.model.impl.LiteralImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.OWL;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;


import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;


// Skos Based. (narrower / broader) 
// include automatic inference for owl:inverseOf declared predicates.
// you need to add your inverseOf rules for both sides inside graph ... "A inverseOf B" AND "B inverseOf A" (Vocabulary Statement)


@Deprecated
public class MultiTerminologyGraph extends AbstractGraph {
	
	public MultiTerminologyGraph(boolean creation, String rep)
			throws SailException {
		
		super(creation, rep);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void createVocabularyStatementsList() {
			vocabularyStatements.add(new ContextStatementImpl(SemanticVocabulary.pathLexChapter,RDF.TYPE, RDFS.DATATYPE,context));
			vocabularyStatements.add(new ContextStatementImpl(SKOS.NARROWER.getURI(),OWL.INVERSEOF,SKOS.BROADER.getURI(),context));
			vocabularyStatements.add(new ContextStatementImpl(SKOS.BROADER.getURI(),OWL.INVERSEOF,SKOS.NARROWER.getURI(),context));
			vocabularyStatements.add(new ContextStatementImpl(SKOS.BROADER.getURI(),OWL.EQUIVALENTPROPERTY,RDFS.SUBCLASSOF,context));
			vocabularyStatements.add(new ContextStatementImpl(SKOS.NARROWER.getURI(),OWL.INVERSEOF,RDFS.SUBCLASSOF,context));
			vocabularyStatements.add(new ContextStatementImpl(RDFS.SUBCLASSOF,OWL.INVERSEOF,SKOS.NARROWER.getURI(),context));
	}
    
	public Map<URI,String> searchInDomainNewTest(String search,URI domain) throws SailException, MalformedQueryException, QueryEvaluationException{
		Map<URI,String> l=Maps.newLinkedHashMap();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri ?prefLabel "+
				"WHERE "+
				"{ " +
				" ?uri rdfs:subClassOf+ <"+domain.toString()+"> . "+
	//			" ?uri rdf:type  skos:Concept . " +
				" ?uri skos:prefLabel ?prefLabel . " +
	    		
	//      		" ?uri rdfs:subClassOf+ <"+domain.toString()+"> . "+
				" FILTER regex (str(?prefLabel),\""+search+"\", \"i\" ) " +
				" } "
				;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				l.put(new URIImpl(a.getValue("uri").toString()),((Literal)a.getValue("prefLabel")).getLabel());
			}
		}
		sc.close();
		return l;
	}
	
	public Map<URI,String> searchInDomain(String search,URI domain) throws SailException, MalformedQueryException, QueryEvaluationException{
		Map<URI,String> l=Maps.newLinkedHashMap();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri ?prefLabel "+
				"WHERE "+
				"{ " +
				" ?uri rdfs:subClassOf+ <"+domain.toString()+"> . "+
	//			" ?uri rdf:type  skos:Concept . " +
				" ?uri skos:prefLabel ?prefLabel . " +
				
				"FILTER regex (?prefLabel,\""+search+"\", \"i\" ) " +
				" } "
				;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				l.put(new URIImpl(a.getValue("uri").toString()),((Literal)a.getValue("prefLabel")).getLabel());
			}
		}
		sc.close();
		return l;
	}
	
	
	public Map<URI,String>  searchStringREGEX (String st) throws SailException, MalformedQueryException, QueryEvaluationException{
		Map<URI,String> l=Maps.newLinkedHashMap();
		
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri ?prefLabel "+
							"WHERE "+
							"{ " +
							" ?uri rdf:type  skos:Concept . " +
							" ?uri skos:prefLabel ?prefLabel . " +
							"FILTER regex (?prefLabel,\""+st+"\", \"i\" ) " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				l.put(new URIImpl(a.getValue("uri").toString()),((Literal)a.getValue("prefLabel")).getLabel());
			}
				
		}
		sc.close();
		
		
	 
		return l;
	}
	public Set<URI>  searchInLocalREGEX (String st) throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> set=Sets.newHashSet();
		
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri "+
							"WHERE "+
							"{ " +
							" ?uri rdf:type  skos:Concept . " +
							" ?uri skos:skosNotation ?code . " +
							"FILTER regex (?code,\""+st+"\", \"i\" ) " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				set.add(new URIImpl(a.getValue("uri").toString()));
			}
				
		}
		sc.close();
		
		
	 
		return set;
	}
	
	
	
	@Override
	protected void createNamespacesMap() {
		    // official namespaces..
			this.useClassicNamespaces();
		    // ubiq and others...
	        this.namespaces.put(SemanticVocabulary.skos_prefix,SemanticVocabulary.skos);
	        this.namespaces.put(SemanticVocabulary.ubiq_prefix,SemanticVocabulary.ubiq);
	}
	
	public List<SkosConcept> getOriginalList() throws SailException, MalformedQueryException, QueryEvaluationException{
		List<URI> l=this.getCollectionMembers(createOrGetOriginList());
		List<SkosConcept> rep=Lists.newArrayList();
		for (URI uri:l){
			rep.add(this.getConcept(uri));
		}
		return rep;
	}
	
	public List<SkosConcept> getCodesList(String type,int level) throws SailException, MalformedQueryException, QueryEvaluationException{
		List<URI> base=this.getCollectionMembers(createOrGetOriginList());
		List<SkosConcept> rep=Lists.newArrayList();
		if (type.equalsIgnoreCase(ContextVocabulary.LEVELS)){
			if (level<0)return rep;
			
			if (level==0){ // only the single list.
				for (URI uri:base){
					rep.add(this.getConcept(uri));
				}
			}else {
				Set<String> uris=Sets.newHashSet();
				for (URI uri:base){
					//System.out.println(uri);
					Set<String> set=this.getNarrowers(uri.toString(), level);
					uris.addAll(set);
				}
				for (String uri:uris){
					rep.add(this.getConcept(new URIImpl(uri)));
				}
			}
		}else if (type.equalsIgnoreCase(ContextVocabulary.FULL)){
			Set<String> uris=Sets.newHashSet();
			for (URI uri:base){
				Set<String> set=this.getNarrowers(uri.toString(), 0);
				uris.addAll(set);
			}
			for (String uri:uris){
				rep.add(this.getConcept(new URIImpl(uri)));
			}
		}
		
		
		
		
		return rep;
	}
	
	
	
	public List<SkosConcept> getFullList() throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> l=this.getConceptsURI();
		List<SkosConcept> rep=Lists.newArrayList();
		for (URI uri:l){
			rep.add(this.getConcept(uri));
		}
		return rep;
		
	}
	
	
	
	public URI createOrGetOriginList() throws SailException{
		URI uri=new URIImpl(this.context+SemanticVocabulary.collection+SemanticVocabulary.originList);
		if (this.entityExist(new ContextStatementImpl(uri,RDF.TYPE,SKOS.COLLECTION.getURI(),this.context))!=null){
			return uri;
		}
		this.addSingleStatement(new ContextStatementImpl(uri,RDF.TYPE,SKOS.COLLECTION.getURI(),this.context));
		return uri;
	}
	
	public void addOriginalMember(URI conceptURI){
		try {
			this.link(conceptURI, SKOS.MEMBER.getURI(), createOrGetOriginList());
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// apply means : create and/or get uri. 
	public URI applyScheme(String oid,String prefLabel,String definition) throws SailException{
		URI uri=new URIImpl(this.context+SemanticVocabulary.scheme+oid);
		URI rep=this.entityExist(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPTSCHEME.getURI(),this.context));
		if (rep==null){
			List<Statement> sts=Lists.newArrayList();
			sts.add(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPTSCHEME.getURI(),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.PREFLABEL.getURI(),vf.createLiteral(prefLabel),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.NOTATION.getURI(),vf.createLiteral(oid),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.DEFINITION.getURI(),vf.createLiteral(definition),this.context));
			
			boolean ok=this.addMultipleStatements(sts);
			if (ok)return uri;
		}else {
			//System.out.println("scheme already there darling...");
		}
		return rep;
	}
	
	public Set<URI> getConceptsURI() throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> set=Sets.newHashSet();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x  "+
							"WHERE "+
							"{ " +
							" ?x rdf:type  skos:Concept ."+
							
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
		
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.getValue("x"));
				set.add(new URIImpl(a.getValue("x").stringValue()));
			}
			
		}
		
		sc.close();
		return set;
	 
	}
	
	public Set<URI> getSchemesURI() throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> set=Sets.newHashSet();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x  "+
							"WHERE "+
							"{ " +
							" ?x rdf:type  skos:ConceptScheme ."+
							
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
		
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.getValue("x"));
				set.add(new URIImpl(a.getValue("x").stringValue()));
			}
			
		}
		
		sc.close();
		return set;
	 
	}
	
	Set<Scheme> schemeCache=Sets.newHashSet();
	private Scheme getFromSchemeCache(URI input){
		for (Scheme s:this.schemeCache){
			if (s.getUri().toString().equalsIgnoreCase(input.toString()))return s;
		}
		return null;
	}
	private void addSchemeCache(Scheme s){
		this.schemeCache.add(s);
	}
	
	
	public Scheme getSchemeBase(URI input) throws SailException, MalformedQueryException, QueryEvaluationException{
		Scheme scheme=null;
		Scheme s=this.getFromSchemeCache(input);
		if (s!=null)return s;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?label ?oid ?def  "+
							"WHERE "+
							"{ " +
							"<"+ input.stringValue()+">		rdf:type  skos:ConceptScheme ."+
							"<"+ input.stringValue()+">		skos:prefLabel  ?label ."+
							"<"+ input.stringValue()+">	  	skos:notation  ?oid ."+
							"<"+ input.stringValue()+">	    skos:definition  ?def ."+
							" } "
							;
		/*
		 * sts.add(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPTSCHEME.getURI(),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.PREFLABEL.getURI(),vf.createLiteral(prefLabel),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.NOTATION.getURI(),vf.createLiteral(oid),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.DEFINITION.getURI(),vf.createLiteral(definition),this.context));
			
		 */
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				scheme=new Scheme();
				scheme.setUri(input);
				Literal loid=(Literal)a.getValue("oid");
				Literal llabel=(Literal)a.getValue("label");
				Literal ldef=(Literal)a.getValue("def");
				scheme.setOid(loid.getLabel());
				scheme.setDef(ldef.getLabel());
				scheme.setPreflabel(new PrefLabel(llabel.getLabel()));
				this.addSchemeCache(scheme);
			}
			
			
		}
		sc.close();
		
		return scheme;
	}
	
	
	
	
	public URI conceptExists(String code,String oid)throws SailException{
		URI uri=new URIImpl(this.context+SemanticVocabulary.concept+oid+"/"+code);
		URI rep=this.entityExist(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPT.getURI(),this.context));
		if (rep==null)return null;
		else return uri;
	}
	// no hierarchical there !
	public URI addOrGetSimpleConcept(SkosConcept c) throws SailException{
		
		URI uri=new URIImpl(this.context+SemanticVocabulary.concept+c.getScheme().getOid()+"/"+c.getSkosNotation());
		URI rep=this.entityExist(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPT.getURI(),this.context));
		if (rep==null){
			List<Statement> sts=Lists.newArrayList();
			sts.add(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPT.getURI(),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.PREFLABEL.getURI(),c.getPreflabel().getLit(),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.NOTATION.getURI(),vf.createLiteral(c.getSkosNotation()),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.INSCHEME.getURI(),c.getScheme().getUri(),this.context));
			if (Strings.emptyToNull(c.getPathLexChapter())!=null){
				LiteralImpl chapter=new LiteralImpl(c.getPathLexChapter(),SemanticVocabulary.pathLexChapter);
				sts.add(new ContextStatementImpl(uri, SKOS.NOTATION.getURI(), chapter,this.context));
			}
			
			boolean ok=this.addMultipleStatements(sts);
			if (ok)return uri;
		}
		return rep;
	

	}
	
	public SkosConcept getErsatzConcept(String skosNotation,String schemeId){
		// schemeId can be urn:oid:2342342342" OR "324234234"
		String schemeOID=null;
		if (schemeId.startsWith("urn:oid:"))schemeOID=schemeId.replace("urn:oid:","");
		else schemeOID=schemeId;
		try {
			if (this.conceptExists(skosNotation, schemeOID)!=null){
				URI uri=new URIImpl(this.context+SemanticVocabulary.concept+schemeOID+"/"+skosNotation);
				try {
					return this.getConcept(uri);
				} catch (MalformedQueryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (QueryEvaluationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	
	public SkosConcept getConcept(URI uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		SkosConcept skos=null;
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?code ?scheme ?prefLabel ?codingSchemeURI  "+
							"WHERE "+
							"{ " +
							"<"+ uri.toString()+"> rdf:type  skos:Concept . " +
							"<"+ uri.toString()+"> skos:notation ?code . " +
							"<"+ uri.toString()+"> skos:prefLabel ?prefLabel . " +
							"<"+ uri.toString()+"> skos:inScheme ?scheme . " +
							// "?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?code) = xsd:string ) " +
						 //	"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			skos=new SkosConcept();
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				Literal v=(Literal)a.getValue("prefLabel");
				skos.setPreflabel(new PrefLabel(v));
				//System.out.println(skos.getPreflabel().getLit().getLabel());
				//System.out.println(skos.getPreflabel().getLit().getLanguage());
				skos.setScheme(this.getSchemeBase(new URIImpl(a.getValue("scheme").stringValue())));
				skos.setSkosNotation(a.getValue("code").stringValue());
				skos.setUri(uri);
				break;
			}
			
		}
		sc.close();
		return skos;
	}
	
	public SkosConcept getFullConcept(URI uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SkosConcept c=this.getConcept(uri);
		c.setBroaders(this.getBroaders(c));
		c.setNarrowers(this.getNarrowers(c));
		return c;
	}
	public boolean addTopConcept(SkosConcept skosConcept) throws SailException{
		return this.addSingleStatement(new ContextStatementImpl(skosConcept.getUri(), SKOS.TOPCONCEPTOF.getURI(), skosConcept.getScheme().getUri(),context));
	}
	
	public Set<URI> getTopConceptsOfScheme(URI schemeURI)throws SailException, MalformedQueryException, QueryEvaluationException,Exception{
		SailConnection sc=sail.getConnection();
		Set<URI> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x  "+
							"WHERE "+
							"{ " +
							" ?x skos:topConceptOf <" + schemeURI.toString()+"> ."+
							
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				
			//	System.out.println(a.getValue("x"));
				set.add(new URIImpl(a.getValue("x").stringValue()));
			}
			
		}
		sc.close();
		
		return set;
	}
	
	public Set<String> getBroaders(String uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri   "+
							"WHERE "+
							"{ " +
							" ?uri rdf:type  skos:Concept . " +
							" <"+ uri+"> skos:broader ?uri . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				set.add(uriB);
			}
			
			
		}
		sc.close();
		return set;
	}
	public Set<SkosConcept> getBroaders(SkosConcept skos) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<SkosConcept> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri   "+
							"WHERE "+
							"{ " +
							" ?uri rdf:type  skos:Concept . " +
							" <"+ skos.getUri().toString()+"> skos:broader ?uri . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		Set<String> uris=Sets.newHashSet();
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				uris.add(uriB);
			}
			
			
		}
		sc.close();
		
		for (String uri:uris){
			SkosConcept broader=this.getConcept(new URIImpl(uri));
			set.add(broader);
		}
		
		return set;
	}
	// should not be used --> kept to show bad example !
	@Deprecated
	public Set<String> getNarrowersBest(Set<String> uris,int level) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT  ?y   "+
							"WHERE "+
							"{";
		queryString+=		  " ?y rdf:type  skos:Concept . ";
		if (level>1){queryString+=" ?x skos:narrower{1,"+level+"} ?y . ";}
		if (level==0){queryString+=" ?x skos:narrower+  ?y . ";}
		if (level==1){queryString+=" ?x skos:narrower  ?y . ";}
		
		
		
		queryString+= "FILTER ( ?x IN ( ";
		int i=0;
		for (String uri:uris){
			if (i==0)queryString+="<"+uri+"> ";
			else queryString+=", <"+uri+"> ";
			i++;
		}
		queryString+=") ) } ";
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("y").toString();
				
				set.add(uriB);
			}
			
			
		}
		sc.close();
		
		/*if (level>1||level==0){
			Set<String> toAdd=Sets.newHashSet();
			for (String sub:set){
				int sublevel=0;
				if (level>1)sublevel=-level-1;
				toAdd.addAll(this.getNarrowers(sub, sublevel));
			}
			set.addAll(toAdd);
		}*/
		
		
		return set;
	}
	
	public Set<String> getBagOfNarrowers(String uri,int level) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?narrowURI  "+
							"WHERE "+
							"{ ";
		//queryString+=		  " ?narrowURI rdf:type  skos:Concept . ";
		if (level>1){queryString+="<"+uri+"> skos:narrower{1,"+level+"} ?narrowURI . ";}
		if (level==0){queryString+="<"+uri+"> skos:narrower+  ?narrowURI . ";}
		if (level==1){queryString+="<"+uri+"> skos:narrower  ?narrowURI . ";}
		queryString+=       " } ";
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			System.out.println("Hasext OK");
			int i=0;
			while (sparqlResults.hasNext()){
				//System.out.println("Hasext OK"+i++);
				BindingSet a=sparqlResults.next();
				String narrowURI=a.getValue("narrowURI").toString();
				set.add(narrowURI);
			
			}
			
			
		}
		sc.close();
		
		return set;
	}
	
	
	public Set<String> getNarrowersBest(int level) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		URI origin=this.createOrGetOriginList();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		set.addAll(this.getCollectionMembersString(origin));
		System.out.println("Before Narrow="+set.size());
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?y ?x  "+
							"WHERE "+
							"{ ";
		queryString+=         " ?x skos:member <"+ origin.toString()+"> .";
		//queryString+=		  " ?y rdf:type  skos:Concept . ";
		if (level>1){queryString+=" ?x skos:narrower{1,"+level+"} ?y . ";}
		if (level==0){queryString+=" ?x skos:narrower+  ?y . ";}
		if (level==1){queryString+=" ?x skos:narrower  ?y . ";}
		
		
		queryString+=" } ";
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		int cptX=0;
		int cptY=0;
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriS=a.getValue("y").toString();
				String uriO=a.getValue("x").toString();
				boolean addedX=set.add(uriO);
				boolean addedY=set.add(uriS);
				if (addedX)cptX++;
				if (addedY)cptY++;
			}
			
			
		}
		sc.close();
		System.out.println("cptX="+cptX);
		System.out.println("cptX="+cptY);
		/*if (level>1||level==0){
			Set<String> toAdd=Sets.newHashSet();
			for (String sub:set){
				int sublevel=0;
				if (level>1)sublevel=-level-1;
				toAdd.addAll(this.getNarrowers(sub, sublevel));
			}
			set.addAll(toAdd);
		}*/
		
		
		return set;
	}
	
	
	@Deprecated
	public Set<String> getNarrowersBest(String uri,int level) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri   "+
							"WHERE "+
							"{";
		queryString+=		  " ?uri rdf:type  skos:Concept . ";
		if (level>1){queryString+=" <"+ uri+"> skos:narrower{1,"+level+"} ?uri . ";}
		if (level==0){queryString+=" <"+ uri+"> skos:narrower+  ?uri . ";}
		if (level==1){queryString+=" <"+ uri+"> skos:narrower  ?uri . ";}
		
		
		queryString+=		" } ";
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				
				set.add(uriB);
			}
			
			
		}
		sc.close();
		
		/*if (level>1||level==0){
			Set<String> toAdd=Sets.newHashSet();
			for (String sub:set){
				int sublevel=0;
				if (level>1)sublevel=-level-1;
				toAdd.addAll(this.getNarrowers(sub, sublevel));
			}
			set.addAll(toAdd);
		}*/
		
		
		return set;
	}
	
	
	
	@Deprecated
	public Set<String> getNarrowers(String uri,int level) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT  ?uri   "+
							"WHERE "+
							"{";
		if (level>=2)queryString+="{";
		queryString+=		  " ?uri rdf:type  skos:Concept . ";
		if (level==0){queryString+=" <"+ uri+"> skos:narrower+ ?uri . ";}
		else if (level==1){queryString+=" <"+ uri+"> skos:narrower ?uri . ";}
		else if (level>=2){
			queryString+=" <"+ uri+"> skos:narrower ?uri . } ";
			String skosN="skos:narrower";
			for (int i=2;i<=level;i++){
				skosN+="/skos:narrower";
				queryString+=" UNION { ?uri rdf:type  skos:Concept .   <"+ uri+"> "+skosN+" ?uri } " ;
			}
			
			
		}
		queryString+=		" } ";
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				
				set.add(uriB);
			}
			
			
		}
		sc.close();
		
		/*if (level>1||level==0){
			Set<String> toAdd=Sets.newHashSet();
			for (String sub:set){
				int sublevel=0;
				if (level>1)sublevel=-level-1;
				toAdd.addAll(this.getNarrowers(sub, sublevel));
			}
			set.addAll(toAdd);
		}*/
		
		
		return set;
	}
	
	
	
	@Deprecated
	public Set<String> getNarrowers(String uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri   "+
							"WHERE "+
							"{ " +
							" ?uri rdf:type  skos:Concept . " +
							" <"+ uri+"> skos:narrower ?uri . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				set.add(uriB);
			}
			
			
		}
		sc.close();
		return set;
	}
	
	
	private Set<SkosConcept> getNarrowersOposite(SkosConcept skos) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<SkosConcept> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri   "+
							"WHERE "+
							"{ " +
							" ?uri rdf:type  skos:Concept . " +
							" ?uri skos:broader  <"+ skos.getUri().toString()+">  . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			Set<String> uris=Sets.newHashSet();
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				uris.add(uriB);
			}
			
			for (String uri:uris){
				SkosConcept narrower=this.getConcept(new URIImpl(uri));
				set.add(narrower);
			}
		}
		sc.close();
		
		return set;
	}
	
	
	
	public Set<SkosConcept> getNarrowers(SkosConcept skos) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<SkosConcept> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri   "+
							"WHERE "+
							"{ " +
				//			" ?uri rdf:type  skos:Concept . " +
							" <"+ skos.getUri().toString()+"> skos:narrower ?uri . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			Set<String> uris=Sets.newHashSet();
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				uris.add(uriB);
			}
			
			for (String uri:uris){
				SkosConcept narrower=this.getConcept(new URIImpl(uri));
				set.add(narrower);
			}
		}
		sc.close();
		
		//if (set.isEmpty())return this.getNarrowersOposite(skos);
		return set;
	}
	
	public SkosConcept getAllNarrowers(SkosConcept skos ,int lim,boolean start) throws SailException, MalformedQueryException, QueryEvaluationException{
		 boolean nolimit=false;
		 int level=0;
		 if (start && lim==0){
			 nolimit=true;
		 }
		 else if (lim==0) {
			 return skos;
		 }
		 else level=lim-1;
		 
		 Set<SkosConcept> skosSet=this.getNarrowers(skos);
		 skos.setNarrowers(skosSet);
		 for (SkosConcept s:skos.getNarrowers()){
			 this.getAllNarrowers(s,level,nolimit);
		 }
		
		 return skos;
	}
	
	public SkosConcept getAllBroaders(SkosConcept skos ,int lim,boolean start) throws SailException, MalformedQueryException, QueryEvaluationException{
		 boolean nolimit=false;
		 int level=0;
		 if (start && lim==0){
			 nolimit=true;
		 }
		 else if (lim==0) {
			 return skos;
		 }
		 else level=lim-1;
		 
		 Set<SkosConcept> skosSet=this.getBroaders(skos);
		 skos.setBroaders(skosSet);
		 for (SkosConcept s:skosSet){
			 this.getAllBroaders(s,level,nolimit);
		 }
		
		 return skos;
	}
	
	public static void main (String ...args){
		MultiTerminologyGraph mtg=null;
		try {
			mtg=new MultiTerminologyGraph(false,"/Reborn/tmp/sem/test1");
			/*mtg.applyScheme("1234242342","coucou","A good day sunshine");
			mtg.applyScheme("1234242342","coucou","A good day sunshine");
			mtg.applyScheme("12342dfsdf42342","coucou","A good day sunshine");
			mtg.applyScheme("1234242342","coucou","A good day sunshine");
			mtg.applyScheme("1234242342","coucou","A good day sunshine");
			
			mtg.applyScheme("12342dfsd42342","coucou","A good day sunshine");
			mtg.applyScheme("1234242342","coucou","A good day sunshine");*/
			Set<URI> uris=mtg.getSchemesURI();
			for (URI uri:uris){
				System.out.println(uri.toString());
				System.out.println(mtg.getSchemeBase(uri).toString());
			}
			mtg.statements();
			try {
				Set<URI> set=mtg.getSchemesURI();
				for (URI uri : set){
					System.out.println(uri.getLocalName());
				}
			} catch (MalformedQueryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (QueryEvaluationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			mtg.disconnect();
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
