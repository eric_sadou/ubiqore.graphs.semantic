package ubiqore.graphs.semantic;

import org.openrdf.model.URI;
   
public class Alignement {
	URI  entity1;
	public URI getEntity1() {
		return entity1;
	}
	public void setEntity1(URI entity1) {
		this.entity1 = entity1;
	}
	public URI getEntity2() {
		return entity2;
	}
	public void setEntity2(URI entity2) {
		this.entity2 = entity2;
	}
	public String getRelation1to2() {
		return relation1to2;
	}
	public void setRelation1to2(String relation1to2) {
		this.relation1to2 = relation1to2;
	}
	public Float getMeasure() {
		return measure;
	}
	public void setMeasure(Float measure) {
		this.measure = measure;
	}
	URI  entity2;
	String relation1to2;
	Float measure;
}
