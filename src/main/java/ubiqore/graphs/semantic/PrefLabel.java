package ubiqore.graphs.semantic;

import java.util.Locale;

import org.openrdf.model.Literal;

public class PrefLabel extends Label {

	public PrefLabel(String st) {
		super(st);
		// TODO Auto-generated constructor stub
	}
  
	public PrefLabel(Literal lit){
		
		super(lit.getLabel());
		this.langage=new Locale(lit.getLanguage());
	}
	
	
}
