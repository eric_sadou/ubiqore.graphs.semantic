package ubiqore.graphs.semantic;

import info.aduna.iteration.CloseableIteration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.openrdf.model.Literal;
import org.openrdf.model.URI;
import org.openrdf.model.impl.ContextStatementImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.impl.BindingImpl;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.repository.sparql.query.SPARQLQueryBindingSet;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
  
public class ContextElementsGraph  extends AbstractGraph{

	List<Category> topCategories=Lists.newArrayList();
	
	private void initTopCategory(){
		if (topCategories.isEmpty()){
			for (int i=0;i<8;i++){
				Category category=new Category();
				category.setURI(this.context+ContextVocabulary.DataElement.getLocalName()+"/"+"EHR4CR_VS_1."+i);
				category.setVSURI(this.context+ContextVocabulary.DataElement.getLocalName()+"/"+"EHR4CR_VS_1."+i);
				this.topCategories.add(category);
			}
			
		}
	}
	public void cachCategory(Category cat){
		initTopCategory();
		this.topCategories.add(cat);
	}
	public String manageAndGetTopCategory(String superValueSet,String myDATAELEMENT){
		initTopCategory();
		String retour=null;
		Category newCat=null;
		// si on a la cat pour myValueSet .. pas besoin de créer autre chose ...
		for (Category cat:topCategories){
			if (cat.getVSURI().equalsIgnoreCase(myDATAELEMENT)){
				return cat.getURI();
			}
		}
		for (Category cat:topCategories){
			if (cat.getVSURI().equalsIgnoreCase(superValueSet)){
				retour=cat.getURI();
				
				newCat=new Category();
				newCat.setURI(cat.getURI());
				newCat.setVSURI(myDATAELEMENT);
				break;
			}
		}
		if (newCat!=null)topCategories.add(newCat);
		System.out.println(myDATAELEMENT+" "+ retour +topCategories.size());
		return retour;
	}
	
	public ContextElementsGraph(boolean creation, String rep) throws SailException {
		super(creation, rep);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void createVocabularyStatementsList() {
		// TODO Auto-generated method stub
		this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.DataElement,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.ROOT,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.TOPCAT,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.CAT,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.CE_CD,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.CE_PQ,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.CE_TS,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.CE_VS,RDF.TYPE, RDFS.DATATYPE,context));
		
		
		this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.elementType,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.value,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.ucumCode,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.ucumName,RDF.TYPE, RDFS.DATATYPE,context));
		
		//this.vocabularyStatements.add(new ContextStatementImpl(DEVoc.DE,RDF.TYPE, RDFS.DATATYPE,context));
		//this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.CD,RDF.TYPE, RDFS.DATATYPE,context));
		//this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.PQ,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.CE_CO,RDF.TYPE, RDFS.DATATYPE,context));
		//this.vocabularyStatements.add(new ContextStatementImpl(ContextVocabulary.TS,RDF.TYPE, RDFS.DATATYPE,context));
		//this.vocabularyStatements.add(new ContextStatementImpl(DEVoc.SC,RDF.TYPE, RDFS.DATATYPE,context));
	}

	@Override
	protected void createNamespacesMap() {
		// TODO Auto-generated method stub
		 // official namespaces..
		// official are managed directly by SAIL. (OWL, RDF, RDFS..)
		this.useClassicNamespaces();
	    // ubiq and others...
        this.namespaces.put(ContextVocabulary.skos_prefix,ContextVocabulary.skos);
        this.namespaces.put(ContextVocabulary.ubiq_prefix,ContextVocabulary.ubiq);
        this.setContext(new URIImpl(ContextVocabulary.context_v1));
	}
	
	
	
	/*
	 
	 * A DataElementEntry can be CE_CD, DE_CO, DE_PQ , CE_TS (?), CE_VS
	 * CD_URI : CD --> URI from MultiTerm Graph.
	 *  metadata (CD / CO etc..) 
	 */
	public URI addDataElementEntry(URI cdUri,Rank rank,String metadata,Unit unit,String dataElementId,String valueSetPrefLabel ,URI valueSetURI,Set<URI> topBroaders) throws SailException{
		
		URI dataElementURI=null;
		URI semType=null;
		
		
		if (metadata.equalsIgnoreCase(ContextVocabulary.CE_VS.getLocalName())){
			semType=ContextVocabulary.CE_VS;
			dataElementURI=new URIImpl(this.context+ContextVocabulary.DataElement.getLocalName()+"/"+dataElementId);
		}
		else if (metadata.equalsIgnoreCase(ContextVocabulary.CE_CD.getLocalName())) {
			semType=ContextVocabulary.CE_CD;
			dataElementURI=new URIImpl(cdUri+"/CE_CD");
		}
		else if (metadata.equalsIgnoreCase(ContextVocabulary.CE_PQ.getLocalName())) {
			semType=ContextVocabulary.CE_PQ;
			dataElementURI=new URIImpl(cdUri+"/CE_PQ");
		}
		else if (metadata.equalsIgnoreCase(ContextVocabulary.CE_TS.getLocalName())) {
			semType=ContextVocabulary.CE_TS;
			dataElementURI=new URIImpl(cdUri+"/CE_TS");
		}
		else if (metadata.equalsIgnoreCase(ContextVocabulary.CE_CO.getLocalName())) {
			semType=ContextVocabulary.CE_CO;
			dataElementURI=new URIImpl(cdUri+"/CE_CO");
		}
		
		
		else if (metadata.equalsIgnoreCase(ContextVocabulary.TOPCAT.getLocalName())) {
			semType=ContextVocabulary.TOPCAT;
			dataElementURI=new URIImpl(this.context+ContextVocabulary.DataElement.getLocalName()+"/"+dataElementId);
		}
		
		else if (metadata.equalsIgnoreCase(ContextVocabulary.ROOT.getLocalName())) {
			semType=ContextVocabulary.ROOT;
			dataElementURI=new URIImpl(this.context+ContextVocabulary.DataElement.getLocalName()+"/"+dataElementId);
		}
		
		else return null;
		
		
		
		
		
		boolean processIsAnUpdate=false;
		try {
			if (this.dataElementExist(dataElementURI)){
				processIsAnUpdate=true;
			}
		} catch (MalformedQueryException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return dataElementURI;
		} catch (QueryEvaluationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return dataElementURI;
		}
		
		
		SailConnection sc= sail.getConnection();

		
		
		if (!processIsAnUpdate){
			// creation de l'entitée de type DATAELEMENT: 
			sc.addStatement(dataElementURI,RDF.TYPE,ContextVocabulary.DataElement,context);
			
			// ajout ds le bag du valueSet superieur :
			if (valueSetURI!=null){
				sc.addStatement(dataElementURI,SKOS.MEMBER.getURI(),valueSetURI,context);
				String topCategory=this.manageAndGetTopCategory(valueSetURI.stringValue(), dataElementURI.stringValue());
				sc.addStatement(dataElementURI,ContextVocabulary.topCategory,new URIImpl(topCategory),context);
			}
			
			//  ajout de la metadata semantique : modification CE_CD est concerné egalemet U453/CD URI dU CE_CD  U453 --CD type ... U43 related to U453/CD
			sc.addStatement(dataElementURI,ContextVocabulary.elementType,semType ,context);
			// ajout de la metadata semantique : 
			/*if (semType!=DEVoc.CE_CD){
				
			}else {
				sc.addStatement(dataElementURI,DEVoc.elementType,DEVoc.CD ,context);
			}*/
			
			// ajout d'information sur les types collections : 
			if (semType==ContextVocabulary.CE_VS){
				sc.addStatement(dataElementURI,RDF.TYPE,SKOS.COLLECTION.getURI(),context);
			}
		}
		
		
		
		// ajout d'un Label au DATAELEMENT , certains Elements n'ont pas de concepts "related"
		if (Strings.emptyToNull(valueSetPrefLabel)!=null){
			sc.addStatement(dataElementURI,SKOS.PREFLABEL.getURI(), vf.createLiteral(valueSetPrefLabel),context);
		}
		
		// relié l'eventuel et probable concept a son element.
		if (cdUri!=null){
			// on relie le concept a ce nouvel element.
			
			// on crée l'info CD pour cet element terminologique pour tous les VS : 
			if (semType.equals(ContextVocabulary.CE_VS)||semType.equals(ContextVocabulary.TOPCAT)){
			//	sc.addStatement(cdUri, ContextVocabulary.elementType, ContextVocabulary.CD, context);
				sc.addStatement(dataElementURI,SKOS.RELATED.getURI(),cdUri, context);
			}
			/*else if (semType.equals(ContextVocabulary.CD)){
				// on le relie au terme ... 
				sc.addStatement(cdUri, ContextVocabulary.elementType, ContextVocabulary.CD, context);
				sc.addStatement(dataElementURI,SKOS.RELATED.getURI(),cdUri, context);
				
			}*/
			else if (semType.equals(ContextVocabulary.CE_CO)){
				// ( CO : Un Value + CD related (normalemennt = code)
				//sc.addStatement(cdUri, ContextVocabulary.elementType, ContextVocabulary.CD, context);
				sc.addStatement(dataElementURI,SKOS.RELATED.getURI(),cdUri, context);
				sc.addStatement(dataElementURI,ContextVocabulary.value,vf.createLiteral(rank.getValue()), context);
				sc.removeStatements(dataElementURI, SKOS.PREFLABEL.getURI(), null, context);
				sc.addStatement(dataElementURI,SKOS.PREFLABEL.getURI() ,vf.createLiteral(rank.getOriginalRank()) , context);
				
			}
			else if (semType.equals(ContextVocabulary.CE_PQ)){
				// ( PQ : Un Value + CD related (normalemennt = code)
				//sc.addStatement(cdUri, ContextVocabulary.elementType, ContextVocabulary.CD, context);
				sc.addStatement(dataElementURI,SKOS.RELATED.getURI(),cdUri, context);
				sc.addStatement(dataElementURI,ContextVocabulary.ucumCode,vf.createLiteral(unit.getUcumValue()), context);
				sc.addStatement(dataElementURI,ContextVocabulary.ucumName,vf.createLiteral(unit.getUcumDefinition()), context);
			}
			else if (semType.equals(ContextVocabulary.CE_TS)||semType.equals(ContextVocabulary.CE_CD)){
				// TS ou CD meme combat
				//sc.addStatement(cdUri, ContextVocabulary.elementType, ContextVocabulary.CD, context);
				sc.addStatement(dataElementURI,SKOS.RELATED.getURI(),cdUri, context);
			}
		   
			if (topBroaders!=null){
				for (URI uri:topBroaders){
					sc.addStatement(cdUri, SKOS.BROADER.getURI(),uri, context);
				}
			}
		
		}
		
		
		try {
			sc.commit();
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			sc.close();
		}
		return dataElementURI;
	}
	
	/*
	* here we declare a new DataElement From ValueSet (TOP_CAT / ROOT ) which info are mainly in DE_sheet. (CE_VS ??)
	* 
	* The infos added here from VS_sheet (hierarchical info)
	 */
	public URI declareSimpleDataElementEntry(String metadata,String dataElementId,URI superValueSetURI,String name,URI scheme) throws SailException{
		
		URI dataElementURI=null;
		URI semType=null;
		if (metadata.equalsIgnoreCase(ContextVocabulary.TOPCAT.getLocalName())){
			semType=ContextVocabulary.TOPCAT;
		}else if (metadata.equalsIgnoreCase(ContextVocabulary.ROOT.getLocalName())){ // 
			semType=ContextVocabulary.ROOT;
		}else if (metadata.equalsIgnoreCase("CD")){ // ValueSet 
			semType=ContextVocabulary.CE_VS;
		}else if (metadata.equalsIgnoreCase("CO")) { // ValueSet
			semType=ContextVocabulary.CE_VS;
		}
		else return null;
		
		dataElementURI=new URIImpl(this.context+ContextVocabulary.DataElement.getLocalName()+"/"+dataElementId);
		
		
		try {
			if (this.dataElementExist(dataElementURI)){
				System.out.println("dataElement Entry already exists !! nothing was add !! ");
				return dataElementURI;
			}
		} catch (MalformedQueryException e1) {
			
			e1.printStackTrace();
			return dataElementURI;
		} catch (QueryEvaluationException e1) {
			
			e1.printStackTrace();
			return dataElementURI;
		}
		
		
		SailConnection sc=sail.getConnection();

		// creation de l'entitée de type DATAELEMENT // TOUT EST DATAELEMENT.
		sc.addStatement(dataElementURI,RDF.TYPE,ContextVocabulary.DataElement,context);
		// ajout ds le bag du valueSet superieur :
		if (superValueSetURI!=null){
			sc.addStatement(dataElementURI,SKOS.MEMBER.getURI(),superValueSetURI,context);
			String topCategory=this.manageAndGetTopCategory(superValueSetURI.stringValue(), dataElementURI.stringValue());
			sc.addStatement(dataElementURI,ContextVocabulary.topCategory,new URIImpl(topCategory),context);
		}
		else {
			// cas du ValueSetCategory haut niveau 
			if (Strings.emptyToNull(name)!=null){
				sc.addStatement(dataElementURI,SKOS.PREFLABEL.getURI(), vf.createLiteral(name),context);
			}
			// ajout on top of the scheme.
			this.setTopConcept(dataElementURI, scheme);
		}
		
		// ajout de la metadata semantique elementType : 
		sc.addStatement(dataElementURI,ContextVocabulary.elementType,semType ,context);
		
		// tous les élements sont des Collections (TopCat / Root / CE_VS)
		sc.addStatement(dataElementURI,RDF.TYPE,SKOS.COLLECTION.getURI(),context);
		
		
		try {
			sc.commit();
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			sc.close();
		}
		return dataElementURI;
	}
	
	
	
	public boolean dataElementExist(URI uri)throws SailException, MalformedQueryException, QueryEvaluationException{
		boolean rep=false;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x  "+
							"WHERE "+
							"{ " +
							 " <"+uri.toString()+"> rdf:type ubiq:DataElement  ."+
							  " <"+ uri.toString() + ">  skos:notation    ?x . " +
							" } "
							;
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		if (sparqlResults.hasNext()){
			rep=true;
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				System.out.println(a);
			}
		}
		sc.close();
		return rep;
	}
	
	public Set<String> getUCUMCodeList()throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<String> ucums=Sets.newHashSet();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?code  "+
							"WHERE "+
							"{ " +
							 " ?x  ubiq:ucumCode ?code ."+
							" } "
							;
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		if (sparqlResults.hasNext()){
			String codeUcum=null;
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				codeUcum=a.getValue("code").stringValue();
				ucums.add(codeUcum);
				
			}
		}
		sc.close();
		
		return ucums;

	}
	
	public DataElement getRoot() throws SailException, MalformedQueryException, QueryEvaluationException{
		String root=null;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?root  "+
							"WHERE "+
							"{ " +
							 " ?root  rdf:type ubiq:DataElement  ."+
							 " ?root  ubiq:elementType ubiq:ROOT  . " +
							" } "
							;
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		if (sparqlResults.hasNext()){
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				root=a.getValue("root").stringValue();
				break;
			}
		}
		sc.close();
		
		return this.getDataElementByURI(new URIImpl(root),true);

	}
	
	@Deprecated
	public DataElement getRootOld(){
		try {
			return this.getDataElementByURI(new URIImpl("http://server.ubiqore.com/v1/DataElement/EHR4CR_VS_1"),true);
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public URI addScheme(String schemeId) throws SailException{
		/*
		 * 1 verifier que l'URI est unique. TODO
		 * 2. ajouter le schemeConcept.
		 */
		URI scheme=new URIImpl(this.context+ContextVocabulary.scheme+schemeId);
		SailConnection sc= sail.getConnection();
		sc.addStatement(scheme,RDF.TYPE,SKOS.CONCEPTSCHEME.getURI(),context);
		sc.addStatement(scheme, SKOS.PREFLABEL.getURI() ,vf.createLiteral(scheme.getLocalName()),context);
		sc.commit();sc.close();
		return scheme;
	}
	
	public boolean setTopConcept(URI uriValueSet,URI scheme) throws SailException{
		return this.addSingleStatement(new ContextStatementImpl(uriValueSet, SKOS.TOPCONCEPTOF.getURI(),scheme,context));
		
	}
	
	public URI getTopCategory(URI valueSetURI )throws SailException, MalformedQueryException, QueryEvaluationException{
		URI  answer=null;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x   "+
							"WHERE "+
							"{ " +
							"<"+ valueSetURI.toString()+"> rdfs:subClassOf  ?x . " +
							"? x  skos:member <http://server.ubiqore.com/v1/DATAELEMENT/EHR4CR_VS_1>. " +
							
						/*	
							"?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?metadata) = xsd:string ) " +
							"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +*/
							" } "
							;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				answer=new URIImpl(a.getValue("x").stringValue());
				
				break;
			}
			
		}
		sc.close();
		
		return answer;
	}
	
	
	/*
	 * Recuperer la list des DataElements pour lesquels le related term est --> conceptURI)
	 */
	public List<DataElement> getDataElementFromConceptUri(String conceptURI)throws SailException, MalformedQueryException, QueryEvaluationException{
		List<DataElement> answer=Lists.newArrayList();;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri  "+
							"WHERE "+
							"{ " +
							"?uri rdf:type  ubiq:DataElement . " +
							" ?uri skos:related <"+conceptURI+ "> . " +
						/*	
							"?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?metadata) = xsd:string ) " +
							"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +*/
							" } "
							;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		ArrayList<String> st=Lists.newArrayList();
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				st.add(a.getValue("uri").stringValue());
				
			}
			
		}
		sc.close();
		
		for (String uri:st){
			answer.add(this.getDataElementByURI(new URIImpl(uri),true));
		}
		return answer;
	}
	/*
	 * to use with related concept URI.
	 */
	public Set<URI> getTopBroaders(URI uri)throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> set=Sets.newHashSet();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?broader  "+
							"WHERE "+
							"{ " +
							" <"+uri.stringValue()+"> skos:broader ?broader   ."+
							
							" } "
							;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
		
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.getValue("c"));
				boolean x=set.add(new URIImpl(a.getValue("broader").stringValue()));
				if (!x)System.out.println(a.getValue("broader").stringValue());
				
			}
			
		}
		
		sc.close();
		return set;
	 
	}
	
	// not be used for ROOT SYSTEM DATAELEMENT !!!! (wil send null) 
	public DataElement getDataElementByURI(URI dataElementURI,boolean direct )throws SailException, MalformedQueryException, QueryEvaluationException{
		DataElement answer=null;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?metadata ?concept ?sup ?topCat ?label ?value ?ucumName ?ucumCode  "+
							"WHERE "+
							"{ " +
							"<"+ dataElementURI.toString()+"> rdf:type  ubiq:DataElement . " +
							"<"+ dataElementURI.toString()+"> ubiq:elementType ?metadata . " +
							"<"+ dataElementURI.toString()+"> skos:prefLabel ?label . " +
							"OPTIONAL { <"+ dataElementURI.toString()+"> skos:member ?sup }. " +
							"OPTIONAL { <"+ dataElementURI.toString()+"> ubiq:topCategory ?topCat } . " +
							"OPTIONAL { <"+ dataElementURI.toString()+"> skos:related ?concept } . " +
							
							"OPTIONAL { <"+ dataElementURI.toString()+"> ubiq:value ?value } . " +
							"OPTIONAL { <"+ dataElementURI.toString()+"> ubiq:ucumName ?ucumName } . " +
							"OPTIONAL { <"+ dataElementURI.toString()+"> ubiq:ucumCode ?ucumCode } " +
						/*	
							"?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?metadata) = xsd:string ) " +
							"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +*/
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				answer=new DataElement();
				answer.setUri(dataElementURI.stringValue());
				answer.setMetadata(a.getValue("metadata").stringValue());
				answer.setLabel(a.getValue("label").stringValue());
				if (a.getValue("sup")!=null)answer.setMemberOfUri(a.getValue("sup").stringValue());
				if (a.getValue("concept")!=null)answer.setRelatedCD(a.getValue("concept").stringValue());
				if (a.getValue("topCat")!=null)answer.setTopCategoryURI(a.getValue("topCat").stringValue());
				if (a.getValue("ucumName")!=null)answer.setPqUcumName(a.getValue("ucumName").stringValue());
				if (a.getValue("ucumCode")!=null)answer.setPqUcumCode(a.getValue("ucumCode").stringValue());
				if (a.getValue("value")!=null)answer.setCoValue(new Float(a.getValue("value").stringValue()));
				/*Literal v=(Literal)a.getValue("prefLabel");
				skos.setPreflabel(new PrefLabel(v));
				System.out.println(skos.getPreflabel().getLit().getLabel());
				System.out.println(skos.getPreflabel().getLit().getLanguage());
				skos.setScheme(new URIImpl(a.getValue("scheme").stringValue()));
				skos.setCodingSchemeURI(a.getValue("codingSchemeURI").stringValue());
				skos.setSkosNotation(a.getValue("code").stringValue());
				skos.setUri(uri);*/
				break;
			}
			
		}
		
		sc.close();
		
		if (answer!=null){
			if (direct){
				if (answer.getMetadata().equalsIgnoreCase(ContextVocabulary.CE_VS.toString())||
						answer.getMetadata().equalsIgnoreCase(ContextVocabulary.TOPCAT.toString())||
						answer.getMetadata().equalsIgnoreCase(ContextVocabulary.ROOT.toString())){
					    answer=this.getCDMembers(answer);
				}
			}
		}
		
		return answer;
		
	}
	
		
	
	
	public void objectURI(String uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x ?y   "+
							"WHERE "+
							"{ " +
							" ?x ?y  <"+ uri+">  . " +
							
							
						/*	
							"?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?metadata) = xsd:string ) " +
							"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +*/
							" } "
							;
		
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
			
				System.out.println(a.getValue("x").stringValue());
				
				System.out.println(a.getValue("y").stringValue());
				
				/*Literal v=(Literal)a.getValue("prefLabel");
				skos.setPreflabel(new PrefLabel(v));
				System.out.println(skos.getPreflabel().getLit().getLabel());
				System.out.println(skos.getPreflabel().getLit().getLanguage());
				skos.setScheme(new URIImpl(a.getValue("scheme").stringValue()));
				skos.setCodingSchemeURI(a.getValue("codingSchemeURI").stringValue());
				skos.setSkosNotation(a.getValue("code").stringValue());
				skos.setUri(uri);*/
				
			}
			
		}
		sc.close();
	}
	
	public void subjectURI(String uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x ?y   "+
							"WHERE "+
							"{ " +
							"  <"+ uri+">  ?x ?y  . " +
							
							
						/*	
							"?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?metadata) = xsd:string ) " +
							"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +*/
							" } "
							;
		
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
			
				System.out.println(a.getValue("x").stringValue());
				
				System.out.println(a.getValue("y").stringValue());
				
				/*Literal v=(Literal)a.getValue("prefLabel");
				skos.setPreflabel(new PrefLabel(v));
				System.out.println(skos.getPreflabel().getLit().getLabel());
				System.out.println(skos.getPreflabel().getLit().getLanguage());
				skos.setScheme(new URIImpl(a.getValue("scheme").stringValue()));
				skos.setCodingSchemeURI(a.getValue("codingSchemeURI").stringValue());
				skos.setSkosNotation(a.getValue("code").stringValue());
				skos.setUri(uri);*/
				
			}
			
		}
		sc.close();
	}
    
	
	
	
	
		
	
	
	
	public DataElement getCDMembers(DataElement vse)throws SailException, MalformedQueryException, QueryEvaluationException{
		DataElement answer=vse;
		String valueSetURI=vse.getUri();
		SailConnection sc=sail.getConnection();
		ArrayList<String> lom=new ArrayList<String>();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?member   "+
							"WHERE "+
							"{ " +
							"  ?member skos:member  <"+ valueSetURI+">  . " +
							
						/*	
							"?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?metadata) = xsd:string ) " +
							"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +*/
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
			//	System.out.println(a.toString());
				lom.add(a.getValue("member").stringValue());
				/*Literal v=(Literal)a.getValue("prefLabel");
				skos.setPreflabel(new PrefLabel(v));
				System.out.println(skos.getPreflabel().getLit().getLabel());
				System.out.println(skos.getPreflabel().getLit().getLanguage());
				skos.setScheme(new URIImpl(a.getValue("scheme").stringValue()));
				skos.setCodingSchemeURI(a.getValue("codingSchemeURI").stringValue());
				skos.setSkosNotation(a.getValue("code").stringValue());
				skos.setUri(uri);*/
				
			}
			
		}
		sc.close();
		
		for (String st:lom){
			answer.addMember(this.getDataElementByURI(new URIImpl(st),false));
		}
	 
		return answer;
		
	}
	
	public Set<DataElement> searchDataElement(URI metadata,               // general Option
											  String labelPattern,        // P4
											  String termlabelPattern,    // P5
											  String termCodePattern,     // P6
											  String labelExactMatch,     // P1
											  String termlabelExactMatch, // P2
											  String termCodeExactMatch,  // P3
											  URI termCodingSchemeURI, // Option for P5 P6 P2 P3
											  URI memberOfUri,
											  URI topCategoryUri,
											  int maxAnswer,			  // general Option
											  int offset,  				  // general Option
											  boolean autocompletion      // general Option
											  )
											  
											  throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<DataElement> results=Sets.newHashSet();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
		
		// AutoCompletion or general contains !
		String searchType="CONTAINS";
		if (autocompletion)searchType="STRSTARTS";
		
		// de_uri : Data Element URI.
		// term_uri : Related Term URI.
		// BASIC FOR EVERY SEARCH !!!!
		String query =this.queryPrepare()+" SELECT DISTINCT ?de_uri "+
				"WHERE "+
				"{ " +
				" ?de_uri rdf:type ubiq:DataElement . " ;
		// END OF BASIC
		// metadata checking.
		if (null !=metadata){
			query += " ?de_uri ubiq:elementType ?metadata .";
			bs.addBinding(new BindingImpl("metadata",metadata));
		}
		
		boolean relatedTermSearch=true;
		String order=null;
		
		if       (!Strings.isNullOrEmpty(labelExactMatch)){ // Priority One !!!
			relatedTermSearch=false;
			order="?labelExactMatch";
			query += " ?de_uri  skos:prefLabel ?labelExactMatch .";
			
			
			bs.addBinding(new BindingImpl("labelExactMatch",vf.createLiteral(labelExactMatch)));
		}
		else if (!Strings.isNullOrEmpty(termlabelExactMatch)){ // Priority Two
			order="?termlabelExactMatch";
			query +=" ?term_uri skos:preflabel ?termlabelExactMatch . ";
			query +=" ?de_uri skos:related ?term_uri . ";
			bs.addBinding(new BindingImpl("termlabelExactMatch",vf.createLiteral("termlabelExactMatch")));
		}
		else if (!Strings.isNullOrEmpty(termCodeExactMatch)){ // Priority Three
			order="?termCodeExactMatch";
			query +=" ?term_uri skos:notation ?termCodeExactMatch . ";
			query +=" ?de_uri skos:related ?term_uri . ";
			bs.addBinding(new BindingImpl("termCodeExactMatch",vf.createLiteral("termCodeExactMatch")));
		}
		else if (!Strings.isNullOrEmpty(labelPattern)){ // Priority 4 !!!
			relatedTermSearch=false;
			order="?prefLabel";
			query += " ?de_uri  skos:prefLabel ?prefLabel .";
			query+= " FILTER "+searchType+" (UCASE(str(?prefLabel)),UCASE(\""+labelPattern+"\")) " ;
		}
		else if (!Strings.isNullOrEmpty(termlabelPattern)){ // Priority 5 !!!
			order="?prefLabel";
			query +=" ?term_uri skos:preflabel ?preflabel . ";
			query +=" ?de_uri skos:related ?term_uri . ";
			query+= " FILTER "+searchType+" (UCASE(str(?prefLabel)),UCASE(\""+termlabelPattern+"\")) " ;
		}
		else if (!Strings.isNullOrEmpty(termCodePattern)){ // Priority 6 !!!
			order="?code";
			query +=" ?term_uri skos:notation ?code . ";
			query +=" ?de_uri skos:related ?term_uri . ";
			query+= " FILTER "+searchType+" (UCASE(?code),UCASE(\""+termCodePattern+"\")) " ;
		}
		
		if (null!=termCodingSchemeURI && relatedTermSearch){
			query +=". ?term_uri skos:inScheme ?termCodingSchemeURI  ";
			bs.addBinding(new BindingImpl("termCodingSchemeURI",termCodingSchemeURI));
		}
		
		if (null != memberOfUri){
			query +=" . ?de_uri skos:member ?memberOfUri ";
			bs.addBinding(new BindingImpl("memberOfUri",memberOfUri));
		}
		
		if (null != topCategoryUri){
			query +=" . ?de_uri ubiq:topCategory ?topCategoryUri ";
			bs.addBinding(new BindingImpl("topCategoryUri",topCategoryUri));
		}
		
		if (order==null)order="?de_uri";
		
		query+=	" } ORDER BY "+order+" OFFSET "+offset+" LIMIT "+ maxAnswer+" ";
		System.err.println(query);
		
		ParsedQuery parsedQuery=parser.parseQuery(query , context.toString());
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		
		sparqlResults =sc.evaluate(parsedQuery.getTupleExpr(),parsedQuery.getDataset(), bs, true);
		if (sparqlResults.hasNext()){
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				results.add(this.getDataElementByURI(new URIImpl(a.getValue("de_uri").stringValue()),false));
			}
		}else {
			System.err.println("No Result for query : "+query);
		}
		sc.close();
		return results;
	}
	
	
	
	// http://answers.semanticweb.com/questions/2238/sparql-regexstro-abc-i-does-not-work-as-case-insensitive
	public Map<URI,String>  searchStringREGEX (String st) throws SailException, MalformedQueryException, QueryEvaluationException{
		Map<URI,String> l=Maps.newLinkedHashMap();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri ?prefLabel "+
							"WHERE "+
							"{ " +
							" ?uri rdf:type ubiq:DataElement . " +
							" ?uri skos:prefLabel ?prefLabel . " +
							"FILTER regex (?prefLabel,\""+st+"\", \"i\"  ) " +
							" } "
							;
		//rdf:type  ubiq:DATAELEMENT
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				System.out.println(a.toString());
				l.put(new URIImpl(a.getValue("uri").toString()),((Literal)a.getValue("prefLabel")).getLabel());
			}
				
		}
		sc.close();
		
		
	 
		return l;
	}
	
}
