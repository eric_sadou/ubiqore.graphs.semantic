package ubiqore.graphs.semantic.simple;

import info.aduna.iteration.CloseableIteration;

import org.openrdf.model.Statement;
import org.openrdf.model.ValueFactory;
import org.openrdf.sail.Sail;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;

import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;

public class SailOutGraph {
	Neo4jGraph baseGraph;
	Sail sail;
	public SailOutGraph(){
		try {
			this.init();
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	private void init () throws SailException{
		Neo4jGraph baseGraph = new Neo4jGraph("/ubiqore/tmp/sailoutTest");
		baseGraph.setCheckElementsInTransaction(true);
		sail= new GraphSail(baseGraph);
		sail.initialize();
	}
	
	public static void main (String ... args){
		
		SailOutGraph g=new SailOutGraph();
		SailConnection sc=null;
		ValueFactory vf = g.sail.getValueFactory();
		try {
			sc = g.sail.getConnection();
			for (int i=0;i<10;i++)sc.addStatement(vf.createURI("http://tinkerpop.com#A"), vf.createURI("http://tinkerpop.com#knows"), vf.createURI("http://tinkerpop.comB#"), vf.createURI("http://tinkerpop.com"));
			CloseableIteration<? extends Statement, SailException> results=sc.getStatements(null,null,null,false);
			int nb=0;
			while(results.hasNext()) {
			   results.next();
			   nb++;
			}
			sc.commit();
			sc.close();
			g.sail.shutDown();
			
			System.out.println(nb);
			//sc.close();
			
		}catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static void main1 (String... args){
	
		SailOutGraph g=new SailOutGraph();
		SailConnection sc=null;
		try {
			sc = g.sail.getConnection();
		
		ValueFactory vf = g.sail.getValueFactory();
		for (int i=0; i<10;i++){
			int j=i+1;
		sc.addStatement(vf.createURI("http://tinkerpop.com#"+i), vf.createURI("http://tinkerpop.com#knows"), vf.createURI("http://tinkerpop.com#"+(j)), vf.createURI("http://tinkerpop.com"));
			
		}
		System.out.println("get statements: ?s ?p ?o ?g");
		CloseableIteration<? extends Statement, SailException> results = sc.getStatements(null, null, null, false);
		
		int  nbStatements=0;
		
		while(results.hasNext()) {
			results.next();
		    //System.out.println(results.next());
		    nbStatements++;
		}
		System.out.println(nbStatements);
		System.out.println("\nget statements: http://tinkerpop.com#3 ?p ?o ?g");
		results = sc.getStatements(vf.createURI("http://tinkerpop.com#3"), null, null, false);
		while(results.hasNext()) {
		    System.out.println(results.next());
		}
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		finally {
			
			try {
				sc.commit();	
				if (sc!=null){
					System.out.println("sc will close");
					sc.close();
				}
				else System.out.println("sc close deja");
				
			} catch (SailException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				finally {
				if (g.baseGraph!=null){
					
					g.baseGraph.shutdown();
				}else {
					System.out.println("g base graph is already shutdown !");
				}
				if (g.sail !=null){
					try {
						g.sail.shutDown();
						System.out.println("g.sail was shutdown...");
					} catch (SailException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				else {
					System.out.println("g.sail already shutdown !");
				}
				}
		}
	}
}
