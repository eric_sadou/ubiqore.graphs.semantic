package ubiqore.graphs.semantic;

public class Rank {
	String uri;
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}

	float value;
	public float getValue() {
		return value;
	}
	public void setValue(float value) {
		this.value = value;
	}
	public String getOriginalRank() {
		return originalRank;
	}
	public void setOriginalRank(String originalRank) {
		this.originalRank = originalRank;
	}
	
	String originalRank;
	
}
