package ubiqore.graphs.semantic;

import java.util.ArrayList;



public class DataElement {
	
	String topCategoryURI;
	public String getTopCategoryURI() {
		return topCategoryURI;
	}

	public void setTopCategoryURI(String topCategoryURI) {
		this.topCategoryURI = topCategoryURI;
	}



	String metadata;
	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getMemberOfUri() {
		return memberOfUri;
	}

	public void setMemberOfUri(String memberOfUri) {
		this.memberOfUri = memberOfUri;
	}

	
	public void setMembers(ArrayList<DataElement> members) {
		this.members = members;
	}

	

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}


	String uri;
	String memberOfUri;
	
	ArrayList<DataElement> members;
	
	
	String relatedCD;
	public String getRelatedCD() {
		return relatedCD;
	}

	public void setRelatedCD(String relatedCD) {
		this.relatedCD = relatedCD;
	}

	public Float getCoValue() {
		return coValue;
	}

	public void setCoValue(Float coValue) {
		this.coValue = coValue;
	}

	public ArrayList<DataElement> getMembers() {
		if (members==null)members=new ArrayList<DataElement>();
		return members;
	}



	String label; // for not concept related element.
	
	Float coValue;
	String pqUcumCode;
	public String getPqUcumCode() {
		return pqUcumCode;
	}

	public void setPqUcumCode(String pqUcumCode) {
		this.pqUcumCode = pqUcumCode;
	}

	public String getPqUcumName() {
		return pqUcumName;
	}

	public void setPqUcumName(String pqUcumName) {
		this.pqUcumName = pqUcumName;
	}



	String pqUcumName;
	

	

	public void addMember(DataElement uri){
		if (members==null)members=new ArrayList<DataElement>();
		 members.add(uri);
	}
	
	
	
	
}