package ubiqore.graphs.semantic.umls;

import java.util.Set;

import ubiqore.graphs.semantic.SkosConcept;

public class MrConsoConcept extends SkosConcept {

	String cui;
	public String getCui() {
		return cui;
	}
	public void setCui(String cui) {
		this.cui = cui;
	}
	public Set<String> getAuis() {
		return auis;
	}
	public void setAuis(Set<String> auis) {
		this.auis = auis;
	}
	
	Set<String> auis;
	String label;
	String code;
}
