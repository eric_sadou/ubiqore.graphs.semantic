package ubiqore.graphs.semantic.umls;

import info.aduna.iteration.CloseableIteration;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.impl.ContextStatementImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;

import ubiqore.graphs.semantic.PrefLabel;
import ubiqore.graphs.semantic.SKOS;
import ubiqore.graphs.semantic.Scheme;
import ubiqore.graphs.semantic.SemanticVocabulary;
import ubiqore.graphs.semantic.SkosConcept;
import ubiqore.graphs.semantic.TerminologiesGraph;

/*Stockage des infos from MRCONSO*/
// 1 concept --> 1 CUI , une liste AUI, 1 label 

public class MrConsoTempGraph extends TerminologiesGraph {
	@Override
	protected void initAccess() throws SailException{
		baseGraph = new Neo4jGraph(rep);
		baseGraph.setCheckElementsInTransaction(true);
		GraphSail<Neo4jGraph> gSail=new GraphSail<Neo4jGraph>(baseGraph);
		gSail.enforceUniqueStatements(false);
		sail= gSail;
		
		sail.initialize();
		vf=sail.getValueFactory();
	}
	public MrConsoTempGraph(boolean creation, String rep) throws SailException {
		super(creation, rep);
		// TODO Auto-generated constructor stub
	}
	

	/*// return NULL : no creation
	// return One uri : creation or Get.
	public URI createOrGetConcept(MrConsoConcept c,Scheme scheme) throws SailException{
		URI already=this.exists(c.getCui());
		if (already!=null)return already;
		
		 * Creation du concept
		 
		URI uri=new URIImpl(this.context+SemanticVocabulary.concept+scheme.getOid()+"/"+c.getCode());
		List<Statement> sts=Lists.newArrayList();
		sts.add(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPT.getURI(),this.context));
		sts.add(new ContextStatementImpl(uri,SKOS.NOTATION.getURI(),vf.createLiteral(c.getCode()),this.context));
		sts.add(new ContextStatementImpl(uri,SKOS.PREFLABEL.getURI(),vf.createLiteral(c.getLabel()),this.context));
		sts.add(new ContextStatementImpl(uri,SemanticVocabulary.CUI,vf.createLiteral(c.getCui()),this.context));
		for (String aui:c.getAuis() ){
			sts.add(new ContextStatementImpl(uri,SemanticVocabulary.AUI,vf.createLiteral(aui),this.context));
		}
		boolean ok=this.addMultipleStatements(sts);
		if (ok)return uri;
		else return null;
		
	}*/
	
	
	public int load(Set<MrConsoConcept> set,Scheme scheme,int commitFlag)throws SailException{
		int i=0;
		System.out.println("Nb concepts to Load: "+ set.size());
		Set<Statement> sts=Sets.newHashSet();
		for (MrConsoConcept c:set){
			URI uri=new URIImpl(this.context+SemanticVocabulary.concept+scheme.getOid()+"/"+c.getSkosNotation());
			sts.add(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPT.getURI(),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.NOTATION.getURI(),vf.createLiteral(c.getSkosNotation()),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.PREFLABEL.getURI(),c.getPreflabel().getLit(),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.INSCHEME.getURI(),scheme.getUri(),this.context));
			sts.add(new ContextStatementImpl(uri,SemanticVocabulary.CUI,vf.createLiteral(c.getCui()),this.context));
			for (String aui:c.getAuis() ){
				sts.add(new ContextStatementImpl(uri,SemanticVocabulary.AUI,vf.createLiteral(aui),this.context));
			}
			i++;
			
		}
		boolean ok=this.addMultipleStatements(sts,commitFlag);
		return i;
	}
	
	@Deprecated
	public MrConsoConcept getConceptByAui(String aui)throws SailException, MalformedQueryException, QueryEvaluationException{
		MrConsoConcept c =null;
		
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?cui ?code ?label  "+
							"WHERE "+
							"{ " +
							
							"?uri ubiq:AUI       \""+aui+"\" . " +
							"?uri ubiq:CUI         ?cui . " +
							"?uri skos:prefLabel   ?label . " +
							"?uri skos:notation    ?code . " +
							" } "
							;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			c=new MrConsoConcept();
			while (sparqlResults.hasNext()){
				BindingSet binding=sparqlResults.next();
				c.setCui(binding.getValue("cui").stringValue());
				c.setSkosNotation(binding.getValue("code").stringValue());
				c.setPreflabel(new PrefLabel((Literal)binding.getValue("label")));
				break;
			}
			
		}
		sc.close();
		
		return c;
		
	}
	Set<Statement> narrowers=Sets.newHashSet();
	public void prepareNarrowers(URI concept1, URI relation1to2 ,URI concept2){
		narrowers.add(new ContextStatementImpl(concept1,relation1to2,concept2,this.context));
	}
	public void loadNarrower(int commitFlag) throws SailException{
		this.addMultipleStatements(narrowers, commitFlag);
	}
	
	
	
	public SkosConcept getConcept(String aui) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		SkosConcept skos=null;
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri ?code ?schemeUri ?prefLabel   "+
							"WHERE "+
							"{ " +
							"?uri ubiq:AUI       \""+aui+"\" . " +
							"?uri rdf:type  skos:Concept . " +
							"?uri skos:notation ?code . " +
							"?uri skos:prefLabel ?prefLabel . " +
							"?uri skos:inScheme ?schemeUri . " +
							// "?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?code) = xsd:string ) " +
						 //	"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			skos=new SkosConcept();
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				Literal v=(Literal)a.getValue("prefLabel");
				skos.setPreflabel(new PrefLabel(v));
				//System.out.println(skos.getPreflabel().getLit().getLabel());
				//System.out.println(skos.getPreflabel().getLit().getLanguage());
				skos.setScheme(this.getSchemeBase(new URIImpl(a.getValue("schemeUri").stringValue())));
				skos.setSkosNotation(a.getValue("code").stringValue());
				skos.setUri(new URIImpl(a.getValue("uri").stringValue()));
				break;
			}
			
		}
		sc.close();
		return skos;
	}
}
