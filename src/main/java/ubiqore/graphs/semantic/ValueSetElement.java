package ubiqore.graphs.semantic;

import java.util.ArrayList;



public class ValueSetElement {
	String topCategoryURI;
	public String getTopCategoryURI() {
		return topCategoryURI;
	}

	public void setTopCategoryURI(String topCategoryURI) {
		this.topCategoryURI = topCategoryURI;
	}



	String metadata;
	public String getMetadata() {
		return metadata;
	}

	public void setMetadata(String metadata) {
		this.metadata = metadata;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public String getMemberOfUri() {
		return memberOfUri;
	}

	public void setMemberOfUri(String memberOfUri) {
		this.memberOfUri = memberOfUri;
	}

	public ArrayList<ValueSetElement> getMembersUri() {
		return membersUri;
	}

	public void setMembersUri(ArrayList<ValueSetElement> membersUri) {
		this.membersUri = membersUri;
	}

	public String getRelatedToUri() {
		return relatedToUri;
	}

	public void setRelatedToUri(String relatedToUri) {
		this.relatedToUri = relatedToUri;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public Rank getRankURI() {
		return rankURI;
	}

	public void setRankURI(Rank rankURI) {
		this.rankURI = rankURI;
	}

	

	String uri;
	String memberOfUri;
	
	ArrayList<ValueSetElement> membersUri;
	String relatedToUri;
	String label; // for not concept related element.
	
	Rank rankURI;
	Unit unitURI;
	
	public Unit getUnitURI() {
		return unitURI;
	}

	public void setUnitURI(Unit unitURI) {
		this.unitURI = unitURI;
	}

	public void addMember(ValueSetElement uri){
		if (membersUri==null)membersUri=new ArrayList<ValueSetElement>();
		 membersUri.add(uri);
	}
	
	
	
	
}