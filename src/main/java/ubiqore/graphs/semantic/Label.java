package ubiqore.graphs.semantic;

import java.util.Locale;

import org.openrdf.model.Literal;
import org.openrdf.model.impl.LiteralImpl;

public class Label {
	
	String value;
	Locale langage=Locale.ENGLISH;
	
	public Label(String st){
		this.value=st;
	}

	public Label(String st,Locale langage){
		this.value=st;
		this.langage=langage;
	}
	public Literal getLit() {
		return new LiteralImpl(value,langage.toString());
	}

}
