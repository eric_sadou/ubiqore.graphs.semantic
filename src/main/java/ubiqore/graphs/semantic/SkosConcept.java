package ubiqore.graphs.semantic;

import java.util.List;
import java.util.Set;
import org.openrdf.model.URI;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;


public class SkosConcept {
	  
	URI uri;
	String version;
	String skosNotation; // unique internal code.
	PrefLabel preflabel;
	List<Label> labels=Lists.newArrayList();
	Scheme scheme;
	String pathLexChapter;
	Set<SkosConcept> broaders=Sets.newHashSet();
	Set<SkosConcept> narrowers=Sets.newHashSet();
	String period;
	Float coValue;
	
	public Float getCoValue() {
		return coValue;
	}
	public void setCoValue(Float coValue) {
		this.coValue = coValue;
	}
	public Scheme getScheme() {
		return scheme;
	} 
	public void setScheme(Scheme scheme) {
		this.scheme = scheme;
	}
	public PrefLabel getPreflabel() {
		return preflabel;
	}
	public void setPreflabel(PrefLabel preflabel) {
		this.preflabel = preflabel;
	}
	public URI getUri() {
		return uri;
	}
	public void setUri(URI uri) {
		this.uri = uri;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
	public String getSkosNotation() {
		return skosNotation;
	}
	public void setSkosNotation(String skosNotation) {
		this.skosNotation = skosNotation;
	}
	
	
	
	
	
	
	public String getPathLexChapter() {
		return pathLexChapter;
	}
	public void setPathLexChapter(String pathLexChapter) {
		this.pathLexChapter = pathLexChapter;
	}
	
	 
	public Set<SkosConcept> getBroaders() {
		return broaders;
	}
	public void setBroaders(Set<SkosConcept> broaders) {
		this.broaders = broaders;
	}
	public Set<SkosConcept> getNarrowers() {
		return narrowers;
	}
	public void setNarrowers(Set<SkosConcept> narrowers) {
		this.narrowers = narrowers;
	}
	
	@Override
	public String toString(){
		
		String st=new String();
		
		st+="Code="+this.getSkosNotation()+"\n";
		st+="prefLabel="+this.getPreflabel().getLit().getLabel()+"\n";
		st+="URI="+this.getUri()+"\n";
		st+="SchemeURI="+this.getScheme().getUri()+"\n";
		for (SkosConcept broader:this.getBroaders()){
			st+="BROADER-DEF"+broader.toString()+"\n";
			st+="BROADER-DEF-END\n";
		}
		for (SkosConcept narrower:this.getNarrowers()){
			st+="NARROWER-DEF"+narrower.toString()+"\n";
			st+="NARROWER-DEF-END\n";
		}
		return st;
	}
	
	public String getPeriod() {
		return period;
	}
	public void setPeriod(String period) {
		this.period = period;
	}
	public List<Label> getLabels() {
		return labels;
	}
	public void setLabels(List<Label> labels) {
		this.labels = labels;
	}
}
