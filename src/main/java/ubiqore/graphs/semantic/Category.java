package ubiqore.graphs.semantic;

public class Category {

	String URI;
	String VSURI;
  
	public String getVSURI() {
		return VSURI;
	}

	public void setVSURI(String vSURI) {
		VSURI = vSURI;
	}

	public String getURI() {
		return URI;
	}

	public void setURI(String uRI) {
		URI = uRI;
	}
}
