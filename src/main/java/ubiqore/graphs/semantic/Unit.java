package ubiqore.graphs.semantic;

import com.google.common.base.Strings;

public class Unit {

	String uri;
	
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	String ucumValue;
	public String getUcumValue() {
		return ucumValue;
	}
	public void setUcumValue(String ucumValue) {
		if (ucumValue==null)return;
		this.ucumValue = ucumValue.trim();
	}
	public String getUcumDefinition() {
		return ucumDefinition;
	}
	public void setUcumDefinition(String ucumDefinition) {
		if (ucumDefinition==null)return;
		this.ucumDefinition = ucumDefinition.trim();
	}
	String ucumDefinition;
	
	public String getUriLocalName(){
		if (Strings.emptyToNull(ucumDefinition)==null)return null;
		else return ucumDefinition.trim().replace(" ", "_");
	}
}
