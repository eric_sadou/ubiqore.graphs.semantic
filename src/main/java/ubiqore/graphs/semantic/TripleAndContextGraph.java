package ubiqore.graphs.semantic;

import info.aduna.iteration.CloseableIteration;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
 
import org.openrdf.model.Literal;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.impl.ContextStatementImpl;
import org.openrdf.model.impl.LiteralImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.DC;
import org.openrdf.model.vocabulary.OWL;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.impl.BindingImpl;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.repository.sparql.query.SPARQLQueryBindingSet;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.helpers.StatementCollector;
import org.openrdf.rio.rdfxml.RDFXMLParser;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;

import com.google.common.base.Charsets;
import com.google.common.base.Splitter;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
//import org.apache.commons.httpclient.util.URIUtil;
import com.google.common.io.Files;


// Rule One : We only persist Narrower Link !!!
// 
public class TripleAndContextGraph extends AbstractGraph {
	
	
	public TripleAndContextGraph(boolean creation, String rep)
			throws SailException {
		
		super(creation, rep);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void createVocabularyStatementsList() {
			vocabularyStatements.add(new ContextStatementImpl(SemanticVocabulary.pathLexChapter,RDF.TYPE, RDFS.DATATYPE,context));
			//vocabularyStatements.add(new ContextStatementImpl(SKOS.NARROWER.getURI(),OWL.INVERSEOF,SKOS.BROADER.getURI(),context));
			vocabularyStatements.add(new ContextStatementImpl(SKOS.BROADER.getURI(),OWL.INVERSEOF,SKOS.NARROWER.getURI(),context));
			//vocabularyStatements.add(new ContextStatementImpl(SKOS.BROADER.getURI(),OWL.EQUIVALENTPROPERTY,RDFS.SUBCLASSOF,context));
			//vocabularyStatements.add(new ContextStatementImpl(SKOS.NARROWER.getURI(),OWL.INVERSEOF,RDFS.SUBCLASSOF,context));
			vocabularyStatements.add(new ContextStatementImpl(RDFS.SUBCLASSOF,OWL.INVERSEOF,SKOS.NARROWER.getURI(),context));
	}
	
	@Override
	public void link(URI concept1, URI relation1to2 ,URI concept2) throws SailException{
		// We only persist the IsInverse OR isEquivalent OR the original statement. 
		 Statement st=new ContextStatementImpl(concept1,relation1to2,concept2,this.context);
		
		 //
		
		if (this.isInverse(relation1to2)){
			this.applyInverse(st);
		}else if (this.isEquivalent(relation1to2)){
			this.applyEquivalent(st);
		}else {
			this.addSingleStatement(st);
		}
	}
	
    
	
	/*
	 * check if candidate is in the tree of a domain
	 */
	
	public boolean searchInDomain(URI candidate ,URI domain) throws SailException, MalformedQueryException, QueryEvaluationException{
		Map<URI,String> l=Maps.newLinkedHashMap();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		URI domainScheme=this.induceSchemeURI(domain);     
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" ASK  "+
	//	        " FROM <"+domainScheme+"> " +
				" WHERE "+
				"{ " +
				" <"+domain.toString()+"> skos:narrower+ <"+candidate.toString()+"> . "+
	//			" ?uri rdf:type  skos:Concept . " +
	//			" ?uri skos:prefLabel ?prefLabel . " +
				" } "
				;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , domainScheme.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		boolean res=false;
		if (sparqlResults.hasNext()){
			//System.out.println(sparqlResults.next().toString());
			res=true;
		//	System.out.println("domain="+domain+" has as narrower .. "+candidate);
		}else {
		//	System.out.println("domain="+domain+" no rel with .. "+candidate);
		}
		sc.close();
		return res;
	}
	
	
	
	
	@Override
	Set<Statement> getStatements(URI conceptUri,int maxLevel,boolean first)throws SailException{
		System.out.println("ovveride !!!");
		Set<Statement> st=Sets.newHashSet();
		int newMaxLevel=0;
		if (maxLevel==0){
			// no check
		}
		else if (maxLevel> 0){
			if (maxLevel==1)newMaxLevel=-1;
			else newMaxLevel=maxLevel-1;
		}
		else return null;
		SailConnection sc=sail.getConnection();
		CloseableIteration<? extends Statement, SailException> statements = sc.getStatements(conceptUri, null, null, false);
		boolean flag=false;
		
		while (statements.hasNext()){
			Statement statement=statements.next();
			
			
			if (first && !flag){ // add Scheme statement
				   URI schemeURI = this.induceSchemeURI(conceptUri);
				   if (!schemeURI.stringValue().equalsIgnoreCase(conceptUri.stringValue())){
						
						st.addAll(this.getStatements(schemeURI,1,false));
				   }
				flag=true;
			}
			// check narrowers ? 
			if (statement.getPredicate().equals(SKOS.NARROWER.getURI())){
				if (maxLevel!=1){
					st.add(statement); // si 1 : les info ne sont pas necessaires...
					st.addAll(this.getStatements(new URIImpl(statement.getObject().toString()),newMaxLevel,false));
				}
				
			}else {
				st.add(statement);
			}
		}
		sc.close();
		return st;
	}
	
	public Map<URI,String> searchInDomain(String search,URI domain) throws SailException, MalformedQueryException, QueryEvaluationException{
		Map<URI,String> l=Maps.newLinkedHashMap();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		URI domainScheme=this.induceSchemeURI(domain);     
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri ?prefLabel "+
	//		    " FROM <"+domainScheme+"> " +
				" WHERE "+
				"{ " +
				" <"+domain.toString()+"> skos:narrower+ ?uri . "+
	//			" ?uri rdf:type  skos:Concept . " +
				" ?uri skos:prefLabel ?prefLabel . " +
				
				"FILTER CONTAINS (UCASE(str(?prefLabel)),UCASE(\""+search+"\"))  " +
				" } "
				;
		
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , this.context.stringValue());
		
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				l.put(new URIImpl(a.getValue("uri").toString()),((Literal)a.getValue("prefLabel")).getLabel());
			}
		}
		sc.close();
		return l;
	}
	
	public Set<URI>  searchInLocalREGEX (String st) throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> set=Sets.newHashSet();
		
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri ?code"+
							"WHERE "+
							"{ " +
							" ?uri rdf:type  skos:Concept . " +
							" ?uri skos:notation ?code . " +
							"FILTER regex (?code,\""+st+"\", \"i\" ) " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				set.add(new URIImpl(a.getValue("uri").toString()));
			}
				
		}
		sc.close();
		
		
	 
		return set;
	}
	
	public Set<URI>  searchLabel(String pattern,URI optionalSchemeURI,int maxAnswer,int offset,boolean autocompletion ) throws SailException, MalformedQueryException, QueryEvaluationException{
		//pattern=pattern.replaceAll("([^a-zA-z0-9])", "\\\\$1");
		String filterType="CONTAINS";
		System.out.println(pattern);
		if (autocompletion)filterType="STRSTARTS";
		
		Set<URI> set=Sets.newHashSet();
		
		if (maxAnswer<=0)maxAnswer=10;
		
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		String FROM="";
		URI myContext=this.context;
		if (optionalSchemeURI!=null){
			FROM= " FROM <"+optionalSchemeURI+"> ";
			myContext=optionalSchemeURI;
			System.out.println(optionalSchemeURI);
		}
		
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri "+
							 FROM +
							"WHERE "+
							"{  "; 
			   queryString+= " ?uri skos:prefLabel ?prefLabel  ";
			   queryString+= " . FILTER "+filterType+" (UCASE(str(?prefLabel)),UCASE(\""+pattern+"\")) " ;
			 queryString+=  
					   //  " . FILTER ( EXISTS {?uri skos:inScheme ?schemeUri }) " +
								
					   " } ORDER BY ?preflabel OFFSET "+offset+" LIMIT "+ maxAnswer+" ";
		;
				
//sPREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>  PREFIX ubiq: <http://server.ubiqore.com/ubiq/core#>  PREFIX foaf: <http://xmlns.com/foaf/0.1/>  PREFIX owl: <http://www.w3.org/2002/07/owl#>  PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>  PREFIX value: <urn:com.tinkerpop.blueprints.pgm.oupls.sail:namespaces>  PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>  PREFIX skos: <http://www.w3.org/2004/02/skos/core#>   SELECT DISTINCT ?uri WHERE {  ?uri skos:inScheme ?schemeUri  FILTER ( EXISTS { ?uri skos:prefLabel ?prefLabel   FILTER contains (?prefLabel , ?pattern , "i" ) } )  } LIMIT 10
// regex , \"i\"		
		System.out.println(queryString);				
		  					
		ParsedQuery query=parser.parseQuery(queryString , myContext.stringValue());
	// http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.1
	//	ParsedQuery query=parser.parseQuery(queryString , "http://server.ubiqore.com/v1/concept/2.16.840.1.113883.6.1/");
			SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
	//		bs.addBinding(new BindingImpl("schemeUri",optionalSchemeURI));
	//		bs.addBinding(new BindingImpl("pattern",vf.createLiteral(pattern)));
			sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
			
		
		System.out.println("Before the boucle of result...");
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				
				BindingSet a=sparqlResults.next();
				System.out.println(a.toString());
				set.add(new URIImpl(a.getValue("uri").toString()));
			}
				
		}else {
			System.out.println("no result for searchLabel .."+queryString);
		}
		sc.close();
		
		
	 
		return set;
	}
	
	
	
	
	
	
	public Set<URI>  searchCode(String pattern,URI optionalSchemeURI,int maxAnswer,int offset,boolean autocompletion ) throws SailException, MalformedQueryException, QueryEvaluationException{
		//pattern=pattern.replaceAll("([^a-zA-z0-9])", "\\\\$1");
		String filterType="CONTAINS";
		System.out.println(pattern);
		if (autocompletion)filterType="STRSTARTS";
		
	//	if (autocompletion)pattern="^"+pattern;
		String FROM="";
		URI myContext=this.context;
		if (optionalSchemeURI!=null){
			FROM= " FROM <"+optionalSchemeURI+"> ";
			myContext=optionalSchemeURI;
			System.out.println(optionalSchemeURI);
		}
		
	
		Set<URI> set=Sets.newHashSet();
		
		if (maxAnswer<=0)maxAnswer=10;
		
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri "+
							FROM +
							" WHERE "+
							"{  "; 
			   queryString+= "   ?uri skos:notation ?code  .";
			   queryString+= "   FILTER "+filterType+" (UCASE(str(?code)),UCASE(\""+pattern+"\")) " ;
			   queryString+=" } ORDER BY ?code OFFSET "+offset+" LIMIT "+ maxAnswer+" ";
							;
				
		
		System.out.println(queryString);				
							
		ParsedQuery query=parser.parseQuery(queryString , myContext.stringValue());
		
		
			SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
			
			//bs.addBinding(new BindingImpl("pattern",vf.createLiteral("\""+pattern+"\"")));
			sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
			
		
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				set.add(new URIImpl(a.getValue("uri").toString()));
			}
				
		}else {
			System.out.println("no result for searchByCode .."+queryString);
		}
		sc.close();
		
		
	 
		return set;
	}
	
	
	
	@Override
	protected void createNamespacesMap() {
		    // official namespaces..
			this.useClassicNamespaces();
		    // ubiq and others...
	        this.namespaces.put(SemanticVocabulary.skos_prefix,SemanticVocabulary.skos);
	        this.namespaces.put(SemanticVocabulary.ubiq_prefix,SemanticVocabulary.ubiq);
	}
	
	public List<SkosConcept> getOriginalList() throws SailException, MalformedQueryException, QueryEvaluationException{
		List<URI> l=this.getCollectionMembers(createOrGetOriginList());
		List<SkosConcept> rep=Lists.newArrayList();
		for (URI uri:l){
			rep.add(this.getConcept(uri));
		}
		return rep;
	}
	
	
	
	
	
	public List<SkosConcept> getFullList() throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> l=this.getConceptsURI();
		List<SkosConcept> rep=Lists.newArrayList();
		for (URI uri:l){
			rep.add(this.getConcept(uri));
		}
		return rep;
		
	}
	
	
	
	public URI createOrGetOriginList() throws SailException{
		URI uri=new URIImpl(this.context+SemanticVocabulary.collection+SemanticVocabulary.originList);
		if (this.entityExist(new ContextStatementImpl(uri,RDF.TYPE,SKOS.COLLECTION.getURI(),this.context))!=null){
			return uri;
		}
		this.addSingleStatement(new ContextStatementImpl(uri,RDF.TYPE,SKOS.COLLECTION.getURI(),this.context));
		return uri;
	}
	
	
	public void addOriginalMember(URI conceptURI,DateTime jodaTime ){
		try {
			this.link(conceptURI, SKOS.MEMBER.getURI(), createOrGetOriginList());
			URI contextUri=this.induceSchemeURI(conceptURI);
			ContextStatementImpl period=new ContextStatementImpl(conceptURI,DC.DATE,vf.createLiteral(jodaTime.toDate()),contextUri);
			this.addSingleStatement(period);
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void addOriginalCOMember(URI conceptURI,float value,DateTime jodaTime){
		try {
			URI contextUri=this.induceSchemeURI(conceptURI);
			this.link(conceptURI, SKOS.MEMBER.getURI(), createOrGetOriginList());
			ContextStatementImpl period=new ContextStatementImpl(conceptURI,DC.DATE,vf.createLiteral(jodaTime.toDate()),contextUri);
			this.addSingleStatement(period);
			this.addSingleStatement(new ContextStatementImpl(conceptURI,SemanticVocabulary.CO,vf.createLiteral(value),contextUri));
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	// apply means : create and/or get uri. 
	public URI applyScheme(String oid,String prefLabel,String definition) throws SailException{
		URI uri=new URIImpl(this.context+oid);
		URI rep=this.entityExist(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPTSCHEME.getURI(),uri));
		if (rep==null){
			List<Statement> sts=Lists.newArrayList();
			sts.add(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPTSCHEME.getURI(),uri));
			sts.add(new ContextStatementImpl(uri,SKOS.PREFLABEL.getURI(),vf.createLiteral(prefLabel),uri));
			sts.add(new ContextStatementImpl(uri,SKOS.NOTATION.getURI(),vf.createLiteral(oid),uri));
			sts.add(new ContextStatementImpl(uri,SKOS.DEFINITION.getURI(),vf.createLiteral(definition),uri));
			DateTime dt=DateTime.now(DateTimeZone.UTC);
			Date jdkDate=null;
			try {
				jdkDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(dt.toString("yyyy-MM-dd HH:mm:ss"));
				sts.add(new ContextStatementImpl(uri,DC.DATE,vf.createLiteral(jdkDate),uri));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			boolean ok=this.addMultipleStatements(sts);
			if (ok)return uri;
		}else {
			//System.out.println("scheme already there darling...");
		}
		return rep;
	}
	
	
	/*
	 *  LES uri qui sont TopConcept OR Broader de quelque chose ...
	 */
	public Set<URI> getConceptsURI() throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> set=Sets.newHashSet();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x  "+
							"WHERE "+
							"{ ?x skos:notation ?code ." +
							"FILTER NOT EXISTS { ?x rdf:type skos:ConceptScheme } "+
							
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
		
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.getValue("x"));
				set.add(new URIImpl(a.getValue("x").stringValue()));
			}
			
		}
		
		sc.close();
		return set;
	 
	}
	
	public Set<URI> getSchemesURI() throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> set=Sets.newHashSet();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x  "+
							"WHERE "+
							"{ " +
							" ?x rdf:type  skos:ConceptScheme ."+
							
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
		
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.getValue("x"));
				set.add(new URIImpl(a.getValue("x").stringValue()));
			}
			
		}
		
		sc.close();
		return set;
	 
	}
	
	Set<Scheme> schemeCache=Sets.newHashSet();
	
	private Scheme getFromSchemeCache(URI input){
		for (Scheme s:this.schemeCache){
			if (s.getUri().toString().equalsIgnoreCase(input.toString()))return s;
		}
		return null;
	}
	private void addSchemeCache(Scheme s){
		this.schemeCache.add(s);
	}
	
	// schemeURI = namespace minus "#" (it means last caracter of namespace. 
	protected URI induceSchemeURI(URI conceptURI){
		// AAA# 
		// substring(O,2) to HAVE AAA 
		if (!conceptURI.stringValue().contains("#"))return conceptURI;
		String u=conceptURI.getNamespace();
		return new URIImpl(u.substring(0, u.length()-1));
		
	}
	
	
	public URI getSchemeUriFromOid(String oid) throws SailException, MalformedQueryException, QueryEvaluationException{
		
		if (oid==null)return null;
		URI schemeUri=null;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?schemeUri  "+
							"WHERE "+
							"{ " +
							
							" ?schemeUri skos:notation  ?oid "+
							" } "
							;
		/*
		 * sts.add(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPTSCHEME.getURI(),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.PREFLABEL.getURI(),vf.createLiteral(prefLabel),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.NOTATION.getURI(),vf.createLiteral(oid),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.DEFINITION.getURI(),vf.createLiteral(definition),this.context));
			
		 */
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
		bs.addBinding(new BindingImpl("oid",vf.createLiteral(oid)));
		
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				schemeUri=new URIImpl(a.getValue("schemeUri").toString());
				
			}
			
			
		}
		sc.close();
		
		return schemeUri;
	}
	
	
	
	public Scheme getSchemeBase(URI input) throws SailException, MalformedQueryException, QueryEvaluationException{
		Scheme scheme=null;
		Scheme s=this.getFromSchemeCache(input);
		if (s!=null)return s;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?label ?oid ?def  "+
							"WHERE "+
							"{ " +
							  " ?uri	    rdf:type  skos:ConceptScheme ."+
							  " ?uri		skos:prefLabel  ?label ."+
							  " ?uri	  	skos:notation  ?oid ."+
							  " ?uri	    skos:definition  ?def ."+
							" } "
							;
		/*
		 * sts.add(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPTSCHEME.getURI(),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.PREFLABEL.getURI(),vf.createLiteral(prefLabel),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.NOTATION.getURI(),vf.createLiteral(oid),this.context));
			sts.add(new ContextStatementImpl(uri,SKOS.DEFINITION.getURI(),vf.createLiteral(definition),this.context));
			
		 */
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
	
		SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
		bs.addBinding(new BindingImpl("uri",input));
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
		

		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				scheme=new Scheme();
				scheme.setUri(input);
				Literal loid=(Literal)a.getValue("oid");
				Literal llabel=(Literal)a.getValue("label");
				Literal ldef=(Literal)a.getValue("def");
				scheme.setOid(loid.getLabel());
				scheme.setDef(ldef.getLabel());
				scheme.setPreflabel(new PrefLabel(llabel.getLabel()));
				this.addSchemeCache(scheme);
			}
			
			
		}
		sc.close();
		
		return scheme;
	}
	
	
	
	
	public URI conceptExists(String code,String oid)throws SailException{
		
		URI uri=new URIImpl(this.context+oid+"#"+code);
		URI rep=this.uriExist(uri);
		if (rep==null)return null;
		
		else return uri;
	}
	// no hierarchical there ! No link to scheme !!! 
	public URI addOrGetSimpleConcept(SkosConcept c) throws SailException{
		
		//URI uri=new URIImpl(this.context+c.getScheme().getOid()+"/"+c.getSkosNotation());
		//Modification uri based on scheme URI
		URI uri=new URIImpl(c.getScheme().getUri()+"#"+c.getSkosNotation());
		
		URI rep=this.uriExist(uri);
		if (rep==null){
			List<Statement> sts=Lists.newArrayList();
		//	sts.add(new ContextStatementImpl(SKOS.CONCEPT.getURI(),RDF.,RDF.,,c.getScheme().getUri()));
		//	sts.add(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPT.getURI(),c.getScheme().getUri()));
			sts.add(new ContextStatementImpl(uri,SKOS.PREFLABEL.getURI(),c.getPreflabel().getLit(),c.getScheme().getUri()));
			sts.add(new ContextStatementImpl(uri,SKOS.NOTATION.getURI(),vf.createLiteral(c.getSkosNotation()),c.getScheme().getUri()));
		//	sts.add(new ContextStatementImpl(uri,SKOS.INSCHEME.getURI(),c.getScheme().getUri(),c.getScheme().getUri()));
			if (Strings.emptyToNull(c.getPathLexChapter())!=null){
				LiteralImpl chapter=new LiteralImpl(c.getPathLexChapter(),SemanticVocabulary.pathLexChapter);
				sts.add(new ContextStatementImpl(uri, SKOS.NOTATION.getURI(), chapter,c.getScheme().getUri()));
			}
			
			boolean ok=this.addMultipleStatements(sts);
			if (ok)return uri;
		}
		return rep;
	

	}
	
	
	
	public String  updateStatements(Set<Statement> sts) throws SailException{
		String res="statistics :";
		int yes=0;
		int no =0;
		int total=0;
		for (Statement st:sts){
			total++;
			if (total%20==0)System.out.println("already treated="+total);
			boolean added=this.updateStatement(st);
			if (added)yes++;else no++;
		}
		return res+"->added statements="+yes+" / refused statements="+no+"/Nb statements="+sts.size();
	}
	
	public boolean updateStatement(Statement st) throws SailException{
		if (this.entityExist(st)!=null)return false;
		else this.addSingleStatement(st);
		return false;
	}
	
		
	
	public SkosConcept getErsatzConcept(String skosNotation,String schemeId,boolean analize){
		// schemeId can be urn:oid:2342342342" OR "324234234"
		// analize boolean : should we analyse the skosNotation to detect a /1.0 ??
		String schemeOID=null;
		String code=skosNotation;
		boolean checkCOValue=false;
		Float coValueToCheck=null;
		if (schemeId.startsWith("urn:oid:"))schemeOID=schemeId.replace("urn:oid:","");
		else schemeOID=schemeId;
		try {
			if (analize && skosNotation.contains("/")){
				// CO declaration case. 
				Iterator<String> stringI=Splitter.on("/").split(skosNotation).iterator();
				String savedCode=code;
				try {
					code="";
					String value="";
					
					boolean first=true;
					while (stringI.hasNext()){
						String nextS=stringI.next();
						if (first)code=nextS;
						else {
							if (!stringI.hasNext()){
								value=nextS;
							}
							else code+="/"+nextS;
						}
						
						first=false;
					}
					
					System.out.println();
					coValueToCheck=new Float(value.trim());}catch (Exception e){
						code=savedCode;
						coValueToCheck=null;}
				
				if (coValueToCheck!=null)checkCOValue=true;
				
			}
			URI uri=this.conceptExists(code, schemeOID);
			if (uri !=null){
				
				try {
					SkosConcept skos=this.getConcept(uri);
					if (checkCOValue){
						if (coValueToCheck.equals(skos.getCoValue())){
							skos.setSkosNotation(skosNotation);
							
						}else return null;
					}
					return skos;
				} catch (MalformedQueryException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (QueryEvaluationException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}
	// enter set is usefull for code+schemeOID only !!! 
	public Set<SkosConcept> validation( Set<SkosConcept> toValidate) throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<SkosConcept> result=Sets.newHashSet();
		for (SkosConcept skos:toValidate){
			SkosConcept res=this.getErsatzConcept(skos.getSkosNotation(), skos.getScheme().getOid(),true);
			//URI uri=new URIImpl(this.context+SemanticVocabulary.concept+skos.getScheme().getOid()+"/"+skos.getSkosNotation());
			//SkosConcept res=this.getConcept(uri);
			if (res!=null)result.add(res);
		}
		return result;
	}
	
	
	public Set<SkosConcept> exactMatchSearch(String code,String prefLabel, String oid) throws SailException, MalformedQueryException, QueryEvaluationException{
		// Preconditions.
		if (Strings.isNullOrEmpty(code) && Strings.isNullOrEmpty(prefLabel))return null;
		
		// Code Priority One
		if (!Strings.isNullOrEmpty(code)){
			// oid present --> Set of One unique return (unique couple code+oid)	
			if ((!Strings.isNullOrEmpty(oid))){
				URI uri=new URIImpl(this.context+oid+"#"+code);
				URI rep=this.entityExist(new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPT.getURI(),this.context));
				if (rep!=null){
					return Sets.newHashSet(this.getConcept(uri));
				}
			}
		}
		
		// recherche from Label (with or without OID OR Code (with no OID)
		String FROM ="";
		URI myContext=this.context;
	
		if ((!Strings.isNullOrEmpty(oid))){
			myContext=new URIImpl(this.context.stringValue()+oid);
			FROM="FROM <"+myContext.stringValue() +"> ";
		}
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri "+ FROM + 
							" WHERE "+
							" {  "; 
		if (!Strings.isNullOrEmpty(code))	   queryString+= "  ?uri skos:notation ?code  . ";
		// else --> check label for sur ...
		else								   queryString+="   ?uri skos:prefLabel ?prefLabel . ";
				
		queryString+= " FILTER NOT EXISTS { ?uri rdf:type skos:ConceptScheme }  }" ;
		
		System.err.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , myContext.stringValue());
				SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
				if (!Strings.isNullOrEmpty(code)) bs.addBinding(new BindingImpl("code",this.vf.createLiteral(code)));
				else bs.addBinding(new BindingImpl("prefLabel",new PrefLabel(prefLabel).getLit()));
				sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
		
		Set<SkosConcept> ret=null;
		if (sparqlResults.hasNext()){
			
			ret=Sets.newHashSet();
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				URI uri=new URIImpl(a.getValue("uri").stringValue());
				ret.add(this.getConcept(uri));
			}
			
		}else {System.err.println("No result for exactMatch Search");}
		sc.close();
		
		return ret;
			
	}
	public SkosConcept getConcept(URI uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		
		
		//System.out.println(sparqlURI);
		SailConnection sc=sail.getConnection();
		SkosConcept skos=null;
		
		SPARQLParser parser=new SPARQLParser();
		String FROM="";
		URI myContext=this.context;
		URI schemeURI =this.induceSchemeURI(uri);
		FROM= " FROM <"+schemeURI.stringValue()+"> ";
		myContext=schemeURI;
			
		
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?code ?prefLabel ?coValue ?period "+
							FROM +
							" WHERE "+
							"{ " +
					//		"?uri rdf:type  skos:Concept . " +
							"?uri skos:notation ?code . " +
							"?uri skos:prefLabel ?prefLabel . " +
				   //		"?uri skos:inScheme ?scheme . " +
							"OPTIONAL { ?uri ubiq:CO ?coValue } . " +
							"OPTIONAL { ?uri dc:date ?period } . " +
							// "?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?code) = xsd:string ) .  " +
							"FILTER NOT EXISTS { ?uri rdf:type skos:ConceptScheme } " +
						 //	"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +
							" } "
							;
		
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , myContext.stringValue());
		SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
		bs.addBinding(new BindingImpl("uri",uri));
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
		
		if (sparqlResults.hasNext()){
			skos=new SkosConcept();
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.toString());
				Literal v=(Literal)a.getValue("prefLabel");
				skos.setPreflabel(new PrefLabel(v));
				//System.out.println(skos.getPreflabel().getLit().getLabel());
				//System.out.println(skos.getPreflabel().getLit().getLanguage());
				skos.setScheme(this.getSchemeBase(this.induceSchemeURI(uri)));
				skos.setSkosNotation(a.getValue("code").stringValue());
				if (a.getValue("coValue")!=null){
					Literal value=(Literal)a.getValue("coValue");
					skos.setCoValue(new Float(value.floatValue()));
				}
				if (a.getValue("period")!=null){
				
					//System.err.println("perioooooooood"+a.getValue("period").toString());
					Literal periode=(Literal)a.getValue("period");
					DateTime dt=new DateTime(periode.getLabel());
					String englishShortName = dt.monthOfYear().getAsText(Locale.ENGLISH);
					skos.setPeriod(englishShortName+":"+dt.getYear());
				}
				skos.setUri(uri);
				break;
			}
			
		}
		sc.close();

		return skos;
	}

public URI getConceptUri(String code,String oid) throws SailException, MalformedQueryException, QueryEvaluationException{
		
		URI rep=null;
		//System.out.println(sparqlURI);
		SailConnection sc=sail.getConnection();
		
		SPARQLParser parser=new SPARQLParser();
		String FROM="";
		URI myContext=this.context;
		URI schemeUri=this.getSchemeUriFromOid(oid);
	
		FROM= " FROM <"+schemeUri.stringValue()+"> ";
		myContext=schemeUri;
			
		
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri  "+
							FROM +
							" WHERE "+
							"{ " +
					//		"?uri rdf:type  skos:Concept . " +
							"?uri skos:notation ?code . " +
		
							" } "
							;
		
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , myContext.stringValue());
		SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
		bs.addBinding(new BindingImpl("code",vf.createLiteral(code)));
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				rep=new URIImpl(a.getValue("uri").stringValue());
				break;
			}
			
		}
		sc.close();
		return rep;
	}
	
	public SkosConcept getFullConcept(URI uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SkosConcept c=this.getConcept(uri);
		c.setBroaders(this.getBroaders(c));
		c.setNarrowers(this.getNarrowers(c));
		return c;
	}
	public boolean addTopConcept(SkosConcept skosConcept) throws SailException{
		return this.addSingleStatement(new ContextStatementImpl(skosConcept.getUri(), SKOS.TOPCONCEPTOF.getURI(), skosConcept.getScheme().getUri(),skosConcept.getScheme().getUri()));
	}
	
	public Set<URI> getTopConceptsOfScheme(URI schemeURI)throws SailException, MalformedQueryException, QueryEvaluationException,Exception{
		SailConnection sc=sail.getConnection();
		Set<URI> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x  "+
							"WHERE "+
							"{ " +
							" ?x skos:topConceptOf <" + schemeURI.toString()+"> ."+
							
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				
			//	System.out.println(a.getValue("x"));
				set.add(new URIImpl(a.getValue("x").stringValue()));
			}
			
		}
		sc.close();
		
		return set;
	}
	
	public Set<String> getTopTerms(String uri,String schemeUri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		String FROM= " FROM <"+schemeUri+"> ";
		
			
		
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri   "+ FROM +
							" WHERE "+
							"{ " +
							" ?uri skos:topConceptOf <"+ schemeUri +"> . " +
							" ?uri skos:narrower+  <"+ uri+">  . " +
						
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString ,schemeUri );
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				set.add(uriB);
			}
			
			
		}
		sc.close();
		return set;
	}
	
	
	public Set<String> getBroaders(String uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		//String FROM=" FROM <"+this.induceSchemeURI(new URIImpl(uri)).stringValue()+"> ";
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri   "+
							" WHERE "+
							"{ " +
							" ?uri  skos:narrower  <"+ uri+">  . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				set.add(uriB);
			}
			
			
		}
		sc.close();
		return set;
	}
	
	public Set<URI> getTopBroadersByLevel(URI uri,int distanceBetweenTopConceptAndGoodLevel) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		
		Set<URI> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		SkosConcept skos=this.getConcept(uri);
		 try {
			 Set<URI> set1=this.getTopConceptsOfScheme(skos.getScheme().getUri());
			 if (set1.contains(uri)){
				 System.out.println("IS Already a Top Concept !!!");
				 return null;
			 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (skos==null)return null;
		int i=distanceBetweenTopConceptAndGoodLevel;
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri "+
							"WHERE "+
							"{ " +
							"?uri skos:narrower+  <"+ uri.stringValue()+"> . "+ 
							
							" FILTER EXISTS {" +
							"	?topConcept skos:narrower{"+i+","+i+"} ?uri  . " +
							"   ?topConcept skos:topConceptOf   <"+skos.getScheme().getUri().stringValue()+ "> . } "
							+
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				set.add(new URIImpl(uriB));
			}
			
			
		}
		sc.close();
		return set;
	}
	
	
	public Set<URI> getTopBroaders(URI uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<URI> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		SkosConcept skos=this.getConcept(uri);
		 try {
			 Set<URI> set1=this.getTopConceptsOfScheme(skos.getScheme().getUri());
			 if (set1.contains(uri)){
				 System.out.println("IS Already a Top Concept !!!");
				 return null;
			 }
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (skos==null)return null;
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?uri "+
							"WHERE "+
							"{ " +
							"?uri skos:narrower+  <"+ uri.stringValue()+"> . "+ 
							
							" FILTER EXISTS {?uri skos:topConceptOf   <"+skos.getScheme().getUri().stringValue()+ "> } "
							+
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				set.add(new URIImpl(uriB));
			}
			
			
		}
		sc.close();
		return set;
	}
	
	public Set<SkosConcept> getBroaders(SkosConcept skos) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<SkosConcept> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri   "+
							"WHERE "+
							"{ " +
							" ?uri skos:narrower <"+ skos.getUri().toString()+"> " +
						
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		Set<String> uris=Sets.newHashSet();
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				uris.add(uriB);
			}
			
			
		}
		sc.close();
		
		for (String uri:uris){
			SkosConcept broader=this.getConcept(new URIImpl(uri));
			set.add(broader);
		}
		
		return set;
	}
	
	
	
	public Set<String> getBagOfNarrowers(String uri,int level) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?narrowURI  "+
							"WHERE "+
							"{ ";
		//queryString+=		  " ?narrowURI rdf:type  skos:Concept . ";
		if (level>1){queryString+="<"+uri+"> skos:narrower{1,"+level+"} ?narrowURI . ";}
		if (level==0){queryString+="<"+uri+"> skos:narrower+  ?narrowURI . ";}
		if (level==1){queryString+="<"+uri+"> skos:narrower  ?narrowURI . ";}
		queryString+=       " } ";
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			System.out.println("getBagOfNarrowers OK");
			int i=0;
			while (sparqlResults.hasNext()){
				//System.out.println("Hasext OK"+i++);
				BindingSet a=sparqlResults.next();
				String narrowURI=a.getValue("narrowURI").toString();
				set.add(narrowURI);
			
			}
			
			
		}
		sc.close();
		
		return set;
	}
	
	
	public Set<String> getCDList(int level) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		URI origin=this.createOrGetOriginList();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		set.addAll(this.getCollectionMembersString(origin));
		System.out.println("Before Narrow="+set.size());
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?y ?x  "+
							"WHERE "+
							"{ ";
		queryString+=         " ?x skos:member <"+ origin.toString()+"> .";
		//queryString+=		  " ?y rdf:type  skos:Concept . ";
		if (level>1){queryString+=" ?x skos:narrower{1,"+level+"} ?y . ";}
		if (level==0){queryString+=" ?x skos:narrower+  ?y . ";}
		if (level==1){queryString+=" ?x skos:narrower  ?y . ";}
		
		
		queryString+=" } ";
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		int cptX=0;
		int cptY=0;
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriS=a.getValue("y").toString();
				String uriO=a.getValue("x").toString();
				boolean addedX=set.add(uriO);
				boolean addedY=set.add(uriS);
				if (addedX)cptX++;
				if (addedY)cptY++;
			}
			
			
		}
		sc.close();	
		
		return set;
	}
	
	
	
	public Set<SkosConcept> getNarrowers(SkosConcept skos) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<SkosConcept> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri   "+
							"WHERE "+
							"{ " +
				//			" ?uri rdf:type  skos:Concept . " +
							" <"+ skos.getUri().toString()+"> skos:narrower ?uri . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			Set<String> uris=Sets.newHashSet();
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				uris.add(uriB);
			}
			
			for (String uri:uris){
				SkosConcept narrower=this.getConcept(new URIImpl(uri));
				set.add(narrower);
			}
		}
		sc.close();
		
		//if (set.isEmpty())return this.getNarrowersOposite(skos);
		return set;
	}
	
	// recursive creation of narrowers...
	public SkosConcept getAllNarrowers(SkosConcept skos ,int lim,boolean start) throws SailException, MalformedQueryException, QueryEvaluationException{
		 boolean nolimit=false;
		 int level=0;
		 if (start && lim==0){
			 nolimit=true;
		 }
		 else if (lim==0) {
			 return skos;
		 }
		 else level=lim-1;
		 
		 Set<SkosConcept> skosSet=this.getNarrowers(skos);
		 skos.setNarrowers(skosSet);
		 for (SkosConcept s:skos.getNarrowers()){
			 this.getAllNarrowers(s,level,nolimit);
		 }
		
		 return skos;
	}
	
	public SkosConcept getAllBroaders(SkosConcept skos ,int lim,boolean start) throws SailException, MalformedQueryException, QueryEvaluationException{
		 boolean nolimit=false;
		 int level=0;
		 if (start && lim==0){
			 nolimit=true;
		 }
		 else if (lim==0) {
			 return skos;
		 }
		 else level=lim-1;
		 
		 Set<SkosConcept> skosSet=this.getBroaders(skos);
		 skos.setBroaders(skosSet);
		 for (SkosConcept s:skosSet){
			 this.getAllBroaders(s,level,nolimit);
		 }
		
		 return skos;
	}
	
	public Set<String> getSubOriginFromLevels(int level) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		URI origin=this.createOrGetOriginList();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		set.addAll(this.getCollectionMembersString(origin));
		System.out.println("Before Narrow="+set.size());
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?y ?x  "+
							"WHERE "+
							"{ ";
		queryString+=         " ?x skos:member <"+ origin.toString()+"> .";
		//queryString+=		  " ?y rdf:type  skos:Concept . ";
		if (level>1){queryString+=" ?x skos:narrower{1,"+level+"} ?y . ";}
		if (level==0){queryString+=" ?x skos:narrower+  ?y . ";}
		if (level==1){queryString+=" ?x skos:narrower  ?y . ";}
		
		
		queryString+=" } ";
		
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		int cptX=0;
		int cptY=0;
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriS=a.getValue("y").toString();
				String uriO=a.getValue("x").toString();
				boolean addedX=set.add(uriO);
				boolean addedY=set.add(uriS);
				if (addedX)cptX++;
				if (addedY)cptY++;
			}
			
			
		}
		sc.close();
		
		
		return set;
	}
	
	/*
	 * Import TSV UTF-8 files.
	 * rules : All lines got same number of element
	 * data should not contain TAB characters 
	 * 3 data cells lines :
	 * oid	code	preflabel
	 * 2 data cell lines : (no scheme oid declaration)
	 * code	preflabel.
	 * [ in that case, a unique scheme is created / code are unique inside the file.]
	 * 
	 */
	public void importTSV(String filePath) throws SailException{
		
		BufferedReader  file=null;
		try {
			file=Files.newReader(new File( filePath), Charsets.UTF_8);
			String fileLine=null;
			boolean first=true;
			int nbOfCells=3;
			int LineNumber=0;
			URI schemeDefault=null;
			while ((fileLine=file.readLine())!=null ){
				LineNumber++;
				fileLine=fileLine.trim();
				if (fileLine.isEmpty()||fileLine.startsWith(" ")||fileLine.startsWith("#")||fileLine.startsWith("//")){
					System.out.println(LineNumber+" <-- Line Number --> comment line detected= "+fileLine);
				}else {
					System.out.println("treated Line=="+fileLine);
					ArrayList<String> orderedCells=Lists.newArrayList();
					Splitter splitter =Splitter.on('	');
					Iterator<String> ite=splitter.split(fileLine).iterator();
					while (ite.hasNext()){
						orderedCells.add(ite.next());
					}
					if (first){
						nbOfCells=orderedCells.size();
						if (nbOfCells !=2 && nbOfCells !=3 ){
							throw new Exception ("No good file format");
						}
						// init for default 2 cells format.
						if (nbOfCells==2)schemeDefault=this.applyScheme("default", "default scheme", "default scheme  for codes");
					}
					first=false;
					
					if (nbOfCells==2){
						SkosConcept skos=new SkosConcept();
						skos.setSkosNotation(orderedCells.get(0).trim());
						skos.setPreflabel(new PrefLabel(orderedCells.get(1).trim()));
						skos.setScheme(this.getSchemeBase(schemeDefault));
						skos.setUri(this.addOrGetSimpleConcept(skos));
						this.link(skos.getUri(), SKOS.TOPCONCEPTOF.getURI(), schemeDefault);
					}
					if (nbOfCells==3){
						SkosConcept skos=new SkosConcept();
						skos.setSkosNotation(orderedCells.get(1).trim());
						skos.setPreflabel(new PrefLabel(orderedCells.get(2).trim()));
						URI schemeURI=this.applyScheme(orderedCells.get(0).trim(), orderedCells.get(0).trim(), orderedCells.get(0).trim());
						skos.setScheme(this.getSchemeBase(schemeURI));
						skos.setUri(this.addOrGetSimpleConcept(skos));
						this.link(skos.getUri(), SKOS.TOPCONCEPTOF.getURI(), schemeURI);
					}
				}
			}
			
		} catch (Exception e1) {
			
			e1.printStackTrace();
			System.err.println("No valid TSV file found");
			return;
		}
		
		
	
	}
	
	/*
	 Importation Rules :
	  URI graph pattern         : Domain/version/scheme_oid 
	  URI graph concept pattern : Domain/version/scheme_oid#code
	  
	  1. ConceptScheme are used  to create graph (sparql context)
	  
	 */
	@Override
public void importRDF(String filePath) throws SailException{
		
		FileReader file=null;
		try {
			file = new FileReader (filePath);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.out.println("nothing done");
			return;
		}
		
		List<Statement> myList = Lists.newArrayList();
		StatementCollector collector = new StatementCollector(myList);
		RDFParser rdfParser = new RDFXMLParser();
		rdfParser.setRDFHandler(collector);
		try {
			   rdfParser.parse(file, "http://server.ubiqore.com/v1");
			  
			} 
			catch (IOException e) {
			  return;
			}
			catch (RDFParseException e) {
				return;
			}
			catch (RDFHandlerException e) {
				return;
			}
		int count=collector.getStatements().size();
		Set<URI> contexts=Sets.newHashSet();
 		for (Statement st:collector.getStatements()){
 		//	System.err.println(SKOS.CONCEPTSCHEME.getURI());
			if (st.getObject().stringValue().equalsIgnoreCase(SKOS.CONCEPTSCHEME.getURI().stringValue())){
				System.err.println(st);
				contexts.add(new URIImpl(st.getSubject().stringValue()));
			}
		}
		System.out.println(count+" --> nb statements inside the file");
		
		this.addMultipleStatements((List<Statement>)collector.getStatements(),contexts);
		
		
	
	}
	
	protected boolean addMultipleStatements(List<Statement> sts,Set<URI> contexts)throws SailException{
		if (sts==null)return false;
		if (sts.isEmpty())return false;
		String string=null;
		try {
			SailConnection sc=sail.getConnection();
			for (Statement st:sts){
				String context=this.context.stringValue();
				for (URI uricontext:contexts){
					if (st.getSubject().stringValue().contains(uricontext.stringValue())){
						context=uricontext.stringValue();
						break;
					}
				}
				sc.addStatement(st.getSubject(),st.getPredicate(),st.getObject(),new URIImpl(context));
				
			}
			sc.commit();
			sc.close();
			return true;
		}catch (Exception e){
			System.out.println("error addStatement..."+string);
			e.printStackTrace();
			if (sc!=null)if (sc.isOpen())sc.close();
		}
		return false;
	}
	
	

}
