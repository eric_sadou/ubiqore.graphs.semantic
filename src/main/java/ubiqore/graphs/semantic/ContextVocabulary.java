package ubiqore.graphs.semantic;

import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;

/*
 * DATAELEMENT VOCABULARY = used inside DATAELEMENTSGraph
 */
public class ContextVocabulary {
	   
	public static final String FULL="full";
	public static final String LEVELS="levels";
	public static final String EXPANDED="expanded";
	
	public static final String ubiq="http://server.ubiqore.com/ubiq/core#";
	public static final String align="http://knowledgeweb.semanticweb.org/heterogeneity/alignment#";
	
	/*
	 * SYSTEM ELEMENTS
	 */
	public static final String DataElement_String =ubiq+"DataElement";
	public static final URI DataElement =new URIImpl(DataElement_String);
	

	public static final String topCategory_String =ubiq+"topCategory";
	public static final URI topCategory =new URIImpl(topCategory_String);
	
	public static final String elementType_String =ubiq+"elementType";
	public static final URI elementType =new URIImpl(elementType_String);
	
	public static final String value_String =ubiq+"value";
	public static final URI value =new URIImpl(value_String);
	
	public static final String ucumCode_String =ubiq+"ucumCode";
	public static final URI ucumCode =new URIImpl(ucumCode_String);
	
	public static final String ucumName_String =ubiq+"ucumName";
	public static final URI    ucumName =new URIImpl(ucumName_String);
	
	public static final String alignment_String=align+"Alignment";
	public static final URI alignment =new URIImpl(alignment_String);
	
	// type level xml map Cell method entity1 entity2 measure relation validity onto2 onto1 Ontology formalism location 
	public static final String type_String =align+"type";
	public static final String level_String =align+"level";
	public static final String xml_String =align+"xml";
	public static final String map_String =align+"map";
	public static final String Cell_String =align+"Cell";
	public static final String method_String =align+"method";
	public static final String entity1_String =align+"entity1";
	public static final String entity2_String =align+"entity2";
	public static final String measure_String =align+"measure";
	public static final String relation_String =align+"relation";
	public static final String validity_String =align+"validity";
	public static final String onto2_String =align+"onto2";
	public static final String onto1_String =align+"onto1";
	public static final String Ontology_String =align+"Ontology";
	public static final String formalism_String =align+"formalism";
	public static final String location_String =align+"location";
	
	public static final URI type =new URIImpl(type_String);
	public static final URI level =new URIImpl(level_String);
	public static final URI xml =new URIImpl(xml_String);
	public static final URI map =new URIImpl(map_String);
	public static final URI Cell =new URIImpl(Cell_String);
	public static final URI method =new URIImpl(method_String);
	public static final URI entity1 =new URIImpl(entity1_String);
	public static final URI entity2 =new URIImpl(entity2_String);
	public static final URI measure =new URIImpl(measure_String);
	public static final URI relation =new URIImpl(relation_String);
	public static final URI validity =new URIImpl(validity_String);
	public static final URI onto2 =new URIImpl(onto2_String);
	public static final URI onto1 =new URIImpl(onto1_String);
	public static final URI Ontology =new URIImpl(Ontology_String);
	public static final URI formalism =new URIImpl(formalism_String);
	public static final URI location =new URIImpl(location_String);
	
	
	
	
	/*
	 * DATA ELEMENTS Vocabulary
	 */
	
	
	
	/*
	 * ROOT : a root context of DATAELEMENTSGraph (many roots could be provide in differents contexts)
	 */
	public static final String ROOT_String =ubiq+"ROOT";
	public static final URI ROOT =new URIImpl(ROOT_String);
	
	/*
	 * TOPCAT : TOP CATEGORY (members of ROOTS)
	 */
	public static final String TOPCAT_String =ubiq+"TOPCAT";
	public static final URI TOPCAT =new URIImpl(TOPCAT_String);
	
	
	/*
	 * CAT : CATEGORY (members of TOPCAT OR CAT )
	 */
	public static final String CAT_String =ubiq+"CAT";
	public static final URI CAT =new URIImpl(CAT_String);
	
	/*
	 * CE_CD : DataElement(DE) composed by unique Concept Descriptor(CD) --> With semantic Expansion ; One of his subconcept could be used ..
	 * Example : System should provide presence of a specific medication / The CD is an entry of Source Terminology to show the way of choice..
	 * Note : Name should be changed if confusion with CD
	 * The Result of a CE_CD is A CD by definition.
	 */
	public static final String CE_CD_String =ubiq+"CE_CD";
	public static final URI CE_CD =new URIImpl(CE_CD_String);
	
	
	/*
	 * CE_PQ : DataElement(DE) composed by a Concept Descriptor(CD) AND a associated Physical Quantity  definition. 
	 * Example : System should provide The Body Size / The CD is BodySize and PQ definition = Meter.
	 * The REsult is a PQ Type Element  56 KG associated to a CD (Weight )
	 */
	public static final String CE_PQ_String =ubiq+"CE_PQ";
	public static final URI CE_PQ =new URIImpl(CE_PQ_String);
	
	/*
	 * CE_TS : DataElement(DE) composed by a Concept Descriptor(CD) But indicates the user that a Point In Time is necessary to the DataElement.
	 * Example : System should provide The Birth Date / The CD is Birth  but a specific TS value shall be provide.
	 *
	 */
	public static final String CE_TS_String =ubiq+"CE_TS";
	public static final URI CE_TS =new URIImpl(CE_TS_String);
	
	/*
	 * CE_VS : DataElement(DE) composed by a Concept Descriptor(CD) But indicates that the Value to choose is On a List of CD /CO Elements
	 * Example : System should provide The ECOG / The CD is ECOG  but a specific ECOG_VALUE should be provide.
	 *
	 */
	public static final String CE_VS_String =ubiq+"CE_VS";
	public static final URI CE_VS =new URIImpl(CE_VS_String);
	
	public static final String skos_prefix="skos";									//	 [ SKOS]
	public static final String ubiq_prefix="ubiq";
	public static final String align_prefix="align";
	
	
	
	/*
	 * DATA TYPE DEFINITION : 
	 */
	
	//public static final String PQ_String =ubiq+"PQ";
	//public static final URI PQ =new URIImpl(PQ_String);
	public static final String CE_CO_String =ubiq+"CE_CO";
	public static final URI CE_CO =new URIImpl(CE_CO_String);

	
	/*
	 * NAMESPACE
	 */
	public static final String org_ns="http://www.w3.org/ns/org#";					//	 [ ORG]
	public static final String org_prefix="org";
	
	public static final String foaf="http://xmlns.com/foaf/0.1/";					//   [ FOAF ]
	public static final String gr="http://purl.org/goodrelations/v1#";				//	 [ GOOD-RELATIONS ]
	public static final String prov="http://www.w3.org/ns/prov#";					//	 [ PROV-O ]	
	public static final String owl="http://www.w3.org/2002/07/owl#";				//	 [ OWL2-PRIMER ]
	public static final String time="http://www.w3.org/2006/time#";				//	 [ OWL-TIME ]
	public static final String rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#";	//	 [ RDF-CONCEPTS ]
	public static final String rdfs="http://www.w3.org/2000/01/rdf-schema#";		//	 [ RDF-SCHEMA ]
	public static final String skos="http://www.w3.org/2004/02/skos/core#";		//	 [ SKOS-REFERENCE ]
	public static final String vcard="http://www.w3.org/2006/vcard/ns#";			//	 [ VCARD ]
	public static final String dct="http://purl.org/dc/terms/";					//	 [ DC11 ]
	
	public static final String domain_default="http://server.ubiqore.com";
	public static final String version_default="v1";
	public static final String context_v1=domain_default+"/"+version_default+"/";
	public static final String scheme="scheme/";
}
