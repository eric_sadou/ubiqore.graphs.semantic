package ubiqore.graphs.semantic;

import java.util.Locale;

import org.openrdf.model.Literal;

public class AltLabel extends Label {

	public AltLabel(String st) {
		super(st);
		// TODO Auto-generated constructor stub
	}

	public AltLabel(Literal lit){
		
		super(lit.getLabel());
		this.langage=new Locale(lit.getLanguage());
	}
	
	
}
