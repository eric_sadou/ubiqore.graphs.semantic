package ubiqore.graphs.semantic;

import info.aduna.iteration.CloseableIteration;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.joda.time.DateTime;
import org.openrdf.model.Namespace;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ContextStatementImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.DC;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedBooleanQuery;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.rio.RDFHandler;
import org.openrdf.rio.RDFHandlerException;
import org.openrdf.rio.RDFParseException;
import org.openrdf.rio.RDFParser;
import org.openrdf.rio.helpers.StatementCollector;
import org.openrdf.rio.n3.N3Writer;
import org.openrdf.rio.rdfxml.RDFXMLParser;
import org.openrdf.rio.rdfxml.RDFXMLWriter;
import org.openrdf.rio.rdfxml.util.RDFXMLPrettyWriter;
import org.openrdf.sail.Sail;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;

import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import com.tinkerpop.blueprints.impls.sail.SailTokens;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;


public abstract class AbstractGraph {
	boolean connected=false;
	public boolean isConnected() {
		return connected;
	}
	

	
	//SailOutGraph g=null;
	SailConnection sc=null;
	protected Neo4jGraph baseGraph =null;
	protected String rep="/reborn/tmp/AbstractGraphDir";
	protected ValueFactory vf=null;
	protected  URI context=new URIImpl(SemanticVocabulary.context_default);
	
	public URI getContext() {
		return context;
	}
	public void setContext(URI context) {
		this.context = context;
	}

	protected Sail sail;
	String queryPrepare=null;
	public void reconnect(){
		if (connected)return;
		try {
			this.initAccess();
			connected=true;
			this.queryPrepare();
			
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public AbstractGraph(boolean creation,String rep) throws SailException{
		if (!Strings.isNullOrEmpty(rep))this.rep=rep;
		
		if (creation){
			this.creationIntitialyze();
		}
		this.initAccess();
		connected=true;
		if (creation){
			this.createVocabularyStatementsList();
			this.createSpecificGraphStatements();
			
		}
		this.createNamespacesMap();
		this.addDefaultNamespaces();
		this.queryPrepare();
	}
	
	protected List<Statement> vocabularyStatements=Lists.newArrayList();
	protected Map<String,String> namespaces=Maps.newHashMap();
	
	protected abstract void createVocabularyStatementsList();
	protected abstract void createNamespacesMap();
	
	protected boolean removeMultipleStatements(Set<Statement> sts)throws SailException{
		
		if (sts==null)return false;
		if (sts.isEmpty())return false;
		try {
			SailConnection sc=sail.getConnection();
			for (Statement st:sts){
				//string=st.getSubject().toString()+" "+st.getPredicate().getLocalName()+ " "+st.getObject().toString();
				//System.out.println("triplet to add "+string);
				sc.removeStatements(st.getSubject(),st.getPredicate(),st.getObject(),st.getContext());
				
			}
			sc.commit();
			sc.close();
			return true;
		}catch (Exception e){
			sc.rollback();
			System.out.println("error removeStatement...rollback for everything");
			e.printStackTrace();
			if (sc!=null)if (sc.isOpen())sc.close();
		}
		return false;
	}
	
	protected boolean addMultipleStatements(List<Statement> sts)throws SailException{
		if (sts==null)return false;
		if (sts.isEmpty())return false;
		String string=null;
		try {
			SailConnection sc=sail.getConnection();
			for (Statement st:sts){
				string=st.getSubject().toString()+" "+st.getPredicate().getLocalName()+ " "+st.getObject().toString();
				//System.out.println("triplet to add "+string);
				sc.addStatement(st.getSubject(),st.getPredicate(),st.getObject(),st.getContext());
				
			}
			sc.commit();
			sc.close();
			return true;
		}catch (Exception e){
			System.out.println("error addStatement..."+string);
			e.printStackTrace();
			if (sc!=null)if (sc.isOpen())sc.close();
		}
		return false;
	}
	
	public boolean addMultipleStatements(Set<Statement> sts,int commitFlag)throws SailException{
		if (sts==null)return false;
		if (sts.isEmpty())return false;
		String string=null;
		System.out.println("nb sts "+sts.size());
		try {
			SailConnection sc=sail.getConnection();
			DateTime start=DateTime.now();
			System.out.println(start.toString()  +" start of commit.");
			int i=0;
			for (Statement st:sts){
				i++;
				if (i%commitFlag==0){
					System.out.println(i+"+++++>"+st.toString());
					DateTime flag=DateTime.now();
					System.out.println((flag.getMillis()-start.getMillis())/1000 +" sec. since last commit. ");
					start=flag;
				}
				string=st.getSubject().toString()+" "+st.getPredicate().getLocalName()+ " "+st.getObject().toString();
				//System.out.println("triplet to add "+string);
				sc.addStatement(st.getSubject(),st.getPredicate(),st.getObject(),st.getContext());
				if (i%commitFlag==0)sc.commit();
			}
			sc.commit();
			sc.close();
			return true;
		}catch (Exception e){
			System.out.println("error addStatement..."+string);
			e.printStackTrace();
			if (sc!=null)if (sc.isOpen())sc.close();
		}
		return false;
	}

	
	public Set<String> getSubDomain(String uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri   "+
							"WHERE "+
							"{ " +
							" ?uri rdf:type  skos:Concept . " +
							" ?uri rdfs:subClassOf <" +uri+"> . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("uri").toString();
				set.add(uriB);
			}
			
			
		}
		sc.close();
		return set;
	}	
	
	
	public Set<String> getTransitiveSubClasses(String fatherURI) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		Set<String> set=Sets.newHashSet();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x   "+
							"WHERE "+
							"{ " +
							" ?x rdf:type  skos:Concept . " +
							" ?x rdfs:subClassOf+ <" +fatherURI+"> . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
			
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				String uriB=a.getValue("x").toString();
				set.add(uriB);
			}
			
			
		}
		sc.close();
		return set;
	}	
	
	
	
	
	
public void importRDF(String filePath) throws SailException{
		
		FileReader file=null;
		try {
			file = new FileReader (filePath);
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			System.out.println("nothing done");
			return;
		}
		
		List<Statement> myList = Lists.newArrayList();
		StatementCollector collector = new StatementCollector(myList);
		RDFParser rdfParser = new RDFXMLParser();
		rdfParser.setRDFHandler(collector);
		try {
			   rdfParser.parse(file, "http://server.ubiqore.com/v1");
			  
			} 
			catch (IOException e) {
			  // handle IO problems (e.g. the file could not be read)
			}
			catch (RDFParseException e) {
			  // handle unrecoverable parse error
			}
			catch (RDFHandlerException e) {
			  // handle a problem encountered by the RDFHandler
			}
		int count=collector.getStatements().size();
		System.out.println(count+" --> nb statements inside the file");
		
		this.addMultipleStatements((List<Statement>)collector.getStatements());
		
		
	
	}
	
	
	protected boolean addSingleStatement(Statement st)throws SailException{
		if (st==null)return false;
		try {
			Resource r=null;
			if (st.getContext()==null){
				r=this.getContext();
			}else r=st.getContext(); 
			//baseGraph.setCheckElementsInTransaction(false);
			SailConnection sc=sail.getConnection();
			sc.addStatement(st.getSubject(),st.getPredicate(),st.getObject(),r);
			
			sc.commit();
			//baseGraph.setCheckElementsInTransaction(true);
			sc.close();
			return true;
		}catch (Exception e){
			e.printStackTrace();
			if (sc!=null)if (sc.isOpen())sc.close();
		}
		return false;
	}
	
	protected URI entityExist(Statement st)throws SailException{
		URI uri=null;
		try {
			SailConnection sc=sail.getConnection();
			ValueFactory vf = sail.getValueFactory();
			
			CloseableIteration<? extends Statement, SailException> results =sc.getStatements(st.getSubject(),st.getPredicate(),st.getObject(),true,st.getContext());
			while(results.hasNext()) {
				
			    Statement first=results.next();
			    uri=(URI) first.getSubject();
			    
			    break;
			}
			
			
			sc.close();
			
			
		}catch (Exception e){
			
			if (sc!=null)if (sc.isOpen())sc.close();
		}
		finally {
			return uri; 
		}
	}
	
	public URI uriExist(URI  uriInput)throws SailException{
		URI uri=null;
		try {
			SailConnection sc=sail.getConnection();
			
			
			CloseableIteration<? extends Statement, SailException> results =sc.getStatements(uriInput,null,null,false);
			while(results.hasNext()) {
				
			    Statement first=results.next();
			    uri=(URI) first.getSubject();
			    
			    break;
			}
			
			
			sc.close();
			
			
		}catch (Exception e){
			
			e.printStackTrace();
		}
		finally {
			if (sc!=null)if (sc.isOpen())sc.close();
		
		}
		return uri; 
	}
	
	protected Set<String> getCollectionMembersString(URI collection) throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<String> set=Sets.newHashSet();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?c  "+
							"WHERE "+
							"{ " +
							" ?c skos:member  <"+collection+"> ."+
							" <"+collection+"> rdf:type skos:Collection . "+  
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
		
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.getValue("c"));
				boolean x=set.add((a.getValue("c").stringValue()));
				if (!x)System.out.println(a.getValue("c").stringValue());
				
			}
			
		}
		
		sc.close();
		return set;
	 
	}
 	
	
	protected List<URI> getCollectionMembers(URI collection) throws SailException, MalformedQueryException, QueryEvaluationException{
		List<URI> set=Lists.newArrayList();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT DISTINCT ?c  "+
							"WHERE "+
							"{ " +
							" ?c skos:member  <"+collection+"> ."+
							" <"+collection+"> rdf:type skos:Collection . "+  
							" } "
							;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
		
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.getValue("c"));
				boolean x=set.add(new URIImpl(a.getValue("c").stringValue()));
				if (!x)System.out.println(a.getValue("c").stringValue());
				
			}
			
		}
		
		sc.close();
		return set;
	 
	}
 	
	
	private void createSpecificGraphStatements() throws SailException{
		if (vocabularyStatements.isEmpty())return;
		
		try {
			SailConnection sc=sail.getConnection();
			ValueFactory vf = sail.getValueFactory();
			for (Statement st:vocabularyStatements)sc.addStatement(st.getSubject(),st.getPredicate(),st.getObject(),st.getContext());
			sc.commit();
			sc.close();
			
		}catch (Exception e){
			
			if (sc!=null)if (sc.isOpen())sc.close();
		}
		
		
	}
	
	
	protected void initAccess() throws SailException{
		baseGraph = new Neo4jGraph(rep);
		baseGraph.setCheckElementsInTransaction(true);
		sail= new GraphSail(baseGraph, "p,c,pc,poc");
		sail.initialize();
		vf=sail.getValueFactory();
	}
	
	
List<URI> inverseDeclarationCache=Lists.newArrayList();
List<URI> equivalentDeclarationCache=Lists.newArrayList();
List<URI> notInverseDeclarationCache=Lists.newArrayList();
List<URI> notEquivalentDeclarationCache=Lists.newArrayList();


    // detecte si RELA = Subject  of a A INVERSE B 
   // si A = sublect of this declaration ... the inverse should be create !
	protected boolean isInverse(URI rela)throws SailException{
		// cache check ...
		for (URI uri:inverseDeclarationCache)if(rela.equals(uri))return true;
		for (URI uri:notInverseDeclarationCache)if(rela.equals(uri))return false;
		
		boolean b=false;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		
		String queryString =this.queryPrepare()+" ASK  "+
							"WHERE "+
							"{ " +
							" <"+rela.stringValue()+"> owl:inverseOf ?q . " +
							" } "
							;
		
		
		
		
		//System.out.println(queryString);
		
		try {
			ParsedBooleanQuery query=(ParsedBooleanQuery) parser.parseQuery(queryString , context.toString());
			
			sparqlResults=sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
			
			try {
				b= sparqlResults.hasNext();	
				
			} 
			catch (QueryEvaluationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (sc.isOpen())sc.close();
		
		if (b)inverseDeclarationCache.add(rela);
		else notInverseDeclarationCache.add(rela);
		return b;
	}
	
	protected boolean isEquivalent(URI rela)throws SailException{
		// cache check ...
		for (URI uri:equivalentDeclarationCache)if(rela.equals(uri))return true;
		for (URI uri:notEquivalentDeclarationCache)if(rela.equals(uri))return false;
		
		boolean b=false;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		
		String queryString =this.queryPrepare()+" ASK  "+
							"WHERE "+
							"{ " +
							" <"+rela.stringValue()+"> owl:equivalentProperty ?q . " +
							" } "
							;
		
		
		
		
		//System.out.println(queryString);
		
		try {
			ParsedBooleanQuery query=(ParsedBooleanQuery) parser.parseQuery(queryString , context.toString());
			
			sparqlResults=sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
			
			try {
				b= sparqlResults.hasNext();	
				
			} 
			catch (QueryEvaluationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sc.close();
		if (b)equivalentDeclarationCache.add(rela);
		else notEquivalentDeclarationCache.add(rela);
		
		return b;
	}
	
	
	protected void applyEquivalent(Statement st) throws SailException{
		Set<URI> predicate=null;
		SailConnection sc=sail.getConnection();
	
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		try {
		String queryString =this.queryPrepare()+" SELECT ?p   "+
							"WHERE "+
							"{ " +
							" <"+st.getPredicate()+"> owl:equivalentProperty  ?p . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		
		
		if (sparqlResults.hasNext()){
			
			predicate=Sets.newHashSet();
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				predicate.add(new URIImpl(a.getValue("p").stringValue()));
			
			}
			
			
		}
		}catch (Exception e){e.printStackTrace();}
		
		sc.close();
		if(predicate!=null){
			for (URI uri:predicate){
				Statement equiv=new ContextStatementImpl(st.getSubject(),uri,new URIImpl(st.getObject().toString()),this.context);
				this.addSingleStatement(equiv);
			}
		}
		
	}
	
	protected void applyInverse(Statement st) throws SailException{
		Set<URI> predicate=null;
		SailConnection sc=sail.getConnection();
	
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		try {
		String queryString =this.queryPrepare()+" SELECT ?p   "+
							"WHERE "+
							"{ " +
							" <"+st.getPredicate()+"> owl:inverseOf ?p . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		
		
		if (sparqlResults.hasNext()){
			
			predicate=Sets.newHashSet();
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				predicate.add(new URIImpl(a.getValue("p").stringValue()));
			}
			
			
		}
		}catch (Exception e){e.printStackTrace();}
		
		sc.close();
		if(predicate!=null){
			for (URI uri:predicate)this.addSingleStatement(new ContextStatementImpl(new URIImpl(st.getObject().toString()), uri, st.getSubject(), this.context));
		}
		
	}
	
	// add a relation / inverse Relation mannaged by owl:inverseOf declaration..
	//				 / equivalent Relation managed by owl:EquivalentProperty declaration...
	public void link(URI concept1, URI relation1to2 ,URI concept2) throws SailException{
			Statement st=new ContextStatementImpl(concept1,relation1to2,concept2,this.context);
			this.addSingleStatement(st);
			
			if (this.isInverse(relation1to2)){
				this.applyInverse(st);
			}
			
			if (this.isEquivalent(relation1to2)){
				this.applyEquivalent(st);
			}
		}
	
	
	protected String queryPrepare() {
		if (Strings.isNullOrEmpty(queryPrepare)){
			String prepare="";
			try {
				SailConnection sc=sail.getConnection();
			CloseableIteration<? extends Namespace, SailException> map=sc.getNamespaces();
			while (map.hasNext()){
				Namespace ns=map.next();
				prepare+="PREFIX "+ns.getPrefix()+": <"+ns.getName() +">  ";
			}
			sc.close();
			}catch (Exception e){
				e.printStackTrace();
			}
			queryPrepare=prepare;
		}
		return queryPrepare;
	}
	
	private void creationIntitialyze(){
		// remove the dir..
		File dir=new File (rep);
		if (dir.exists()){
			System.out.println("The graph dir will be delete and all files inside.. sorry baby!! it's too late now..");
			AbstractGraph.recursiveDelete(dir);
			System.out.println("Graph will be install on the fresh place now..");
		}else {
			dir.mkdirs();
		}
	}
	
	
	private static void recursiveDelete(File file) {
        if (!file.exists()) return;
        if (file.isDirectory()) {
           for (File f : file.listFiles()) recursiveDelete(f);
           file.delete();
        } else {
           file.delete();
        }
} 

	 public void addDefaultNamespaces() {
		 if (this.namespaces.isEmpty())return;
		 Iterator<Entry<String, String>> it=this.namespaces.entrySet().iterator();
		 while (it.hasNext()){
			 Entry<String, String> entry=it.next();
			 this.addNamespace(entry.getKey(), entry.getValue());
		 }
	 }

	 public void addNamespace(final String prefix, final String namespace)  {
		 	SailConnection sc=null;
	        try {
	        	sc=sail.getConnection();
	            sc.setNamespace(prefix, namespace);
	            sc.commit();
	        } catch (SailException e) {
	            throw new RuntimeException(e.getMessage(), e);
	        }finally {
	        	if (sc!=null){
	        		try {
						sc.close();
					} catch (SailException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	        	}
	        }
	    }

	protected void useClassicNamespaces(){
		    this.namespaces.put(SailTokens.RDF_PREFIX, SailTokens.RDF_NS);
	        this.namespaces.put(SailTokens.RDFS_PREFIX, SailTokens.RDFS_NS);
	        this.namespaces.put(SailTokens.OWL_PREFIX, SailTokens.OWL_NS);
	        this.namespaces.put(SailTokens.XSD_PREFIX, SailTokens.XSD_NS);
	        this.namespaces.put(SailTokens.FOAF_PREFIX, SailTokens.FOAF_NS);
	        this.namespaces.put(DC.PREFIX, DC.NAMESPACE);
	}
		public void statements() throws SailException{
		
		SailConnection sc=sail.getConnection();
		
		CloseableIteration<? extends Statement, SailException> results = sc.getStatements(null, null, null, false);
		while(results.hasNext()) {
		    System.out.println(results.next());
		}

		System.out.println("\nget statements:  ?p ?o ?g");
		
		sc.close();
	}
	
		public Set<Statement> getFullStatements() throws SailException{
			Set<Statement> set=Sets.newHashSet();
			SailConnection sc=sail.getConnection();
			
			CloseableIteration<? extends Statement, SailException> results = sc.getStatements(null, null, null, false);
			while(results.hasNext()) {
			    set.add(results.next());
			}

			System.out.println("\nget statements:  ?p ?o ?g");
			
			sc.close();
			return set;
		}
			
		
		
		public void statements(URI object,boolean infered)throws SailException{
			
			SailConnection sc=sail.getConnection();
			
			CloseableIteration<? extends Statement, SailException> results = sc.getStatements(null, null, object, infered);
			while(results.hasNext()) {
			    System.out.println("statement "+results.next());
			}

			
			sc.close();
		}
		public void statements(boolean infered) throws SailException{
			
			SailConnection sc=sail.getConnection();
			
			CloseableIteration<? extends Statement, SailException> results = sc.getStatements(null, null, null, infered);
			while(results.hasNext()) {
			    System.out.println("statement "+results.next());
			}

			
			sc.close();
		}
		
	public void disconnect(){
		if (!connected)return;
		try {
			sail.shutDown();
			this.queryPrepare=null;
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		connected=false;
	}
	
	public void exportRDF(String where) throws SailException{
		RDFHandler rdfxmlWriter = null;
		if (where==null)rdfxmlWriter=new RDFXMLPrettyWriter(System.out);
		else {
			File f=new File (where);
			java.io.FileOutputStream fos;
			try {
				fos = new java.io.FileOutputStream(f);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
			
			 rdfxmlWriter=new RDFXMLPrettyWriter(fos);
		}
		
		SailConnection sc=sail.getConnection();
		CloseableIteration<? extends Namespace, SailException> nss=sc.getNamespaces();
		
		//CloseableIteration<? extends Statement, SailException> res=sc.getStatements(null, null, null, true, this.context);
		CloseableIteration<? extends Statement, SailException> res=sc.getStatements(null, null, null, false);
		try {
			rdfxmlWriter.startRDF();
		} catch (RDFHandlerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		while (nss.hasNext()){
			try {
				Namespace ns=nss.next();
				rdfxmlWriter.handleNamespace(ns.getPrefix(),ns.getName());
			} catch (RDFHandlerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		while(res.hasNext()) {
			try {
				
				rdfxmlWriter.handleStatement(res.next());
			} catch (RDFHandlerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		}
		try {
			rdfxmlWriter.endRDF();
		} catch (RDFHandlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sc.close();
	}
	
	public void exportN3(String where) throws SailException{
		RDFHandler n3Writer = null;
		if (where==null){
			n3Writer=new N3Writer(System.out);
		}
		else {
			File f=new File (where);
			java.io.FileOutputStream fos;
			try {
				fos = new java.io.FileOutputStream(f);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
			
			n3Writer=new N3Writer(fos);
		}
		
		SailConnection sc=sail.getConnection();
		CloseableIteration<? extends Namespace, SailException> nss=sc.getNamespaces();
		
		//CloseableIteration<? extends Statement, SailException> res=sc.getStatements(null, null, null, true, this.context);
		CloseableIteration<? extends Statement, SailException> res=sc.getStatements(null, null, null, false);
		try {
			n3Writer.startRDF();
		} catch (RDFHandlerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		while (nss.hasNext()){
			try {
				Namespace ns=nss.next();
				n3Writer.handleNamespace(ns.getPrefix(),ns.getName());
			} catch (RDFHandlerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		while(res.hasNext()) {
			try {
				
				n3Writer.handleStatement(res.next());
			} catch (RDFHandlerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		}
		try {
			n3Writer.endRDF();
		} catch (RDFHandlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sc.close();
	}
	
	
	public void exportRDFOfSubTree(String where,URI conceptUri,int level) throws SailException{
		RDFHandler rdfxmlWriter = null;
		if (where==null)rdfxmlWriter=new RDFXMLWriter(System.out);
		else {
			File f=new File (where);
			java.io.FileOutputStream fos;
			try {
				fos = new java.io.FileOutputStream(f);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return;
			}
			
			 rdfxmlWriter=new RDFXMLWriter(fos);
		}
		
		SailConnection sc=sail.getConnection();
		CloseableIteration<? extends Namespace, SailException> nss=sc.getNamespaces();
		
			try {
			rdfxmlWriter.startRDF();
		} catch (RDFHandlerException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		while (nss.hasNext()){
			try {
				Namespace ns=nss.next();
				rdfxmlWriter.handleNamespace(ns.getPrefix(),ns.getName());
			} catch (RDFHandlerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		Set<Statement> statements=this.getStatements(conceptUri, level, true);
		
		for (Statement statement:statements) {
			try {
				rdfxmlWriter.handleStatement(statement);
				
			} catch (RDFHandlerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		    
		}
		try {
			rdfxmlWriter.endRDF();
		} catch (RDFHandlerException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		sc.close();
	}
	
	Set<Statement> getStatements(URI conceptUri,int maxLevel,boolean first)throws SailException{
		Set<Statement> st=Sets.newHashSet();
		int newMaxLevel=0;
		if (maxLevel==0){
			// no check
		}
		else if (maxLevel> 0){
			if (maxLevel==1)newMaxLevel=-1;
			else newMaxLevel=maxLevel-1;
		}
		else return null;
		SailConnection sc=sail.getConnection();
		CloseableIteration<? extends Statement, SailException> statements = sc.getStatements(conceptUri, null, null, false);
		while (statements.hasNext()){
			Statement statement=statements.next();
			if (first){ // add Scheme statement
				if (statement.getPredicate().equals(SKOS.INSCHEME.getURI())){
					URI scheme=new URIImpl(statement.getObject().toString());
					st.addAll(this.getStatements(scheme,1,false));
				}
			}
			// check narrowers ? 
			if (statement.getPredicate().equals(SKOS.NARROWER.getURI())){
				if (maxLevel!=1){
					st.add(statement); // si 1 : les info ne sont pas necessaires...
					st.addAll(this.getStatements(new URIImpl(statement.getObject().toString()),newMaxLevel,false));
				}
				
			}else {
				st.add(statement);
			}
		}
		sc.close();
		return st;
	}
	@Deprecated
	Set<Statement> getRelatedSchemeStatementsToDestroy(URI schemeURI)throws SailException{
		Set<Statement> st=Sets.newHashSet();
		
		/*SailConnection sc=sail.getConnection();
		CloseableIteration<? extends Statement, SailException> statements = sc.getStatements(null, SKOS.INSCHEME.getURI(), schemeURI, true);
		while (statements.hasNext()){
			Statement statement=statements.next();
			if (first){ // add Scheme statement
				if (statement.getPredicate().equals(SKOS.INSCHEME.getURI())){
					URI scheme=new URIImpl(statement.getObject().toString());
					st.addAll(this.getStatements(scheme,1,false));
				}
			}
			// check narrowers ? 
			if (statement.getPredicate().equals(SKOS.NARROWER.getURI())){
				if (maxLevel!=1){
					st.add(statement); // si 1 : les info ne sont pas necessaires...
					st.addAll(this.getStatements(new URIImpl(statement.getObject().toString()),newMaxLevel,false));
				}
				
			}else {
				st.add(statement);
			}
		}
		sc.close();*/
		return st;
	}
}