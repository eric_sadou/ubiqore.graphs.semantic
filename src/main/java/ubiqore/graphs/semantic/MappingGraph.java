package ubiqore.graphs.semantic;

import info.aduna.iteration.CloseableIteration;

import java.util.List;
import java.util.Set;

import org.openrdf.model.BNode;
import org.openrdf.model.Literal;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.impl.ContextStatementImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.impl.BindingImpl;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.repository.sparql.query.SPARQLQueryBindingSet;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class MappingGraph extends AbstractGraph{

	public MappingGraph(boolean creation, String rep) throws SailException {
		super(creation, rep);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void createVocabularyStatementsList() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void createNamespacesMap() {
		  // official namespaces..
		this.useClassicNamespaces();
	    // ubiq and others...
        this.namespaces.put(ContextVocabulary.skos_prefix,ContextVocabulary.skos);
        this.namespaces.put(ContextVocabulary.ubiq_prefix,ContextVocabulary.ubiq);
        this.namespaces.put(ContextVocabulary.align_prefix,ContextVocabulary.align);
	}

	public void createAlignement() throws SailException{ // the blank node root 
		BNode align=sail.getValueFactory().createBNode(ContextVocabulary.align_prefix+":"+ContextVocabulary.alignment.getLocalName());
		//this.addSingleStatement(new ContextStatementImpl(uri,RDF.TYPE,,this.context));
		List<Statement> sts=Lists.newArrayList();
		sts.add(new ContextStatementImpl(align, ContextVocabulary.type,vf.createLiteral("**") , this.context));
		sts.add(new ContextStatementImpl(align, ContextVocabulary.level,vf.createLiteral("0") , this.context));
		sts.add(new ContextStatementImpl(align, ContextVocabulary.xml,vf.createLiteral("yes") , this.context));
		this.addMultipleStatements(sts);
		
	}
	
	public void addMapping (URI entity1 ,URI entity2 ,float measure, String methode,String relation,String validity)throws SailException{
		BNode align=sail.getValueFactory().createBNode(ContextVocabulary.align_prefix+":"+ContextVocabulary.alignment.getLocalName());
		
		BNode Cell=sail.getValueFactory().createBNode();
		BNode map=sail.getValueFactory().createBNode();
		
		List<Statement> sts=Lists.newArrayList();
		
		sts.add(new ContextStatementImpl(Cell, ContextVocabulary.method,vf.createLiteral("manual") , this.context));
		sts.add(new ContextStatementImpl(Cell, ContextVocabulary.entity1,entity1 , this.context));
		sts.add(new ContextStatementImpl(Cell, ContextVocabulary.entity2,entity2 , this.context));
		sts.add(new ContextStatementImpl(Cell, ContextVocabulary.measure,vf.createLiteral(measure), this.context));
		sts.add(new ContextStatementImpl(Cell, ContextVocabulary.relation,vf.createLiteral(relation), this.context));
		sts.add(new ContextStatementImpl(Cell, ContextVocabulary.validity,vf.createLiteral("VALID"), this.context));
		
		sts.add(new ContextStatementImpl(map,ContextVocabulary.Cell,Cell , this.context));
		
		sts.add(new ContextStatementImpl(align,ContextVocabulary.map,map , this.context));
		this.addMultipleStatements(sts);
	}
	
	public Set<Alignement> getMappings() throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<Alignement> set=Sets.newHashSet();
		
		SailConnection sc=sail.getConnection();
		SkosConcept skos=null;
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?entity1 ?entity2 ?relation1to2 ?mea "+
							"WHERE "+
							"{ " +
							" ?cell align:entity1  ?entity1 . " +
							" ?cell align:entity2  ?entity2 .  " +
							" ?cell align:relation ?relation1to2 . "+
							" ?cell align:measure  ?mea . "+
						    " ?cell align:validity \""+"VALID"+ "\" . " +
						
							" } "
							;
		
		SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
		
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
		
		if (sparqlResults.hasNext()){
			skos=new SkosConcept();
			while (sparqlResults.hasNext()){
				BindingSet bindingS=sparqlResults.next();
				System.out.println(bindingS);
				URI entity1=(URI)bindingS.getValue("entity1");
				URI entity2=(URI)bindingS.getValue("entity2");
				Alignement a=new Alignement();
				a.setEntity1(entity1);
				a.setEntity2(entity2);
				Value v=bindingS.getValue("mea");
				Literal l=(Literal)v;
				a.setMeasure(l.floatValue());
				Literal relation=(Literal)bindingS.getValue("relation1to2");
				System.out.println(relation.getLabel());
				a.setRelation1to2(relation.getLabel());
				set.add(a);
			
			}
			
		}
		sc.close();
		return set;
	}
	
	public Set<Alignement> getMapping (URI entity1)throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<Alignement> set=Sets.newHashSet();
		SailConnection sc=sail.getConnection();
		SkosConcept skos=null;
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?entity2 ?relation1to2 ?mea "+
							"WHERE "+
							"{ " +
							" ?cell align:entity1  ?entity1 . " +
							" ?cell align:entity2  ?entity2 .  " +
							" ?cell align:relation ?relation1to2 . "+
							" ?cell align:measure  ?mea . "+
						    " ?cell align:validity \""+"VALID"+ "\" . " +
						
							" } "
							;
		
		SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
		bs.addBinding(new BindingImpl("entity1",entity1));
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
		
		if (sparqlResults.hasNext()){
			skos=new SkosConcept();
			while (sparqlResults.hasNext()){
				BindingSet bindingS=sparqlResults.next();
				//System.out.println(a.toString());
				URI entity2=(URI)bindingS.getValue("entity2");
				Alignement a=new Alignement();
				a.setEntity1(entity1);
				a.setEntity2(entity2);
				Value v=bindingS.getValue("mea");
				Literal l=(Literal)v;
				a.setMeasure(l.floatValue());
				Literal relation=(Literal)bindingS.getValue("relation1to2");
			
				a.setRelation1to2(relation.getLabel());
				set.add(a);
			
			}
			
		}
		sc.close();
		return set;
	}
	
	public Set<Alignement> getInverseMapping (URI entity2)throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<Alignement> set=Sets.newHashSet();
		SailConnection sc=sail.getConnection();
		SkosConcept skos=null;
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?entity1 ?relation1to2 "+
							"WHERE "+
							"{ " +
							" ?cell align:entity2  ?entity2 . " +
							" ?cell align:entity1  ?entity1 .  " +
							" ?cell align:relation ?relation1to2 . "+
						    " ?cell align:validity \""+"VALID"+ "\" . " +
						
							" } "
							;
		SPARQLQueryBindingSet bs=new SPARQLQueryBindingSet();
		bs.addBinding(new BindingImpl("entity2",entity2));
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), bs, true);
		
		if (sparqlResults.hasNext()){
			skos=new SkosConcept();
			while (sparqlResults.hasNext()){
				BindingSet bindingS=sparqlResults.next();
				//System.out.println(a.toString());
				URI entity1=(URI)bindingS.getValue("entity1");
				Alignement a=new Alignement();
				a.setEntity1(entity1);
				a.setEntity2(entity2);
				a.setRelation1to2(bindingS.getValue("relation1to2").toString());
				set.add(a);
			
			}
			
		}
		sc.close();
		return set;
	}
	
	public static void main(String... args){
		MappingGraph g=null;
		try {
			g=new MappingGraph(true,"/Reborn/tmp/graphs/mapping");
			g.createAlignement();
			g.addMapping(new URIImpl("http://toto"), new URIImpl("http://titi"), (float)1.0 , "MANUAL", "EQUIV", "VALID");
			g.addMapping(new URIImpl("http://toto"), new URIImpl("http://azazeazeazeaz"), (float)1.0 , "MANUAL", "EQUIV", "VALID");
			g.addMapping(new URIImpl("http://tata"), new URIImpl("http://tutu"), (float)1.0 , "MANUAL", "EQUIV", "VALID");
			g.exportRDF("/Reborn/tmp/mapping.rdf");
			g.exportRDF(null);
			g.statements();
			
			try {
				//Set<Alignement> set=g.getMapping(new URIImpl("http://toto"));
				//for (Alignement a:set)System.out.println(a.getEntity1()+"--"+a.getRelation1to2()+"-->"+a.getEntity2());
				Set<Alignement> setFull=g.getMappings();
				for (Alignement a:setFull)System.out.println(a.getEntity1()+"--"+a.getRelation1to2()+"-->"+a.getEntity2()+"###"+a.getMeasure());
			} catch (MalformedQueryException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (QueryEvaluationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			if (g!=null)g.disconnect();
		}
		
	}
}
