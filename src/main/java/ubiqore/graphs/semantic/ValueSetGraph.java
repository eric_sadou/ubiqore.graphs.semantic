package ubiqore.graphs.semantic;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import info.aduna.iteration.CloseableIteration;

import org.openrdf.model.Literal;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ContextStatementImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.util.LiteralUtil;
import org.openrdf.model.vocabulary.RDF;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;





import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;

@Deprecated
public class ValueSetGraph  extends AbstractGraph{

	List<Category> topCategories=Lists.newArrayList();
	
	private void initCategory(){
		if (topCategories.isEmpty()){
			for (int i=0;i<7;i++){
				Category category=new Category();
				category.setURI(this.context+SemanticVocabulary.VALUESETELEMENT.getLocalName()+"/"+"EHR4CR_VS_1."+i);
				category.setVSURI(this.context+SemanticVocabulary.VALUESETELEMENT.getLocalName()+"/"+"EHR4CR_VS_1."+i);
				this.topCategories.add(category);
			}
			
		}
	}
	public void cachCategory(Category cat){
		initCategory();
		this.topCategories.add(cat);
	}
	public String manageAndGetTopCategory(String superValueSet,String myValueSetElement){
		initCategory();
		String retour=null;
		Category newCat=null;
		// si on a la cat pour myValueSet .. pas besoin de créer autre chose ...
		for (Category cat:topCategories){
			if (cat.getVSURI().equalsIgnoreCase(myValueSetElement)){
				return cat.getURI();
			}
		}
		for (Category cat:topCategories){
			if (cat.getVSURI().equalsIgnoreCase(superValueSet)){
				retour=cat.getURI();
				
				newCat=new Category();
				newCat.setURI(cat.getURI());
				newCat.setVSURI(myValueSetElement);
				break;
			}
		}
		if (newCat!=null)topCategories.add(newCat);
		System.out.println(myValueSetElement+" "+ retour +topCategories.size());
		return retour;
	}
	
	public ValueSetGraph(boolean creation, String rep) throws SailException {
		super(creation, rep);
		// TODO Auto-generated constructor stub
	}

	@Override
	protected void createVocabularyStatementsList() {
		// TODO Auto-generated method stub
		this.vocabularyStatements.add(new ContextStatementImpl(SemanticVocabulary.VALUESETELEMENT,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(SemanticVocabulary.UNIT,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(SemanticVocabulary.RANK,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(SemanticVocabulary.DE,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(SemanticVocabulary.CD,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(SemanticVocabulary.PQ,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(SemanticVocabulary.CO,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(SemanticVocabulary.TS,RDF.TYPE, RDFS.DATATYPE,context));
		this.vocabularyStatements.add(new ContextStatementImpl(SemanticVocabulary.SC,RDF.TYPE, RDFS.DATATYPE,context));
	}

	@Override
	protected void createNamespacesMap() {
		// TODO Auto-generated method stub
		 // official namespaces..
		this.useClassicNamespaces();
	    // ubiq and others...
        this.namespaces.put(SemanticVocabulary.skos_prefix,SemanticVocabulary.skos);
        this.namespaces.put(SemanticVocabulary.ubiq_prefix,SemanticVocabulary.ubiq);
	}
	
	/*
	 * A valueSetEntry is a ValueSetElement wich is a member of a super ValueSet (=a ValueSetElement CD/CO)
	 * valueSetURI can be null  --> will be attached later !!! (for excel files DataElements declaration)
	 * A ValueSetEntry can be CD, CO, PQ , TS (?)
	 */
	public URI addValueSetEntry(URI conceptURI,Rank rank,String metadata,Unit unit,String valueSetElementId,String valueSetPrefLabel ,URI valueSetURI) throws SailException{
		
		URI valueSetEntryURI=null;
		URI semType=null;
		if (metadata.equalsIgnoreCase(SemanticVocabulary.CD.getLocalName())){
			semType=SemanticVocabulary.CD;
			valueSetEntryURI=new URIImpl(this.context+SemanticVocabulary.VALUESETELEMENT.getLocalName()+"/"+valueSetElementId);
		}else if (metadata.equalsIgnoreCase(SemanticVocabulary.CO.getLocalName())) {
			semType=SemanticVocabulary.CO;
			valueSetEntryURI=new URIImpl(this.context+SemanticVocabulary.VALUESETELEMENT.getLocalName()+"/"+valueSetElementId);
		}else if (metadata.equalsIgnoreCase(SemanticVocabulary.PQ.getLocalName())) {
			semType=SemanticVocabulary.PQ;
			// PQ : l'element a associé n'est pas un valueSet mais une valeur direct 
			valueSetEntryURI=new URIImpl(valueSetURI+"/"+conceptURI.getLocalName());
		}
		else if (metadata.equalsIgnoreCase(SemanticVocabulary.SC.getLocalName())){
			semType=SemanticVocabulary.SC;
			valueSetEntryURI=new URIImpl(valueSetURI+"/"+conceptURI.getLocalName());
		}
		else if (metadata.equalsIgnoreCase(SemanticVocabulary.TS.getLocalName())){
			semType=SemanticVocabulary.TS;
			valueSetEntryURI=new URIImpl(valueSetURI+"/"+conceptURI.getLocalName());
		}
		else return null;
		
		boolean processIsAnUpdate=false;
		try {
			if (this.valueSetElementExists(valueSetEntryURI)){
				processIsAnUpdate=true;
			}
		} catch (MalformedQueryException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return valueSetURI;
		} catch (QueryEvaluationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
			return valueSetURI;
		}
		
		
		SailConnection sc= sail.getConnection();

		
		
		if (!processIsAnUpdate){
			// creation de l'entitée de type valueSetElement: 
			sc.addStatement(valueSetEntryURI,RDF.TYPE,SemanticVocabulary.VALUESETELEMENT,context);
			// ajout ds le bag du valueSet superieur :
			if (valueSetURI!=null){
				sc.addStatement(valueSetEntryURI,SKOS.MEMBER.getURI(),valueSetURI,context);
				String topCategory=this.manageAndGetTopCategory(valueSetURI.stringValue(), valueSetEntryURI.stringValue());
				sc.addStatement(valueSetEntryURI,SemanticVocabulary.TopCategory,new URIImpl(topCategory),context);
			}
			
			// ajout de la metadata semantique : 
			sc.addStatement(valueSetEntryURI,SemanticVocabulary.DE,semType ,context);
			
			// ajout d'information sur les types collections : 
			if (semType==SemanticVocabulary.CO){
				sc.addStatement(valueSetEntryURI,RDF.TYPE,SKOS.ORDEREDCOLLECTION.getURI(),context);
			}
			if (semType==SemanticVocabulary.CD){
				sc.addStatement(valueSetEntryURI,RDF.TYPE,SKOS.COLLECTION.getURI(),context);
			}
		}
		
		// ajout d'un Label au valueSetElement , certains Elements n'ont pas de concepts "related"
		if (Strings.emptyToNull(valueSetPrefLabel)!=null){
			sc.addStatement(valueSetEntryURI,SKOS.PREFLABEL.getURI(), vf.createLiteral(valueSetPrefLabel),context);
		}
		
		// relié l'eventuel et probable concept a son element.
		if (conceptURI!=null){
			// on relie le concept a ce nouvel element.
			sc.addStatement(valueSetEntryURI,SKOS.RELATED.getURI(),conceptURI, context);
		    // cas d'un pere de type CO (induit par la presence d'un rank) --> attention astuce..
			if (rank!=null&& valueSetURI!=null){
				//sc.addStatement(conceptURI,SKOS.MEMBER.getURI(),valueSetURI,context);
				// créer un niveau related to concept et related to valueSet
				URI rankURI=new URIImpl(valueSetEntryURI.toString()+"/"+conceptURI.getLocalName());
				sc.addStatement(rankURI,SKOS.RELATED.getURI(),valueSetEntryURI, context);
				sc.addStatement(rankURI,RDF.TYPE,SemanticVocabulary.RANK,context);
				sc.addStatement(rankURI, SKOS.PREFLABEL.getURI() ,vf.createLiteral(rank.getOriginalRank()) , context);
				sc.addStatement(rankURI, SKOS.NOTATION.getURI(), vf.createLiteral(rank.getValue()), context);
			}
		
		}
		
		/*
		 * Les unités seront dans une version ultérieure reliées a des concepts UCUM NCI Thesaurus (?) !
		 */
		if (unit!=null&& semType==SemanticVocabulary.PQ){
			URI unitURI=new URIImpl(this.context+SemanticVocabulary.UNIT.getLocalName()+"/"+unit.getUriLocalName());
			// TODO maybe unit is already there --> check statement.
			// creation de l'unité 
			sc.addStatement(unitURI,RDF.TYPE,SemanticVocabulary.UNIT,context);
			sc.addStatement(unitURI ,SKOS.RELATED.getURI(),valueSetEntryURI,context);
			sc.addStatement(unitURI, SKOS.PREFLABEL.getURI() ,vf.createLiteral(unit.getUcumValue()) , context);
			sc.addStatement(unitURI, SKOS.NOTATION.getURI(), vf.createLiteral(unit.getUcumDefinition()), context);
			
		}
		
		try {
			sc.commit();
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			sc.close();
		}
		return valueSetEntryURI;
	}
	
	/*
	* here we declare a new VSE (CO or CD) which info are mainly in DE_sheet.
	* The infos added here from VS_sheet (hierarchical info)
	 */
	public URI declareSimpleValueSetEntry(String metadata,String valueSetElementId,URI superValueSetURI,String name,URI scheme) throws SailException{
		
		URI valueSetEntryURI=null;
		URI semType=null;
		if (metadata.equalsIgnoreCase(SemanticVocabulary.CD.getLocalName())){
			semType=SemanticVocabulary.CD;
			valueSetEntryURI=new URIImpl(this.context+SemanticVocabulary.VALUESETELEMENT.getLocalName()+"/"+valueSetElementId);
		}else if (metadata.equalsIgnoreCase(SemanticVocabulary.CO.getLocalName())) {
			semType=SemanticVocabulary.CO;
			valueSetEntryURI=new URIImpl(this.context+SemanticVocabulary.VALUESETELEMENT.getLocalName()+"/"+valueSetElementId);
		}
		else return null;
		
		try {
			if (this.valueSetElementExists(valueSetEntryURI)){
				System.out.println("EntrySet already exists !! nothing was add !! ");
				return valueSetEntryURI;
			}
		} catch (MalformedQueryException e1) {
			
			e1.printStackTrace();
			return valueSetEntryURI;
		} catch (QueryEvaluationException e1) {
			
			e1.printStackTrace();
			return valueSetEntryURI;
		}
		
		
		SailConnection sc=sail.getConnection();

	
		
		// creation de l'entitée de type valueSetElement: 
		sc.addStatement(valueSetEntryURI,RDF.TYPE,SemanticVocabulary.VALUESETELEMENT,context);
		// ajout ds le bag du valueSet superieur :
		if (superValueSetURI!=null){
			sc.addStatement(valueSetEntryURI,SKOS.MEMBER.getURI(),superValueSetURI,context);
			String topCategory=this.manageAndGetTopCategory(superValueSetURI.stringValue(), valueSetEntryURI.stringValue());
			sc.addStatement(valueSetEntryURI,SemanticVocabulary.TopCategory,new URIImpl(topCategory),context);
		}
		else {
			// cas du ValueSetCategory haut niveau 
			if (Strings.emptyToNull(name)!=null){
				sc.addStatement(valueSetEntryURI,SKOS.PREFLABEL.getURI(), vf.createLiteral(name),context);
			}
			// ajout on top of the scheme.
			this.setTopConcept(valueSetEntryURI, scheme);
		}
		
		// ajout de la metadata semantique : 
		sc.addStatement(valueSetEntryURI,SemanticVocabulary.DE,semType ,context);
		
		// ajout d'information sur les types collections : 
		if (semType==SemanticVocabulary.CO){
			sc.addStatement(valueSetEntryURI,RDF.TYPE,SKOS.ORDEREDCOLLECTION.getURI(),context);
		}
		if (semType==SemanticVocabulary.CD){
			sc.addStatement(valueSetEntryURI,RDF.TYPE,SKOS.COLLECTION.getURI(),context);
		}
		
		try {
			sc.commit();
		}catch (Exception e){
			e.printStackTrace();
		}finally {
			sc.close();
		}
		return valueSetEntryURI;
	}
	
	public boolean valueSetElementExists(URI uri)throws SailException, MalformedQueryException, QueryEvaluationException{
		boolean rep=false;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x  "+
							"WHERE "+
							"{ " +
							 " <"+uri.toString()+"> rdf:type ubiq:ValueSetElement  ."+
							  " <"+ uri.toString() + ">  skos:notation    ?x . " +
							" } "
							;
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		if (sparqlResults.hasNext()){
			rep=true;
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				System.out.println(a);
			}
		}
		sc.close();
		return rep;
	}
	
	public ValueSetElement getRoot(){
		try {
			return this.getValueSetByURIWithoutRelatedConcept(new URIImpl("http://server.ubiqore.com/v1/ValueSetElement/EHR4CR_VS_1"));
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (QueryEvaluationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public URI addScheme(String schemeId) throws SailException{
		/*
		 * 1 verifier que l'URI est unique. TODO
		 * 2. ajouter le schemeConcept.
		 */
		URI scheme=new URIImpl(this.context+SemanticVocabulary.scheme+schemeId);
		SailConnection sc= sail.getConnection();
		sc.addStatement(scheme,RDF.TYPE,SKOS.CONCEPTSCHEME.getURI(),context);
		sc.addStatement(scheme, SKOS.PREFLABEL.getURI() ,vf.createLiteral(scheme.getLocalName()),context);
		sc.commit();sc.close();
		return scheme;
	}
	
	public boolean setTopConcept(URI uriValueSet,URI scheme) throws SailException{
		return this.addSingleStatement(new ContextStatementImpl(uriValueSet, SKOS.TOPCONCEPTOF.getURI(),scheme,context));
		
	}
	
	public URI getTopCategory(URI valueSetURI )throws SailException, MalformedQueryException, QueryEvaluationException{
		URI  answer=null;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x   "+
							"WHERE "+
							"{ " +
							"<"+ valueSetURI.toString()+"> rdfs:subClassOf  ?x . " +
							"? x  skos:member <http://server.ubiqore.com/v1/ValueSetElement/EHR4CR_VS_1>. " +
							
						/*	
							"?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?metadata) = xsd:string ) " +
							"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +*/
							" } "
							;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				answer=new URIImpl(a.getValue("x").stringValue());
				
				break;
			}
			
		}
		sc.close();
		
		return answer;
	}
	private ValueSetElement  getValueSetByURIWithoutRelatedConcept(URI valueSetURI)throws SailException, MalformedQueryException, QueryEvaluationException{
		ValueSetElement answer=new ValueSetElement();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?metadata ?name   "+
							"WHERE "+
							"{ " +
							"<"+ valueSetURI.toString()+"> rdf:type  ubiq:ValueSetElement . " +
							"<"+ valueSetURI.toString()+"> ubiq:DE ?metadata . " +
							"<"+ valueSetURI.toString()+"> skos:prefLabel ?name . " +
							
						/*	
							"?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?metadata) = xsd:string ) " +
							"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +*/
							" } "
							;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				answer=new ValueSetElement();
				answer.setUri(valueSetURI.stringValue());
				answer.setMetadata(a.getValue("metadata").stringValue());
				answer.setLabel(a.getValue("name").stringValue());
				break;
			}
			
		}
		sc.close();
		
		if (answer!=null){
			// chaque type peut avoir un rank car etant dans un CO....
			answer=this.addPotentialRank(answer);
			if (answer.getMetadata().equalsIgnoreCase(SemanticVocabulary.CD.toString())){
					answer=this.getCDMembers(answer);
			}
			else if (answer.getMetadata().equalsIgnoreCase(SemanticVocabulary.CO.toString())){
					answer=this.getCDMembers(answer);
			}
			if (answer.getMetadata().equalsIgnoreCase(SemanticVocabulary.PQ.toString())){
				answer=this.addPotentialUnit(answer);
			}
		}
	 
		return answer;
	}
	
	
	public List<ValueSetElement> getValueSetFromConceptUri(String conceptURI)throws SailException, MalformedQueryException, QueryEvaluationException{
		List<ValueSetElement> answer=Lists.newArrayList();;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri  "+
							"WHERE "+
							"{ " +
							"?uri rdf:type  ubiq:ValueSetElement . " +
							" ?uri skos:related <"+conceptURI+ "> . " +
						/*	
							"?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?metadata) = xsd:string ) " +
							"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +*/
							" } "
							;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		ArrayList<String> st=Lists.newArrayList();
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				st.add(a.getValue("uri").stringValue());
				
			}
			
		}
		sc.close();
		
		for (String uri:st){
			answer.add(this.getValueSetByURI(new URIImpl(uri),true));
		}
		return answer;
	}
	
	public ValueSetElement getValueSetByURI(URI valueSetURI,boolean direct )throws SailException, MalformedQueryException, QueryEvaluationException{
		ValueSetElement answer=null;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?metadata ?concept ?supVSE ?topCat ?label  "+
							"WHERE "+
							"{ " +
							"<"+ valueSetURI.toString()+"> rdf:type  ubiq:ValueSetElement . " +
							"<"+ valueSetURI.toString()+"> ubiq:DE ?metadata . " +
							"<"+ valueSetURI.toString()+"> skos:related ?concept . " +
							"<"+ valueSetURI.toString()+"> skos:member ?supVSE . " +
							"<"+ valueSetURI.toString()+"> skos:prefLabel ?label . " +
							"<"+ valueSetURI.toString()+"> ubiq:topCategory ?topCat . " +
						/*	
							"?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?metadata) = xsd:string ) " +
							"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +*/
							" } "
							;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				System.out.println(a.toString());
				answer=new ValueSetElement();
				answer.setUri(valueSetURI.stringValue());
				answer.setMetadata(a.getValue("metadata").stringValue());
				answer.setMemberOfUri(a.getValue("supVSE").stringValue());
				answer.setRelatedToUri(a.getValue("concept").stringValue());
				answer.setLabel(a.getValue("label").stringValue());
				answer.setTopCategoryURI(a.getValue("topCat").stringValue());
				/*Literal v=(Literal)a.getValue("prefLabel");
				skos.setPreflabel(new PrefLabel(v));
				System.out.println(skos.getPreflabel().getLit().getLabel());
				System.out.println(skos.getPreflabel().getLit().getLanguage());
				skos.setScheme(new URIImpl(a.getValue("scheme").stringValue()));
				skos.setCodingSchemeURI(a.getValue("codingSchemeURI").stringValue());
				skos.setSkosNotation(a.getValue("code").stringValue());
				skos.setUri(uri);*/
				break;
			}
			
		}
		
		sc.close();
		
		if (answer!=null){
			// chaque type peut avoir un rank car etant dans un CO....
			answer=this.addPotentialRank(answer);
			
			if (direct){
				if (answer.getMetadata().equalsIgnoreCase(SemanticVocabulary.CD.toString())){
					answer=this.getCDMembers(answer);
				}
				else if (answer.getMetadata().equalsIgnoreCase(SemanticVocabulary.CO.toString())){
					answer=this.getCDMembers(answer);
				}
			}
			if (answer.getMetadata().equalsIgnoreCase(SemanticVocabulary.PQ.toString())){
				answer=this.addPotentialUnit(answer);
			}
		}
		
		return answer;
		
	}
	
		
	
	
	public void objectURI(String uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x ?y   "+
							"WHERE "+
							"{ " +
							" ?x ?y  <"+ uri+">  . " +
							
							
						/*	
							"?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?metadata) = xsd:string ) " +
							"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +*/
							" } "
							;
		
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
			
				System.out.println(a.getValue("x").stringValue());
				
				System.out.println(a.getValue("y").stringValue());
				
				/*Literal v=(Literal)a.getValue("prefLabel");
				skos.setPreflabel(new PrefLabel(v));
				System.out.println(skos.getPreflabel().getLit().getLabel());
				System.out.println(skos.getPreflabel().getLit().getLanguage());
				skos.setScheme(new URIImpl(a.getValue("scheme").stringValue()));
				skos.setCodingSchemeURI(a.getValue("codingSchemeURI").stringValue());
				skos.setSkosNotation(a.getValue("code").stringValue());
				skos.setUri(uri);*/
				
			}
			
		}
		sc.close();
	}
	
	public void subjectURI(String uri) throws SailException, MalformedQueryException, QueryEvaluationException{
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?x ?y   "+
							"WHERE "+
							"{ " +
							"  <"+ uri+">  ?x ?y  . " +
							
							
						/*	
							"?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?metadata) = xsd:string ) " +
							"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +*/
							" } "
							;
		
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
			
				System.out.println(a.getValue("x").stringValue());
				
				System.out.println(a.getValue("y").stringValue());
				
				/*Literal v=(Literal)a.getValue("prefLabel");
				skos.setPreflabel(new PrefLabel(v));
				System.out.println(skos.getPreflabel().getLit().getLabel());
				System.out.println(skos.getPreflabel().getLit().getLanguage());
				skos.setScheme(new URIImpl(a.getValue("scheme").stringValue()));
				skos.setCodingSchemeURI(a.getValue("codingSchemeURI").stringValue());
				skos.setSkosNotation(a.getValue("code").stringValue());
				skos.setUri(uri);*/
				
			}
			
		}
		sc.close();
	}
    
	public ValueSetElement addPotentialRank(ValueSetElement vse)throws SailException, MalformedQueryException, QueryEvaluationException{
		ValueSetElement answer=vse;
	    String valueSetURI=vse.getUri();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?rankURI ?rankOrigin ?rankValue  "+
							"WHERE "+
							"{ " +
							" ?rankURI skos:related  <"+ valueSetURI+">  . " +
							" ?rankURI rdf:type ubiq:Rank ." +
							" ?rankURI skos:prefLabel ?rankOrigin  ." +
							" ?rankURI skos:notation ?rankValue ." +
							
						/*	
							"?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?metadata) = xsd:string ) " +
							"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +*/
							" } "
							;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				System.out.println(a.toString());
				Rank rank=new Rank();
				rank.setUri(a.getValue("rankURI").stringValue());
				Literal v=(Literal)a.getValue("rankOrigin");
				rank.setOriginalRank(v.getLabel());
				
				Literal l=(Literal)a.getValue("rankValue");
				rank.setValue(LiteralUtil.getFloatValue(l, 0));
				answer.setRankURI(rank);
				/*Literal v=(Literal)a.getValue("prefLabel");
				skos.setPreflabel(new PrefLabel(v));
				System.out.println(skos.getPreflabel().getLit().getLabel());
				System.out.println(skos.getPreflabel().getLit().getLanguage());
				skos.setScheme(new URIImpl(a.getValue("scheme").stringValue()));
				skos.setCodingSchemeURI(a.getValue("codingSchemeURI").stringValue());
				skos.setSkosNotation(a.getValue("code").stringValue());
				skos.setUri(uri);*/
				break;
				
			}
			
		}
		sc.close();
	 
		return answer;
		
	}
	
	
	public ValueSetElement addPotentialUnit(ValueSetElement vse)throws SailException, MalformedQueryException, QueryEvaluationException{
		
		/*
		 * if (unit!=null&& semType==SemanticVocabulary.PQ){
			URI unitURI=new URIImpl(SemanticTerminology.ehr4crV1+SemanticVocabulary.UNIT.getLocalName()+"/"+unit.getUriLocalName());
			// TODO maybe unit is already there --> check statement.
			// creation de l'unité 
			sc.addStatement(unitURI,RDF.TYPE,SemanticVocabulary.UNIT,context);
			sc.addStatement(unitURI ,SKOSRDFVocabulary.RELATED.getURI(),valueSetEntryURI,context);
			sc.addStatement(unitURI, SKOSRDFVocabulary.PREFLABEL.getURI() ,vf.createLiteral(unit.getUcumValue()) , context);
			sc.addStatement(unitURI, SKOSRDFVocabulary.NOTATION.getURI(), vf.createLiteral(unit.getUcumDefinition()), context);
			
		}
		 */
		
		ValueSetElement answer=vse;
	    String valueSetURI=vse.getUri();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?unitURI ?ucumValue ?ucumDefinition  "+
							"WHERE "+
							"{ " +
							" ?unitURI skos:related  <"+ valueSetURI+">  . " +
							" ?unitURI rdf:type ubiq:Unit ." +
							" ?unitURI skos:prefLabel ?ucumValue  ." +
							" ?unitURI skos:notation ?ucumDefinition ." +
							
						/*	
							"?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?metadata) = xsd:string ) " +
							"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +*/
							" } "
							;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				System.out.println(a.toString());
				Unit unit=new Unit();
				unit.setUri(a.getValue("unitURI").stringValue());
				
				Literal v=(Literal)a.getValue("ucumValue");
				
				unit.setUcumValue(v.getLabel());
				
				Literal l=(Literal)a.getValue("ucumDefinition");
				unit.setUcumDefinition(l.getLabel());
				answer.setUnitURI(unit);
				/*Literal v=(Literal)a.getValue("prefLabel");
				skos.setPreflabel(new PrefLabel(v));
				System.out.println(skos.getPreflabel().getLit().getLabel());
				System.out.println(skos.getPreflabel().getLit().getLanguage());
				skos.setScheme(new URIImpl(a.getValue("scheme").stringValue()));
				skos.setCodingSchemeURI(a.getValue("codingSchemeURI").stringValue());
				skos.setSkosNotation(a.getValue("code").stringValue());
				skos.setUri(uri);*/
				break;
				
			}
			
		}
		sc.close();
	 
		return answer;
		
	}
	
	public void updateTopCateogory(){
		
	}
	
	public ValueSetElement getCDMembers(ValueSetElement vse)throws SailException, MalformedQueryException, QueryEvaluationException{
		ValueSetElement answer=vse;
		String valueSetURI=vse.getUri();
		SailConnection sc=sail.getConnection();
		ArrayList<String> lom=new ArrayList<String>();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?member   "+
							"WHERE "+
							"{ " +
							"  ?member skos:member  <"+ valueSetURI+">  . " +
							
						/*	
							"?scheme skos:notation ?codingSchemeURI ." +
							"FILTER ( datatype(?metadata) = xsd:string ) " +
							"FILTER ( datatype(?codingSchemeURI) = xsd:string ) " +*/
							" } "
							;
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				System.out.println(a.toString());
				lom.add(a.getValue("member").stringValue());
				/*Literal v=(Literal)a.getValue("prefLabel");
				skos.setPreflabel(new PrefLabel(v));
				System.out.println(skos.getPreflabel().getLit().getLabel());
				System.out.println(skos.getPreflabel().getLit().getLanguage());
				skos.setScheme(new URIImpl(a.getValue("scheme").stringValue()));
				skos.setCodingSchemeURI(a.getValue("codingSchemeURI").stringValue());
				skos.setSkosNotation(a.getValue("code").stringValue());
				skos.setUri(uri);*/
				
			}
			
		}
		sc.close();
		
		for (String st:lom){
			answer.addMember(this.getValueSetByURI(new URIImpl(st),false));
		}
	 
		return answer;
		
	}
	
	// http://answers.semanticweb.com/questions/2238/sparql-regexstro-abc-i-does-not-work-as-case-insensitive
	public Map<URI,String>  searchStringREGEX (String st) throws SailException, MalformedQueryException, QueryEvaluationException{
		Map<URI,String> l=Maps.newLinkedHashMap();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri ?prefLabel "+
							"WHERE "+
							"{ " +
							" ?uri rdf:type  ubiq:ValueSetElement . " +
							" ?uri skos:prefLabel ?prefLabel . " +
							"FILTER regex (str(?prefLabel),\""+st+"\" ) " +
							" } "
							;
		//rdf:type  ubiq:ValueSetElement
		System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		if (sparqlResults.hasNext()){
			
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				System.out.println(a.toString());
				l.put(new URIImpl(a.getValue("uri").toString()),((Literal)a.getValue("prefLabel")).getLabel());
			}
				
		}
		sc.close();
		
		
	 
		return l;
	}
	
}
