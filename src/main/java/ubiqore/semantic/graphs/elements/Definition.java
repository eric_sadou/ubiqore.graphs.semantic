package ubiqore.semantic.graphs.elements;

import org.openrdf.model.Literal;
import org.openrdf.model.Value;

import ubiqore.semantic.graphs.elements.sparql.SparqlData;

public class Definition implements SparqlData{
	public Definition(){}
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getObjectLabel() {
		// TODO Auto-generated method stub
		return "def";
	}

	public void compute(Value v) {
		Literal l=(Literal)v;
		this.value=l.getLabel();
		
	}
}
