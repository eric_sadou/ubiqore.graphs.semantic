package ubiqore.semantic.graphs.elements;

import java.util.HashSet;
import java.util.Set;

import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;

//RDFVocabulary
public enum SKOS {
  
    LABELRELATED("labelRelated"),

    MEMBER("member"),

    MEMBERLIST("memberList"),

    MAPPINGRELATION("mappingRelation"),

    BROADMATCH("broadMatch"),

    NARROWMATCH("narrowMatch"),

    RELATEDMATCH("relatedMatch"),

    EXACTMATCH("exactMatch"),

    BROADER("broader"),

    NARROWER("narrower"),

    BROADERTRANS("broaderTransitive"),

    NARROWERTRANS("narrowerTransitive"),

    RELATED("related"),

    HASTOPCONCEPT("hasTopConcept"),

    SEMANTICRELATION("semanticRelation"),

    CONCEPT("Concept"),

    LABELRELATION("LabelRelation"),

    SEELABELRELATION("seeLabelRelation"),

    COLLECTION("Collection"),

    CONCEPTSCHEME("ConceptScheme"),

    TOPCONCEPTOF("topConceptOf"),

    INSCHEME("inScheme"),

    CLOSEMATCH("closeMatch"),

    DOCUMENT("Document"),

    IMAGE("Image"),

    ORDEREDCOLLECTION("OrderedCollection"),

    COLLECTABLEPROPERTY("CollectableProperty"),

    RESOURCE("Resource"),

    PREFLABEL("prefLabel"),

    ALTLABEL("altLabel"),

    COMMENT("comment"),

    EXAMPLE("example"),

    NOTE("note"),

    NOTATION("notation"),

    SCOPENOTE("scopeNote"),

    HIDDENLABEL("hiddenLabel"),

    EDITORIALNOTE("editorialNote"),

    HISTORYNOTE("historyNote"),

    DEFINITION("definition"),

    CHANGENOTE("changeNote");

    public static final Set<URI> ALL_URIS;

    static {
        ALL_URIS = new HashSet<URI>();
        for(SKOS v : values()) {
            ALL_URIS.add(v.getURI());
        }
    }

    private String localName;

    private String namespace = "http://www.w3.org/2004/02/skos/core#";
    private String prefix = "skos";
    private URI uri;
    private String format;

    public String getFormat() {
		return format;
	}


	SKOS(String localname) {
        this.localName = localname;
        this.uri = new URIImpl(namespace + localname);
        this.format = prefix+":"+localName;
    }


    public String getLocalName() {
        return localName;
    }


    public URI getURI() {
        return uri;
    }

}
