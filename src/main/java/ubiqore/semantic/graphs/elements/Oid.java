package ubiqore.semantic.graphs.elements;

import org.openrdf.model.Literal;
import org.openrdf.model.Value;

import ubiqore.semantic.graphs.elements.sparql.SparqlData;

import com.google.common.base.Preconditions;

public class Oid implements SparqlData {
	
	public Oid(){
		
	}
	public Oid(String oid){
		Preconditions.checkNotNull(oid);
		this.value=oid;
	}
	private String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getObjectLabel() {
		// TODO Auto-generated method stub
		return "oid";
	}

	public void compute(Value v) {
		Literal loid=(Literal)v;
		this.setValue(loid.getLabel());
	}

	
}
