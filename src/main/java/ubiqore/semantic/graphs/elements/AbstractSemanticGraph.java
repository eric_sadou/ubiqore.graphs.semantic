package ubiqore.semantic.graphs.elements;

import info.aduna.iteration.CloseableIteration;

import java.io.File;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import org.openrdf.model.Namespace;
import org.openrdf.model.Resource;
import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ContextStatementImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.query.BindingSet;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.query.impl.EmptyBindingSet;
import org.openrdf.query.parser.ParsedBooleanQuery;
import org.openrdf.query.parser.ParsedQuery;
import org.openrdf.query.parser.sparql.SPARQLParser;
import org.openrdf.sail.Sail;
import org.openrdf.sail.SailConnection;
import org.openrdf.sail.SailException;

import ubiqore.graphs.semantic.AbstractGraph;
import ubiqore.graphs.semantic.SemanticVocabulary;
import ubiqore.graphs.semantic.SkosConcept;
import ubiqore.semantic.graphs.elements.sparql.URICondition;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import com.google.common.base.Strings;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import com.tinkerpop.blueprints.impls.neo4j.Neo4jGraph;
import com.tinkerpop.blueprints.impls.sail.SailTokens;
import com.tinkerpop.blueprints.oupls.sail.GraphSail;

public abstract class AbstractSemanticGraph {
	private boolean connected=false;
	protected SailConnection sc=null;
	protected Neo4jGraph baseGraph =null;
	protected Sail sail;
	protected ValueFactory vf=null;
	//protected URI context=new URIImpl(SemanticVocabulary.context_default);
	protected Context context;
	protected String rep; // the rep in which the graph dir is in.
	protected String graphName;
	protected List<Statement> vocabularyStatements=Lists.newArrayList();
	protected Map<String,String> namespaces=Maps.newHashMap();
	String queryPrepare=null;
	
	List<URI> inverseDeclarationCache=Lists.newArrayList();
	List<URI> equivalentDeclarationCache=Lists.newArrayList();
	List<URI> notInverseDeclarationCache=Lists.newArrayList();
	List<URI> notEquivalentDeclarationCache=Lists.newArrayList();
	
	protected boolean isInverse(URI rela)throws SailException{
		// cache check ...
		for (URI uri:inverseDeclarationCache)if(rela.equals(uri))return true;
		for (URI uri:notInverseDeclarationCache)if(rela.equals(uri))return false;
		
		boolean b=false;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		
		String queryString =this.queryPrepare()+" ASK  "+
							"WHERE "+
							"{ " +
							" <"+rela.stringValue()+"> owl:inverseOf ?q . " +
							" } "
							;
		
		
		
		
		//System.out.println(queryString);
		
		try {
			ParsedBooleanQuery query=(ParsedBooleanQuery) parser.parseQuery(queryString , context.toString());
			
			sparqlResults=sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
			
			try {
				b= sparqlResults.hasNext();	
				
			} 
			catch (QueryEvaluationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		if (sc.isOpen())sc.close();
		
		if (b)inverseDeclarationCache.add(rela);
		else notInverseDeclarationCache.add(rela);
		return b;
	}
	
	protected boolean isEquivalent(URI rela)throws SailException{
		// cache check ...
		for (URI uri:equivalentDeclarationCache)if(rela.equals(uri))return true;
		for (URI uri:notEquivalentDeclarationCache)if(rela.equals(uri))return false;
		
		boolean b=false;
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		
		String queryString =this.queryPrepare()+" ASK  "+
							"WHERE "+
							"{ " +
							" <"+rela.stringValue()+"> owl:equivalentProperty ?q . " +
							" } "
							;
		
		
		
		
		//System.out.println(queryString);
		
		try {
			ParsedBooleanQuery query=(ParsedBooleanQuery) parser.parseQuery(queryString , context.toString());
			
			sparqlResults=sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
			
			try {
				b= sparqlResults.hasNext();	
				
			} 
			catch (QueryEvaluationException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (MalformedQueryException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		sc.close();
		if (b)equivalentDeclarationCache.add(rela);
		else notEquivalentDeclarationCache.add(rela);
		
		return b;
	}
	
	
	protected void applyEquivalent(Statement st) throws SailException{
		Set<URI> predicate=null;
		SailConnection sc=sail.getConnection();
	
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		try {
		String queryString =this.queryPrepare()+" SELECT ?p   "+
							"WHERE "+
							"{ " +
							" <"+st.getPredicate()+"> owl:equivalentProperty  ?p . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		
		
		if (sparqlResults.hasNext()){
			
			predicate=Sets.newHashSet();
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				predicate.add(new URIImpl(a.getValue("p").stringValue()));
			
			}
			
			
		}
		}catch (Exception e){e.printStackTrace();}
		
		sc.close();
		if(predicate!=null){
			for (URI uri:predicate){
				Statement equiv=new ContextStatementImpl(st.getSubject(),uri,new URIImpl(st.getObject().toString()),this.context.getUri());
				this.addSingleStatement(equiv);
			}
		}
		
	}
	
	protected void applyInverse(Statement st) throws SailException{
		Set<URI> predicate=null;
		SailConnection sc=sail.getConnection();
	
		SPARQLParser parser=new SPARQLParser();
		
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		try {
		String queryString =this.queryPrepare()+" SELECT ?p   "+
							"WHERE "+
							"{ " +
							" ?p owl:inverseOf <"+st.getPredicate()+"> . " +
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), false);
		
		
		
		if (sparqlResults.hasNext()){
			
			predicate=Sets.newHashSet();
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				predicate.add(new URIImpl(a.getValue("p").stringValue()));
			}
			
			
		}
		}catch (Exception e){e.printStackTrace();}
		
		sc.close();
		if(predicate!=null){
			for (URI uri:predicate)this.addSingleStatement(new ContextStatementImpl(new URIImpl(st.getObject().toString()), uri, st.getSubject(), this.context.getUri()));
		}
		
	}
	
	public AbstractSemanticGraph(boolean creation,String rep,String graphName, Context context) throws SailException{
		Preconditions.checkArgument(!Strings.isNullOrEmpty(rep));
		Preconditions.checkArgument(!Strings.isNullOrEmpty(graphName));
		this.graphName=graphName;
		File f=new File(rep);
		Preconditions.checkArgument(f.isDirectory());
		this.rep=rep;
		
		if (null==context){
			this.context=new Context(new URIImpl(Defaults.contextURI_S));
		}
		
		if (creation){
			this.creationIntitialyze();
		}else {
			File graphDir=new File(rep+"/"+graphName);
			Preconditions.checkArgument(graphDir.isDirectory());
		}
		this.initAccess();
		connected=true;
		if (creation){
			this.createVocabularyStatementsList();
			this.createSpecificGraphStatements();
			
		}
		this.createNamespacesMap();
		this.addDefaultNamespaces();
		this.queryPrepare();
	}
	
	protected abstract void createVocabularyStatementsList();
	protected abstract void createNamespacesMap();
	protected abstract void link(URI subject, URI predicate ,URI object) throws SailException;
	
	protected boolean addSingleStatement(Statement st)throws SailException{
		if (st==null)return false;
		try {
			Resource r=null;
			if (st.getContext()==null){
				r=context.getUri();
			}else r=st.getContext(); 
			//baseGraph.setCheckElementsInTransaction(false);
			SailConnection sc=sail.getConnection();
			sc.addStatement(st.getSubject(),st.getPredicate(),st.getObject(),r);
			
			sc.commit();
			//baseGraph.setCheckElementsInTransaction(true);
			sc.close();
			return true;
		}catch (Exception e){
			e.printStackTrace();
			if (sc!=null)if (sc.isOpen())sc.close();
		}
		return false;
	}
	protected void initAccess() throws SailException{
		baseGraph = new Neo4jGraph(rep+"/"+graphName);
		baseGraph.setCheckElementsInTransaction(true);
		sail= new GraphSail(baseGraph);
		sail.initialize();
		vf=sail.getValueFactory();
	}
	
	
	
	private void creationIntitialyze(){
		// if we are here rep is a valide directory 
		File dir=new File (rep+"/"+graphName);
		if (dir.exists()){
			System.out.println("The graph dir will be delete and all files inside.. sorry but it's too late now..");
			AbstractSemanticGraph.recursiveDelete(dir);
			System.out.println("Graph will be install on the fresh place now..");
		}else {
			dir.mkdirs();
		}
	}
	
	protected String queryPrepare() {
		if (Strings.isNullOrEmpty(queryPrepare)){
			String prepare="";
			try {
				SailConnection sc=sail.getConnection();
			CloseableIteration<? extends Namespace, SailException> map=sc.getNamespaces();
			while (map.hasNext()){
				Namespace ns=map.next();
				prepare+="PREFIX "+ns.getPrefix()+": <"+ns.getName() +">  ";
			}
			sc.close();
			}catch (Exception e){
				e.printStackTrace();
			}
			queryPrepare=prepare;
		}
		return queryPrepare;
	}
	
	private static void recursiveDelete(File file) {
        if (!file.exists()) return;
        if (file.isDirectory()) {
           for (File f : file.listFiles()) recursiveDelete(f);
           file.delete();
        } else {
           file.delete();
        }
} 
	
	
	/*
	 * AbstractSemanticGraph
	 */
	public boolean isConnected() {
		return connected;
	}
	
	private void createSpecificGraphStatements() throws SailException{
		if (vocabularyStatements.isEmpty())return;
		
		try {
			SailConnection sc=sail.getConnection();
			ValueFactory vf = sail.getValueFactory();
			for (Statement st:vocabularyStatements)sc.addStatement(st.getSubject(),st.getPredicate(),st.getObject(),st.getContext());
			sc.commit();
			sc.close();
			
		}catch (Exception e){
			
			if (sc!=null)if (sc.isOpen())sc.close();
		}
		
		
	}
	
	public void addDefaultNamespaces() {
		 if (this.namespaces.isEmpty())return;
		 Iterator<Entry<String, String>> it=this.namespaces.entrySet().iterator();
		 while (it.hasNext()){
			 Entry<String, String> entry=it.next();
			 this.addNamespace(entry.getKey(), entry.getValue());
		 }
	 }
	
	 public void addNamespace(final String prefix, final String namespace)  {
		 	SailConnection sc=null;
	        try {
	        	sc=sail.getConnection();
	            sc.setNamespace(prefix, namespace);
	            sc.commit();
	        } catch (SailException e) {
	            throw new RuntimeException(e.getMessage(), e);
	        }finally {
	        	if (sc!=null){
	        		try {
						sc.close();
					} catch (SailException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
	        	}
	        }
	    }
	 
	 protected void useClassicNamespaces(){
		    this.namespaces.put(SailTokens.RDF_PREFIX, SailTokens.RDF_NS);
	        this.namespaces.put(SailTokens.RDFS_PREFIX, SailTokens.RDFS_NS);
	        this.namespaces.put(SailTokens.OWL_PREFIX, SailTokens.OWL_NS);
	        this.namespaces.put(SailTokens.XSD_PREFIX, SailTokens.XSD_NS);
	        this.namespaces.put(SailTokens.FOAF_PREFIX, SailTokens.FOAF_NS);
	}
	 

	protected Statement testStatement(Statement st)throws SailException{
			
			Statement first=null;
			try {
				SailConnection sc=sail.getConnection();
				CloseableIteration<? extends Statement, SailException> results =sc.getStatements(st.getSubject(),st.getPredicate(),st.getObject(),true,st.getContext());
				while(results.hasNext()) {
					first=results.next();
				    break;
				}
				sc.close();
			}catch (Exception e){
				if (sc!=null)if (sc.isOpen())sc.close();
			}
			return first; 
	}
	
	protected boolean addMultipleStatements(List<Statement> sts)throws SailException{
		if (sts==null)return false;
		if (sts.isEmpty())return false;
		String string=null;
		try {
			SailConnection sc=sail.getConnection();
			for (Statement st:sts){
				//string=st.getSubject().toString()+" "+st.getPredicate().getLocalName()+ " "+st.getObject().toString();
				//System.out.println("triplet to add "+string);
				sc.addStatement(st.getSubject(),st.getPredicate(),st.getObject(),st.getContext());
				
			}
			sc.commit();
			sc.close();
			return true;
		}catch (Exception e){
			System.out.println("error addStatement..."+string);
			e.printStackTrace();
			if (sc!=null)if (sc.isOpen())sc.close();
		}
		return false;
	}
	
	public Set<URI> getAllElements(Element el)throws SailException, MalformedQueryException, QueryEvaluationException{
		Set<URI> set=Sets.newHashSet();
		SailConnection sc=sail.getConnection();
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+" SELECT ?uri  "+
							"WHERE "+
							"{ " +
							" ?uri rdf:type  "+el.getShortLabelRDFType()+" ."+
							
							" } "
							;
		//System.out.println(queryString);
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		
		if (sparqlResults.hasNext()){
		
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				//System.out.println(a.getValue("x"));
				set.add(new URIImpl(a.getValue("x").stringValue()));
			}
			
		}
		
		sc.close();
		return set;
	 
	}
	
	public Element  getElementBaseFromURI(Element el,URI uri)throws SailException, MalformedQueryException, QueryEvaluationException{
	
		SailConnection sc=sail.getConnection();
		
		SPARQLParser parser=new SPARQLParser();
		CloseableIteration<? extends BindingSet,QueryEvaluationException> sparqlResults;
		String queryString =this.queryPrepare()+ " SELECT";
		for (URICondition cond:el.getConditionsForBasics()){
			queryString+=" ?"+cond.getObject();
		}
		queryString+=" WHERE { ";
		for (URICondition cond:el.getConditionsForBasics()){
			String condition="<"+ uri.toString()+"> "+cond.getPredicate()+" ?"+cond.getObject();
			if (cond.isOptional()){
				condition = "OPTIONAL { "+condition+ "} ";
			}
			condition = condition +" . ";
			queryString+=condition;
		} 
		queryString+=" } ";
		
		ParsedQuery query=parser.parseQuery(queryString , context.toString());
		sparqlResults =sc.evaluate(query.getTupleExpr(),query.getDataset(), new EmptyBindingSet(), true);
		if (sparqlResults.hasNext()){
			while (sparqlResults.hasNext()){
				BindingSet a=sparqlResults.next();
				for (URICondition cond:el.getConditionsForBasics()){
					Value v=a.getValue(cond.getObject());
					if (v!=null)cond.setRes(v);
				}
				break;
			}
		}
		sc.close();
		return el;
	}
}
