package ubiqore.semantic.graphs.elements.sparql;

import org.openrdf.model.Value;

public interface SparqlData {
 
	public String getObjectLabel();
	public void compute(Value v);
}
