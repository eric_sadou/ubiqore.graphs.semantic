package ubiqore.semantic.graphs.elements.sparql;
 
import org.openrdf.model.Value;

public class URICondition {
	SparqlData data=null;
	String filter=null;
	public String getFilter() {
		return filter;
	}
	public void setFilter(String filter) {
		this.filter = filter;
	}
	public URICondition(SparqlData data){
		this.data=data;
		this.object=data.getObjectLabel();
	}
	
	String  predicate;
	public String getPredicate() {
		return predicate;
	}
	public void setPredicate(String predicate) {
		this.predicate = predicate;
	}
	public String getObject() {
		return object;
	}
	public void setObject(String object) {
		this.object = object;
	}
	public boolean isOptional() {
		return isOptional;
	}
	public void setOptional(boolean isOptional) {
		this.isOptional = isOptional;
	}
	String  object;
	boolean isOptional=false;
	
	
	Value res;
	public Value getRes() {
		return res;
	}
	public void setRes(Value res) {
		this.res = res;
		this.data.compute(res);
	}
}
