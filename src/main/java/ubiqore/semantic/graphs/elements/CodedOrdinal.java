package ubiqore.semantic.graphs.elements;

import org.openrdf.model.Literal;
import org.openrdf.model.Value;

import ubiqore.semantic.graphs.elements.sparql.SparqlData;

public class CodedOrdinal implements SparqlData {

	public CodedOrdinal(){
		
	}
	Float value;

	public Float getValue() {
		return value;
	}

	public void setValue(Float value) {
		this.value = value;
	}

	public String getObjectLabel() {
		// TODO Auto-generated method stub
		return "coValue";
	}

	public void compute(Value v) {
		// TODO Auto-generated method stub
		Literal value=(Literal)v;
		this.value=new Float(value.floatValue());
	}
}
