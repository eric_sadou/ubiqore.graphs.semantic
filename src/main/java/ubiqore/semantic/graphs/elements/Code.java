package ubiqore.semantic.graphs.elements;

import org.openrdf.model.Value;

import ubiqore.semantic.graphs.elements.sparql.SparqlData;

public class Code implements SparqlData{
	String value;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getObjectLabel() {
		// TODO Auto-generated method stub
		return "code";
	}

	public void compute(Value v) {
		
		this.setValue(v.stringValue());
	}
}
