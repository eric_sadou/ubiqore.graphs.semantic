package ubiqore.semantic.graphs.elements;

import java.util.Locale;

import org.openrdf.model.Literal;
import org.openrdf.model.impl.LiteralImpl;

public class Label {
	
	String value;
	Locale langage=Locale.ENGLISH;
	
	public Label(String st){
		this.value=st;
	}

	public Literal getLit() {
		return new LiteralImpl(value,langage.toString());
	}

}
