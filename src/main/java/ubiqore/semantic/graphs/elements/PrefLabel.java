package ubiqore.semantic.graphs.elements;

import java.util.Locale;

import org.openrdf.model.Literal;
import org.openrdf.model.Value;

import ubiqore.semantic.graphs.elements.sparql.SparqlData;

public class PrefLabel extends Label implements SparqlData{
	
	public PrefLabel(){
		super(null);
	}
	
	public PrefLabel(String st) {
		super(st);
		// TODO Auto-generated constructor stub
	}

	public PrefLabel(Literal lit){
		
		super(lit.getLabel());
		this.langage=new Locale(lit.getLanguage());
	}

	public String getObjectLabel() {
		// TODO Auto-generated method stub
		return "prefLabel";
	}

	public void compute(Value v) {
		Literal llabel=(Literal)v;
		this.langage=new Locale(llabel.getLanguage());
		this.value=llabel.getLabel();
	}
	
	
}
