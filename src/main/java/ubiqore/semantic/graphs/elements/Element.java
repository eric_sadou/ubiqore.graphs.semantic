package ubiqore.semantic.graphs.elements;

import java.util.List;

import org.openrdf.model.Statement;
import org.openrdf.model.Value;
import org.openrdf.model.ValueFactory;

import ubiqore.semantic.graphs.elements.sparql.URICondition;

public interface  Element {
	/*
	 * Element de graph : could be "Concept" , "Scheme" etc ...
	 */
	// un element doit pouvoir etre persisté.
	public void persist();
	public void atomicPersist();
	
	// doit pouvoir etre loadé ??
	public Element load();
	public String getVocabulary();
	public Statement declaration(); // each element should have a declaration statement type : A rdf:type B.
	public String getShortLabelRDFType();
	public List<URICondition> getConditionsForBasics();
	public List<Statement>  generateStatements(Context context,ValueFactory vf);
	
}
