package ubiqore.semantic.graphs.elements;

public class Defaults {
	public static final String domain_S="http://server.ubiqore.com";
	public static final String version_S="v1";
	public static final String contextURI_S=domain_S+"/"+version_S+"/";
}
