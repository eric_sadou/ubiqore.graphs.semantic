package ubiqore.semantic.graphs.elements.ehr4cr;

import java.util.List;
import java.util.Set;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.impl.ContextStatementImpl;
import org.openrdf.model.vocabulary.OWL;
import org.openrdf.model.vocabulary.RDFS;
import org.openrdf.query.MalformedQueryException;
import org.openrdf.query.QueryEvaluationException;
import org.openrdf.sail.SailException;

import com.google.common.base.Preconditions;

import ubiqore.semantic.graphs.elements.SKOS;
import ubiqore.semantic.graphs.elements.AbstractSemanticGraph;
import ubiqore.semantic.graphs.elements.Context;

/*
 * Manage MultiTerm , uses skos:broader as hierarchical direct unique link... 
 */ 
 
public class MultiTerminologyGraph extends AbstractSemanticGraph {

	public MultiTerminologyGraph(boolean creation, String rep,
			String graphName, Context context) throws SailException {
		super(creation, rep, graphName, context);
		// TODO Auto-generated constructor stub
	}

	// Broader is persisted.
	// for import statements ... from narrower or subclass , we use OWL rules to manage them.
	@Override
	protected void createVocabularyStatementsList() {
		// TODO Auto-generated method stub
		vocabularyStatements.add(new ContextStatementImpl(SKOS.NARROWER.getURI(),OWL.INVERSEOF,SKOS.BROADER.getURI(),context.getUri()));
		vocabularyStatements.add(new ContextStatementImpl(RDFS.SUBCLASSOF,OWL.EQUIVALENTPROPERTY,SKOS.NARROWER.getURI(),context.getUri()));
	}

	@Override
	protected void createNamespacesMap() {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected void link(URI subject, URI predicate, URI object)
			throws SailException {
		
		 Statement st=new ContextStatementImpl(subject,predicate,object,this.context.getUri());
		 if (this.isInverse(predicate)){
				this.applyInverse(st);
			}else if (this.isEquivalent(predicate)){
				this.applyEquivalent(st);
			}else {
				this.addSingleStatement(st);
			}
	}
	
	
	public URI createScheme(Scheme scheme){
		Preconditions.checkNotNull(scheme, "scheme is null");
		URI uri=null;
		List<Statement> sts=scheme.generateStatements(this.context, this.vf);
		try {
			Statement exists=this.testStatement(scheme.declaration());
			if (exists==null){ // no scheme with this URI , creation can appen
				boolean ok=this.addMultipleStatements(sts);
				if (ok)uri=scheme.getUri();
			}
			
		} catch (SailException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return uri;
	}
	
	// return all the schemes URI inside the graph based on "rdf:type" declaration.
	public Set<URI> getSchemesURI() throws SailException, MalformedQueryException, QueryEvaluationException{
		return this.getAllElements(new Scheme());
	}
	
	// gerer le cache !!
	public Scheme getSchemeBase(URI schemeURI) throws SailException, MalformedQueryException, QueryEvaluationException{
		return (Scheme)this.getElementBaseFromURI(new Scheme(), schemeURI);
	}
}
