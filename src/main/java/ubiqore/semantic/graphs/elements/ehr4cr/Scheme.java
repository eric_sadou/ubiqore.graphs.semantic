package ubiqore.semantic.graphs.elements.ehr4cr;

import java.util.List;
import java.util.Set;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ContextStatementImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.impl.ValueFactoryImpl;
import org.openrdf.model.vocabulary.RDF;


import ubiqore.semantic.graphs.elements.Context;
import ubiqore.semantic.graphs.elements.Definition;
import ubiqore.semantic.graphs.elements.Element;
import ubiqore.semantic.graphs.elements.Oid;
import ubiqore.semantic.graphs.elements.PrefLabel;
import ubiqore.semantic.graphs.elements.SKOS;
import ubiqore.semantic.graphs.elements.sparql.URICondition;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

public class Scheme implements Element{
	private final String vocabulary="scheme/";
	private PrefLabel prefLabel=new PrefLabel();
	private Oid oid=new Oid();
	private URI uri;
	public PrefLabel getPrefLabel() {
		return prefLabel;
	}
	public void setPrefLabel(PrefLabel prefLabel) {
		this.prefLabel = prefLabel;
	}
	public Oid getOid() {
		return oid;
	}
	public void setOid(Oid oid) {
		this.oid = oid;
	}
	public URI getUri() {
		return uri;
	}
	public void setUri(URI uri) {
		this.uri = uri;
	}
	public Set<Term> getTopElement() {
		return topElement;
	}
	public void setTopElement(Set<Term> topElement) {
		this.topElement = topElement;
	}
	public Definition getDefinition() {
		return definition;
	}
	public void setDefinition(Definition definition) {
		this.definition = definition;
	}
	private Set<Term> topElement=Sets.newHashSet();
	private Definition definition=new Definition();
	private Statement declaration;
	
	public Scheme(){}
	
	public Scheme(Oid oid,PrefLabel p,Definition def){
		Preconditions.checkNotNull(oid);
		Preconditions.checkNotNull(p);
		
		this.oid=oid;
		this.prefLabel=p;
		this.definition=def;
		
	}
	public void persist() {
		// TODO Auto-generated method stub
		
	}

	public Element load() {
		// TODO Auto-generated method stub
		return null;
	}

	public void atomicPersist() {
		// TODO Auto-generated method stub
		
	}

	public String getVocabulary() {
		// TODO Auto-generated method stub
		return this.vocabulary;
	}
	List<Statement>  creation;
	
	public List<Statement>  generateStatements(Context context,ValueFactory vf){
		
		Preconditions.checkArgument(this.oid!=null,"no oid");
		this.uri=new URIImpl(context+this.vocabulary+this.oid.getValue());
		
		Preconditions.checkArgument(this.uri!=null,"no uri");
		Preconditions.checkArgument(this.prefLabel!=null,"no preflabel");
		
		List<Statement> sts=Lists.newArrayList();
		declaration=new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPTSCHEME.getURI(),context.getUri());
		sts.add(declaration);
		sts.add(new ContextStatementImpl(uri,SKOS.PREFLABEL.getURI(),prefLabel.getLit(),context.getUri()));
		sts.add(new ContextStatementImpl(uri,SKOS.NOTATION.getURI(),vf.createLiteral(oid.getValue()),context.getUri()));
		if (definition!=null)sts.add(new ContextStatementImpl(uri,SKOS.DEFINITION.getURI(),vf.createLiteral(definition.getValue()),context.getUri()));
		
		creation=sts;
		return creation;
	}
	
	public Statement declaration(){
		return declaration;
	}
	public String getShortLabelRDFType() {
		// TODO Auto-generated method stub
		return SKOS.CONCEPTSCHEME.getFormat();
	}
	public List<URICondition> getConditionsForBasics() {
		List<URICondition> l=Lists.newArrayList();
		
		URICondition c1=new URICondition(this.oid);
		c1.setPredicate(SKOS.NOTATION.getFormat());
		c1.setOptional(false);
		
		l.add(c1);
		
		URICondition c2=new URICondition(this.prefLabel);
		c2.setPredicate(SKOS.PREFLABEL.getFormat());
		c2.setOptional(false);
		
		l.add(c2);
		
		URICondition c3=new URICondition(this.definition);
		c3.setPredicate(SKOS.DEFINITION.getFormat());
		c3.setOptional(true);
		
		l.add(c3);
		
		return l;
	}
}
