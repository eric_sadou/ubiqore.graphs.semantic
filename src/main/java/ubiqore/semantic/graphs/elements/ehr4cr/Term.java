package ubiqore.semantic.graphs.elements.ehr4cr;

import java.util.List;

import org.openrdf.model.Statement;
import org.openrdf.model.URI;
import org.openrdf.model.ValueFactory;
import org.openrdf.model.impl.ContextStatementImpl;
import org.openrdf.model.impl.URIImpl;
import org.openrdf.model.vocabulary.RDF;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;

import ubiqore.semantic.graphs.elements.Code;
import ubiqore.semantic.graphs.elements.CodedOrdinal;
import ubiqore.semantic.graphs.elements.Context;
import ubiqore.semantic.graphs.elements.Element;
import ubiqore.semantic.graphs.elements.PrefLabel;
import ubiqore.semantic.graphs.elements.SKOS;
import ubiqore.semantic.graphs.elements.sparql.URICondition;

public class Term implements Element{
	public Term(){}
	
	private final String vocabulary="term/";
	private Statement declaration;
	private PrefLabel prefLabel=new PrefLabel();
	private URI uri;
	private Scheme scheme;
	public Statement getDeclaration() {
		return declaration;
	}

	public void setDeclaration(Statement declaration) {
		this.declaration = declaration;
	}

	public PrefLabel getPrefLabel() {
		return prefLabel;
	}

	public void setPrefLabel(PrefLabel prefLabel) {
		this.prefLabel = prefLabel;
	}

	public URI getUri() {
		return uri;
	}

	public void setUri(URI uri) {
		this.uri = uri;
	}

	public Scheme getScheme() {
		return scheme;
	}

	public void setScheme(Scheme scheme) {
		this.scheme = scheme;
	}

	public Code getCode() {
		return code;
	}

	public void setCode(Code code) {
		this.code = code;
	}

	public CodedOrdinal getCodedOrdinal() {
		return codedOrdinal;
	}

	public void setCodedOrdinal(CodedOrdinal codedOrdinal) {
		this.codedOrdinal = codedOrdinal;
	}

	private Code code;
	private CodedOrdinal codedOrdinal=new CodedOrdinal();
	
	
	public void persist() {
		// TODO Auto-generated method stub
		
	}

	public void atomicPersist() {
		// TODO Auto-generated method stub
		
	}

	public Element load() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getVocabulary() {
		// TODO Auto-generated method stub
		return this.vocabulary;
	}

	public Statement declaration() {
		// TODO Auto-generated method stub
		return this.declaration;
	}

	public String getShortLabelRDFType() {
		// TODO Auto-generated method stub
		return SKOS.CONCEPT.getFormat();
	}

	public List<URICondition> getConditionsForBasics() {
		// TODO Auto-generated method stub
		return null;
	}

	public List<Statement> generateStatements(Context context, ValueFactory vf) {
		Preconditions.checkArgument(this.code!=null,"no code");
		Preconditions.checkArgument(this.scheme!=null,"no scheme");
		this.uri=new URIImpl(context+this.vocabulary+this.scheme.getOid()+"/"+code.getValue());
		Preconditions.checkArgument(this.uri!=null,"no uri");
		Preconditions.checkArgument(this.prefLabel!=null,"no preflabel");
		List<Statement> sts=Lists.newArrayList();
		declaration=new ContextStatementImpl(uri,RDF.TYPE,SKOS.CONCEPT.getURI(),context.getUri());
		sts.add(declaration);
		sts.add(new ContextStatementImpl(uri,SKOS.PREFLABEL.getURI(),prefLabel.getLit(),context.getUri()));
		sts.add(new ContextStatementImpl(uri,SKOS.NOTATION.getURI(),vf.createLiteral(code.getValue()),context.getUri()));
		sts.add(new ContextStatementImpl(uri,SKOS.INSCHEME.getURI(),scheme.getUri(),context.getUri()));
		return sts;
	}

}
