package ubiqore.services.shared;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Scheme {

	String oid,label,urn,uri;
	Set<ErsatzTerm> topTerms;
	
	@Override
	public String toString() {
		return "Scheme [oid=" + oid + ", label=" + label + ", urn=" + urn
				+ ", uri=" + uri + ", topTerms=" + topTerms + "]";
	}

	public String getOid() {
		return oid;
	}

	public void setOid(String oid) {
		this.oid = oid;
	}

	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public String getUrn() {
		return urn;
	}

	public void setUrn(String urn) {
		this.urn = urn;
	}

	public String getUri() {
		return uri;
	}

	public void setUri(String uri) {
		this.uri = uri;
	}

	public Set<ErsatzTerm> getTopTerms() {
		return topTerms;
	}

	public void setTopTerms(Set<ErsatzTerm> topTerms) {
		this.topTerms = topTerms;
	}

	
	
}
