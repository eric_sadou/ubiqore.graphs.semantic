package ubiqore.services.shared;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import com.google.common.collect.Sets;

/*
 * Term is used to access Term
 */
@XmlRootElement
public class Term extends AbstractTerm {

	public static final String TRANSITIVE ="skos:narrowerTransitive";
	public static final String DIRECT ="skos:narrower";
	
	
	String managedSubLevels;
	public String getManagedSubLevels() {
		return managedSubLevels;
	}

	public void setManagedSubLevels(String managedSubLevels) {
		this.managedSubLevels = managedSubLevels;
	}

	public String getSemanticOfNarrowers() {
		return semanticOfNarrowers;
	}

	public void setSemanticOfNarrowers(String semanticOfNarrowers) {
		this.semanticOfNarrowers = semanticOfNarrowers;
	}




	String semanticOfNarrowers;

	Set<ErsatzTerm> narrowers = Sets.newHashSet();    // direct narrowers Terms.
	
	
	
	

	public void addNarrower(ErsatzTerm term) {
		this.narrowers.add(term);
	}

	public String getCodingSchemeOID() {
		return codingSchemeOID;
	}

	

	public Set<ErsatzTerm> getNarrowers() {
		return narrowers;
	}

	

	
	public void setNarrowers(Set<ErsatzTerm> narrowers) {
		this.narrowers = narrowers;
	}

	

	
	

}
