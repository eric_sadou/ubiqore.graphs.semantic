package ubiqore.services.shared;

import java.util.Set;

import javax.xml.bind.annotation.XmlRootElement;

import com.google.common.collect.Sets;

@XmlRootElement
public class ErsatzTerm extends AbstractTerm {

	@Override
	public String toString() {
		return "ErsatzTerm [uri=" + uri + ", codingSchemeOID="
				+ codingSchemeOID + ", skosNotation=" + skosNotation
				+ ", codingSchemeLabel=" + codingSchemeName + ", preflabel="
				+ preflabel + ", ehr4crCodingSchemeURI="
				+ ehr4crCodingSchemeURI + "]";
	}
	
	Set<ErsatzTerm> topBroaders=Sets.newHashSet();
	
	public Set<ErsatzTerm> getTopBroaders() {
		return topBroaders;
	}



	public void setTopBroaders(Set<ErsatzTerm> topBroaders) {
		this.topBroaders = topBroaders;
	}

}
