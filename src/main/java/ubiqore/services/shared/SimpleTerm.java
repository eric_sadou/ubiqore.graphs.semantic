package ubiqore.services.shared;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class SimpleTerm {
	String label;
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getUri() {
		return uri;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
	String uri;
}
