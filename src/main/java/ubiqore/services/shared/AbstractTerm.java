package ubiqore.services.shared;

import java.util.Set;

import com.google.common.collect.Sets;

public abstract class AbstractTerm {

	String uri; 				// internal EHR4CR system URI.
	String codingSchemeOID; 	// official codingSchemeOID.
	String skosNotation; 		// official code.
	String codingSchemeName;   // official codingSchemeLabel
	String preflabel;			// official prefered Label.
	String ehr4crCodingSchemeURI; //internal codingSchemeURI.
	


	public void copy(AbstractTerm at){
		uri=at.uri;
		codingSchemeOID=at.codingSchemeOID;
		skosNotation=at.skosNotation;
		codingSchemeName=at.codingSchemeName;
		preflabel=at.preflabel;
		ehr4crCodingSchemeURI=at.ehr4crCodingSchemeURI;
		
	}
	
	
	
	public String getCodingSchemeName() {
		return codingSchemeName;
	}
	public String getCodingSchemeOID() {
		return codingSchemeOID;
	}
	
	
	
	
	public String getEhr4crCodingSchemeURI() {
		return ehr4crCodingSchemeURI;
	}
	public String getPreflabel() {
		return preflabel;
	}
	public String getSkosNotation() {
		return skosNotation;
	}
	public String getUri() {
		return uri;
	}
	public void setCodingSchemeName(String codingSchemeName) {
		this.codingSchemeName = codingSchemeName;
	}
	public void setCodingSchemeOID(String codingSchemeOID) {
		this.codingSchemeOID = codingSchemeOID;
	}
	public void setEhr4crCodingSchemeURI(String ehr4crCodingSchemeURI) {
		this.ehr4crCodingSchemeURI = ehr4crCodingSchemeURI;
	}
	public void setPreflabel(String preflabel) {
		this.preflabel = preflabel;
	}
	public void setSkosNotation(String skosNotation) {
		this.skosNotation = skosNotation;
	}
	public void setUri(String uri) {
		this.uri = uri;
	}
}
