package ubiqore.graphs.semantic;

import java.util.Set;

import org.joda.time.DateTime;
import org.openrdf.model.URI;
import org.openrdf.model.impl.URIImpl;

public class TestGraph {

	public TestGraph(){
		
	}
	
	/*@Test
	public void test1(){
		TripleAndContextGraph aGraph=null;
		try {
			aGraph=new TripleAndContextGraph(true,"/Reborn/testTriple");
			
			Scheme scheme=new Scheme();
			scheme.setDef("schemeA");
			scheme.setOid("213123123123");
			scheme.setPreflabel(new PrefLabel("schemeA"));
			URI schemeURI=aGraph.applyScheme(scheme.getOid(), scheme.getPreflabel().getLit().getLabel(),scheme.getDef());
			scheme=aGraph.getSchemeBase(schemeURI);
			
			SkosConcept skosA=new SkosConcept();
			skosA.setSkosNotation("skosA");
			skosA.setPreflabel(new PrefLabel("jacky"));
			skosA.setScheme(scheme);
			URI uri =aGraph.addOrGetSimpleConcept(skosA);
			skosA.setUri(uri);
			System.out.println(uri);
			aGraph.addTopConcept(skosA);
		
			scheme.setDef("schemeB");
			scheme.setOid("23423423423");
			scheme.setPreflabel(new PrefLabel("schemeB"));
			schemeURI=aGraph.applyScheme(scheme.getOid(), scheme.getPreflabel().getLit().getLabel(),scheme.getDef());
			scheme=aGraph.getSchemeBase(schemeURI);
			
			SkosConcept skosB=new SkosConcept();
			skosB.setSkosNotation("CAM:skosB/CO");
			skosB.setPreflabel(new PrefLabel("jacky"));
			skosB.setScheme(scheme);
			uri =aGraph.addOrGetSimpleConcept(skosB);
			skosB.setUri(uri);
			aGraph.addOriginalCOMember(uri, (float)0,DateTime.now());
			aGraph.statements();
			
			DateTime td=new DateTime();
			System.out.println(td);
			aGraph.link(skosA.getUri(), SKOS.NARROWER.getURI(), skosB.getUri());
			System.out.println(aGraph.searchInDomain(skosB.getUri(), skosA.getUri()));
			System.out.println("skosB Namespace=<"+uri.getNamespace()+"> skosB LocalName="+uri.getLocalName());
			
			System.out.println(aGraph.searchLabel("jack", scheme.getUri(), 2, 0, false));
			System.out.println(aGraph.searchLabel("jack", null, 2, 0, false));
			aGraph.statements();
			aGraph.exportRDFOfSubTree("/EHR4CR/data/export.rdf", skosA.getUri(), 0);
			System.out.println(aGraph.getConceptsURI().size());
			System.out.println(aGraph.getSchemesURI());
			System.err.println("test getConcept "+aGraph.getConcept(skosA.getUri()).getSkosNotation());
			System.err.println("test getConcept get Periode ! "+aGraph.getConcept(skosB.getUri()).getPeriod());
			System.out.println("test getErsaztConcept "+aGraph.getErsatzConcept("CAM:skosB/CO", "urn:oid:213123123123", false).getSkosNotation());
			System.out.println("test getErsaztConcept "+aGraph.getErsatzConcept("CAM:skosB/CO/0", "urn:oid:213123123123", true).getSkosNotation());
			System.out.println("test search inside a tree "+aGraph.searchInDomain("ja", skosA.getUri()));
			aGraph.exportRDF("/EHR4CR/data/fake.rdf");
		}catch (Exception e){
			e.printStackTrace();
			
		}
		finally {
			if (aGraph!=null)aGraph.disconnect();
		}
	}
	
	@Test
	public void test2(){
		TripleAndContextGraph aGraph=null;
		try {
			aGraph=new TripleAndContextGraph(true,"/Reborn/testTriple");
			
			aGraph.importRDF("/EHR4CR/data/hospitalList.rdf");
			
			aGraph.statements();
			System.out.println(aGraph.searchCode("FAKE", new URIImpl("http://server.ubiqore.com/v1/hospital"), 4, 0, true));
			System.err.println(aGraph.getSchemeUriFromOid("hospital"));
			URI u=aGraph.getConceptUri("FAKE:002", "hospital");
			System.err.println(aGraph.getConcept(u).getPreflabel().getLit().getLabel());
		}catch (Exception e){
			e.printStackTrace();
			
		}
		finally {
			if (aGraph!=null)aGraph.disconnect();
		}
	}
	
	@Test
	public void test3(){
		TripleAndContextGraph aGraph=null;
		try {
			aGraph=new TripleAndContextGraph(true,"/Reborn/testTriple");
			
			//aGraph.importTSV("/EHR4CR/data/TermList3.tsv");
			aGraph.importTSV("/EHR4CR/data/TermList2.tsv");
			aGraph.statements();
			System.out.println("should be One [codesearch B0003 / No oid] ="+aGraph.exactMatchSearch("B0003", null, null).size());
			System.out.println("should be One [codesearch B0003 / default Oid] ="+aGraph.exactMatchSearch("B0003", null, "default" ).size());
			
			System.out.println("should be One [labelsearch Baby / NO OID] ="+aGraph.exactMatchSearch(null, "Baby", null).size());
			System.out.println("should be One [labelsearch Baby / default oid] ="+aGraph.exactMatchSearch(null, "Baby", "default").size());
			
			System.out.println(aGraph.getConcept(aGraph.searchLabel("ba", null, 5, 0, false).iterator().next()).getPreflabel().getLit().getLabel());
		}catch (Exception e){
			e.printStackTrace();
			
		}
		finally {
			if (aGraph!=null)aGraph.disconnect();
		}
	}
	
	@Test 
	public void test4(){
		ContextElementsGraph ecg=null;
		try {
			ecg=new ContextElementsGraph(true,"/Reborn/testContext");
			Rank rank=new Rank();
			rank.setOriginalRank("toto");
			rank.setValue(new Float(0.34).floatValue());
			Unit u=new Unit();
			u.setUcumDefinition("kilogramm !!");
			u.setUcumValue("kg");
			URI uri=ecg.addDataElementEntry((URI)new URIImpl("http://server.ubiqore.com/v1/2.16.840.1.113883.6.73#L01"), 
					rank, 
					"CE_CO", 
					null, 
					null, 
					"yeah", 
					null, 
					null);
			
			URI uri2=ecg.addDataElementEntry((URI)new URIImpl("http://server.ubiqore.com/v1/2.16.840.1.113883.6.73#L04"), 
					null, 
					"CE_PQ", 
					u, 
					null, 
					"yeah", 
					null, 
					null);
			
			ecg.statements();
			Set<String> sl=ecg.getUCUMCodeList();
			for (String s:sl){
				System.out.println("1 ucum est : "+s);
			}
		}catch (Exception e ){
			e.printStackTrace();
			
		}finally {
			if (ecg!=null)ecg.disconnect();
		}
	}
	
	@Test 
	public void test5(){
		TripleAndContextGraph aGraph=null;
		
		try {
			aGraph=new TripleAndContextGraph(true,"/Reborn/testTriple");
			
			Scheme scheme=new Scheme();
			scheme.setDef("@");
			scheme.setOid("@");
			scheme.setPreflabel(new PrefLabel("@"));
			URI schemeURI=aGraph.applyScheme(scheme.getOid(), scheme.getPreflabel().getLit().getLabel(),scheme.getDef());
			scheme=aGraph.getSchemeBase(schemeURI);
			
			
			aGraph.statements();
			aGraph.exportRDF("/EHR4CR/data/fake.rdf");
		}catch (Exception e){
			e.printStackTrace();
			
		}
		finally {
			if (aGraph!=null)aGraph.disconnect();
		}
	}*/
}
