package ubiqore.graphs.semantic;

import java.util.Set;

import org.openrdf.model.URI;
import org.openrdf.sail.SailException;

public class ImportTest {

	public static void main(String ... args){
		TerminologiesGraph graph=null;
		try {
		//	graph=new TerminologiesGraph(true,"/Reborn/data/importTestingGraph/graph1");
		//	graph.importRDF("/Reborn/data/importTestingGraph/1370405776775.rdf");
			graph=new TerminologiesGraph(false,"/Reborn/data/importTestingGraph/full");
			
			graph.importRDF("/Reborn/data/importTestingGraph/toto.rdf");
			Set<URI> schemesUri=graph.getSchemesURI();
			for (URI uri:schemesUri){
				System.out.println(graph.getSchemeBase(uri));
				Set<URI> topCUris=graph.getTopConceptsOfScheme(uri);
				for (URI uriC:topCUris){
					System.out.println("	"+graph.getConcept(uriC).getPreflabel().getLit().getLabel());
				}
			}
			
			Set<URI>  test=graph.searchLabel("Hypertensive renal disease", null, 10,10,true);
			for (URI uri:test){
				System.out.println(uri);
			}
			graph.disconnect();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if (graph!=null)graph.disconnect();
		}
	}
	
	public static void main1(String ... args){
		TerminologiesGraph graph=null;
		try {
			graph=new TerminologiesGraph(false,"/Reborn/data/ehr4cr/graphs/test/full");
			graph.exportRDF("/Reborn/data/importTestingGraph/toto.rdf");
			/*graph.importRDF("/Reborn/data/importTestingGraph/1370405776775.rdf");
			Set<URI> schemesUri=graph.getSchemesURI();
			for (URI uri:schemesUri){
				System.out.println(graph.getSchemeBase(uri));
				Set<URI> topCUris=graph.getTopConceptsOfScheme(uri);
				for (URI uriC:topCUris){
					System.out.println("	"+graph.getConcept(uriC).getPreflabel().getLit().getLabel());
				}
			}
			
			Set<URI>  test=graph.searchLabel("Hypertensive renal disease", null, 10,true);
			for (URI uri:test){
				System.out.println(uri);
			}*/
			graph.disconnect();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			if (graph!=null)graph.disconnect();
		}
	}
}
